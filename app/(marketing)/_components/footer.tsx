import Link from "next/link";
import { Logo } from "./logo";

import { Button } from "@/components/ui/button";

export const Footer = () => {
  return (
    <div className="flex items-center w-full p-6 bg-background z-50 dark:bg-[#1F1F1F]">
      <div className="md:ml-auto w-full justify-between md:justify-end flex items-center gap-x-2 text-muted-foreground">
        <Link href="https://github.com/dikyariff" target="_blank">
          <p className="bg-accent px-4 py-2 rounded-md text-sm font-medium hover:bg-accent hover:text-accent-foreground hover:cursor-pointer">Developed by Diki</p>
        </Link>
      </div>
    </div>
  );
};
