var _STRINGS = {
    Ad: {
        Mobile: {
            Preroll: {
                ReadyIn: "The game is ready in ",
                Loading: "Your game is loading...",
                Close: "Close"
            },
            Header: {
                ReadyIn: "The game is ready in ",
                Loading: "Your game is loading...",
                Close: "Close"
            },
            End: {
                ReadyIn: "Advertisement ends in ",
                Loading: "Please wait ...",
                Close: "Close"
            }
        }
    },
    Splash: {
        Loading: "Loading ...",
        LogoLine1: "Some text here",
        LogoLine2: "powered by MarketJS",
        LogoLine3: "none",
        TapToStart: "TAP TO START"
    },
    Game: {
        TryYourLuck: "TRY YOUR LUCK",
        Spin: "SPIN",
        Settings: "SETTINGS",
        BGM: "BGM",
        SFX: "SFX",
        Home: "HOME",
        Resume: "RESUME",
        Exit: "EXIT",
        On: "ON",
        Off: "OFF",
        YouLost: "YOU LOST!",
        YouWon: "YOU WON!",
        BetterLuck: "Better luck",
        NextTime: "next time.",
        OrganizerWillContact: "The organizer will contact you",
        AfterTheEvent: "after the event is over."
    },
    WinPopup: {
        prizeTextSize: 300,
        prizeTextSizeWithImage: 75,
        prizeTextOffset: 400,
        prizeTextOffsetWithImage: 325,
        prizeImageOffset: 475
    },
    Wheel: {
        prizeImageOffset: 185,
        sliceTextOffset: 199,
        sliceTextSize: 56
    },
    WheelSlices: [{
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#8eafe5",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/trip.png",
        sliceText: "",
        sliceTextColor: "#003499",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: "Trip for 2 to Hawaii"
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#ffffff",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/health.png",
        sliceText: "",
        sliceTextColor: "#ffffff",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: "Free medical checkup"
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#8eafe5",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/car.png",
        sliceText: "",
        sliceTextColor: "#003499",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: ["Priority test drive", "for the new Tesla"]
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#ffffff",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/phone.png",
        sliceText: "",
        sliceTextColor: "#ffffff",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: ["20% off", "your cellphone plan"]
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#8eafe5",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/coffee.png",
        sliceText: "",
        sliceTextColor: "#003499",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: ["$5 off your next", "cup of Starbucks"]
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#ffffff",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/plant.png",
        sliceText: "",
        sliceTextColor: "#ffffff",
        prizeImage: null,
        prizeType: "lose",
        prizeTextFull: ""
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#8eafe5",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/credit-card.png",
        sliceText: "",
        sliceTextColor: "#003499",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: "2x your credit card points"
    }, {
        sliceBackgroundImage: null,
        sliceBackgroundColor: "#ffffff",
        slicePrizeImage: "/static/spin-arena/v1/media/graphics/game/wheel/slices/drink.png",
        sliceText: "",
        sliceTextColor: "#ffffff",
        prizeImage: null,
        prizeType: "win",
        prizeTextFull: ["Ladies night", "Free drink on the house"]
    }]
};
var _SETTINGS = {
    API: {
        Enabled: !0,
        Log: {
            Events: {
                InitializeGame: !0,
                EndGame: !0,
                Level: {
                    Begin: !0,
                    End: !0,
                    Win: !0,
                    Lose: !0,
                    Draw: !0
                }
            }
        }
    },
    Ad: {
        Mobile: {
            Preroll: {
                Enabled: !1,
                Duration: 5,
                Width: 300,
                Height: 250,
                Rotation: {
                    Enabled: !1,
                    Weight: {
                        MobileAdInGamePreroll: 40,
                        MobileAdInGamePreroll2: 40,
                        MobileAdInGamePreroll3: 20
                    }
                }
            },
            Header: {
                Enabled: !1,
                Duration: 5,
                Width: 320,
                Height: 50,
                Rotation: {
                    Enabled: !1,
                    Weight: {
                        MobileAdInGameHeader: 40,
                        MobileAdInGameHeader2: 40,
                        MobileAdInGameHeader3: 20
                    }
                }
            },
            Footer: {
                Enabled: !1,
                Duration: 5,
                Width: 320,
                Height: 50,
                Rotation: {
                    Enabled: !1,
                    Weight: {
                        MobileAdInGameFooter: 40,
                        MobileAdInGameFooter2: 40,
                        MobileAdInGameFooter3: 20
                    }
                }
            },
            End: {
                Enabled: !1,
                Duration: 1,
                Width: 300,
                Height: 250,
                Rotation: {
                    Enabled: !1,
                    Weight: {
                        MobileAdInGameEnd: 40,
                        MobileAdInGameEnd2: 40,
                        MobileAdInGameEnd3: 20
                    }
                }
            }
        }
    },
    Language: {
        Default: "en"
    },
    DeveloperBranding: {
        Splash: {
            Enabled: !1
        },
        Logo: {
            Enabled: !1,
            Link: "http://google.com",
            LinkEnabled: !1,
            NewWindow: !0,
            Width: 166,
            Height: 61
        }
    },
    Branding: {
        Splash: {
            Enabled: !1
        },
        Logo: {
            Enabled: !0,
            Link: "http://google.com",
            LinkEnabled: !1,
            NewWindow: !0,
            Width: 166,
            Height: 61
        }
    },
    MoreGames: {
        Enabled: !1,
        Link: "http://www.marketjs.com/game/links/mobile",
        NewWindow: !0
    },
    TapToStartAudioUnlock: {
        Enabled: !1
    }
};
var MobileAdInGamePreroll = {
    ad_duration: _SETTINGS.Ad.Mobile.Preroll.Duration,
    ad_width: _SETTINGS.Ad.Mobile.Preroll.Width,
    ad_height: _SETTINGS.Ad.Mobile.Preroll.Height,
    ready_in: _STRINGS.Ad.Mobile.Preroll.ReadyIn,
    loading: _STRINGS.Ad.Mobile.Preroll.Loading,
    close: _STRINGS.Ad.Mobile.Preroll.Close + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
    Initialize: function() {
        if (_SETTINGS.Ad.Mobile.Preroll.Rotation.Enabled) {
            var b = _SETTINGS.Ad.Mobile.Preroll.Rotation.Weight
              , c = b.MobileAdInGamePreroll
              , e = c + b.MobileAdInGamePreroll2;
            b = e + b.MobileAdInGamePreroll3;
            var l = Math.floor(100 * Math.random());
            console.log("seed: ", l);
            l <= c ? this.selectedOverlayName = "MobileAdInGamePreroll" : l <= e ? this.selectedOverlayName = "MobileAdInGamePreroll2" : l <= b && (this.selectedOverlayName = "MobileAdInGamePreroll3");
            console.log("Ad rotating preroll enabled")
        } else
            this.selectedOverlayName = "MobileAdInGamePreroll",
            console.log("Ad rotating preroll disabled");
        console.log("selected:", this.selectedOverlayName);
        this.overlay = $("#" + this.selectedOverlayName);
        this.box = $("#" + this.selectedOverlayName + "-Box");
        this.game = $("#game");
        this.boxContents = {
            footer: $("#" + this.selectedOverlayName + "-Box-Footer"),
            header: $("#" + this.selectedOverlayName + "-Box-Header"),
            close: $("#" + this.selectedOverlayName + "-Box-Close"),
            body: $("#" + this.selectedOverlayName + "-Box-Body")
        };
        this.box.width(this.ad_width);
        this.box.height(this.ad_height);
        this.box.css("left", (this.overlay.width() - this.box.width()) / 2);
        this.box.css("top", (this.overlay.height() - this.box.height() - this.boxContents.header.height() - this.boxContents.footer.height()) / 2);
        this.overlay.show(this.Timer(this.ad_duration))
    },
    Timer: function(b) {
        var c = b
          , e = setInterval(function() {
            MobileAdInGamePreroll.boxContents.header.text(MobileAdInGamePreroll.ready_in + c + "...");
            MobileAdInGamePreroll.boxContents.footer.text(MobileAdInGamePreroll.loading);
            c--;
            0 > c && (clearInterval(e),
            MobileAdInGamePreroll.boxContents.close.css("left", MobileAdInGamePreroll.boxContents.body.width() - 23),
            MobileAdInGamePreroll.boxContents.close.show(),
            MobileAdInGamePreroll.boxContents.header.html(MobileAdInGamePreroll.close),
            MobileAdInGamePreroll.boxContents.footer.text(""))
        }, 1E3)
    },
    Close: function() {
        this.boxContents.close.hide();
        this.overlay.hide()
    }
};
var MobileAdInGameHeader = {
    ad_duration: _SETTINGS.Ad.Mobile.Header.Duration,
    ad_width: _SETTINGS.Ad.Mobile.Header.Width,
    ad_height: _SETTINGS.Ad.Mobile.Header.Height,
    Initialize: function() {
        if (_SETTINGS.Ad.Mobile.Header.Rotation.Enabled) {
            var b = _SETTINGS.Ad.Mobile.Header.Rotation.Weight
              , c = b.MobileAdInGameHeader
              , e = c + b.MobileAdInGameHeader2;
            b = e + b.MobileAdInGameHeader3;
            var l = Math.floor(100 * Math.random());
            console.log("seed: ", l);
            l <= c ? this.selectedOverlayName = "MobileAdInGameHeader" : l <= e ? this.selectedOverlayName = "MobileAdInGameHeader2" : l <= b && (this.selectedOverlayName = "MobileAdInGameHeader3");
            console.log("Ad rotating header enabled")
        } else
            this.selectedOverlayName = "MobileAdInGameHeader",
            console.log("Ad rotating header disabled");
        this.div = $("#" + this.selectedOverlayName);
        this.game = $("#game");
        this.div.width(this.ad_width);
        this.div.height(this.ad_height);
        this.div.css("left", this.game.position().left + (this.game.width() - this.div.width()) / 2);
        this.div.css("top", 0);
        this.div.show(this.Timer(this.ad_duration))
    },
    Timer: function(b) {
        var c = setInterval(function() {
            b--;
            0 > b && (MobileAdInGameHeader.div.hide(),
            clearInterval(c))
        }, 1E3)
    }
};
var MobileAdInGameFooter = {
    ad_duration: _SETTINGS.Ad.Mobile.Footer.Duration,
    ad_width: _SETTINGS.Ad.Mobile.Footer.Width,
    ad_height: _SETTINGS.Ad.Mobile.Footer.Height,
    Initialize: function() {
        if (_SETTINGS.Ad.Mobile.Footer.Rotation.Enabled) {
            var b = _SETTINGS.Ad.Mobile.Footer.Rotation.Weight
              , c = b.MobileAdInGameFooter
              , e = c + b.MobileAdInGameFooter2;
            b = e + b.MobileAdInGameFooter3;
            var l = Math.floor(100 * Math.random());
            console.log("seed: ", l);
            l <= c ? this.selectedOverlayName = "MobileAdInGameFooter" : l <= e ? this.selectedOverlayName = "MobileAdInGameFooter2" : l <= b && (this.selectedOverlayName = "MobileAdInGameFooter3");
            console.log("Ad rotating footer enabled")
        } else
            this.selectedOverlayName = "MobileAdInGameFooter",
            console.log("Ad rotating footer disabled");
        this.div = $("#" + this.selectedOverlayName);
        this.game = $("#game");
        this.div.width(this.ad_width);
        this.div.height(this.ad_height);
        this.div.css("left", this.game.position().left + (this.game.width() - this.div.width()) / 2);
        this.div.css("top", this.game.height() - this.div.height() - 5);
        this.div.show(this.Timer(this.ad_duration))
    },
    Timer: function(b) {
        var c = setInterval(function() {
            b--;
            0 > b && (MobileAdInGameFooter.div.hide(),
            clearInterval(c))
        }, 1E3)
    }
};
var MobileAdInGameEnd = {
    ad_duration: _SETTINGS.Ad.Mobile.End.Duration,
    ad_width: _SETTINGS.Ad.Mobile.End.Width,
    ad_height: _SETTINGS.Ad.Mobile.End.Height,
    ready_in: _STRINGS.Ad.Mobile.End.ReadyIn,
    loading: _STRINGS.Ad.Mobile.End.Loading,
    close: _STRINGS.Ad.Mobile.End.Close + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
    Initialize: function() {
        if (_SETTINGS.Ad.Mobile.End.Rotation.Enabled) {
            var b = _SETTINGS.Ad.Mobile.End.Rotation.Weight
              , c = b.MobileAdInGameEnd
              , e = c + b.MobileAdInGameEnd2;
            b = e + b.MobileAdInGameEnd3;
            var l = Math.floor(100 * Math.random());
            console.log("seed: ", l);
            l <= c ? this.selectedOverlayName = "MobileAdInGameEnd" : l <= e ? this.selectedOverlayName = "MobileAdInGameEnd2" : l <= b && (this.selectedOverlayName = "MobileAdInGameEnd3");
            console.log("Ad rotating end enabled")
        } else
            this.selectedOverlayName = "MobileAdInGameEnd",
            console.log("Ad rotating end disabled");
        console.log("selected:", this.selectedOverlayName);
        this.overlay = $("#" + this.selectedOverlayName);
        this.box = $("#" + this.selectedOverlayName + "-Box");
        this.game = $("#game");
        this.boxContents = {
            footer: $("#" + this.selectedOverlayName + "-Box-Footer"),
            header: $("#" + this.selectedOverlayName + "-Box-Header"),
            close: $("#" + this.selectedOverlayName + "-Box-Close"),
            body: $("#" + this.selectedOverlayName + "-Box-Body")
        };
        this.box.width(this.ad_width);
        this.box.height(this.ad_height);
        this.box.css("left", (this.overlay.width() - this.box.width()) / 2);
        this.box.css("top", (this.overlay.height() - this.box.height() - this.boxContents.header.height() - this.boxContents.footer.height()) / 2);
        this.overlay.show(this.Timer(this.ad_duration))
    },
    Timer: function(b) {
        var c = b
          , e = setInterval(function() {
            MobileAdInGameEnd.boxContents.header.text(MobileAdInGameEnd.ready_in + c + "...");
            MobileAdInGameEnd.boxContents.footer.text(MobileAdInGameEnd.loading);
            c--;
            0 > c && (clearInterval(e),
            MobileAdInGameEnd.boxContents.close.css("left", MobileAdInGameEnd.boxContents.body.width() - 23),
            MobileAdInGameEnd.boxContents.close.show(),
            MobileAdInGameEnd.boxContents.header.html(MobileAdInGameEnd.close),
            MobileAdInGameEnd.boxContents.footer.text(""))
        }, 1E3)
    },
    Close: function() {
        this.boxContents.close.hide();
        this.overlay.hide()
    }
};
!function(b, c) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = b.document ? c(b, !0) : function(e) {
        if (!e.document)
            throw Error("jQuery requires a window with a document");
        return c(e)
    }
    : c(b)
}("undefined" != typeof window ? window : this, function(b, c) {
    function e(d, f) {
        f = f || la;
        var m = f.createElement("script");
        m.text = d;
        f.head.appendChild(m).parentNode.removeChild(m)
    }
    function l(d) {
        var f = !!d && "length"in d && d.length
          , m = n.type(d);
        return "function" !== m && !n.isWindow(d) && ("array" === m || 0 === f || "number" == typeof f && 0 < f && f - 1 in d)
    }
    function q(d, f) {
        return d.nodeName && d.nodeName.toLowerCase() === f.toLowerCase()
    }
    function x(d, f, m) {
        return n.isFunction(f) ? n.grep(d, function(p, t) {
            return !!f.call(p, t, p) !== m
        }) : f.nodeType ? n.grep(d, function(p) {
            return p === f !== m
        }) : "string" != typeof f ? n.grep(d, function(p) {
            return -1 < Db.call(f, p) !== m
        }) : Lc.test(f) ? n.filter(f, d, m) : (f = n.filter(f, d),
        n.grep(d, function(p) {
            return -1 < Db.call(f, p) !== m && 1 === p.nodeType
        }))
    }
    function C(d, f) {
        for (; (d = d[f]) && 1 !== d.nodeType; )
            ;
        return d
    }
    function A(d) {
        var f = {};
        return n.each(d.match(Ua) || [], function(m, p) {
            f[p] = !0
        }),
        f
    }
    function E(d) {
        return d
    }
    function N(d) {
        throw d;
    }
    function g(d, f, m, p) {
        var t;
        try {
            d && n.isFunction(t = d.promise) ? t.call(d).done(f).fail(m) : d && n.isFunction(t = d.then) ? t.call(d, f, m) : f.apply(void 0, [d].slice(p))
        } catch (u) {
            m.apply(void 0, [u])
        }
    }
    function r() {
        la.removeEventListener("DOMContentLoaded", r);
        b.removeEventListener("load", r);
        n.ready()
    }
    function y() {
        this.expando = n.expando + y.uid++
    }
    function F(d, f, m) {
        var p;
        if (void 0 === m && 1 === d.nodeType)
            if (p = "data-" + f.replace(Mc, "-$&").toLowerCase(),
            m = d.getAttribute(p),
            "string" == typeof m) {
                try {
                    p = m,
                    m = "true" === p || "false" !== p && ("null" === p ? null : p === +p + "" ? +p : Nc.test(p) ? JSON.parse(p) : p)
                } catch (t) {}
                Ra.set(d, f, m)
            } else
                m = void 0;
        return m
    }
    function G(d, f, m, p) {
        var t, u = 1, B = 20, H = p ? function() {
            return p.cur()
        }
        : function() {
            return n.css(d, f, "")
        }
        , K = H(), P = m && m[3] || (n.cssNumber[f] ? "" : "px"), J = (n.cssNumber[f] || "px" !== P && +K) && Eb.exec(n.css(d, f));
        if (J && J[3] !== P) {
            P = P || J[3];
            m = m || [];
            J = +K || 1;
            do
                u = u || ".5",
                J /= u,
                n.style(d, f, J + P);
            while (u !== (u = H() / K) && 1 !== u && --B)
        }
        return m && (J = +J || +K || 0,
        t = m[1] ? J + (m[1] + 1) * m[2] : +m[2],
        p && (p.unit = P,
        p.start = J,
        p.end = t)),
        t
    }
    function M(d, f) {
        for (var m, p, t = [], u = 0, B = d.length; u < B; u++)
            if (p = d[u],
            p.style)
                if (m = p.style.display,
                f) {
                    if ("none" === m && (t[u] = ca.get(p, "display") || null,
                    t[u] || (p.style.display = "")),
                    "" === p.style.display && Kb(p)) {
                        m = u;
                        var H = void 0
                          , K = p.ownerDocument;
                        p = p.nodeName;
                        var P = gc[p];
                        K = P ? P : (H = K.body.appendChild(K.createElement(p)),
                        P = n.css(H, "display"),
                        H.parentNode.removeChild(H),
                        "none" === P && (P = "block"),
                        gc[p] = P,
                        P);
                        t[m] = K
                    }
                } else
                    "none" !== m && (t[u] = "none",
                    ca.set(p, "display", m));
        for (u = 0; u < B; u++)
            null != t[u] && (d[u].style.display = t[u]);
        return d
    }
    function O(d, f) {
        var m;
        return m = "undefined" != typeof d.getElementsByTagName ? d.getElementsByTagName(f || "*") : "undefined" != typeof d.querySelectorAll ? d.querySelectorAll(f || "*") : [],
        void 0 === f || f && q(d, f) ? n.merge([d], m) : m
    }
    function V(d, f) {
        for (var m = 0, p = d.length; m < p; m++)
            ca.set(d[m], "globalEval", !f || ca.get(f[m], "globalEval"))
    }
    function fa(d, f, m, p, t) {
        for (var u, B, H, K, P = f.createDocumentFragment(), J = [], T = 0, W = d.length; T < W; T++)
            if (u = d[T],
            u || 0 === u)
                if ("object" === n.type(u))
                    n.merge(J, u.nodeType ? [u] : u);
                else if (Oc.test(u)) {
                    B = B || P.appendChild(f.createElement("div"));
                    H = (hc.exec(u) || ["", ""])[1].toLowerCase();
                    H = Va[H] || Va._default;
                    B.innerHTML = H[1] + n.htmlPrefilter(u) + H[2];
                    for (H = H[0]; H--; )
                        B = B.lastChild;
                    n.merge(J, B.childNodes);
                    B = P.firstChild;
                    B.textContent = ""
                } else
                    J.push(f.createTextNode(u));
        P.textContent = "";
        for (T = 0; u = J[T++]; )
            if (p && -1 < n.inArray(u, p))
                t && t.push(u);
            else if (K = n.contains(u.ownerDocument, u),
            B = O(P.appendChild(u), "script"),
            K && V(B),
            m)
                for (H = 0; u = B[H++]; )
                    ic.test(u.type || "") && m.push(u);
        return P
    }
    function va() {
        return !0
    }
    function Fa() {
        return !1
    }
    function qb() {
        try {
            return la.activeElement
        } catch (d) {}
    }
    function ib(d, f, m, p, t, u) {
        var B, H;
        if ("object" == typeof f) {
            "string" != typeof m && (p = p || m,
            m = void 0);
            for (H in f)
                ib(d, H, m, p, f[H], u);
            return d
        }
        if (null == p && null == t ? (t = m,
        p = m = void 0) : null == t && ("string" == typeof m ? (t = p,
        p = void 0) : (t = p,
        p = m,
        m = void 0)),
        !1 === t)
            t = Fa;
        else if (!t)
            return d;
        return 1 === u && (B = t,
        t = function(K) {
            return n().off(K),
            B.apply(this, arguments)
        }
        ,
        t.guid = B.guid || (B.guid = n.guid++)),
        d.each(function() {
            n.event.add(this, f, t, p, m)
        })
    }
    function nb(d, f) {
        return q(d, "table") && q(11 !== f.nodeType ? f : f.firstChild, "tr") ? n(">tbody", d)[0] || d : d
    }
    function pa(d) {
        return d.type = (null !== d.getAttribute("type")) + "/" + d.type,
        d
    }
    function Wa(d) {
        var f = Pc.exec(d.type);
        return f ? d.type = f[1] : d.removeAttribute("type"),
        d
    }
    function Sa(d, f) {
        var m, p, t, u, B, H;
        if (1 === f.nodeType) {
            if (ca.hasData(d) && (m = ca.access(d),
            p = ca.set(f, m),
            H = m.events))
                for (t in delete p.handle,
                p.events = {},
                H)
                    for (m = 0,
                    p = H[t].length; m < p; m++)
                        n.event.add(f, t, H[t][m]);
            Ra.hasData(d) && (u = Ra.access(d),
            B = n.extend({}, u),
            Ra.set(f, B))
        }
    }
    function Ca(d, f, m, p) {
        f = jc.apply([], f);
        var t, u, B, H = 0, K = d.length, P = K - 1, J = f[0], T = n.isFunction(J);
        if (T || 1 < K && "string" == typeof J && !ua.checkClone && Qc.test(J))
            return d.each(function(ha) {
                var X = d.eq(ha);
                T && (f[0] = J.call(this, ha, X.html()));
                Ca(X, f, m, p)
            });
        if (K && (t = fa(f, d[0].ownerDocument, !1, d, p),
        u = t.firstChild,
        1 === t.childNodes.length && (t = u),
        u || p)) {
            u = n.map(O(t, "script"), pa);
            for (B = u.length; H < K; H++) {
                var W = t;
                H !== P && (W = n.clone(W, !0, !0),
                B && n.merge(u, O(W, "script")));
                m.call(d[H], W, H)
            }
            if (B)
                for (t = u[u.length - 1].ownerDocument,
                n.map(u, Wa),
                H = 0; H < B; H++)
                    W = u[H],
                    ic.test(W.type || "") && !ca.access(W, "globalEval") && n.contains(t, W) && (W.src ? n._evalUrl && n._evalUrl(W.src) : e(W.textContent.replace(Rc, ""), t))
        }
        return d
    }
    function Ta(d, f, m) {
        for (var p = f ? n.filter(f, d) : d, t = 0; null != (f = p[t]); t++)
            m || 1 !== f.nodeType || n.cleanData(O(f)),
            f.parentNode && (m && n.contains(f.ownerDocument, f) && V(O(f, "script")),
            f.parentNode.removeChild(f));
        return d
    }
    function Xa(d, f, m) {
        var p, t, u, B, H = d.style;
        return m = m || Lb(d),
        m && (B = m.getPropertyValue(f) || m[f],
        "" !== B || n.contains(d.ownerDocument, d) || (B = n.style(d, f)),
        !ua.pixelMarginRight() && Vb.test(B) && kc.test(f) && (p = H.width,
        t = H.minWidth,
        u = H.maxWidth,
        H.minWidth = H.maxWidth = H.width = B,
        B = m.width,
        H.width = p,
        H.minWidth = t,
        H.maxWidth = u)),
        void 0 !== B ? B + "" : B
    }
    function Ya(d, f) {
        return {
            get: function() {
                return d() ? void delete this.get : (this.get = f).apply(this, arguments)
            }
        }
    }
    function Za(d) {
        var f = n.cssProps[d];
        if (!f) {
            f = n.cssProps;
            a: {
                var m = d;
                if (!(m in lc)) {
                    for (var p = m[0].toUpperCase() + m.slice(1), t = mc.length; t--; )
                        if (m = mc[t] + p,
                        m in lc)
                            break a;
                    m = void 0
                }
            }
            f = f[d] = m || d
        }
        return f
    }
    function $a(d, f, m) {
        return (d = Eb.exec(f)) ? Math.max(0, d[2] - (m || 0)) + (d[3] || "px") : f
    }
    function rb(d, f, m, p, t) {
        var u = 0;
        for (f = m === (p ? "border" : "content") ? 4 : "width" === f ? 1 : 0; 4 > f; f += 2)
            "margin" === m && (u += n.css(d, m + sb[f], !0, t)),
            p ? ("content" === m && (u -= n.css(d, "padding" + sb[f], !0, t)),
            "margin" !== m && (u -= n.css(d, "border" + sb[f] + "Width", !0, t))) : (u += n.css(d, "padding" + sb[f], !0, t),
            "padding" !== m && (u += n.css(d, "border" + sb[f] + "Width", !0, t)));
        return u
    }
    function tb(d, f, m) {
        var p, t = Lb(d), u = Xa(d, f, t), B = "border-box" === n.css(d, "boxSizing", !1, t);
        return Vb.test(u) ? u : (p = B && (ua.boxSizingReliable() || u === d.style[f]),
        "auto" === u && (u = d["offset" + f[0].toUpperCase() + f.slice(1)]),
        u = parseFloat(u) || 0,
        u + rb(d, f, m || (B ? "border" : "content"), p, t) + "px")
    }
    function Ga(d, f, m, p, t) {
        return new Ga.prototype.init(d,f,m,p,t)
    }
    function Na() {
        Mb && (!1 === la.hidden && b.requestAnimationFrame ? b.requestAnimationFrame(Na) : b.setTimeout(Na, n.fx.interval),
        n.fx.tick())
    }
    function xa() {
        return b.setTimeout(function() {
            Bb = void 0
        }),
        Bb = n.now()
    }
    function Nb(d, f) {
        var m = 0
          , p = {
            height: d
        };
        for (f = f ? 1 : 0; 4 > m; m += 2 - f) {
            var t = sb[m];
            p["margin" + t] = p["padding" + t] = d
        }
        return f && (p.opacity = p.width = d),
        p
    }
    function nc(d, f, m) {
        for (var p, t = (cb.tweeners[f] || []).concat(cb.tweeners["*"]), u = 0, B = t.length; u < B; u++)
            if (p = t[u].call(m, f, d))
                return p
    }
    function Sc(d, f) {
        var m, p, t, u, B;
        for (m in d)
            if (p = n.camelCase(m),
            t = f[p],
            u = d[m],
            Array.isArray(u) && (t = u[1],
            u = d[m] = u[0]),
            m !== p && (d[p] = u,
            delete d[m]),
            B = n.cssHooks[p],
            B && "expand"in B)
                for (m in u = B.expand(u),
                delete d[p],
                u)
                    m in d || (d[m] = u[m],
                    f[m] = t);
            else
                f[p] = t
    }
    function cb(d, f, m) {
        var p, t = 0, u = cb.prefilters.length, B = n.Deferred().always(function() {
            delete H.elem
        }), H = function() {
            if (p)
                return !1;
            var P = Bb || xa();
            P = Math.max(0, K.startTime + K.duration - P);
            for (var J = 1 - (P / K.duration || 0), T = 0, W = K.tweens.length; T < W; T++)
                K.tweens[T].run(J);
            return B.notifyWith(d, [K, J, P]),
            1 > J && W ? P : (W || B.notifyWith(d, [K, 1, 0]),
            B.resolveWith(d, [K]),
            !1)
        }, K = B.promise({
            elem: d,
            props: n.extend({}, f),
            opts: n.extend(!0, {
                specialEasing: {},
                easing: n.easing._default
            }, m),
            originalProperties: f,
            originalOptions: m,
            startTime: Bb || xa(),
            duration: m.duration,
            tweens: [],
            createTween: function(P, J) {
                P = n.Tween(d, K.opts, P, J, K.opts.specialEasing[P] || K.opts.easing);
                return K.tweens.push(P),
                P
            },
            stop: function(P) {
                var J = 0
                  , T = P ? K.tweens.length : 0;
                if (p)
                    return this;
                for (p = !0; J < T; J++)
                    K.tweens[J].run(1);
                return P ? (B.notifyWith(d, [K, 1, 0]),
                B.resolveWith(d, [K, P])) : B.rejectWith(d, [K, P]),
                this
            }
        });
        m = K.props;
        for (Sc(m, K.opts.specialEasing); t < u; t++)
            if (f = cb.prefilters[t].call(K, d, m, K.opts))
                return n.isFunction(f.stop) && (n._queueHooks(K.elem, K.opts.queue).stop = n.proxy(f.stop, f)),
                f;
        return n.map(m, nc, K),
        n.isFunction(K.opts.start) && K.opts.start.call(d, K),
        K.progress(K.opts.progress).done(K.opts.done, K.opts.complete).fail(K.opts.fail).always(K.opts.always),
        n.fx.timer(n.extend(H, {
            elem: d,
            anim: K,
            queue: K.opts.queue
        })),
        K
    }
    function ub(d) {
        return (d.match(Ua) || []).join(" ")
    }
    function vb(d) {
        return d.getAttribute && d.getAttribute("class") || ""
    }
    function Wb(d, f, m, p) {
        var t;
        if (Array.isArray(f))
            n.each(f, function(u, B) {
                m || Tc.test(d) ? p(d, B) : Wb(d + "[" + ("object" == typeof B && null != B ? u : "") + "]", B, m, p)
            });
        else if (m || "object" !== n.type(f))
            p(d, f);
        else
            for (t in f)
                Wb(d + "[" + t + "]", f[t], m, p)
    }
    function oc(d) {
        return function(f, m) {
            "string" != typeof f && (m = f,
            f = "*");
            var p = 0
              , t = f.toLowerCase().match(Ua) || [];
            if (n.isFunction(m))
                for (; f = t[p++]; )
                    "+" === f[0] ? (f = f.slice(1) || "*",
                    (d[f] = d[f] || []).unshift(m)) : (d[f] = d[f] || []).push(m)
        }
    }
    function pc(d, f, m, p) {
        function t(H) {
            var K;
            return u[H] = !0,
            n.each(d[H] || [], function(P, J) {
                P = J(f, m, p);
                return "string" != typeof P || B || u[P] ? B ? !(K = P) : void 0 : (f.dataTypes.unshift(P),
                t(P),
                !1)
            }),
            K
        }
        var u = {}
          , B = d === Xb;
        return t(f.dataTypes[0]) || !u["*"] && t("*")
    }
    function Yb(d, f) {
        var m, p, t = n.ajaxSettings.flatOptions || {};
        for (m in f)
            void 0 !== f[m] && ((t[m] ? d : p ||= {})[m] = f[m]);
        return p && n.extend(!0, d, p),
        d
    }
    var wb = []
      , la = b.document
      , Uc = Object.getPrototypeOf
      , xb = wb.slice
      , jc = wb.concat
      , Zb = wb.push
      , Db = wb.indexOf
      , Ob = {}
      , qc = Ob.toString
      , Pb = Ob.hasOwnProperty
      , rc = Pb.toString
      , Vc = rc.call(Object)
      , ua = {}
      , n = function(d, f) {
        return new n.fn.init(d,f)
    }
      , Wc = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g
      , Xc = /^-ms-/
      , Yc = /-([a-z])/g
      , Zc = function(d, f) {
        return f.toUpperCase()
    };
    n.fn = n.prototype = {
        jquery: "3.2.1",
        constructor: n,
        length: 0,
        toArray: function() {
            return xb.call(this)
        },
        get: function(d) {
            return null == d ? xb.call(this) : 0 > d ? this[d + this.length] : this[d]
        },
        pushStack: function(d) {
            d = n.merge(this.constructor(), d);
            return d.prevObject = this,
            d
        },
        each: function(d) {
            return n.each(this, d)
        },
        map: function(d) {
            return this.pushStack(n.map(this, function(f, m) {
                return d.call(f, m, f)
            }))
        },
        slice: function() {
            return this.pushStack(xb.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(d) {
            var f = this.length;
            d = +d + (0 > d ? f : 0);
            return this.pushStack(0 <= d && d < f ? [this[d]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: Zb,
        sort: wb.sort,
        splice: wb.splice
    };
    n.extend = n.fn.extend = function() {
        var d, f, m, p, t = arguments[0] || {}, u = 1, B = arguments.length, H = !1;
        "boolean" == typeof t && (H = t,
        t = arguments[u] || {},
        u++);
        "object" == typeof t || n.isFunction(t) || (t = {});
        for (u === B && (t = this,
        u--); u < B; u++)
            if (null != (d = arguments[u]))
                for (f in d) {
                    var K = t[f]
                      , P = d[f];
                    t !== P && (H && P && (n.isPlainObject(P) || (m = Array.isArray(P))) ? (m ? (m = !1,
                    p = K && Array.isArray(K) ? K : []) : p = K && n.isPlainObject(K) ? K : {},
                    t[f] = n.extend(H, p, P)) : void 0 !== P && (t[f] = P))
                }
        return t
    }
    ;
    n.extend({
        expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(d) {
            throw Error(d);
        },
        noop: function() {},
        isFunction: function(d) {
            return "function" === n.type(d)
        },
        isWindow: function(d) {
            return null != d && d === d.window
        },
        isNumeric: function(d) {
            var f = n.type(d);
            return ("number" === f || "string" === f) && !isNaN(d - parseFloat(d))
        },
        isPlainObject: function(d) {
            var f, m;
            return !(!d || "[object Object]" !== qc.call(d)) && (!(f = Uc(d)) || (m = Pb.call(f, "constructor") && f.constructor,
            "function" == typeof m && rc.call(m) === Vc))
        },
        isEmptyObject: function(d) {
            for (var f in d)
                return !1;
            return !0
        },
        type: function(d) {
            return null == d ? d + "" : "object" == typeof d || "function" == typeof d ? Ob[qc.call(d)] || "object" : typeof d
        },
        globalEval: function(d) {
            e(d)
        },
        camelCase: function(d) {
            return d.replace(Xc, "ms-").replace(Yc, Zc)
        },
        each: function(d, f) {
            var m, p = 0;
            if (l(d))
                for (m = d.length; p < m && !1 !== f.call(d[p], p, d[p]); p++)
                    ;
            else
                for (p in d)
                    if (!1 === f.call(d[p], p, d[p]))
                        break;
            return d
        },
        trim: function(d) {
            return null == d ? "" : (d + "").replace(Wc, "")
        },
        makeArray: function(d, f) {
            f = f || [];
            return null != d && (l(Object(d)) ? n.merge(f, "string" == typeof d ? [d] : d) : Zb.call(f, d)),
            f
        },
        inArray: function(d, f, m) {
            return null == f ? -1 : Db.call(f, d, m)
        },
        merge: function(d, f) {
            for (var m = +f.length, p = 0, t = d.length; p < m; p++)
                d[t++] = f[p];
            return d.length = t,
            d
        },
        grep: function(d, f, m) {
            for (var p = [], t = 0, u = d.length, B = !m; t < u; t++)
                m = !f(d[t], t),
                m !== B && p.push(d[t]);
            return p
        },
        map: function(d, f, m) {
            var p, t = 0, u = [];
            if (l(d))
                for (p = d.length; t < p; t++) {
                    var B = f(d[t], t, m);
                    null != B && u.push(B)
                }
            else
                for (t in d)
                    B = f(d[t], t, m),
                    null != B && u.push(B);
            return jc.apply([], u)
        },
        guid: 1,
        proxy: function(d, f) {
            var m, p, t;
            if ("string" == typeof f && (m = d[f],
            f = d,
            d = m),
            n.isFunction(d))
                return p = xb.call(arguments, 2),
                t = function() {
                    return d.apply(f || this, p.concat(xb.call(arguments)))
                }
                ,
                t.guid = d.guid = d.guid || n.guid++,
                t
        },
        now: Date.now,
        support: ua
    });
    "function" == typeof Symbol && (n.fn[Symbol.iterator] = wb[Symbol.iterator]);
    n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(d, f) {
        Ob["[object " + f + "]"] = f.toLowerCase()
    });
    var Ab = function(d) {
        function f(z, D, I, L) {
            var Q, R, U, Y, S = D && D.ownerDocument, aa = D ? D.nodeType : 9;
            if (I = I || [],
            "string" != typeof z || !z || 1 !== aa && 9 !== aa && 11 !== aa)
                return I;
            if (!L && ((D ? D.ownerDocument || D : ya) !== ja && yb(D),
            D = D || ja,
            Ha)) {
                if (11 !== aa && (Y = $c.exec(z)))
                    if (Q = Y[1])
                        if (9 === aa) {
                            if (!(R = D.getElementById(Q)))
                                return I;
                            if (R.id === Q)
                                return I.push(R),
                                I
                        } else {
                            if (S && (R = S.getElementById(Q)) && Ia(D, R) && R.id === Q)
                                return I.push(R),
                                I
                        }
                    else {
                        if (Y[2])
                            return ob.apply(I, D.getElementsByTagName(z)),
                            I;
                        if ((Q = Y[3]) && za.getElementsByClassName && D.getElementsByClassName)
                            return ob.apply(I, D.getElementsByClassName(Q)),
                            I
                    }
                if (!(!za.qsa || db[z + " "] || qa && qa.test(z))) {
                    if (1 !== aa) {
                        S = D;
                        var ba = z
                    } else if ("object" !== D.nodeName.toLowerCase()) {
                        (U = D.getAttribute("id")) ? U = U.replace(sc, tc) : D.setAttribute("id", U = ka);
                        R = Qb(z);
                        for (Q = R.length; Q--; )
                            R[Q] = "#" + U + " " + ha(R[Q]);
                        ba = R.join(",");
                        S = $b.test(z) && T(D.parentNode) || D
                    }
                    if (ba)
                        try {
                            return ob.apply(I, S.querySelectorAll(ba)),
                            I
                        } catch (Z) {} finally {
                            U === ka && D.removeAttribute("id")
                        }
                }
            }
            return ia(z.replace(Rb, "$1"), D, I, L)
        }
        function m() {
            function z(I, L) {
                return D.push(I + " ") > ma.cacheLength && delete z[D.shift()],
                z[I + " "] = L
            }
            var D = [];
            return z
        }
        function p(z) {
            return z[ka] = !0,
            z
        }
        function t(z) {
            var D = ja.createElement("fieldset");
            try {
                return !!z(D)
            } catch (I) {
                return !1
            } finally {
                D.parentNode && D.parentNode.removeChild(D)
            }
        }
        function u(z, D) {
            z = z.split("|");
            for (var I = z.length; I--; )
                ma.attrHandle[z[I]] = D
        }
        function B(z, D) {
            var I = D && z
              , L = I && 1 === z.nodeType && 1 === D.nodeType && z.sourceIndex - D.sourceIndex;
            if (L)
                return L;
            if (I)
                for (; I = I.nextSibling; )
                    if (I === D)
                        return -1;
            return z ? 1 : -1
        }
        function H(z) {
            return function(D) {
                return "input" === D.nodeName.toLowerCase() && D.type === z
            }
        }
        function K(z) {
            return function(D) {
                var I = D.nodeName.toLowerCase();
                return ("input" === I || "button" === I) && D.type === z
            }
        }
        function P(z) {
            return function(D) {
                return "form"in D ? D.parentNode && !1 === D.disabled ? "label"in D ? "label"in D.parentNode ? D.parentNode.disabled === z : D.disabled === z : D.isDisabled === z || D.isDisabled !== !z && ad(D) === z : D.disabled === z : "label"in D && D.disabled === z
            }
        }
        function J(z) {
            return p(function(D) {
                return D = +D,
                p(function(I, L) {
                    for (var Q, R = z([], I.length, D), U = R.length; U--; )
                        I[Q = R[U]] && (I[Q] = !(L[Q] = I[Q]))
                })
            })
        }
        function T(z) {
            return z && "undefined" != typeof z.getElementsByTagName && z
        }
        function W() {}
        function ha(z) {
            for (var D = 0, I = z.length, L = ""; D < I; D++)
                L += z[D].value;
            return L
        }
        function X(z, D, I) {
            var L = D.dir
              , Q = D.next
              , R = Q || L
              , U = I && "parentNode" === R
              , Y = Fb++;
            return D.first ? function(S, aa, ba) {
                for (; S = S[L]; )
                    if (1 === S.nodeType || U)
                        return z(S, aa, ba);
                return !1
            }
            : function(S, aa, ba) {
                var Z, ra, Ba, Aa = [ta, Y];
                if (ba)
                    for (; S = S[L]; ) {
                        if ((1 === S.nodeType || U) && z(S, aa, ba))
                            return !0
                    }
                else
                    for (; S = S[L]; )
                        if (1 === S.nodeType || U)
                            if (Ba = S[ka] || (S[ka] = {}),
                            ra = Ba[S.uniqueID] || (Ba[S.uniqueID] = {}),
                            Q && Q === S.nodeName.toLowerCase())
                                S = S[L] || S;
                            else {
                                if ((Z = ra[R]) && Z[0] === ta && Z[1] === Y)
                                    return Aa[2] = Z[2];
                                if (ra[R] = Aa,
                                Aa[2] = z(S, aa, ba))
                                    return !0
                            }
                return !1
            }
        }
        function oa(z) {
            return 1 < z.length ? function(D, I, L) {
                for (var Q = z.length; Q--; )
                    if (!z[Q](D, I, L))
                        return !1;
                return !0
            }
            : z[0]
        }
        function na(z, D, I, L, Q) {
            for (var R, U = [], Y = 0, S = z.length, aa = null != D; Y < S; Y++)
                (R = z[Y]) && (I && !I(R, L, Q) || (U.push(R),
                aa && D.push(Y)));
            return U
        }
        function eb(z, D, I, L, Q, R) {
            return L && !L[ka] && (L = eb(L)),
            Q && !Q[ka] && (Q = eb(Q, R)),
            p(function(U, Y, S, aa) {
                var ba, Z = [], ra = [], Ba = Y.length, Aa;
                if (!(Aa = U)) {
                    Aa = D || "*";
                    for (var da = S.nodeType ? [S] : S, hb = [], Oa = 0, fb = da.length; Oa < fb; Oa++)
                        f(Aa, da[Oa], hb);
                    Aa = hb
                }
                Aa = !z || !U && D ? Aa : na(Aa, Z, z, S, aa);
                da = I ? Q || (U ? z : Ba || L) ? [] : Y : Aa;
                if (I && I(Aa, da, S, aa),
                L) {
                    var Ka = na(da, ra);
                    L(Ka, [], S, aa);
                    for (S = Ka.length; S--; )
                        (ba = Ka[S]) && (da[ra[S]] = !(Aa[ra[S]] = ba))
                }
                if (U) {
                    if (Q || z) {
                        if (Q) {
                            Ka = [];
                            for (S = da.length; S--; )
                                (ba = da[S]) && Ka.push(Aa[S] = ba);
                            Q(null, da = [], Ka, aa)
                        }
                        for (S = da.length; S--; )
                            (ba = da[S]) && -1 < (Ka = Q ? zb(U, ba) : Z[S]) && (U[Ka] = !(Y[Ka] = ba))
                    }
                } else
                    da = na(da === Y ? da.splice(Ba, da.length) : da),
                    Q ? Q(null, Y, da, aa) : ob.apply(Y, da)
            })
        }
        function Ja(z) {
            for (var D, I, L = z.length, Q = ma.relative[z[0].type], R = Q || ma.relative[" "], U = Q ? 1 : 0, Y = X(function(ba) {
                return ba === D
            }, R, !0), S = X(function(ba) {
                return -1 < zb(D, ba)
            }, R, !0), aa = [function(ba, Z, ra) {
                ba = !Q && (ra || Z !== Da) || ((D = Z).nodeType ? Y(ba, Z, ra) : S(ba, Z, ra));
                return D = null,
                ba
            }
            ]; U < L; U++)
                if (R = ma.relative[z[U].type])
                    aa = [X(oa(aa), R)];
                else {
                    if (R = ma.filter[z[U].type].apply(null, z[U].matches),
                    R[ka]) {
                        for (I = ++U; I < L && !ma.relative[z[I].type]; I++)
                            ;
                        return eb(1 < U && oa(aa), 1 < U && ha(z.slice(0, U - 1).concat({
                            value: " " === z[U - 2].type ? "*" : ""
                        })).replace(Rb, "$1"), R, U < I && Ja(z.slice(U, I)), I < L && Ja(z = z.slice(I)), I < L && ha(z))
                    }
                    aa.push(R)
                }
            return oa(aa)
        }
        function ea(z, D) {
            var I = 0 < D.length
              , L = 0 < z.length
              , Q = function(R, U, Y, S, aa) {
                var ba, Z, ra = 0, Ba = "0", Aa = R && [], da = [], hb = Da, Oa = R || L && ma.find.TAG("*", aa), fb = ta += null == hb ? 1 : Math.random() || .1, Ka = Oa.length;
                for (aa && (Da = U === ja || U || aa); Ba !== Ka && null != (ba = Oa[Ba]); Ba++) {
                    if (L && ba) {
                        var ac = 0;
                        for (U || ba.ownerDocument === ja || (yb(ba),
                        Y = !Ha); Z = z[ac++]; )
                            if (Z(ba, U || ja, Y)) {
                                S.push(ba);
                                break
                            }
                        aa && (ta = fb)
                    }
                    I && ((ba = !Z && ba) && ra--,
                    R && Aa.push(ba))
                }
                if (ra += Ba,
                I && Ba !== ra) {
                    for (ac = 0; Z = D[ac++]; )
                        Z(Aa, da, U, Y);
                    if (R) {
                        if (0 < ra)
                            for (; Ba--; )
                                Aa[Ba] || da[Ba] || (da[Ba] = bd.call(S));
                        da = na(da)
                    }
                    ob.apply(S, da);
                    aa && !R && 0 < da.length && 1 < ra + D.length && f.uniqueSort(S)
                }
                return aa && (ta = fb,
                Da = hb),
                Aa
            };
            return I ? p(Q) : Q
        }
        var Ea, Pa, ia, Da, wa, Qa, ja, La, Ha, qa, sa, gb, Ia, ka = "sizzle" + 1 * new Date, ya = d.document, ta = 0, Fb = 0, ab = m(), Ma = m(), db = m(), bb = function(z, D) {
            return z === D && (Qa = !0),
            0
        }, pb = {}.hasOwnProperty, jb = [], bd = jb.pop, cd = jb.push, ob = jb.push, uc = jb.slice, zb = function(z, D) {
            for (var I = 0, L = z.length; I < L; I++)
                if (z[I] === D)
                    return I;
            return -1
        }, dd = RegExp("[\\x20\\t\\r\\n\\f]+", "g"), Rb = RegExp("^[\\x20\\t\\r\\n\\f]+|((?:^|[^\\\\])(?:\\\\.)*)[\\x20\\t\\r\\n\\f]+$", "g"), ed = RegExp("^[\\x20\\t\\r\\n\\f]*,[\\x20\\t\\r\\n\\f]*"), fd = RegExp("^[\\x20\\t\\r\\n\\f]*([>+~]|[\\x20\\t\\r\\n\\f])[\\x20\\t\\r\\n\\f]*"), gd = RegExp("=[\\x20\\t\\r\\n\\f]*([^\\]'\"]*?)[\\x20\\t\\r\\n\\f]*\\]", "g"), hd = RegExp(":((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)"), id = RegExp("^(?:\\\\.|[\\w-]|[^\x00-\\xa0])+$"), Sb = {
            ID: RegExp("^#((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)"),
            CLASS: RegExp("^\\.((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)"),
            TAG: RegExp("^((?:\\\\.|[\\w-]|[^\x00-\\xa0])+|[*])"),
            ATTR: RegExp("^\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\]"),
            PSEUDO: RegExp("^:((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)"),
            CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\([\\x20\\t\\r\\n\\f]*(even|odd|(([+-]|)(\\d*)n|)[\\x20\\t\\r\\n\\f]*(?:([+-]|)[\\x20\\t\\r\\n\\f]*(\\d+)|))[\\x20\\t\\r\\n\\f]*\\)|)", "i"),
            bool: RegExp("^(?:checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)$", "i"),
            needsContext: RegExp("^[\\x20\\t\\r\\n\\f]*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\([\\x20\\t\\r\\n\\f]*((?:-\\d)?\\d*)[\\x20\\t\\r\\n\\f]*\\)|)(?=[^-]|$)", "i")
        }, jd = /^(?:input|select|textarea|button)$/i, kd = /^h\d$/i, Gb = /^[^{]+\{\s*\[native \w/, $c = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, $b = /[+~]/, kb = RegExp("\\\\([\\da-f]{1,6}[\\x20\\t\\r\\n\\f]?|([\\x20\\t\\r\\n\\f])|.)", "ig"), lb = function(z, D, I) {
            z = "0x" + D - 65536;
            return z !== z || I ? D : 0 > z ? String.fromCharCode(z + 65536) : String.fromCharCode(z >> 10 | 55296, 1023 & z | 56320)
        }, sc = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, tc = function(z, D) {
            return D ? "\x00" === z ? "\ufffd" : z.slice(0, -1) + "\\" + z.charCodeAt(z.length - 1).toString(16) + " " : "\\" + z
        }, vc = function() {
            yb()
        }, ad = X(function(z) {
            return !0 === z.disabled && ("form"in z || "label"in z)
        }, {
            dir: "parentNode",
            next: "legend"
        });
        try {
            ob.apply(jb = uc.call(ya.childNodes), ya.childNodes),
            jb[ya.childNodes.length].nodeType
        } catch (z) {
            ob = {
                apply: jb.length ? function(D, I) {
                    cd.apply(D, uc.call(I))
                }
                : function(D, I) {
                    for (var L = D.length, Q = 0; D[L++] = I[Q++]; )
                        ;
                    D.length = L - 1
                }
            }
        }
        var za = f.support = {}
          , ld = f.isXML = function(z) {
            z = z && (z.ownerDocument || z).documentElement;
            return !!z && "HTML" !== z.nodeName
        }
          , yb = f.setDocument = function(z) {
            var D, I;
            z = z ? z.ownerDocument || z : ya;
            return z !== ja && 9 === z.nodeType && z.documentElement ? (ja = z,
            La = ja.documentElement,
            Ha = !ld(ja),
            ya !== ja && (I = ja.defaultView) && I.top !== I && (I.addEventListener ? I.addEventListener("unload", vc, !1) : I.attachEvent && I.attachEvent("onunload", vc)),
            za.attributes = t(function(L) {
                return L.className = "i",
                !L.getAttribute("className")
            }),
            za.getElementsByTagName = t(function(L) {
                return L.appendChild(ja.createComment("")),
                !L.getElementsByTagName("*").length
            }),
            za.getElementsByClassName = Gb.test(ja.getElementsByClassName),
            za.getById = t(function(L) {
                return La.appendChild(L).id = ka,
                !ja.getElementsByName || !ja.getElementsByName(ka).length
            }),
            za.getById ? (ma.filter.ID = function(L) {
                var Q = L.replace(kb, lb);
                return function(R) {
                    return R.getAttribute("id") === Q
                }
            }
            ,
            ma.find.ID = function(L, Q) {
                if ("undefined" != typeof Q.getElementById && Ha)
                    return (L = Q.getElementById(L)) ? [L] : []
            }
            ) : (ma.filter.ID = function(L) {
                var Q = L.replace(kb, lb);
                return function(R) {
                    return (R = "undefined" != typeof R.getAttributeNode && R.getAttributeNode("id")) && R.value === Q
                }
            }
            ,
            ma.find.ID = function(L, Q) {
                if ("undefined" != typeof Q.getElementById && Ha) {
                    var R, U = Q.getElementById(L);
                    if (U) {
                        if (R = U.getAttributeNode("id"),
                        R && R.value === L)
                            return [U];
                        var Y = Q.getElementsByName(L);
                        for (Q = 0; U = Y[Q++]; )
                            if (R = U.getAttributeNode("id"),
                            R && R.value === L)
                                return [U]
                    }
                    return []
                }
            }
            ),
            ma.find.TAG = za.getElementsByTagName ? function(L, Q) {
                return "undefined" != typeof Q.getElementsByTagName ? Q.getElementsByTagName(L) : za.qsa ? Q.querySelectorAll(L) : void 0
            }
            : function(L, Q) {
                var R = []
                  , U = 0;
                Q = Q.getElementsByTagName(L);
                if ("*" === L) {
                    for (; L = Q[U++]; )
                        1 === L.nodeType && R.push(L);
                    return R
                }
                return Q
            }
            ,
            ma.find.CLASS = za.getElementsByClassName && function(L, Q) {
                if ("undefined" != typeof Q.getElementsByClassName && Ha)
                    return Q.getElementsByClassName(L)
            }
            ,
            sa = [],
            qa = [],
            (za.qsa = Gb.test(ja.querySelectorAll)) && (t(function(L) {
                La.appendChild(L).innerHTML = "<a id='" + ka + "'></a><select id='" + ka + "-\r\\' msallowcapture=''><option selected=''></option></select>";
                L.querySelectorAll("[msallowcapture^='']").length && qa.push("[*^$]=[\\x20\\t\\r\\n\\f]*(?:''|\"\")");
                L.querySelectorAll("[selected]").length || qa.push("\\[[\\x20\\t\\r\\n\\f]*(?:value|checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)");
                L.querySelectorAll("[id~=" + ka + "-]").length || qa.push("~=");
                L.querySelectorAll(":checked").length || qa.push(":checked");
                L.querySelectorAll("a#" + ka + "+*").length || qa.push(".#.+[+~]")
            }),
            t(function(L) {
                L.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var Q = ja.createElement("input");
                Q.setAttribute("type", "hidden");
                L.appendChild(Q).setAttribute("name", "D");
                L.querySelectorAll("[name=d]").length && qa.push("name[\\x20\\t\\r\\n\\f]*[*^$|!~]?=");
                2 !== L.querySelectorAll(":enabled").length && qa.push(":enabled", ":disabled");
                La.appendChild(L).disabled = !0;
                2 !== L.querySelectorAll(":disabled").length && qa.push(":enabled", ":disabled");
                L.querySelectorAll("*,:x");
                qa.push(",.*:")
            })),
            (za.matchesSelector = Gb.test(gb = La.matches || La.webkitMatchesSelector || La.mozMatchesSelector || La.oMatchesSelector || La.msMatchesSelector)) && t(function(L) {
                za.disconnectedMatch = gb.call(L, "*");
                gb.call(L, "[s!='']:x");
                sa.push("!=", ":((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)")
            }),
            qa = qa.length && new RegExp(qa.join("|")),
            sa = sa.length && new RegExp(sa.join("|")),
            D = Gb.test(La.compareDocumentPosition),
            Ia = D || Gb.test(La.contains) ? function(L, Q) {
                var R = 9 === L.nodeType ? L.documentElement : L;
                Q = Q && Q.parentNode;
                return L === Q || !(!Q || 1 !== Q.nodeType || !(R.contains ? R.contains(Q) : L.compareDocumentPosition && 16 & L.compareDocumentPosition(Q)))
            }
            : function(L, Q) {
                if (Q)
                    for (; Q = Q.parentNode; )
                        if (Q === L)
                            return !0;
                return !1
            }
            ,
            bb = D ? function(L, Q) {
                if (L === Q)
                    return Qa = !0,
                    0;
                var R = !L.compareDocumentPosition - !Q.compareDocumentPosition;
                return R ? R : (R = (L.ownerDocument || L) === (Q.ownerDocument || Q) ? L.compareDocumentPosition(Q) : 1,
                1 & R || !za.sortDetached && Q.compareDocumentPosition(L) === R ? L === ja || L.ownerDocument === ya && Ia(ya, L) ? -1 : Q === ja || Q.ownerDocument === ya && Ia(ya, Q) ? 1 : wa ? zb(wa, L) - zb(wa, Q) : 0 : 4 & R ? -1 : 1)
            }
            : function(L, Q) {
                if (L === Q)
                    return Qa = !0,
                    0;
                var R = 0
                  , U = L.parentNode
                  , Y = Q.parentNode
                  , S = [L]
                  , aa = [Q];
                if (!U || !Y)
                    return L === ja ? -1 : Q === ja ? 1 : U ? -1 : Y ? 1 : wa ? zb(wa, L) - zb(wa, Q) : 0;
                if (U === Y)
                    return B(L, Q);
                for (; L = L.parentNode; )
                    S.unshift(L);
                for (L = Q; L = L.parentNode; )
                    aa.unshift(L);
                for (; S[R] === aa[R]; )
                    R++;
                return R ? B(S[R], aa[R]) : S[R] === ya ? -1 : aa[R] === ya ? 1 : 0
            }
            ,
            ja) : ja
        }
        ;
        f.matches = function(z, D) {
            return f(z, null, null, D)
        }
        ;
        f.matchesSelector = function(z, D) {
            if ((z.ownerDocument || z) !== ja && yb(z),
            D = D.replace(gd, "='$1']"),
            !(!za.matchesSelector || !Ha || db[D + " "] || sa && sa.test(D) || qa && qa.test(D)))
                try {
                    var I = gb.call(z, D);
                    if (I || za.disconnectedMatch || z.document && 11 !== z.document.nodeType)
                        return I
                } catch (L) {}
            return 0 < f(D, ja, null, [z]).length
        }
        ;
        f.contains = function(z, D) {
            return (z.ownerDocument || z) !== ja && yb(z),
            Ia(z, D)
        }
        ;
        f.attr = function(z, D) {
            (z.ownerDocument || z) !== ja && yb(z);
            var I = ma.attrHandle[D.toLowerCase()];
            I = I && pb.call(ma.attrHandle, D.toLowerCase()) ? I(z, D, !Ha) : void 0;
            return void 0 !== I ? I : za.attributes || !Ha ? z.getAttribute(D) : (I = z.getAttributeNode(D)) && I.specified ? I.value : null
        }
        ;
        f.escape = function(z) {
            return (z + "").replace(sc, tc)
        }
        ;
        f.error = function(z) {
            throw Error("Syntax error, unrecognized expression: " + z);
        }
        ;
        f.uniqueSort = function(z) {
            var D, I = [], L = 0, Q = 0;
            if (Qa = !za.detectDuplicates,
            wa = !za.sortStable && z.slice(0),
            z.sort(bb),
            Qa) {
                for (; D = z[Q++]; )
                    D === z[Q] && (L = I.push(Q));
                for (; L--; )
                    z.splice(I[L], 1)
            }
            return wa = null,
            z
        }
        ;
        var bc = f.getText = function(z) {
            var D, I = "", L = 0;
            if (D = z.nodeType)
                if (1 === D || 9 === D || 11 === D) {
                    if ("string" == typeof z.textContent)
                        return z.textContent;
                    for (z = z.firstChild; z; z = z.nextSibling)
                        I += bc(z)
                } else {
                    if (3 === D || 4 === D)
                        return z.nodeValue
                }
            else
                for (; D = z[L++]; )
                    I += bc(D);
            return I
        }
          , ma = f.selectors = {
            cacheLength: 50,
            createPseudo: p,
            match: Sb,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(z) {
                    return z[1] = z[1].replace(kb, lb),
                    z[3] = (z[3] || z[4] || z[5] || "").replace(kb, lb),
                    "~=" === z[2] && (z[3] = " " + z[3] + " "),
                    z.slice(0, 4)
                },
                CHILD: function(z) {
                    return z[1] = z[1].toLowerCase(),
                    "nth" === z[1].slice(0, 3) ? (z[3] || f.error(z[0]),
                    z[4] = +(z[4] ? z[5] + (z[6] || 1) : 2 * ("even" === z[3] || "odd" === z[3])),
                    z[5] = +(z[7] + z[8] || "odd" === z[3])) : z[3] && f.error(z[0]),
                    z
                },
                PSEUDO: function(z) {
                    var D, I = !z[6] && z[2];
                    return Sb.CHILD.test(z[0]) ? null : (z[3] ? z[2] = z[4] || z[5] || "" : I && hd.test(I) && (D = Qb(I, !0)) && (D = I.indexOf(")", I.length - D) - I.length) && (z[0] = z[0].slice(0, D),
                    z[2] = I.slice(0, D)),
                    z.slice(0, 3))
                }
            },
            filter: {
                TAG: function(z) {
                    var D = z.replace(kb, lb).toLowerCase();
                    return "*" === z ? function() {
                        return !0
                    }
                    : function(I) {
                        return I.nodeName && I.nodeName.toLowerCase() === D
                    }
                },
                CLASS: function(z) {
                    var D = ab[z + " "];
                    return D || (D = new RegExp("(^|[\\x20\\t\\r\\n\\f])" + z + "([\\x20\\t\\r\\n\\f]|$)"),
                    ab(z, function(I) {
                        return D.test("string" == typeof I.className && I.className || "undefined" != typeof I.getAttribute && I.getAttribute("class") || "")
                    }))
                },
                ATTR: function(z, D, I) {
                    return function(L) {
                        L = f.attr(L, z);
                        return null == L ? "!=" === D : !D || (L += "",
                        "=" === D ? L === I : "!=" === D ? L !== I : "^=" === D ? I && 0 === L.indexOf(I) : "*=" === D ? I && -1 < L.indexOf(I) : "$=" === D ? I && L.slice(-I.length) === I : "~=" === D ? -1 < (" " + L.replace(dd, " ") + " ").indexOf(I) : "|=" === D && (L === I || L.slice(0, I.length + 1) === I + "-"))
                    }
                },
                CHILD: function(z, D, I, L, Q) {
                    var R = "nth" !== z.slice(0, 3)
                      , U = "last" !== z.slice(-4)
                      , Y = "of-type" === D;
                    return 1 === L && 0 === Q ? function(S) {
                        return !!S.parentNode
                    }
                    : function(S, aa, ba) {
                        var Z, ra;
                        aa = R !== U ? "nextSibling" : "previousSibling";
                        var Ba = S.parentNode
                          , Aa = Y && S.nodeName.toLowerCase();
                        ba = !ba && !Y;
                        var da = !1;
                        if (Ba) {
                            if (R) {
                                for (; aa; ) {
                                    for (Z = S; Z = Z[aa]; )
                                        if (Y ? Z.nodeName.toLowerCase() === Aa : 1 === Z.nodeType)
                                            return !1;
                                    var hb = aa = "only" === z && !hb && "nextSibling"
                                }
                                return !0
                            }
                            if (hb = [U ? Ba.firstChild : Ba.lastChild],
                            U && ba) {
                                Z = Ba;
                                var Oa = Z[ka] || (Z[ka] = {})
                                  , fb = Oa[Z.uniqueID] || (Oa[Z.uniqueID] = {})
                                  , Ka = fb[z] || [];
                                da = (ra = Ka[0] === ta && Ka[1]) && Ka[2];
                                for (Z = ra && Ba.childNodes[ra]; Z = ++ra && Z && Z[aa] || (da = ra = 0) || hb.pop(); )
                                    if (1 === Z.nodeType && ++da && Z === S) {
                                        fb[z] = [ta, ra, da];
                                        break
                                    }
                            } else if (ba && (Z = S,
                            Oa = Z[ka] || (Z[ka] = {}),
                            fb = Oa[Z.uniqueID] || (Oa[Z.uniqueID] = {}),
                            Ka = fb[z] || [],
                            ra = Ka[0] === ta && Ka[1],
                            da = ra),
                            !1 === da)
                                for (; (Z = ++ra && Z && Z[aa] || (da = ra = 0) || hb.pop()) && ((Y ? Z.nodeName.toLowerCase() !== Aa : 1 !== Z.nodeType) || !++da || (ba && (Oa = Z[ka] || (Z[ka] = {}),
                                fb = Oa[Z.uniqueID] || (Oa[Z.uniqueID] = {}),
                                fb[z] = [ta, da]),
                                Z !== S)); )
                                    ;
                            return da -= Q,
                            da === L || 0 === da % L && 0 <= da / L
                        }
                    }
                },
                PSEUDO: function(z, D) {
                    var I, L = ma.pseudos[z] || ma.setFilters[z.toLowerCase()] || f.error("unsupported pseudo: " + z);
                    return L[ka] ? L(D) : 1 < L.length ? (I = [z, z, "", D],
                    ma.setFilters.hasOwnProperty(z.toLowerCase()) ? p(function(Q, R) {
                        for (var U, Y = L(Q, D), S = Y.length; S--; )
                            U = zb(Q, Y[S]),
                            Q[U] = !(R[U] = Y[S])
                    }) : function(Q) {
                        return L(Q, 0, I)
                    }
                    ) : L
                }
            },
            pseudos: {
                not: p(function(z) {
                    var D = []
                      , I = []
                      , L = Pa(z.replace(Rb, "$1"));
                    return L[ka] ? p(function(Q, R, U, Y) {
                        var S;
                        U = L(Q, null, Y, []);
                        for (Y = Q.length; Y--; )
                            (S = U[Y]) && (Q[Y] = !(R[Y] = S))
                    }) : function(Q, R, U) {
                        return D[0] = Q,
                        L(D, null, U, I),
                        D[0] = null,
                        !I.pop()
                    }
                }),
                has: p(function(z) {
                    return function(D) {
                        return 0 < f(z, D).length
                    }
                }),
                contains: p(function(z) {
                    return z = z.replace(kb, lb),
                    function(D) {
                        return -1 < (D.textContent || D.innerText || bc(D)).indexOf(z)
                    }
                }),
                lang: p(function(z) {
                    return id.test(z || "") || f.error("unsupported lang: " + z),
                    z = z.replace(kb, lb).toLowerCase(),
                    function(D) {
                        var I;
                        do
                            if (I = Ha ? D.lang : D.getAttribute("xml:lang") || D.getAttribute("lang"))
                                return I = I.toLowerCase(),
                                I === z || 0 === I.indexOf(z + "-");
                        while ((D = D.parentNode) && 1 === D.nodeType);
                        return !1
                    }
                }),
                target: function(z) {
                    var D = d.location && d.location.hash;
                    return D && D.slice(1) === z.id
                },
                root: function(z) {
                    return z === La
                },
                focus: function(z) {
                    return z === ja.activeElement && (!ja.hasFocus || ja.hasFocus()) && !!(z.type || z.href || ~z.tabIndex)
                },
                enabled: P(!1),
                disabled: P(!0),
                checked: function(z) {
                    var D = z.nodeName.toLowerCase();
                    return "input" === D && !!z.checked || "option" === D && !!z.selected
                },
                selected: function(z) {
                    return z.parentNode && z.parentNode.selectedIndex,
                    !0 === z.selected
                },
                empty: function(z) {
                    for (z = z.firstChild; z; z = z.nextSibling)
                        if (6 > z.nodeType)
                            return !1;
                    return !0
                },
                parent: function(z) {
                    return !ma.pseudos.empty(z)
                },
                header: function(z) {
                    return kd.test(z.nodeName)
                },
                input: function(z) {
                    return jd.test(z.nodeName)
                },
                button: function(z) {
                    var D = z.nodeName.toLowerCase();
                    return "input" === D && "button" === z.type || "button" === D
                },
                text: function(z) {
                    var D;
                    return "input" === z.nodeName.toLowerCase() && "text" === z.type && (null == (D = z.getAttribute("type")) || "text" === D.toLowerCase())
                },
                first: J(function() {
                    return [0]
                }),
                last: J(function(z, D) {
                    return [D - 1]
                }),
                eq: J(function(z, D, I) {
                    return [0 > I ? I + D : I]
                }),
                even: J(function(z, D) {
                    for (var I = 0; I < D; I += 2)
                        z.push(I);
                    return z
                }),
                odd: J(function(z, D) {
                    for (var I = 1; I < D; I += 2)
                        z.push(I);
                    return z
                }),
                lt: J(function(z, D, I) {
                    for (D = 0 > I ? I + D : I; 0 <= --D; )
                        z.push(D);
                    return z
                }),
                gt: J(function(z, D, I) {
                    for (I = 0 > I ? I + D : I; ++I < D; )
                        z.push(I);
                    return z
                })
            }
        };
        ma.pseudos.nth = ma.pseudos.eq;
        for (Ea in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        })
            ma.pseudos[Ea] = H(Ea);
        for (Ea in {
            submit: !0,
            reset: !0
        })
            ma.pseudos[Ea] = K(Ea);
        W.prototype = ma.filters = ma.pseudos;
        ma.setFilters = new W;
        var Qb = f.tokenize = function(z, D) {
            var I, L, Q, R, U;
            if (R = Ma[z + " "])
                return D ? 0 : R.slice(0);
            R = z;
            var Y = [];
            for (U = ma.preFilter; R; ) {
                S && !(I = ed.exec(R)) || (I && (R = R.slice(I[0].length) || R),
                Y.push(L = []));
                var S = !1;
                (I = fd.exec(R)) && (S = I.shift(),
                L.push({
                    value: S,
                    type: I[0].replace(Rb, " ")
                }),
                R = R.slice(S.length));
                for (Q in ma.filter)
                    !(I = Sb[Q].exec(R)) || U[Q] && !(I = U[Q](I)) || (S = I.shift(),
                    L.push({
                        value: S,
                        type: Q,
                        matches: I
                    }),
                    R = R.slice(S.length));
                if (!S)
                    break
            }
            return D ? R.length : R ? f.error(z) : Ma(z, Y).slice(0)
        }
        ;
        return Pa = f.compile = function(z, D) {
            var I, L = [], Q = [], R = db[z + " "];
            if (!R) {
                D ||= Qb(z);
                for (I = D.length; I--; )
                    R = Ja(D[I]),
                    R[ka] ? L.push(R) : Q.push(R);
                R = db(z, ea(Q, L));
                R.selector = z
            }
            return R
        }
        ,
        ia = f.select = function(z, D, I, L) {
            var Q, R, U, Y, S, aa = "function" == typeof z && z, ba = !L && Qb(z = aa.selector || z);
            if (I = I || [],
            1 === ba.length) {
                if (R = ba[0] = ba[0].slice(0),
                2 < R.length && "ID" === (U = R[0]).type && 9 === D.nodeType && Ha && ma.relative[R[1].type]) {
                    if (D = (ma.find.ID(U.matches[0].replace(kb, lb), D) || [])[0],
                    !D)
                        return I;
                    aa && (D = D.parentNode);
                    z = z.slice(R.shift().value.length)
                }
                for (Q = Sb.needsContext.test(z) ? 0 : R.length; Q-- && (U = R[Q],
                !ma.relative[Y = U.type]); )
                    if ((S = ma.find[Y]) && (L = S(U.matches[0].replace(kb, lb), $b.test(R[0].type) && T(D.parentNode) || D))) {
                        if (R.splice(Q, 1),
                        z = L.length && ha(R),
                        !z)
                            return ob.apply(I, L),
                            I;
                        break
                    }
            }
            return (aa || Pa(z, ba))(L, D, !Ha, I, !D || $b.test(z) && T(D.parentNode) || D),
            I
        }
        ,
        za.sortStable = ka.split("").sort(bb).join("") === ka,
        za.detectDuplicates = !!Qa,
        yb(),
        za.sortDetached = t(function(z) {
            return 1 & z.compareDocumentPosition(ja.createElement("fieldset"))
        }),
        t(function(z) {
            return z.innerHTML = "<a href='#'></a>",
            "#" === z.firstChild.getAttribute("href")
        }) || u("type|href|height|width", function(z, D, I) {
            if (!I)
                return z.getAttribute(D, "type" === D.toLowerCase() ? 1 : 2)
        }),
        za.attributes && t(function(z) {
            return z.innerHTML = "<input/>",
            z.firstChild.setAttribute("value", ""),
            "" === z.firstChild.getAttribute("value")
        }) || u("value", function(z, D, I) {
            if (!I && "input" === z.nodeName.toLowerCase())
                return z.defaultValue
        }),
        t(function(z) {
            return null == z.getAttribute("disabled")
        }) || u("checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", function(z, D, I) {
            var L;
            if (!I)
                return !0 === z[D] ? D.toLowerCase() : (L = z.getAttributeNode(D)) && L.specified ? L.value : null
        }),
        f
    }(b);
    n.find = Ab;
    n.expr = Ab.selectors;
    n.expr[":"] = n.expr.pseudos;
    n.uniqueSort = n.unique = Ab.uniqueSort;
    n.text = Ab.getText;
    n.isXMLDoc = Ab.isXML;
    n.contains = Ab.contains;
    n.escapeSelector = Ab.escape;
    var Cb = function(d, f, m) {
        for (var p = [], t = void 0 !== m; (d = d[f]) && 9 !== d.nodeType; )
            if (1 === d.nodeType) {
                if (t && n(d).is(m))
                    break;
                p.push(d)
            }
        return p
    }
      , wc = function(d, f) {
        for (var m = []; d; d = d.nextSibling)
            1 === d.nodeType && d !== f && m.push(d);
        return m
    }
      , xc = n.expr.match.needsContext
      , yc = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i
      , Lc = /^.[^:#\[\.,]*$/;
    n.filter = function(d, f, m) {
        var p = f[0];
        return m && (d = ":not(" + d + ")"),
        1 === f.length && 1 === p.nodeType ? n.find.matchesSelector(p, d) ? [p] : [] : n.find.matches(d, n.grep(f, function(t) {
            return 1 === t.nodeType
        }))
    }
    ;
    n.fn.extend({
        find: function(d) {
            var f, m = this.length, p = this;
            if ("string" != typeof d)
                return this.pushStack(n(d).filter(function() {
                    for (f = 0; f < m; f++)
                        if (n.contains(p[f], this))
                            return !0
                }));
            var t = this.pushStack([]);
            for (f = 0; f < m; f++)
                n.find(d, p[f], t);
            return 1 < m ? n.uniqueSort(t) : t
        },
        filter: function(d) {
            return this.pushStack(x(this, d || [], !1))
        },
        not: function(d) {
            return this.pushStack(x(this, d || [], !0))
        },
        is: function(d) {
            return !!x(this, "string" == typeof d && xc.test(d) ? n(d) : d || [], !1).length
        }
    });
    var md = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (n.fn.init = function(d, f, m) {
        var p, t;
        if (!d)
            return this;
        if (m = m || nd,
        "string" == typeof d) {
            if (p = "<" === d[0] && ">" === d[d.length - 1] && 3 <= d.length ? [null, d, null] : md.exec(d),
            !p || !p[1] && f)
                return !f || f.jquery ? (f || m).find(d) : this.constructor(f).find(d);
            if (p[1]) {
                if (f = f instanceof n ? f[0] : f,
                n.merge(this, n.parseHTML(p[1], f && f.nodeType ? f.ownerDocument || f : la, !0)),
                yc.test(p[1]) && n.isPlainObject(f))
                    for (p in f)
                        n.isFunction(this[p]) ? this[p](f[p]) : this.attr(p, f[p]);
                return this
            }
            return t = la.getElementById(p[2]),
            t && (this[0] = t,
            this.length = 1),
            this
        }
        return d.nodeType ? (this[0] = d,
        this.length = 1,
        this) : n.isFunction(d) ? void 0 !== m.ready ? m.ready(d) : d(n) : n.makeArray(d, this)
    }
    ).prototype = n.fn;
    var nd = n(la)
      , od = /^(?:parents|prev(?:Until|All))/
      , pd = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    n.fn.extend({
        has: function(d) {
            var f = n(d, this)
              , m = f.length;
            return this.filter(function() {
                for (var p = 0; p < m; p++)
                    if (n.contains(this, f[p]))
                        return !0
            })
        },
        closest: function(d, f) {
            var m, p = 0, t = this.length, u = [], B = "string" != typeof d && n(d);
            if (!xc.test(d))
                for (; p < t; p++)
                    for (m = this[p]; m && m !== f; m = m.parentNode)
                        if (11 > m.nodeType && (B ? -1 < B.index(m) : 1 === m.nodeType && n.find.matchesSelector(m, d))) {
                            u.push(m);
                            break
                        }
            return this.pushStack(1 < u.length ? n.uniqueSort(u) : u)
        },
        index: function(d) {
            return d ? "string" == typeof d ? Db.call(n(d), this[0]) : Db.call(this, d.jquery ? d[0] : d) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(d, f) {
            return this.pushStack(n.uniqueSort(n.merge(this.get(), n(d, f))))
        },
        addBack: function(d) {
            return this.add(null == d ? this.prevObject : this.prevObject.filter(d))
        }
    });
    n.each({
        parent: function(d) {
            return (d = d.parentNode) && 11 !== d.nodeType ? d : null
        },
        parents: function(d) {
            return Cb(d, "parentNode")
        },
        parentsUntil: function(d, f, m) {
            return Cb(d, "parentNode", m)
        },
        next: function(d) {
            return C(d, "nextSibling")
        },
        prev: function(d) {
            return C(d, "previousSibling")
        },
        nextAll: function(d) {
            return Cb(d, "nextSibling")
        },
        prevAll: function(d) {
            return Cb(d, "previousSibling")
        },
        nextUntil: function(d, f, m) {
            return Cb(d, "nextSibling", m)
        },
        prevUntil: function(d, f, m) {
            return Cb(d, "previousSibling", m)
        },
        siblings: function(d) {
            return wc((d.parentNode || {}).firstChild, d)
        },
        children: function(d) {
            return wc(d.firstChild)
        },
        contents: function(d) {
            return q(d, "iframe") ? d.contentDocument : (q(d, "template") && (d = d.content || d),
            n.merge([], d.childNodes))
        }
    }, function(d, f) {
        n.fn[d] = function(m, p) {
            var t = n.map(this, f, m);
            return "Until" !== d.slice(-5) && (p = m),
            p && "string" == typeof p && (t = n.filter(p, t)),
            1 < this.length && (pd[d] || n.uniqueSort(t),
            od.test(d) && t.reverse()),
            this.pushStack(t)
        }
    });
    var Ua = /[^\x20\t\r\n\f]+/g;
    n.Callbacks = function(d) {
        d = "string" == typeof d ? A(d) : n.extend({}, d);
        var f, m, p, t, u = [], B = [], H = -1, K = function() {
            t = t || d.once;
            for (p = f = !0; B.length; H = -1)
                for (m = B.shift(); ++H < u.length; )
                    !1 === u[H].apply(m[0], m[1]) && d.stopOnFalse && (H = u.length,
                    m = !1);
            d.memory || (m = !1);
            f = !1;
            t && (u = m ? [] : "")
        }, P = {
            add: function() {
                return u && (m && !f && (H = u.length - 1,
                B.push(m)),
                function W(T) {
                    n.each(T, function(ha, X) {
                        n.isFunction(X) ? d.unique && P.has(X) || u.push(X) : X && X.length && "string" !== n.type(X) && W(X)
                    })
                }(arguments),
                m && !f && K()),
                this
            },
            remove: function() {
                return n.each(arguments, function(J, T) {
                    for (var W; -1 < (W = n.inArray(T, u, W)); )
                        u.splice(W, 1),
                        W <= H && H--
                }),
                this
            },
            has: function(J) {
                return J ? -1 < n.inArray(J, u) : 0 < u.length
            },
            empty: function() {
                return u &&= [],
                this
            },
            disable: function() {
                return t = B = [],
                u = m = "",
                this
            },
            disabled: function() {
                return !u
            },
            lock: function() {
                return t = B = [],
                m || f || (u = m = ""),
                this
            },
            locked: function() {
                return !!t
            },
            fireWith: function(J, T) {
                return t || (T = T || [],
                T = [J, T.slice ? T.slice() : T],
                B.push(T),
                f || K()),
                this
            },
            fire: function() {
                return P.fireWith(this, arguments),
                this
            },
            fired: function() {
                return !!p
            }
        };
        return P
    }
    ;
    n.extend({
        Deferred: function(d) {
            var f = [["notify", "progress", n.Callbacks("memory"), n.Callbacks("memory"), 2], ["resolve", "done", n.Callbacks("once memory"), n.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", n.Callbacks("once memory"), n.Callbacks("once memory"), 1, "rejected"]]
              , m = "pending"
              , p = {
                state: function() {
                    return m
                },
                always: function() {
                    return t.done(arguments).fail(arguments),
                    this
                },
                "catch": function(u) {
                    return p.then(null, u)
                },
                pipe: function() {
                    var u = arguments;
                    return n.Deferred(function(B) {
                        n.each(f, function(H, K) {
                            var P = n.isFunction(u[K[4]]) && u[K[4]];
                            t[K[1]](function() {
                                var J = P && P.apply(this, arguments);
                                J && n.isFunction(J.promise) ? J.promise().progress(B.notify).done(B.resolve).fail(B.reject) : B[K[0] + "With"](this, P ? [J] : arguments)
                            })
                        });
                        u = null
                    }).promise()
                },
                then: function(u, B, H) {
                    function K(J, T, W, ha) {
                        return function() {
                            var X = this
                              , oa = arguments
                              , na = function() {
                                var Ja;
                                if (!(J < P)) {
                                    if (Ja = W.apply(X, oa),
                                    Ja === T.promise())
                                        throw new TypeError("Thenable self-resolution");
                                    var ea = Ja && ("object" == typeof Ja || "function" == typeof Ja) && Ja.then;
                                    n.isFunction(ea) ? ha ? ea.call(Ja, K(P, T, E, ha), K(P, T, N, ha)) : (P++,
                                    ea.call(Ja, K(P, T, E, ha), K(P, T, N, ha), K(P, T, E, T.notifyWith))) : (W !== E && (X = void 0,
                                    oa = [Ja]),
                                    (ha || T.resolveWith)(X, oa))
                                }
                            }
                              , eb = ha ? na : function() {
                                try {
                                    na()
                                } catch (Ja) {
                                    n.Deferred.exceptionHook && n.Deferred.exceptionHook(Ja, eb.stackTrace),
                                    J + 1 >= P && (W !== N && (X = void 0,
                                    oa = [Ja]),
                                    T.rejectWith(X, oa))
                                }
                            }
                            ;
                            J ? eb() : (n.Deferred.getStackHook && (eb.stackTrace = n.Deferred.getStackHook()),
                            b.setTimeout(eb))
                        }
                    }
                    var P = 0;
                    return n.Deferred(function(J) {
                        f[0][3].add(K(0, J, n.isFunction(H) ? H : E, J.notifyWith));
                        f[1][3].add(K(0, J, n.isFunction(u) ? u : E));
                        f[2][3].add(K(0, J, n.isFunction(B) ? B : N))
                    }).promise()
                },
                promise: function(u) {
                    return null != u ? n.extend(u, p) : p
                }
            }
              , t = {};
            return n.each(f, function(u, B) {
                var H = B[2]
                  , K = B[5];
                p[B[1]] = H.add;
                K && H.add(function() {
                    m = K
                }, f[3 - u][2].disable, f[0][2].lock);
                H.add(B[3].fire);
                t[B[0]] = function() {
                    return t[B[0] + "With"](this === t ? void 0 : this, arguments),
                    this
                }
                ;
                t[B[0] + "With"] = H.fireWith
            }),
            p.promise(t),
            d && d.call(t, t),
            t
        },
        when: function(d) {
            var f = arguments.length
              , m = f
              , p = Array(m)
              , t = xb.call(arguments)
              , u = n.Deferred()
              , B = function(H) {
                return function(K) {
                    p[H] = this;
                    t[H] = 1 < arguments.length ? xb.call(arguments) : K;
                    --f || u.resolveWith(p, t)
                }
            };
            if (1 >= f && (g(d, u.done(B(m)).resolve, u.reject, !f),
            "pending" === u.state() || n.isFunction(t[m] && t[m].then)))
                return u.then();
            for (; m--; )
                g(t[m], B(m), u.reject);
            return u.promise()
        }
    });
    var qd = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    n.Deferred.exceptionHook = function(d, f) {
        b.console && b.console.warn && d && qd.test(d.name) && b.console.warn("jQuery.Deferred exception: " + d.message, d.stack, f)
    }
    ;
    n.readyException = function(d) {
        b.setTimeout(function() {
            throw d;
        })
    }
    ;
    var cc = n.Deferred();
    n.fn.ready = function(d) {
        return cc.then(d)["catch"](function(f) {
            n.readyException(f)
        }),
        this
    }
    ;
    n.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(d) {
            (!0 === d ? --n.readyWait : n.isReady) || (n.isReady = !0,
            !0 !== d && 0 < --n.readyWait || cc.resolveWith(la, [n]))
        }
    });
    n.ready.then = cc.then;
    "complete" === la.readyState || "loading" !== la.readyState && !la.documentElement.doScroll ? b.setTimeout(n.ready) : (la.addEventListener("DOMContentLoaded", r),
    b.addEventListener("load", r));
    var mb = function(d, f, m, p, t, u, B) {
        var H = 0
          , K = d.length
          , P = null == m;
        if ("object" === n.type(m))
            for (H in t = !0,
            m)
                mb(d, f, H, m[H], !0, u, B);
        else if (void 0 !== p && (t = !0,
        n.isFunction(p) || (B = !0),
        P && (B ? (f.call(d, p),
        f = null) : (P = f,
        f = function(J, T, W) {
            return P.call(n(J), W)
        }
        )),
        f))
            for (; H < K; H++)
                f(d[H], m, B ? p : p.call(d[H], H, f(d[H], m)));
        return t ? d : P ? f.call(d) : K ? f(d[0], m) : u
    }
      , Tb = function(d) {
        return 1 === d.nodeType || 9 === d.nodeType || !+d.nodeType
    };
    y.uid = 1;
    y.prototype = {
        cache: function(d) {
            var f = d[this.expando];
            return f || (f = {},
            Tb(d) && (d.nodeType ? d[this.expando] = f : Object.defineProperty(d, this.expando, {
                value: f,
                configurable: !0
            }))),
            f
        },
        set: function(d, f, m) {
            var p;
            d = this.cache(d);
            if ("string" == typeof f)
                d[n.camelCase(f)] = m;
            else
                for (p in f)
                    d[n.camelCase(p)] = f[p];
            return d
        },
        get: function(d, f) {
            return void 0 === f ? this.cache(d) : d[this.expando] && d[this.expando][n.camelCase(f)]
        },
        access: function(d, f, m) {
            return void 0 === f || f && "string" == typeof f && void 0 === m ? this.get(d, f) : (this.set(d, f, m),
            void 0 !== m ? m : f)
        },
        remove: function(d, f) {
            var m, p = d[this.expando];
            if (void 0 !== p) {
                if (void 0 !== f)
                    for (Array.isArray(f) ? f = f.map(n.camelCase) : (f = n.camelCase(f),
                    f = f in p ? [f] : f.match(Ua) || []),
                    m = f.length; m--; )
                        delete p[f[m]];
                (void 0 === f || n.isEmptyObject(p)) && (d.nodeType ? d[this.expando] = void 0 : delete d[this.expando])
            }
        },
        hasData: function(d) {
            d = d[this.expando];
            return void 0 !== d && !n.isEmptyObject(d)
        }
    };
    var ca = new y
      , Ra = new y
      , Nc = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/
      , Mc = /[A-Z]/g;
    n.extend({
        hasData: function(d) {
            return Ra.hasData(d) || ca.hasData(d)
        },
        data: function(d, f, m) {
            return Ra.access(d, f, m)
        },
        removeData: function(d, f) {
            Ra.remove(d, f)
        },
        _data: function(d, f, m) {
            return ca.access(d, f, m)
        },
        _removeData: function(d, f) {
            ca.remove(d, f)
        }
    });
    n.fn.extend({
        data: function(d, f) {
            var m, p, t, u = this[0], B = u && u.attributes;
            if (void 0 === d) {
                if (this.length && (t = Ra.get(u),
                1 === u.nodeType && !ca.get(u, "hasDataAttrs"))) {
                    for (m = B.length; m--; )
                        B[m] && (p = B[m].name,
                        0 === p.indexOf("data-") && (p = n.camelCase(p.slice(5)),
                        F(u, p, t[p])));
                    ca.set(u, "hasDataAttrs", !0)
                }
                return t
            }
            return "object" == typeof d ? this.each(function() {
                Ra.set(this, d)
            }) : mb(this, function(H) {
                var K;
                if (u && void 0 === H) {
                    if ((K = Ra.get(u, d),
                    void 0 !== K) || (K = F(u, d),
                    void 0 !== K))
                        return K
                } else
                    this.each(function() {
                        Ra.set(this, d, H)
                    })
            }, null, f, 1 < arguments.length, null, !0)
        },
        removeData: function(d) {
            return this.each(function() {
                Ra.remove(this, d)
            })
        }
    });
    n.extend({
        queue: function(d, f, m) {
            var p;
            if (d)
                return f = (f || "fx") + "queue",
                p = ca.get(d, f),
                m && (!p || Array.isArray(m) ? p = ca.access(d, f, n.makeArray(m)) : p.push(m)),
                p || []
        },
        dequeue: function(d, f) {
            f = f || "fx";
            var m = n.queue(d, f)
              , p = m.length
              , t = m.shift()
              , u = n._queueHooks(d, f)
              , B = function() {
                n.dequeue(d, f)
            };
            "inprogress" === t && (t = m.shift(),
            p--);
            t && ("fx" === f && m.unshift("inprogress"),
            delete u.stop,
            t.call(d, B, u));
            !p && u && u.empty.fire()
        },
        _queueHooks: function(d, f) {
            var m = f + "queueHooks";
            return ca.get(d, m) || ca.access(d, m, {
                empty: n.Callbacks("once memory").add(function() {
                    ca.remove(d, [f + "queue", m])
                })
            })
        }
    });
    n.fn.extend({
        queue: function(d, f) {
            var m = 2;
            return "string" != typeof d && (f = d,
            d = "fx",
            m--),
            arguments.length < m ? n.queue(this[0], d) : void 0 === f ? this : this.each(function() {
                var p = n.queue(this, d, f);
                n._queueHooks(this, d);
                "fx" === d && "inprogress" !== p[0] && n.dequeue(this, d)
            })
        },
        dequeue: function(d) {
            return this.each(function() {
                n.dequeue(this, d)
            })
        },
        clearQueue: function(d) {
            return this.queue(d || "fx", [])
        },
        promise: function(d, f) {
            var m, p = 1, t = n.Deferred(), u = this, B = this.length, H = function() {
                --p || t.resolveWith(u, [u])
            };
            "string" != typeof d && (f = d,
            d = void 0);
            for (d = d || "fx"; B--; )
                (m = ca.get(u[B], d + "queueHooks")) && m.empty && (p++,
                m.empty.add(H));
            return H(),
            t.promise(f)
        }
    });
    var zc = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source
      , Eb = new RegExp("^(?:([+-])=|)(" + zc + ")([a-z%]*)$","i")
      , sb = ["Top", "Right", "Bottom", "Left"]
      , Kb = function(d, f) {
        return d = f || d,
        "none" === d.style.display || "" === d.style.display && n.contains(d.ownerDocument, d) && "none" === n.css(d, "display")
    }
      , Ac = function(d, f, m, p) {
        var t, u = {};
        for (t in f)
            u[t] = d.style[t],
            d.style[t] = f[t];
        m = m.apply(d, p || []);
        for (t in f)
            d.style[t] = u[t];
        return m
    }
      , gc = {};
    n.fn.extend({
        show: function() {
            return M(this, !0)
        },
        hide: function() {
            return M(this)
        },
        toggle: function(d) {
            return "boolean" == typeof d ? d ? this.show() : this.hide() : this.each(function() {
                Kb(this) ? n(this).show() : n(this).hide()
            })
        }
    });
    var Bc = /^(?:checkbox|radio)$/i
      , hc = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i
      , ic = /^$|\/(?:java|ecma)script/i
      , Va = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    Va.optgroup = Va.option;
    Va.tbody = Va.tfoot = Va.colgroup = Va.caption = Va.thead;
    Va.th = Va.td;
    var Oc = /<|&#?\w+;/;
    !function() {
        var d = la.createDocumentFragment().appendChild(la.createElement("div"))
          , f = la.createElement("input");
        f.setAttribute("type", "radio");
        f.setAttribute("checked", "checked");
        f.setAttribute("name", "t");
        d.appendChild(f);
        ua.checkClone = d.cloneNode(!0).cloneNode(!0).lastChild.checked;
        d.innerHTML = "<textarea>x</textarea>";
        ua.noCloneChecked = !!d.cloneNode(!0).lastChild.defaultValue
    }();
    var Ub = la.documentElement
      , rd = /^key/
      , sd = /^(?:mouse|pointer|contextmenu|drag|drop)|click/
      , Cc = /^([^.]*)(?:\.(.+)|)/;
    n.event = {
        global: {},
        add: function(d, f, m, p, t) {
            var u, B, H, K, P, J, T, W;
            if (K = ca.get(d))
                for (m.handler && (u = m,
                m = u.handler,
                t = u.selector),
                t && n.find.matchesSelector(Ub, t),
                m.guid || (m.guid = n.guid++),
                (H = K.events) || (H = K.events = {}),
                (B = K.handle) || (B = K.handle = function(oa) {
                    return "undefined" != typeof n && n.event.triggered !== oa.type ? n.event.dispatch.apply(d, arguments) : void 0
                }
                ),
                f = (f || "").match(Ua) || [""],
                K = f.length; K--; ) {
                    var ha = Cc.exec(f[K]) || []
                      , X = W = ha[1];
                    ha = (ha[2] || "").split(".").sort();
                    X && (J = n.event.special[X] || {},
                    X = (t ? J.delegateType : J.bindType) || X,
                    J = n.event.special[X] || {},
                    P = n.extend({
                        type: X,
                        origType: W,
                        data: p,
                        handler: m,
                        guid: m.guid,
                        selector: t,
                        needsContext: t && n.expr.match.needsContext.test(t),
                        namespace: ha.join(".")
                    }, u),
                    (T = H[X]) || (T = H[X] = [],
                    T.delegateCount = 0,
                    J.setup && !1 !== J.setup.call(d, p, ha, B) || d.addEventListener && d.addEventListener(X, B)),
                    J.add && (J.add.call(d, P),
                    P.handler.guid || (P.handler.guid = m.guid)),
                    t ? T.splice(T.delegateCount++, 0, P) : T.push(P),
                    n.event.global[X] = !0)
                }
        },
        remove: function(d, f, m, p, t) {
            var u, B, H, K, P, J, T, W, ha = ca.hasData(d) && ca.get(d);
            if (ha && (K = ha.events)) {
                f = (f || "").match(Ua) || [""];
                for (P = f.length; P--; )
                    if (H = Cc.exec(f[P]) || [],
                    J = W = H[1],
                    T = (H[2] || "").split(".").sort(),
                    J) {
                        var X = n.event.special[J] || {};
                        J = (p ? X.delegateType : X.bindType) || J;
                        var oa = K[J] || [];
                        H = H[2] && new RegExp("(^|\\.)" + T.join("\\.(?:.*\\.|)") + "(\\.|$)");
                        for (B = u = oa.length; u--; ) {
                            var na = oa[u];
                            !t && W !== na.origType || m && m.guid !== na.guid || H && !H.test(na.namespace) || p && p !== na.selector && ("**" !== p || !na.selector) || (oa.splice(u, 1),
                            na.selector && oa.delegateCount--,
                            X.remove && X.remove.call(d, na))
                        }
                        B && !oa.length && (X.teardown && !1 !== X.teardown.call(d, T, ha.handle) || n.removeEvent(d, J, ha.handle),
                        delete K[J])
                    } else
                        for (J in K)
                            n.event.remove(d, J + f[P], m, p, !0);
                n.isEmptyObject(K) && ca.remove(d, "handle events")
            }
        },
        dispatch: function(d) {
            var f = n.event.fix(d), m, p, t, u, B = Array(arguments.length), H = (ca.get(this, "events") || {})[f.type] || [], K = n.event.special[f.type] || {};
            B[0] = f;
            for (m = 1; m < arguments.length; m++)
                B[m] = arguments[m];
            if (f.delegateTarget = this,
            !K.preDispatch || !1 !== K.preDispatch.call(this, f)) {
                var P = n.event.handlers.call(this, f, H);
                for (m = 0; (t = P[m++]) && !f.isPropagationStopped(); )
                    for (f.currentTarget = t.elem,
                    H = 0; (u = t.handlers[H++]) && !f.isImmediatePropagationStopped(); )
                        f.rnamespace && !f.rnamespace.test(u.namespace) || (f.handleObj = u,
                        f.data = u.data,
                        p = ((n.event.special[u.origType] || {}).handle || u.handler).apply(t.elem, B),
                        void 0 !== p && !1 === (f.result = p) && (f.preventDefault(),
                        f.stopPropagation()));
                return K.postDispatch && K.postDispatch.call(this, f),
                f.result
            }
        },
        handlers: function(d, f) {
            var m, p = [], t = f.delegateCount, u = d.target;
            if (t && u.nodeType && !("click" === d.type && 1 <= d.button))
                for (; u !== this; u = u.parentNode || this)
                    if (1 === u.nodeType && ("click" !== d.type || !0 !== u.disabled)) {
                        var B = []
                          , H = {};
                        for (m = 0; m < t; m++) {
                            var K = f[m]
                              , P = K.selector + " ";
                            void 0 === H[P] && (H[P] = K.needsContext ? -1 < n(P, this).index(u) : n.find(P, this, null, [u]).length);
                            H[P] && B.push(K)
                        }
                        B.length && p.push({
                            elem: u,
                            handlers: B
                        })
                    }
            return u = this,
            t < f.length && p.push({
                elem: u,
                handlers: f.slice(t)
            }),
            p
        },
        addProp: function(d, f) {
            Object.defineProperty(n.Event.prototype, d, {
                enumerable: !0,
                configurable: !0,
                get: n.isFunction(f) ? function() {
                    if (this.originalEvent)
                        return f(this.originalEvent)
                }
                : function() {
                    if (this.originalEvent)
                        return this.originalEvent[d]
                }
                ,
                set: function(m) {
                    Object.defineProperty(this, d, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: m
                    })
                }
            })
        },
        fix: function(d) {
            return d[n.expando] ? d : new n.Event(d)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== qb() && this.focus)
                        return this.focus(),
                        !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === qb() && this.blur)
                        return this.blur(),
                        !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && q(this, "input"))
                        return this.click(),
                        !1
                },
                _default: function(d) {
                    return q(d.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(d) {
                    void 0 !== d.result && d.originalEvent && (d.originalEvent.returnValue = d.result)
                }
            }
        }
    };
    n.removeEvent = function(d, f, m) {
        d.removeEventListener && d.removeEventListener(f, m)
    }
    ;
    n.Event = function(d, f) {
        return this instanceof n.Event ? (d && d.type ? (this.originalEvent = d,
        this.type = d.type,
        this.isDefaultPrevented = d.defaultPrevented || void 0 === d.defaultPrevented && !1 === d.returnValue ? va : Fa,
        this.target = d.target && 3 === d.target.nodeType ? d.target.parentNode : d.target,
        this.currentTarget = d.currentTarget,
        this.relatedTarget = d.relatedTarget) : this.type = d,
        f && n.extend(this, f),
        this.timeStamp = d && d.timeStamp || n.now(),
        void (this[n.expando] = !0)) : new n.Event(d,f)
    }
    ;
    n.Event.prototype = {
        constructor: n.Event,
        isDefaultPrevented: Fa,
        isPropagationStopped: Fa,
        isImmediatePropagationStopped: Fa,
        isSimulated: !1,
        preventDefault: function() {
            var d = this.originalEvent;
            this.isDefaultPrevented = va;
            d && !this.isSimulated && d.preventDefault()
        },
        stopPropagation: function() {
            var d = this.originalEvent;
            this.isPropagationStopped = va;
            d && !this.isSimulated && d.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var d = this.originalEvent;
            this.isImmediatePropagationStopped = va;
            d && !this.isSimulated && d.stopImmediatePropagation();
            this.stopPropagation()
        }
    };
    n.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(d) {
            var f = d.button;
            return null == d.which && rd.test(d.type) ? null != d.charCode ? d.charCode : d.keyCode : !d.which && void 0 !== f && sd.test(d.type) ? 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0 : d.which
        }
    }, n.event.addProp);
    n.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(d, f) {
        n.event.special[d] = {
            delegateType: f,
            bindType: f,
            handle: function(m) {
                var p, t = m.relatedTarget, u = m.handleObj;
                return t && (t === this || n.contains(this, t)) || (m.type = u.origType,
                p = u.handler.apply(this, arguments),
                m.type = f),
                p
            }
        }
    });
    n.fn.extend({
        on: function(d, f, m, p) {
            return ib(this, d, f, m, p)
        },
        one: function(d, f, m, p) {
            return ib(this, d, f, m, p, 1)
        },
        off: function(d, f, m) {
            var p, t;
            if (d && d.preventDefault && d.handleObj)
                return p = d.handleObj,
                n(d.delegateTarget).off(p.namespace ? p.origType + "." + p.namespace : p.origType, p.selector, p.handler),
                this;
            if ("object" == typeof d) {
                for (t in d)
                    this.off(t, f, d[t]);
                return this
            }
            return !1 !== f && "function" != typeof f || (m = f,
            f = void 0),
            !1 === m && (m = Fa),
            this.each(function() {
                n.event.remove(this, d, m, f)
            })
        }
    });
    var td = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi
      , ud = /<script|<style|<link/i
      , Qc = /checked\s*(?:[^=]|=\s*.checked.)/i
      , Pc = /^true\/(.*)/
      , Rc = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    n.extend({
        htmlPrefilter: function(d) {
            return d.replace(td, "<$1></$2>")
        },
        clone: function(d, f, m) {
            var p, t = d.cloneNode(!0), u = n.contains(d.ownerDocument, d);
            if (!(ua.noCloneChecked || 1 !== d.nodeType && 11 !== d.nodeType || n.isXMLDoc(d))) {
                var B = O(t)
                  , H = O(d)
                  , K = 0;
                for (p = H.length; K < p; K++) {
                    var P = H[K]
                      , J = B[K]
                      , T = J.nodeName.toLowerCase();
                    "input" === T && Bc.test(P.type) ? J.checked = P.checked : "input" !== T && "textarea" !== T || (J.defaultValue = P.defaultValue)
                }
            }
            if (f)
                if (m)
                    for (H = H || O(d),
                    B = B || O(t),
                    K = 0,
                    p = H.length; K < p; K++)
                        Sa(H[K], B[K]);
                else
                    Sa(d, t);
            return B = O(t, "script"),
            0 < B.length && V(B, !u && O(d, "script")),
            t
        },
        cleanData: function(d) {
            for (var f, m, p, t = n.event.special, u = 0; void 0 !== (m = d[u]); u++)
                if (Tb(m)) {
                    if (f = m[ca.expando]) {
                        if (f.events)
                            for (p in f.events)
                                t[p] ? n.event.remove(m, p) : n.removeEvent(m, p, f.handle);
                        m[ca.expando] = void 0
                    }
                    m[Ra.expando] && (m[Ra.expando] = void 0)
                }
        }
    });
    n.fn.extend({
        detach: function(d) {
            return Ta(this, d, !0)
        },
        remove: function(d) {
            return Ta(this, d)
        },
        text: function(d) {
            return mb(this, function(f) {
                return void 0 === f ? n.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = f)
                })
            }, null, d, arguments.length)
        },
        append: function() {
            return Ca(this, arguments, function(d) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || nb(this, d).appendChild(d)
            })
        },
        prepend: function() {
            return Ca(this, arguments, function(d) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var f = nb(this, d);
                    f.insertBefore(d, f.firstChild)
                }
            })
        },
        before: function() {
            return Ca(this, arguments, function(d) {
                this.parentNode && this.parentNode.insertBefore(d, this)
            })
        },
        after: function() {
            return Ca(this, arguments, function(d) {
                this.parentNode && this.parentNode.insertBefore(d, this.nextSibling)
            })
        },
        empty: function() {
            for (var d, f = 0; null != (d = this[f]); f++)
                1 === d.nodeType && (n.cleanData(O(d, !1)),
                d.textContent = "");
            return this
        },
        clone: function(d, f) {
            return d = null != d && d,
            f = null == f ? d : f,
            this.map(function() {
                return n.clone(this, d, f)
            })
        },
        html: function(d) {
            return mb(this, function(f) {
                var m = this[0] || {}
                  , p = 0
                  , t = this.length;
                if (void 0 === f && 1 === m.nodeType)
                    return m.innerHTML;
                if ("string" == typeof f && !ud.test(f) && !Va[(hc.exec(f) || ["", ""])[1].toLowerCase()]) {
                    f = n.htmlPrefilter(f);
                    try {
                        for (; p < t; p++)
                            m = this[p] || {},
                            1 === m.nodeType && (n.cleanData(O(m, !1)),
                            m.innerHTML = f);
                        m = 0
                    } catch (u) {}
                }
                m && this.empty().append(f)
            }, null, d, arguments.length)
        },
        replaceWith: function() {
            var d = [];
            return Ca(this, arguments, function(f) {
                var m = this.parentNode;
                0 > n.inArray(this, d) && (n.cleanData(O(this)),
                m && m.replaceChild(f, this))
            }, d)
        }
    });
    n.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(d, f) {
        n.fn[d] = function(m) {
            for (var p = [], t = n(m), u = t.length - 1, B = 0; B <= u; B++)
                m = B === u ? this : this.clone(!0),
                n(t[B])[f](m),
                Zb.apply(p, m.get());
            return this.pushStack(p)
        }
    });
    var kc = /^margin/
      , Vb = new RegExp("^(" + zc + ")(?!px)[a-z%]+$","i")
      , Lb = function(d) {
        var f = d.ownerDocument.defaultView;
        return f && f.opener || (f = b),
        f.getComputedStyle(d)
    };
    !function() {
        function d() {
            if (B) {
                B.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%";
                B.innerHTML = "";
                Ub.appendChild(u);
                var H = b.getComputedStyle(B);
                f = "1%" !== H.top;
                t = "2px" === H.marginLeft;
                m = "4px" === H.width;
                B.style.marginRight = "50%";
                p = "4px" === H.marginRight;
                Ub.removeChild(u);
                B = null
            }
        }
        var f, m, p, t, u = la.createElement("div"), B = la.createElement("div");
        B.style && (B.style.backgroundClip = "content-box",
        B.cloneNode(!0).style.backgroundClip = "",
        ua.clearCloneStyle = "content-box" === B.style.backgroundClip,
        u.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",
        u.appendChild(B),
        n.extend(ua, {
            pixelPosition: function() {
                return d(),
                f
            },
            boxSizingReliable: function() {
                return d(),
                m
            },
            pixelMarginRight: function() {
                return d(),
                p
            },
            reliableMarginLeft: function() {
                return d(),
                t
            }
        }))
    }();
    var vd = /^(none|table(?!-c[ea]).+)/
      , Dc = /^--/
      , wd = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }
      , Ec = {
        letterSpacing: "0",
        fontWeight: "400"
    }
      , mc = ["Webkit", "Moz", "ms"]
      , lc = la.createElement("div").style;
    n.extend({
        cssHooks: {
            opacity: {
                get: function(d, f) {
                    if (f)
                        return d = Xa(d, "opacity"),
                        "" === d ? "1" : d
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(d, f, m, p) {
            if (d && 3 !== d.nodeType && 8 !== d.nodeType && d.style) {
                var t, u, B, H = n.camelCase(f), K = Dc.test(f), P = d.style;
                return K || (f = Za(H)),
                B = n.cssHooks[f] || n.cssHooks[H],
                void 0 === m ? B && "get"in B && void 0 !== (t = B.get(d, !1, p)) ? t : P[f] : (u = typeof m,
                "string" === u && (t = Eb.exec(m)) && t[1] && (m = G(d, f, t),
                u = "number"),
                null != m && m === m && ("number" === u && (m += t && t[3] || (n.cssNumber[H] ? "" : "px")),
                ua.clearCloneStyle || "" !== m || 0 !== f.indexOf("background") || (P[f] = "inherit"),
                B && "set"in B && void 0 === (m = B.set(d, m, p)) || (K ? P.setProperty(f, m) : P[f] = m)),
                void 0)
            }
        },
        css: function(d, f, m, p) {
            var t, u, B, H = n.camelCase(f);
            return Dc.test(f) || (f = Za(H)),
            B = n.cssHooks[f] || n.cssHooks[H],
            B && "get"in B && (t = B.get(d, !0, m)),
            void 0 === t && (t = Xa(d, f, p)),
            "normal" === t && f in Ec && (t = Ec[f]),
            "" === m || m ? (u = parseFloat(t),
            !0 === m || isFinite(u) ? u || 0 : t) : t
        }
    });
    n.each(["height", "width"], function(d, f) {
        n.cssHooks[f] = {
            get: function(m, p, t) {
                if (p)
                    return !vd.test(n.css(m, "display")) || m.getClientRects().length && m.getBoundingClientRect().width ? tb(m, f, t) : Ac(m, wd, function() {
                        return tb(m, f, t)
                    })
            },
            set: function(m, p, t) {
                var u, B = t && Lb(m);
                t = t && rb(m, f, t, "border-box" === n.css(m, "boxSizing", !1, B), B);
                return t && (u = Eb.exec(p)) && "px" !== (u[3] || "px") && (m.style[f] = p,
                p = n.css(m, f)),
                $a(m, p, t)
            }
        }
    });
    n.cssHooks.marginLeft = Ya(ua.reliableMarginLeft, function(d, f) {
        if (f)
            return (parseFloat(Xa(d, "marginLeft")) || d.getBoundingClientRect().left - Ac(d, {
                marginLeft: 0
            }, function() {
                return d.getBoundingClientRect().left
            })) + "px"
    });
    n.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(d, f) {
        n.cssHooks[d + f] = {
            expand: function(m) {
                var p = 0
                  , t = {};
                for (m = "string" == typeof m ? m.split(" ") : [m]; 4 > p; p++)
                    t[d + sb[p] + f] = m[p] || m[p - 2] || m[0];
                return t
            }
        };
        kc.test(d) || (n.cssHooks[d + f].set = $a)
    });
    n.fn.extend({
        css: function(d, f) {
            return mb(this, function(m, p, t) {
                var u, B = {}, H = 0;
                if (Array.isArray(p)) {
                    t = Lb(m);
                    for (u = p.length; H < u; H++)
                        B[p[H]] = n.css(m, p[H], !1, t);
                    return B
                }
                return void 0 !== t ? n.style(m, p, t) : n.css(m, p)
            }, d, f, 1 < arguments.length)
        }
    });
    n.Tween = Ga;
    Ga.prototype = {
        constructor: Ga,
        init: function(d, f, m, p, t, u) {
            this.elem = d;
            this.prop = m;
            this.easing = t || n.easing._default;
            this.options = f;
            this.start = this.now = this.cur();
            this.end = p;
            this.unit = u || (n.cssNumber[m] ? "" : "px")
        },
        cur: function() {
            var d = Ga.propHooks[this.prop];
            return d && d.get ? d.get(this) : Ga.propHooks._default.get(this)
        },
        run: function(d) {
            var f, m = Ga.propHooks[this.prop];
            return this.options.duration ? this.pos = f = n.easing[this.easing](d, this.options.duration * d, 0, 1, this.options.duration) : this.pos = f = d,
            this.now = (this.end - this.start) * f + this.start,
            this.options.step && this.options.step.call(this.elem, this.now, this),
            m && m.set ? m.set(this) : Ga.propHooks._default.set(this),
            this
        }
    };
    Ga.prototype.init.prototype = Ga.prototype;
    Ga.propHooks = {
        _default: {
            get: function(d) {
                var f;
                return 1 !== d.elem.nodeType || null != d.elem[d.prop] && null == d.elem.style[d.prop] ? d.elem[d.prop] : (f = n.css(d.elem, d.prop, ""),
                f && "auto" !== f ? f : 0)
            },
            set: function(d) {
                n.fx.step[d.prop] ? n.fx.step[d.prop](d) : 1 !== d.elem.nodeType || null == d.elem.style[n.cssProps[d.prop]] && !n.cssHooks[d.prop] ? d.elem[d.prop] = d.now : n.style(d.elem, d.prop, d.now + d.unit)
            }
        }
    };
    Ga.propHooks.scrollTop = Ga.propHooks.scrollLeft = {
        set: function(d) {
            d.elem.nodeType && d.elem.parentNode && (d.elem[d.prop] = d.now)
        }
    };
    n.easing = {
        linear: function(d) {
            return d
        },
        swing: function(d) {
            return .5 - Math.cos(d * Math.PI) / 2
        },
        _default: "swing"
    };
    n.fx = Ga.prototype.init;
    n.fx.step = {};
    var Bb, Mb, xd = /^(?:toggle|show|hide)$/, yd = /queueHooks$/;
    n.Animation = n.extend(cb, {
        tweeners: {
            "*": [function(d, f) {
                var m = this.createTween(d, f);
                return G(m.elem, d, Eb.exec(f), m),
                m
            }
            ]
        },
        tweener: function(d, f) {
            n.isFunction(d) ? (f = d,
            d = ["*"]) : d = d.match(Ua);
            for (var m, p = 0, t = d.length; p < t; p++)
                m = d[p],
                cb.tweeners[m] = cb.tweeners[m] || [],
                cb.tweeners[m].unshift(f)
        },
        prefilters: [function(d, f, m) {
            var p, t, u, B, H, K, P, J, T = "width"in f || "height"in f, W = this, ha = {}, X = d.style, oa = d.nodeType && Kb(d), na = ca.get(d, "fxshow");
            m.queue || (B = n._queueHooks(d, "fx"),
            null == B.unqueued && (B.unqueued = 0,
            H = B.empty.fire,
            B.empty.fire = function() {
                B.unqueued || H()
            }
            ),
            B.unqueued++,
            W.always(function() {
                W.always(function() {
                    B.unqueued--;
                    n.queue(d, "fx").length || B.empty.fire()
                })
            }));
            for (p in f)
                if (t = f[p],
                xd.test(t)) {
                    if (delete f[p],
                    u = u || "toggle" === t,
                    t === (oa ? "hide" : "show")) {
                        if ("show" !== t || !na || void 0 === na[p])
                            continue;
                        oa = !0
                    }
                    ha[p] = na && na[p] || n.style(d, p)
                }
            if (K = !n.isEmptyObject(f),
            K || !n.isEmptyObject(ha))
                for (p in T && 1 === d.nodeType && (m.overflow = [X.overflow, X.overflowX, X.overflowY],
                P = na && na.display,
                null == P && (P = ca.get(d, "display")),
                J = n.css(d, "display"),
                "none" === J && (P ? J = P : (M([d], !0),
                P = d.style.display || P,
                J = n.css(d, "display"),
                M([d]))),
                ("inline" === J || "inline-block" === J && null != P) && "none" === n.css(d, "float") && (K || (W.done(function() {
                    X.display = P
                }),
                null == P && (J = X.display,
                P = "none" === J ? "" : J)),
                X.display = "inline-block")),
                m.overflow && (X.overflow = "hidden",
                W.always(function() {
                    X.overflow = m.overflow[0];
                    X.overflowX = m.overflow[1];
                    X.overflowY = m.overflow[2]
                })),
                K = !1,
                ha)
                    K || (na ? "hidden"in na && (oa = na.hidden) : na = ca.access(d, "fxshow", {
                        display: P
                    }),
                    u && (na.hidden = !oa),
                    oa && M([d], !0),
                    W.done(function() {
                        oa || M([d]);
                        ca.remove(d, "fxshow");
                        for (p in ha)
                            n.style(d, p, ha[p])
                    })),
                    K = nc(oa ? na[p] : 0, p, W),
                    p in na || (na[p] = K.start,
                    oa && (K.end = K.start,
                    K.start = 0))
        }
        ],
        prefilter: function(d, f) {
            f ? cb.prefilters.unshift(d) : cb.prefilters.push(d)
        }
    });
    n.speed = function(d, f, m) {
        var p = d && "object" == typeof d ? n.extend({}, d) : {
            complete: m || !m && f || n.isFunction(d) && d,
            duration: d,
            easing: m && f || f && !n.isFunction(f) && f
        };
        return n.fx.off ? p.duration = 0 : "number" != typeof p.duration && (p.duration in n.fx.speeds ? p.duration = n.fx.speeds[p.duration] : p.duration = n.fx.speeds._default),
        null != p.queue && !0 !== p.queue || (p.queue = "fx"),
        p.old = p.complete,
        p.complete = function() {
            n.isFunction(p.old) && p.old.call(this);
            p.queue && n.dequeue(this, p.queue)
        }
        ,
        p
    }
    ;
    n.fn.extend({
        fadeTo: function(d, f, m, p) {
            return this.filter(Kb).css("opacity", 0).show().end().animate({
                opacity: f
            }, d, m, p)
        },
        animate: function(d, f, m, p) {
            var t = n.isEmptyObject(d)
              , u = n.speed(f, m, p);
            f = function() {
                var B = cb(this, n.extend({}, d), u);
                (t || ca.get(this, "finish")) && B.stop(!0)
            }
            ;
            return f.finish = f,
            t || !1 === u.queue ? this.each(f) : this.queue(u.queue, f)
        },
        stop: function(d, f, m) {
            var p = function(t) {
                var u = t.stop;
                delete t.stop;
                u(m)
            };
            return "string" != typeof d && (m = f,
            f = d,
            d = void 0),
            f && !1 !== d && this.queue(d || "fx", []),
            this.each(function() {
                var t = !0
                  , u = null != d && d + "queueHooks"
                  , B = n.timers
                  , H = ca.get(this);
                if (u)
                    H[u] && H[u].stop && p(H[u]);
                else
                    for (u in H)
                        H[u] && H[u].stop && yd.test(u) && p(H[u]);
                for (u = B.length; u--; )
                    B[u].elem !== this || null != d && B[u].queue !== d || (B[u].anim.stop(m),
                    t = !1,
                    B.splice(u, 1));
                !t && m || n.dequeue(this, d)
            })
        },
        finish: function(d) {
            return !1 !== d && (d = d || "fx"),
            this.each(function() {
                var f = ca.get(this)
                  , m = f[d + "queue"]
                  , p = f[d + "queueHooks"]
                  , t = n.timers
                  , u = m ? m.length : 0;
                f.finish = !0;
                n.queue(this, d, []);
                p && p.stop && p.stop.call(this, !0);
                for (p = t.length; p--; )
                    t[p].elem === this && t[p].queue === d && (t[p].anim.stop(!0),
                    t.splice(p, 1));
                for (p = 0; p < u; p++)
                    m[p] && m[p].finish && m[p].finish.call(this);
                delete f.finish
            })
        }
    });
    n.each(["toggle", "show", "hide"], function(d, f) {
        var m = n.fn[f];
        n.fn[f] = function(p, t, u) {
            return null == p || "boolean" == typeof p ? m.apply(this, arguments) : this.animate(Nb(f, !0), p, t, u)
        }
    });
    n.each({
        slideDown: Nb("show"),
        slideUp: Nb("hide"),
        slideToggle: Nb("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(d, f) {
        n.fn[d] = function(m, p, t) {
            return this.animate(f, m, p, t)
        }
    });
    n.timers = [];
    n.fx.tick = function() {
        var d = 0
          , f = n.timers;
        for (Bb = n.now(); d < f.length; d++) {
            var m = f[d];
            m() || f[d] !== m || f.splice(d--, 1)
        }
        f.length || n.fx.stop();
        Bb = void 0
    }
    ;
    n.fx.timer = function(d) {
        n.timers.push(d);
        n.fx.start()
    }
    ;
    n.fx.interval = 13;
    n.fx.start = function() {
        Mb || (Mb = !0,
        Na())
    }
    ;
    n.fx.stop = function() {
        Mb = null
    }
    ;
    n.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    n.fn.delay = function(d, f) {
        return d = n.fx ? n.fx.speeds[d] || d : d,
        f = f || "fx",
        this.queue(f, function(m, p) {
            var t = b.setTimeout(m, d);
            p.stop = function() {
                b.clearTimeout(t)
            }
        })
    }
    ;
    (function() {
        var d = la.createElement("input")
          , f = la.createElement("select").appendChild(la.createElement("option"));
        d.type = "checkbox";
        ua.checkOn = "" !== d.value;
        ua.optSelected = f.selected;
        d = la.createElement("input");
        d.value = "t";
        d.type = "radio";
        ua.radioValue = "t" === d.value
    }
    )();
    var Hb = n.expr.attrHandle;
    n.fn.extend({
        attr: function(d, f) {
            return mb(this, n.attr, d, f, 1 < arguments.length)
        },
        removeAttr: function(d) {
            return this.each(function() {
                n.removeAttr(this, d)
            })
        }
    });
    n.extend({
        attr: function(d, f, m) {
            var p, t, u = d.nodeType;
            if (3 !== u && 8 !== u && 2 !== u)
                return "undefined" == typeof d.getAttribute ? n.prop(d, f, m) : (1 === u && n.isXMLDoc(d) || (t = n.attrHooks[f.toLowerCase()] || (n.expr.match.bool.test(f) ? zd : void 0)),
                void 0 !== m ? null === m ? void n.removeAttr(d, f) : t && "set"in t && void 0 !== (p = t.set(d, m, f)) ? p : (d.setAttribute(f, m + ""),
                m) : t && "get"in t && null !== (p = t.get(d, f)) ? p : (p = n.find.attr(d, f),
                null == p ? void 0 : p))
        },
        attrHooks: {
            type: {
                set: function(d, f) {
                    if (!ua.radioValue && "radio" === f && q(d, "input")) {
                        var m = d.value;
                        return d.setAttribute("type", f),
                        m && (d.value = m),
                        f
                    }
                }
            }
        },
        removeAttr: function(d, f) {
            var m = 0
              , p = f && f.match(Ua);
            if (p && 1 === d.nodeType)
                for (; f = p[m++]; )
                    d.removeAttribute(f)
        }
    });
    var zd = {
        set: function(d, f, m) {
            return !1 === f ? n.removeAttr(d, m) : d.setAttribute(m, m),
            m
        }
    };
    n.each(n.expr.match.bool.source.match(/\w+/g), function(d, f) {
        var m = Hb[f] || n.find.attr;
        Hb[f] = function(p, t, u) {
            var B, H, K = t.toLowerCase();
            return u || (H = Hb[K],
            Hb[K] = B,
            B = null != m(p, t, u) ? K : null,
            Hb[K] = H),
            B
        }
    });
    var Ad = /^(?:input|select|textarea|button)$/i
      , Bd = /^(?:a|area)$/i;
    n.fn.extend({
        prop: function(d, f) {
            return mb(this, n.prop, d, f, 1 < arguments.length)
        },
        removeProp: function(d) {
            return this.each(function() {
                delete this[n.propFix[d] || d]
            })
        }
    });
    n.extend({
        prop: function(d, f, m) {
            var p, t, u = d.nodeType;
            if (3 !== u && 8 !== u && 2 !== u)
                return 1 === u && n.isXMLDoc(d) || (f = n.propFix[f] || f,
                t = n.propHooks[f]),
                void 0 !== m ? t && "set"in t && void 0 !== (p = t.set(d, m, f)) ? p : d[f] = m : t && "get"in t && null !== (p = t.get(d, f)) ? p : d[f]
        },
        propHooks: {
            tabIndex: {
                get: function(d) {
                    var f = n.find.attr(d, "tabindex");
                    return f ? parseInt(f, 10) : Ad.test(d.nodeName) || Bd.test(d.nodeName) && d.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    });
    ua.optSelected || (n.propHooks.selected = {
        get: function(d) {
            d = d.parentNode;
            return d && d.parentNode && d.parentNode.selectedIndex,
            null
        },
        set: function(d) {
            d = d.parentNode;
            d && (d.selectedIndex,
            d.parentNode && d.parentNode.selectedIndex)
        }
    });
    n.each("tabIndex readOnly maxLength cellSpacing cellPadding rowSpan colSpan useMap frameBorder contentEditable".split(" "), function() {
        n.propFix[this.toLowerCase()] = this
    });
    n.fn.extend({
        addClass: function(d) {
            var f, m, p, t, u, B, H = 0;
            if (n.isFunction(d))
                return this.each(function(K) {
                    n(this).addClass(d.call(this, K, vb(this)))
                });
            if ("string" == typeof d && d)
                for (f = d.match(Ua) || []; m = this[H++]; )
                    if (t = vb(m),
                    p = 1 === m.nodeType && " " + ub(t) + " ") {
                        for (B = 0; u = f[B++]; )
                            0 > p.indexOf(" " + u + " ") && (p += u + " ");
                        p = ub(p);
                        t !== p && m.setAttribute("class", p)
                    }
            return this
        },
        removeClass: function(d) {
            var f, m, p, t, u, B, H = 0;
            if (n.isFunction(d))
                return this.each(function(K) {
                    n(this).removeClass(d.call(this, K, vb(this)))
                });
            if (!arguments.length)
                return this.attr("class", "");
            if ("string" == typeof d && d)
                for (f = d.match(Ua) || []; m = this[H++]; )
                    if (t = vb(m),
                    p = 1 === m.nodeType && " " + ub(t) + " ") {
                        for (B = 0; u = f[B++]; )
                            for (; -1 < p.indexOf(" " + u + " "); )
                                p = p.replace(" " + u + " ", " ");
                        p = ub(p);
                        t !== p && m.setAttribute("class", p)
                    }
            return this
        },
        toggleClass: function(d, f) {
            var m = typeof d;
            return "boolean" == typeof f && "string" === m ? f ? this.addClass(d) : this.removeClass(d) : n.isFunction(d) ? this.each(function(p) {
                n(this).toggleClass(d.call(this, p, vb(this), f), f)
            }) : this.each(function() {
                var p, t;
                if ("string" === m) {
                    var u = 0
                      , B = n(this);
                    for (t = d.match(Ua) || []; p = t[u++]; )
                        B.hasClass(p) ? B.removeClass(p) : B.addClass(p)
                } else
                    void 0 !== d && "boolean" !== m || (p = vb(this),
                    p && ca.set(this, "__className__", p),
                    this.setAttribute && this.setAttribute("class", p || !1 === d ? "" : ca.get(this, "__className__") || ""))
            })
        },
        hasClass: function(d) {
            var f, m = 0;
            for (d = " " + d + " "; f = this[m++]; )
                if (1 === f.nodeType && -1 < (" " + ub(vb(f)) + " ").indexOf(d))
                    return !0;
            return !1
        }
    });
    var Cd = /\r/g;
    n.fn.extend({
        val: function(d) {
            var f, m, p, t = this[0];
            if (arguments.length)
                return p = n.isFunction(d),
                this.each(function(u) {
                    var B;
                    1 === this.nodeType && (B = p ? d.call(this, u, n(this).val()) : d,
                    null == B ? B = "" : "number" == typeof B ? B += "" : Array.isArray(B) && (B = n.map(B, function(H) {
                        return null == H ? "" : H + ""
                    })),
                    f = n.valHooks[this.type] || n.valHooks[this.nodeName.toLowerCase()],
                    f && "set"in f && void 0 !== f.set(this, B, "value") || (this.value = B))
                });
            if (t)
                return f = n.valHooks[t.type] || n.valHooks[t.nodeName.toLowerCase()],
                f && "get"in f && void 0 !== (m = f.get(t, "value")) ? m : (m = t.value,
                "string" == typeof m ? m.replace(Cd, "") : null == m ? "" : m)
        }
    });
    n.extend({
        valHooks: {
            option: {
                get: function(d) {
                    var f = n.find.attr(d, "value");
                    return null != f ? f : ub(n.text(d))
                }
            },
            select: {
                get: function(d) {
                    var f, m, p = d.options, t = d.selectedIndex, u = "select-one" === d.type, B = u ? null : [], H = u ? t + 1 : p.length;
                    for (m = 0 > t ? H : u ? t : 0; m < H; m++)
                        if (f = p[m],
                        !(!f.selected && m !== t || f.disabled || f.parentNode.disabled && q(f.parentNode, "optgroup"))) {
                            if (d = n(f).val(),
                            u)
                                return d;
                            B.push(d)
                        }
                    return B
                },
                set: function(d, f) {
                    for (var m, p = d.options, t = n.makeArray(f), u = p.length; u--; )
                        f = p[u],
                        (f.selected = -1 < n.inArray(n.valHooks.option.get(f), t)) && (m = !0);
                    return m || (d.selectedIndex = -1),
                    t
                }
            }
        }
    });
    n.each(["radio", "checkbox"], function() {
        n.valHooks[this] = {
            set: function(d, f) {
                if (Array.isArray(f))
                    return d.checked = -1 < n.inArray(n(d).val(), f)
            }
        };
        ua.checkOn || (n.valHooks[this].get = function(d) {
            return null === d.getAttribute("value") ? "on" : d.value
        }
        )
    });
    var Fc = /^(?:focusinfocus|focusoutblur)$/;
    n.extend(n.event, {
        trigger: function(d, f, m, p) {
            var t, u, B, H, K, P = [m || la], J = Pb.call(d, "type") ? d.type : d, T = Pb.call(d, "namespace") ? d.namespace.split(".") : [];
            if (t = u = m = m || la,
            3 !== m.nodeType && 8 !== m.nodeType && !Fc.test(J + n.event.triggered) && (-1 < J.indexOf(".") && (T = J.split("."),
            J = T.shift(),
            T.sort()),
            B = 0 > J.indexOf(":") && "on" + J,
            d = d[n.expando] ? d : new n.Event(J,"object" == typeof d && d),
            d.isTrigger = p ? 2 : 3,
            d.namespace = T.join("."),
            d.rnamespace = d.namespace ? new RegExp("(^|\\.)" + T.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
            d.result = void 0,
            d.target || (d.target = m),
            f = null == f ? [d] : n.makeArray(f, [d]),
            K = n.event.special[J] || {},
            p || !K.trigger || !1 !== K.trigger.apply(m, f))) {
                if (!p && !K.noBubble && !n.isWindow(m)) {
                    var W = K.delegateType || J;
                    for (Fc.test(W + J) || (t = t.parentNode); t; t = t.parentNode)
                        P.push(t),
                        u = t;
                    u === (m.ownerDocument || la) && P.push(u.defaultView || u.parentWindow || b)
                }
                for (T = 0; (t = P[T++]) && !d.isPropagationStopped(); )
                    d.type = 1 < T ? W : K.bindType || J,
                    (H = (ca.get(t, "events") || {})[d.type] && ca.get(t, "handle")) && H.apply(t, f),
                    (H = B && t[B]) && H.apply && Tb(t) && (d.result = H.apply(t, f),
                    !1 === d.result && d.preventDefault());
                return d.type = J,
                p || d.isDefaultPrevented() || K._default && !1 !== K._default.apply(P.pop(), f) || !Tb(m) || B && n.isFunction(m[J]) && !n.isWindow(m) && (u = m[B],
                u && (m[B] = null),
                n.event.triggered = J,
                m[J](),
                n.event.triggered = void 0,
                u && (m[B] = u)),
                d.result
            }
        },
        simulate: function(d, f, m) {
            d = n.extend(new n.Event, m, {
                type: d,
                isSimulated: !0
            });
            n.event.trigger(d, null, f)
        }
    });
    n.fn.extend({
        trigger: function(d, f) {
            return this.each(function() {
                n.event.trigger(d, f, this)
            })
        },
        triggerHandler: function(d, f) {
            var m = this[0];
            if (m)
                return n.event.trigger(d, f, m, !0)
        }
    });
    n.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(d, f) {
        n.fn[f] = function(m, p) {
            return 0 < arguments.length ? this.on(f, null, m, p) : this.trigger(f)
        }
    });
    n.fn.extend({
        hover: function(d, f) {
            return this.mouseenter(d).mouseleave(f || d)
        }
    });
    ua.focusin = "onfocusin"in b;
    ua.focusin || n.each({
        focus: "focusin",
        blur: "focusout"
    }, function(d, f) {
        var m = function(p) {
            n.event.simulate(f, p.target, n.event.fix(p))
        };
        n.event.special[f] = {
            setup: function() {
                var p = this.ownerDocument || this
                  , t = ca.access(p, f);
                t || p.addEventListener(d, m, !0);
                ca.access(p, f, (t || 0) + 1)
            },
            teardown: function() {
                var p = this.ownerDocument || this
                  , t = ca.access(p, f) - 1;
                t ? ca.access(p, f, t) : (p.removeEventListener(d, m, !0),
                ca.remove(p, f))
            }
        }
    });
    var Ib = b.location
      , Gc = n.now()
      , dc = /\?/;
    n.parseXML = function(d) {
        if (!d || "string" != typeof d)
            return null;
        try {
            var f = (new b.DOMParser).parseFromString(d, "text/xml")
        } catch (m) {
            f = void 0
        }
        return f && !f.getElementsByTagName("parsererror").length || n.error("Invalid XML: " + d),
        f
    }
    ;
    var Tc = /\[\]$/
      , Hc = /\r?\n/g
      , Dd = /^(?:submit|button|image|reset|file)$/i
      , Ed = /^(?:input|select|textarea|keygen)/i;
    n.param = function(d, f) {
        var m, p = [], t = function(u, B) {
            B = n.isFunction(B) ? B() : B;
            p[p.length] = encodeURIComponent(u) + "=" + encodeURIComponent(null == B ? "" : B)
        };
        if (Array.isArray(d) || d.jquery && !n.isPlainObject(d))
            n.each(d, function() {
                t(this.name, this.value)
            });
        else
            for (m in d)
                Wb(m, d[m], f, t);
        return p.join("&")
    }
    ;
    n.fn.extend({
        serialize: function() {
            return n.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var d = n.prop(this, "elements");
                return d ? n.makeArray(d) : this
            }).filter(function() {
                var d = this.type;
                return this.name && !n(this).is(":disabled") && Ed.test(this.nodeName) && !Dd.test(d) && (this.checked || !Bc.test(d))
            }).map(function(d, f) {
                d = n(this).val();
                return null == d ? null : Array.isArray(d) ? n.map(d, function(m) {
                    return {
                        name: f.name,
                        value: m.replace(Hc, "\r\n")
                    }
                }) : {
                    name: f.name,
                    value: d.replace(Hc, "\r\n")
                }
            }).get()
        }
    });
    var Fd = /%20/g
      , Gd = /#.*$/
      , Hd = /([?&])_=[^&]*/
      , Id = /^(.*?):[ \t]*([^\r\n]*)$/gm
      , Jd = /^(?:GET|HEAD)$/
      , Kd = /^\/\//
      , Ic = {}
      , Xb = {}
      , Jc = "*/".concat("*")
      , ec = la.createElement("a");
    ec.href = Ib.href;
    n.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Ib.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Ib.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Jc,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": n.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(d, f) {
            return f ? Yb(Yb(d, n.ajaxSettings), f) : Yb(n.ajaxSettings, d)
        },
        ajaxPrefilter: oc(Ic),
        ajaxTransport: oc(Xb),
        ajax: function(d, f) {
            function m(ia, Da, wa, Qa) {
                var ja, La, Ha, qa = Da;
                if (!Pa) {
                    Pa = !0;
                    B && b.clearTimeout(B);
                    p = void 0;
                    t = Qa || "";
                    ea.readyState = 0 < ia ? 4 : 0;
                    Qa = 200 <= ia && 300 > ia || 304 === ia;
                    if (wa) {
                        for (var sa = J, gb = ea, Ia, ka, ya, ta, Fb = sa.contents, ab = sa.dataTypes; "*" === ab[0]; )
                            ab.shift(),
                            void 0 === Ia && (Ia = sa.mimeType || gb.getResponseHeader("Content-Type"));
                        if (Ia)
                            for (ka in Fb)
                                if (Fb[ka] && Fb[ka].test(Ia)) {
                                    ab.unshift(ka);
                                    break
                                }
                        if (ab[0]in wa)
                            ya = ab[0];
                        else {
                            for (ka in wa) {
                                if (!ab[0] || sa.converters[ka + " " + ab[0]]) {
                                    ya = ka;
                                    break
                                }
                                ta ||= ka
                            }
                            ya = ya || ta
                        }
                        sa = wa = ya ? (ya !== ab[0] && ab.unshift(ya),
                        wa[ya]) : void 0
                    }
                    a: {
                        wa = J;
                        Ia = sa;
                        ka = ea;
                        ya = Qa;
                        var Ma, db, bb;
                        sa = {};
                        gb = wa.dataTypes.slice();
                        if (gb[1])
                            for (Ma in wa.converters)
                                sa[Ma.toLowerCase()] = wa.converters[Ma];
                        for (ta = gb.shift(); ta; )
                            if (wa.responseFields[ta] && (ka[wa.responseFields[ta]] = Ia),
                            !bb && ya && wa.dataFilter && (Ia = wa.dataFilter(Ia, wa.dataType)),
                            bb = ta,
                            ta = gb.shift())
                                if ("*" === ta)
                                    ta = bb;
                                else if ("*" !== bb && bb !== ta) {
                                    if (Ma = sa[bb + " " + ta] || sa["* " + ta],
                                    !Ma)
                                        for (pb in sa)
                                            if (db = pb.split(" "),
                                            db[1] === ta && (Ma = sa[bb + " " + db[0]] || sa["* " + db[0]])) {
                                                !0 === Ma ? Ma = sa[pb] : !0 !== sa[pb] && (ta = db[0],
                                                gb.unshift(db[1]));
                                                break
                                            }
                                    if (!0 !== Ma)
                                        if (Ma && wa["throws"])
                                            Ia = Ma(Ia);
                                        else
                                            try {
                                                Ia = Ma(Ia)
                                            } catch (jb) {
                                                var pb = {
                                                    state: "parsererror",
                                                    error: Ma ? jb : "No conversion from " + bb + " to " + ta
                                                };
                                                break a
                                            }
                                }
                        pb = {
                            state: "success",
                            data: Ia
                        }
                    }
                    sa = pb;
                    Qa ? (J.ifModified && (Ha = ea.getResponseHeader("Last-Modified"),
                    Ha && (n.lastModified[Ea] = Ha),
                    Ha = ea.getResponseHeader("etag"),
                    Ha && (n.etag[Ea] = Ha)),
                    204 === ia || "HEAD" === J.type ? qa = "nocontent" : 304 === ia ? qa = "notmodified" : (qa = sa.state,
                    ja = sa.data,
                    La = sa.error,
                    Qa = !La)) : (La = qa,
                    !ia && qa || (qa = "error",
                    0 > ia && (ia = 0)));
                    ea.status = ia;
                    ea.statusText = (Da || qa) + "";
                    Qa ? ha.resolveWith(T, [ja, qa, ea]) : ha.rejectWith(T, [ea, qa, La]);
                    ea.statusCode(oa);
                    oa = void 0;
                    H && W.trigger(Qa ? "ajaxSuccess" : "ajaxError", [ea, J, Qa ? ja : La]);
                    X.fireWith(T, [ea, qa]);
                    H && (W.trigger("ajaxComplete", [ea, J]),
                    --n.active || n.event.trigger("ajaxStop"))
                }
            }
            "object" == typeof d && (f = d,
            d = void 0);
            f = f || {};
            var p, t, u, B, H, K, P, J = n.ajaxSetup({}, f), T = J.context || J, W = J.context && (T.nodeType || T.jquery) ? n(T) : n.event, ha = n.Deferred(), X = n.Callbacks("once memory"), oa = J.statusCode || {}, na = {}, eb = {}, Ja = "canceled", ea = {
                readyState: 0,
                getResponseHeader: function(ia) {
                    var Da;
                    if (Pa) {
                        if (!u)
                            for (u = {}; Da = Id.exec(t); )
                                u[Da[1].toLowerCase()] = Da[2];
                        Da = u[ia.toLowerCase()]
                    }
                    return null == Da ? null : Da
                },
                getAllResponseHeaders: function() {
                    return Pa ? t : null
                },
                setRequestHeader: function(ia, Da) {
                    return null == Pa && (ia = eb[ia.toLowerCase()] = eb[ia.toLowerCase()] || ia,
                    na[ia] = Da),
                    this
                },
                overrideMimeType: function(ia) {
                    return null == Pa && (J.mimeType = ia),
                    this
                },
                statusCode: function(ia) {
                    var Da;
                    if (ia)
                        if (Pa)
                            ea.always(ia[ea.status]);
                        else
                            for (Da in ia)
                                oa[Da] = [oa[Da], ia[Da]];
                    return this
                },
                abort: function(ia) {
                    ia = ia || Ja;
                    return p && p.abort(ia),
                    m(0, ia),
                    this
                }
            };
            if (ha.promise(ea),
            J.url = ((d || J.url || Ib.href) + "").replace(Kd, Ib.protocol + "//"),
            J.type = f.method || f.type || J.method || J.type,
            J.dataTypes = (J.dataType || "*").toLowerCase().match(Ua) || [""],
            null == J.crossDomain) {
                d = la.createElement("a");
                try {
                    d.href = J.url,
                    d.href = d.href,
                    J.crossDomain = ec.protocol + "//" + ec.host != d.protocol + "//" + d.host
                } catch (ia) {
                    J.crossDomain = !0
                }
            }
            if (J.data && J.processData && "string" != typeof J.data && (J.data = n.param(J.data, J.traditional)),
            pc(Ic, J, f, ea),
            Pa)
                return ea;
            (H = n.event && J.global) && 0 === n.active++ && n.event.trigger("ajaxStart");
            J.type = J.type.toUpperCase();
            J.hasContent = !Jd.test(J.type);
            var Ea = J.url.replace(Gd, "");
            J.hasContent ? J.data && J.processData && 0 === (J.contentType || "").indexOf("application/x-www-form-urlencoded") && (J.data = J.data.replace(Fd, "+")) : (P = J.url.slice(Ea.length),
            J.data && (Ea += (dc.test(Ea) ? "&" : "?") + J.data,
            delete J.data),
            !1 === J.cache && (Ea = Ea.replace(Hd, "$1"),
            P = (dc.test(Ea) ? "&" : "?") + "_=" + Gc++ + P),
            J.url = Ea + P);
            J.ifModified && (n.lastModified[Ea] && ea.setRequestHeader("If-Modified-Since", n.lastModified[Ea]),
            n.etag[Ea] && ea.setRequestHeader("If-None-Match", n.etag[Ea]));
            (J.data && J.hasContent && !1 !== J.contentType || f.contentType) && ea.setRequestHeader("Content-Type", J.contentType);
            ea.setRequestHeader("Accept", J.dataTypes[0] && J.accepts[J.dataTypes[0]] ? J.accepts[J.dataTypes[0]] + ("*" !== J.dataTypes[0] ? ", " + Jc + "; q=0.01" : "") : J.accepts["*"]);
            for (K in J.headers)
                ea.setRequestHeader(K, J.headers[K]);
            if (J.beforeSend && (!1 === J.beforeSend.call(T, ea, J) || Pa))
                return ea.abort();
            if (Ja = "abort",
            X.add(J.complete),
            ea.done(J.success),
            ea.fail(J.error),
            p = pc(Xb, J, f, ea)) {
                if (ea.readyState = 1,
                H && W.trigger("ajaxSend", [ea, J]),
                Pa)
                    return ea;
                J.async && 0 < J.timeout && (B = b.setTimeout(function() {
                    ea.abort("timeout")
                }, J.timeout));
                try {
                    var Pa = !1;
                    p.send(na, m)
                } catch (ia) {
                    if (Pa)
                        throw ia;
                    m(-1, ia)
                }
            } else
                m(-1, "No Transport");
            return ea
        },
        getJSON: function(d, f, m) {
            return n.get(d, f, m, "json")
        },
        getScript: function(d, f) {
            return n.get(d, void 0, f, "script")
        }
    });
    n.each(["get", "post"], function(d, f) {
        n[f] = function(m, p, t, u) {
            return n.isFunction(p) && (u = u || t,
            t = p,
            p = void 0),
            n.ajax(n.extend({
                url: m,
                type: f,
                dataType: u,
                data: p,
                success: t
            }, n.isPlainObject(m) && m))
        }
    });
    n._evalUrl = function(d) {
        return n.ajax({
            url: d,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }
    ;
    n.fn.extend({
        wrapAll: function(d) {
            var f;
            return this[0] && (n.isFunction(d) && (d = d.call(this[0])),
            f = n(d, this[0].ownerDocument).eq(0).clone(!0),
            this[0].parentNode && f.insertBefore(this[0]),
            f.map(function() {
                for (var m = this; m.firstElementChild; )
                    m = m.firstElementChild;
                return m
            }).append(this)),
            this
        },
        wrapInner: function(d) {
            return n.isFunction(d) ? this.each(function(f) {
                n(this).wrapInner(d.call(this, f))
            }) : this.each(function() {
                var f = n(this)
                  , m = f.contents();
                m.length ? m.wrapAll(d) : f.append(d)
            })
        },
        wrap: function(d) {
            var f = n.isFunction(d);
            return this.each(function(m) {
                n(this).wrapAll(f ? d.call(this, m) : d)
            })
        },
        unwrap: function(d) {
            return this.parent(d).not("body").each(function() {
                n(this).replaceWith(this.childNodes)
            }),
            this
        }
    });
    n.expr.pseudos.hidden = function(d) {
        return !n.expr.pseudos.visible(d)
    }
    ;
    n.expr.pseudos.visible = function(d) {
        return !!(d.offsetWidth || d.offsetHeight || d.getClientRects().length)
    }
    ;
    n.ajaxSettings.xhr = function() {
        try {
            return new b.XMLHttpRequest
        } catch (d) {}
    }
    ;
    var Ld = {
        0: 200,
        1223: 204
    }
      , Jb = n.ajaxSettings.xhr();
    ua.cors = !!Jb && "withCredentials"in Jb;
    ua.ajax = Jb = !!Jb;
    n.ajaxTransport(function(d) {
        var f, m;
        if (ua.cors || Jb && !d.crossDomain)
            return {
                send: function(p, t) {
                    var u, B = d.xhr();
                    if (B.open(d.type, d.url, d.async, d.username, d.password),
                    d.xhrFields)
                        for (u in d.xhrFields)
                            B[u] = d.xhrFields[u];
                    d.mimeType && B.overrideMimeType && B.overrideMimeType(d.mimeType);
                    d.crossDomain || p["X-Requested-With"] || (p["X-Requested-With"] = "XMLHttpRequest");
                    for (u in p)
                        B.setRequestHeader(u, p[u]);
                    f = function(H) {
                        return function() {
                            f && (f = m = B.onload = B.onerror = B.onabort = B.onreadystatechange = null,
                            "abort" === H ? B.abort() : "error" === H ? "number" != typeof B.status ? t(0, "error") : t(B.status, B.statusText) : t(Ld[B.status] || B.status, B.statusText, "text" !== (B.responseType || "text") || "string" != typeof B.responseText ? {
                                binary: B.response
                            } : {
                                text: B.responseText
                            }, B.getAllResponseHeaders()))
                        }
                    }
                    ;
                    B.onload = f();
                    m = B.onerror = f("error");
                    void 0 !== B.onabort ? B.onabort = m : B.onreadystatechange = function() {
                        4 === B.readyState && b.setTimeout(function() {
                            f && m()
                        })
                    }
                    ;
                    f = f("abort");
                    try {
                        B.send(d.hasContent && d.data || null)
                    } catch (H) {
                        if (f)
                            throw H;
                    }
                },
                abort: function() {
                    f && f()
                }
            }
    });
    n.ajaxPrefilter(function(d) {
        d.crossDomain && (d.contents.script = !1)
    });
    n.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(d) {
                return n.globalEval(d),
                d
            }
        }
    });
    n.ajaxPrefilter("script", function(d) {
        void 0 === d.cache && (d.cache = !1);
        d.crossDomain && (d.type = "GET")
    });
    n.ajaxTransport("script", function(d) {
        if (d.crossDomain) {
            var f, m;
            return {
                send: function(p, t) {
                    f = n("<script>").prop({
                        charset: d.scriptCharset,
                        src: d.url
                    }).on("load error", m = function(u) {
                        f.remove();
                        m = null;
                        u && t("error" === u.type ? 404 : 200, u.type)
                    }
                    );
                    la.head.appendChild(f[0])
                },
                abort: function() {
                    m && m()
                }
            }
        }
    });
    var Kc = []
      , fc = /(=)\?(?=&|$)|\?\?/;
    n.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var d = Kc.pop() || n.expando + "_" + Gc++;
            return this[d] = !0,
            d
        }
    });
    n.ajaxPrefilter("json jsonp", function(d, f, m) {
        var p, t, u, B = !1 !== d.jsonp && (fc.test(d.url) ? "url" : "string" == typeof d.data && 0 === (d.contentType || "").indexOf("application/x-www-form-urlencoded") && fc.test(d.data) && "data");
        if (B || "jsonp" === d.dataTypes[0])
            return p = d.jsonpCallback = n.isFunction(d.jsonpCallback) ? d.jsonpCallback() : d.jsonpCallback,
            B ? d[B] = d[B].replace(fc, "$1" + p) : !1 !== d.jsonp && (d.url += (dc.test(d.url) ? "&" : "?") + d.jsonp + "=" + p),
            d.converters["script json"] = function() {
                return u || n.error(p + " was not called"),
                u[0]
            }
            ,
            d.dataTypes[0] = "json",
            t = b[p],
            b[p] = function() {
                u = arguments
            }
            ,
            m.always(function() {
                void 0 === t ? n(b).removeProp(p) : b[p] = t;
                d[p] && (d.jsonpCallback = f.jsonpCallback,
                Kc.push(p));
                u && n.isFunction(t) && t(u[0]);
                u = t = void 0
            }),
            "script"
    });
    ua.createHTMLDocument = function() {
        var d = la.implementation.createHTMLDocument("").body;
        return d.innerHTML = "<form></form><form></form>",
        2 === d.childNodes.length
    }();
    n.parseHTML = function(d, f, m) {
        if ("string" != typeof d)
            return [];
        "boolean" == typeof f && (m = f,
        f = !1);
        var p, t, u;
        return f || (ua.createHTMLDocument ? (f = la.implementation.createHTMLDocument(""),
        p = f.createElement("base"),
        p.href = la.location.href,
        f.head.appendChild(p)) : f = la),
        t = yc.exec(d),
        u = !m && [],
        t ? [f.createElement(t[1])] : (t = fa([d], f, u),
        u && u.length && n(u).remove(),
        n.merge([], t.childNodes))
    }
    ;
    n.fn.load = function(d, f, m) {
        var p, t, u, B = this, H = d.indexOf(" ");
        return -1 < H && (p = ub(d.slice(H)),
        d = d.slice(0, H)),
        n.isFunction(f) ? (m = f,
        f = void 0) : f && "object" == typeof f && (t = "POST"),
        0 < B.length && n.ajax({
            url: d,
            type: t || "GET",
            dataType: "html",
            data: f
        }).done(function(K) {
            u = arguments;
            B.html(p ? n("<div>").append(n.parseHTML(K)).find(p) : K)
        }).always(m && function(K, P) {
            B.each(function() {
                m.apply(this, u || [K.responseText, P, K])
            })
        }
        ),
        this
    }
    ;
    n.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(d, f) {
        n.fn[f] = function(m) {
            return this.on(f, m)
        }
    });
    n.expr.pseudos.animated = function(d) {
        return n.grep(n.timers, function(f) {
            return d === f.elem
        }).length
    }
    ;
    n.offset = {
        setOffset: function(d, f, m) {
            var p, t, u, B = n.css(d, "position"), H = n(d), K = {};
            "static" === B && (d.style.position = "relative");
            var P = H.offset()
              , J = n.css(d, "top")
              , T = n.css(d, "left");
            ("absolute" === B || "fixed" === B) && -1 < (J + T).indexOf("auto") ? (p = H.position(),
            u = p.top,
            t = p.left) : (u = parseFloat(J) || 0,
            t = parseFloat(T) || 0);
            n.isFunction(f) && (f = f.call(d, m, n.extend({}, P)));
            null != f.top && (K.top = f.top - P.top + u);
            null != f.left && (K.left = f.left - P.left + t);
            "using"in f ? f.using.call(d, K) : H.css(K)
        }
    };
    n.fn.extend({
        offset: function(d) {
            if (arguments.length)
                return void 0 === d ? this : this.each(function(B) {
                    n.offset.setOffset(this, d, B)
                });
            var f, m, p, t, u = this[0];
            if (u)
                return u.getClientRects().length ? (p = u.getBoundingClientRect(),
                f = u.ownerDocument,
                m = f.documentElement,
                t = f.defaultView,
                {
                    top: p.top + t.pageYOffset - m.clientTop,
                    left: p.left + t.pageXOffset - m.clientLeft
                }) : {
                    top: 0,
                    left: 0
                }
        },
        position: function() {
            if (this[0]) {
                var d, f, m = this[0], p = {
                    top: 0,
                    left: 0
                };
                return "fixed" === n.css(m, "position") ? f = m.getBoundingClientRect() : (d = this.offsetParent(),
                f = this.offset(),
                q(d[0], "html") || (p = d.offset()),
                p = {
                    top: p.top + n.css(d[0], "borderTopWidth", !0),
                    left: p.left + n.css(d[0], "borderLeftWidth", !0)
                }),
                {
                    top: f.top - p.top - n.css(m, "marginTop", !0),
                    left: f.left - p.left - n.css(m, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var d = this.offsetParent; d && "static" === n.css(d, "position"); )
                    d = d.offsetParent;
                return d || Ub
            })
        }
    });
    n.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(d, f) {
        var m = "pageYOffset" === f;
        n.fn[d] = function(p) {
            return mb(this, function(t, u, B) {
                var H;
                return n.isWindow(t) ? H = t : 9 === t.nodeType && (H = t.defaultView),
                void 0 === B ? H ? H[f] : t[u] : void (H ? H.scrollTo(m ? H.pageXOffset : B, m ? B : H.pageYOffset) : t[u] = B)
            }, d, p, arguments.length)
        }
    });
    n.each(["top", "left"], function(d, f) {
        n.cssHooks[f] = Ya(ua.pixelPosition, function(m, p) {
            if (p)
                return p = Xa(m, f),
                Vb.test(p) ? n(m).position()[f] + "px" : p
        })
    });
    n.each({
        Height: "height",
        Width: "width"
    }, function(d, f) {
        n.each({
            padding: "inner" + d,
            content: f,
            "": "outer" + d
        }, function(m, p) {
            n.fn[p] = function(t, u) {
                var B = arguments.length && (m || "boolean" != typeof t)
                  , H = m || (!0 === t || !0 === u ? "margin" : "border");
                return mb(this, function(K, P, J) {
                    var T;
                    return n.isWindow(K) ? 0 === p.indexOf("outer") ? K["inner" + d] : K.document.documentElement["client" + d] : 9 === K.nodeType ? (T = K.documentElement,
                    Math.max(K.body["scroll" + d], T["scroll" + d], K.body["offset" + d], T["offset" + d], T["client" + d])) : void 0 === J ? n.css(K, P, H) : n.style(K, P, J, H)
                }, f, B ? t : void 0, B)
            }
        })
    });
    n.fn.extend({
        bind: function(d, f, m) {
            return this.on(d, null, f, m)
        },
        unbind: function(d, f) {
            return this.off(d, null, f)
        },
        delegate: function(d, f, m, p) {
            return this.on(f, d, m, p)
        },
        undelegate: function(d, f, m) {
            return 1 === arguments.length ? this.off(d, "**") : this.off(f, d || "**", m)
        }
    });
    n.holdReady = function(d) {
        d ? n.readyWait++ : n.ready(!0)
    }
    ;
    n.isArray = Array.isArray;
    n.parseJSON = JSON.parse;
    n.nodeName = q;
    "function" == typeof define && define.amd && define("jquery", [], function() {
        return n
    });
    var Md = b.jQuery
      , Nd = b.$;
    return n.noConflict = function(d) {
        return b.$ === n && (b.$ = Nd),
        d && b.jQuery === n && (b.jQuery = Md),
        n
    }
    ,
    c || (b.jQuery = b.$ = n),
    n
});
function getInternetExplorerVersion() {
    var b = -1;
    "Microsoft Internet Explorer" == navigator.appName && null != RegExp("MSIE ([0-9]{1,}[.0-9]{0,})").exec(navigator.userAgent) && (b = parseFloat(RegExp.$1));
    return b
}
var ie = getInternetExplorerVersion();
function getQueryVariable(b) {
    for (var c = window.location.search.substring(1).split("&"), e = 0; e < c.length; e++) {
        var l = c[e].match(/([^=]+?)=(.+)/);
        if (l && decodeURIComponent(l[1]) == b)
            return decodeURIComponent(l[2])
    }
}
this.jukebox = {};
jukebox.Player = function(b, c) {
    this.id = ++jukebox.__jukeboxId;
    this.origin = c || null;
    this.settings = {};
    for (var e in this.defaults)
        this.settings[e] = this.defaults[e];
    if ("[object Object]" === Object.prototype.toString.call(b))
        for (var l in b)
            this.settings[l] = b[l];
    "[object Function]" === Object.prototype.toString.call(jukebox.Manager) && (jukebox.Manager = new jukebox.Manager);
    this.resource = this.isPlaying = null;
    "[object Object]" === Object.prototype.toString.call(jukebox.Manager) ? this.resource = jukebox.Manager.getPlayableResource(this.settings.resources) : this.resource = this.settings.resources[0] || null;
    if (null === this.resource)
        throw "Your browser can't playback the given resources - or you have missed to include jukebox.Manager";
    this.__init();
    return this
}
;
jukebox.__jukeboxId = 0;
jukebox.Player.prototype = {
    defaults: {
        resources: [],
        autoplay: !1,
        spritemap: {},
        flashMediaElement: "./swf/FlashMediaElement.swf",
        timeout: 1E3
    },
    __addToManager: function(b) {
        !0 !== this.__wasAddedToManager && (jukebox.Manager.add(this),
        this.__wasAddedToManager = !0)
    },
    __init: function() {
        var b = this, c = this.settings, e = {}, l;
        jukebox.Manager && void 0 !== jukebox.Manager.features && (e = jukebox.Manager.features);
        if (!0 === e.html5audio) {
            this.context = new Audio;
            this.context.src = this.resource;
            if (null === this.origin) {
                var q = function(x) {
                    b.__addToManager(x)
                };
                this.context.addEventListener("canplaythrough", q, !0);
                window.setTimeout(function() {
                    b.context.removeEventListener("canplaythrough", q, !0);
                    q("timeout")
                }, c.timeout)
            }
            this.context.autobuffer = !0;
            this.context.preload = !0;
            for (l in this.HTML5API)
                this[l] = this.HTML5API[l];
            1 < e.channels ? !0 === c.autoplay ? this.context.autoplay = !0 : void 0 !== c.spritemap[c.autoplay] && this.play(c.autoplay) : 1 === e.channels && void 0 !== c.spritemap[c.autoplay] && (this.backgroundMusic = c.spritemap[c.autoplay],
            this.backgroundMusic.started = Date.now ? Date.now() : +new Date,
            this.play(c.autoplay));
            1 == e.channels && !0 !== c.canPlayBackground && (window.addEventListener("pagehide", function() {
                null !== b.isPlaying && (b.pause(),
                b.__wasAutoPaused = !0)
            }),
            window.addEventListener("pageshow", function() {
                b.__wasAutoPaused && (b.resume(),
                delete b._wasAutoPaused)
            }))
        } else if (!0 === e.flashaudio) {
            for (l in this.FLASHAPI)
                this[l] = this.FLASHAPI[l];
            e = ["id=jukebox-flashstream-" + this.id, "autoplay=" + c.autoplay, "file=" + window.encodeURIComponent(this.resource)];
            this.__initFlashContext(e);
            !0 === c.autoplay ? this.play(0) : c.spritemap[c.autoplay] && this.play(c.autoplay)
        } else
            throw "Your Browser does not support Flash Audio or HTML5 Audio.";
    },
    __initFlashContext: function(b) {
        var c = this.settings.flashMediaElement, e, l = {
            flashvars: b.join("&"),
            quality: "high",
            bgcolor: "#000000",
            wmode: "transparent",
            allowscriptaccess: "always",
            allowfullscreen: "true"
        };
        if (navigator.userAgent.match(/MSIE/)) {
            var q = document.createElement("div");
            document.getElementsByTagName("body")[0].appendChild(q);
            var x = document.createElement("object");
            x.id = "jukebox-flashstream-" + this.id;
            x.setAttribute("type", "application/x-shockwave-flash");
            x.setAttribute("classid", "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000");
            x.setAttribute("width", "0");
            x.setAttribute("height", "0");
            l.movie = c + "?x=" + (Date.now ? Date.now() : +new Date);
            l.flashvars = b.join("&amp;");
            for (e in l)
                b = document.createElement("param"),
                b.setAttribute("name", e),
                b.setAttribute("value", l[e]),
                x.appendChild(b);
            q.outerHTML = x.outerHTML;
            this.context = document.getElementById("jukebox-flashstream-" + this.id)
        } else {
            q = document.createElement("embed");
            q.id = "jukebox-flashstream-" + this.id;
            q.setAttribute("type", "application/x-shockwave-flash");
            q.setAttribute("width", "100");
            q.setAttribute("height", "100");
            l.play = !1;
            l.loop = !1;
            l.src = c + "?x=" + (Date.now ? Date.now() : +new Date);
            for (e in l)
                q.setAttribute(e, l[e]);
            document.getElementsByTagName("body")[0].appendChild(q);
            this.context = q
        }
    },
    backgroundHackForiOS: function() {
        if (void 0 !== this.backgroundMusic) {
            var b = Date.now ? Date.now() : +new Date;
            void 0 === this.backgroundMusic.started ? (this.backgroundMusic.started = b,
            this.setCurrentTime(this.backgroundMusic.start)) : (this.backgroundMusic.lastPointer = (b - this.backgroundMusic.started) / 1E3 % (this.backgroundMusic.end - this.backgroundMusic.start) + this.backgroundMusic.start,
            this.play(this.backgroundMusic.lastPointer))
        }
    },
    play: function(b, c) {
        if (null !== this.isPlaying && !0 !== c)
            void 0 !== jukebox.Manager && jukebox.Manager.addToQueue(b, this.id);
        else {
            c = this.settings.spritemap;
            if (void 0 !== c[b])
                var e = c[b].start;
            else if ("number" === typeof b) {
                e = b;
                for (var l in c)
                    if (e >= c[l].start && e <= c[l].end) {
                        b = l;
                        break
                    }
            }
            void 0 !== e && "[object Object]" === Object.prototype.toString.call(c[b]) && (this.isPlaying = this.settings.spritemap[b],
            this.context.play && this.context.play(),
            this.wasReady = this.setCurrentTime(e))
        }
    },
    stop: function() {
        this.__lastPosition = 0;
        this.isPlaying = null;
        this.backgroundMusic ? this.backgroundHackForiOS() : this.context.pause();
        return !0
    },
    pause: function() {
        this.isPlaying = null;
        this.__lastPosition = this.getCurrentTime();
        this.context.pause();
        return this.__lastPosition
    },
    resume: function(b) {
        b = "number" === typeof b ? b : this.__lastPosition;
        if (null !== b)
            return this.play(b),
            this.__lastPosition = null,
            !0;
        this.context.play();
        return !1
    },
    HTML5API: {
        getVolume: function() {
            return this.context.volume || 1
        },
        setVolume: function(b) {
            this.context.volume = b;
            return 1E-4 > Math.abs(this.context.volume - b) ? !0 : !1
        },
        getCurrentTime: function() {
            return this.context.currentTime || 0
        },
        setCurrentTime: function(b) {
            try {
                return this.context.currentTime = b,
                !0
            } catch (c) {
                return !1
            }
        }
    },
    FLASHAPI: {
        getVolume: function() {
            return this.context && "function" === typeof this.context.getVolume ? this.context.getVolume() : 1
        },
        setVolume: function(b) {
            return this.context && "function" === typeof this.context.setVolume ? (this.context.setVolume(b),
            !0) : !1
        },
        getCurrentTime: function() {
            return this.context && "function" === typeof this.context.getCurrentTime ? this.context.getCurrentTime() : 0
        },
        setCurrentTime: function(b) {
            return this.context && "function" === typeof this.context.setCurrentTime ? this.context.setCurrentTime(b) : !1
        }
    }
};
if (void 0 === this.jukebox)
    throw "jukebox.Manager requires jukebox.Player (Player.js) to run properly.";
jukebox.Manager = function(b) {
    this.features = {};
    this.codecs = {};
    this.__players = {};
    this.__playersLength = 0;
    this.__clones = {};
    this.__queue = [];
    this.settings = {};
    for (var c in this.defaults)
        this.settings[c] = this.defaults[c];
    if ("[object Object]" === Object.prototype.toString.call(b))
        for (var e in b)
            this.settings[e] = b[e];
    this.__detectFeatures();
    jukebox.Manager.__initialized = !1 === this.settings.useGameLoop ? window.setInterval(function() {
        jukebox.Manager.loop()
    }, 20) : !0
}
;
jukebox.Manager.prototype = {
    defaults: {
        useFlash: !1,
        useGameLoop: !1
    },
    __detectFeatures: function() {
        var b = window.Audio && new Audio;
        if (b && b.canPlayType && !1 === this.settings.useFlash) {
            for (var c = [{
                e: "3gp",
                m: ["audio/3gpp", "audio/amr"]
            }, {
                e: "aac",
                m: ["audio/aac", "audio/aacp"]
            }, {
                e: "amr",
                m: ["audio/amr", "audio/3gpp"]
            }, {
                e: "caf",
                m: ["audio/IMA-ADPCM", "audio/x-adpcm", 'audio/x-aiff; codecs="IMA-ADPCM, ADPCM"']
            }, {
                e: "m4a",
                m: 'audio/mp4{audio/mp4; codecs="mp4a.40.2,avc1.42E01E"{audio/mpeg4{audio/mpeg4-generic{audio/mp4a-latm{audio/MP4A-LATM{audio/x-m4a'.split("{")
            }, {
                e: "mp3",
                m: ["audio/mp3", "audio/mpeg", 'audio/mpeg; codecs="mp3"', "audio/MPA", "audio/mpa-robust"]
            }, {
                e: "mpga",
                m: ["audio/MPA", "audio/mpa-robust", "audio/mpeg", "video/mpeg"]
            }, {
                e: "mp4",
                m: ["audio/mp4", "video/mp4"]
            }, {
                e: "ogg",
                m: ["application/ogg", "audio/ogg", 'audio/ogg; codecs="theora, vorbis"', "video/ogg", 'video/ogg; codecs="theora, vorbis"']
            }, {
                e: "wav",
                m: ["audio/wave", "audio/wav", 'audio/wav; codecs="1"', "audio/x-wav", "audio/x-pn-wav"]
            }, {
                e: "webm",
                m: ["audio/webm", 'audio/webm; codecs="vorbis"', "video/webm"]
            }], e, l, q = 0, x = c.length; q < x; q++)
                if (l = c[q].e,
                c[q].m.length && "object" === typeof c[q].m)
                    for (var C = 0, A = c[q].m.length; C < A; C++)
                        if (e = c[q].m[C],
                        "" !== b.canPlayType(e)) {
                            this.codecs[l] = e;
                            break
                        } else
                            this.codecs[l] || (this.codecs[l] = !1);
            this.features.html5audio = !!(this.codecs.mp3 || this.codecs.ogg || this.codecs.webm || this.codecs.wav);
            this.features.channels = 8;
            b.volume = .1337;
            this.features.volume = !!(1E-4 > Math.abs(b.volume - .1337));
            navigator.userAgent.match(/iPhone|iPod|iPad/i) && (this.features.channels = 1)
        }
        this.features.flashaudio = !!navigator.mimeTypes["application/x-shockwave-flash"] || !!navigator.plugins["Shockwave Flash"] || !1;
        if (window.ActiveXObject)
            try {
                new ActiveXObject("ShockwaveFlash.ShockwaveFlash.10"),
                this.features.flashaudio = !0
            } catch (E) {}
        !0 === this.settings.useFlash && (this.features.flashaudio = !0);
        !0 !== this.features.flashaudio || this.features.html5audio || (this.codecs.mp3 = "audio/mp3",
        this.codecs.mpga = "audio/mpeg",
        this.codecs.mp4 = "audio/mp4",
        this.codecs.m4a = "audio/mp4",
        this.codecs["3gp"] = "audio/3gpp",
        this.codecs.amr = "audio/amr",
        this.features.volume = !0,
        this.features.channels = 1)
    },
    __getPlayerById: function(b) {
        return this.__players && void 0 !== this.__players[b] ? this.__players[b] : null
    },
    __getClone: function(b, c) {
        for (var e in this.__clones) {
            var l = this.__clones[e];
            if (null === l.isPlaying && l.origin === b)
                return l
        }
        if ("[object Object]" === Object.prototype.toString.call(c)) {
            e = {};
            for (var q in c)
                e[q] = c[q];
            e.autoplay = !1;
            b = new jukebox.Player(e,b);
            b.isClone = !0;
            b.wasReady = !1;
            return this.__clones[b.id] = b
        }
        return null
    },
    loop: function() {
        if (0 !== this.__playersLength)
            if (this.__queue.length && this.__playersLength < this.features.channels) {
                var b = this.__queue[0]
                  , c = this.__getPlayerById(b.origin);
                if (null !== c) {
                    var e = this.__getClone(b.origin, c.settings);
                    null !== e && (!0 === this.features.volume && (c = this.__players[b.origin]) && e.setVolume(c.getVolume()),
                    this.add(e),
                    e.play(b.pointer, !0))
                }
                this.__queue.splice(0, 1)
            } else
                for (e in this.__queue.length && 1 === this.features.channels && (b = this.__queue[0],
                c = this.__getPlayerById(b.origin),
                null !== c && c.play(b.pointer, !0),
                this.__queue.splice(0, 1)),
                this.__players)
                    b = this.__players[e],
                    c = b.getCurrentTime() || 0,
                    b.isPlaying && !1 === b.wasReady ? b.wasReady = b.setCurrentTime(b.isPlaying.start) : b.isPlaying && !0 === b.wasReady ? c > b.isPlaying.end && (!0 === b.isPlaying.loop ? b.play(b.isPlaying.start, !0) : b.stop()) : b.isClone && null === b.isPlaying ? this.remove(b) : void 0 !== b.backgroundMusic && null === b.isPlaying && c > b.backgroundMusic.end && b.backgroundHackForiOS()
    },
    getPlayableResource: function(b) {
        "[object Array]" !== Object.prototype.toString.call(b) && (b = [b]);
        for (var c = 0, e = b.length; c < e; c++) {
            var l = b[c]
              , q = l.match(/\.([^\.]*)$/)[1];
            if (q && this.codecs[q])
                return l
        }
        return null
    },
    add: function(b) {
        return b instanceof jukebox.Player && void 0 === this.__players[b.id] ? (this.__playersLength++,
        this.__players[b.id] = b,
        !0) : !1
    },
    remove: function(b) {
        return b instanceof jukebox.Player && void 0 !== this.__players[b.id] ? (this.__playersLength--,
        delete this.__players[b.id],
        !0) : !1
    },
    addToQueue: function(b, c) {
        return "string" !== typeof b && "number" !== typeof b || void 0 === this.__players[c] ? !1 : (this.__queue.push({
            pointer: b,
            origin: c
        }),
        !0)
    }
};
(function() {
    var b = function() {
        this.init()
    };
    b.prototype = {
        init: function() {
            var g = this || c;
            g._counter = 1E3;
            g._html5AudioPool = [];
            g.html5PoolSize = 10;
            g._codecs = {};
            g._howls = [];
            g._muted = !1;
            g._volume = 1;
            g._canPlayEvent = "canplaythrough";
            g._navigator = "undefined" !== typeof window && window.navigator ? window.navigator : null;
            g.masterGain = null;
            g.noAudio = !1;
            g.usingWebAudio = !0;
            g.autoSuspend = !1;
            g.ctx = null;
            g.autoUnlock = !0;
            g._setup();
            return g
        },
        volume: function(g) {
            var r = this || c;
            g = parseFloat(g);
            r.ctx || N();
            if ("undefined" !== typeof g && 0 <= g && 1 >= g) {
                r._volume = g;
                if (r._muted)
                    return r;
                r.usingWebAudio && r.masterGain.gain.setValueAtTime(g, c.ctx.currentTime);
                for (var y = 0; y < r._howls.length; y++)
                    if (!r._howls[y]._webAudio)
                        for (var F = r._howls[y]._getSoundIds(), G = 0; G < F.length; G++) {
                            var M = r._howls[y]._soundById(F[G]);
                            M && M._node && (M._node.volume = M._volume * g)
                        }
                return r
            }
            return r._volume
        },
        mute: function(g) {
            var r = this || c;
            r.ctx || N();
            r._muted = g;
            r.usingWebAudio && r.masterGain.gain.setValueAtTime(g ? 0 : r._volume, c.ctx.currentTime);
            for (var y = 0; y < r._howls.length; y++)
                if (!r._howls[y]._webAudio)
                    for (var F = r._howls[y]._getSoundIds(), G = 0; G < F.length; G++) {
                        var M = r._howls[y]._soundById(F[G]);
                        M && M._node && (M._node.muted = g ? !0 : M._muted)
                    }
            return r
        },
        stop: function() {
            for (var g = this || c, r = 0; r < g._howls.length; r++)
                g._howls[r].stop();
            return g
        },
        unload: function() {
            for (var g = this || c, r = g._howls.length - 1; 0 <= r; r--)
                g._howls[r].unload();
            g.usingWebAudio && g.ctx && "undefined" !== typeof g.ctx.close && (g.ctx.close(),
            g.ctx = null,
            N());
            return g
        },
        codecs: function(g) {
            return (this || c)._codecs[g.replace(/^x-/, "")]
        },
        _setup: function() {
            var g = this || c;
            g.state = g.ctx ? g.ctx.state || "suspended" : "suspended";
            g._autoSuspend();
            if (!g.usingWebAudio)
                if ("undefined" !== typeof Audio)
                    try {
                        var r = new Audio;
                        "undefined" === typeof r.oncanplaythrough && (g._canPlayEvent = "canplay")
                    } catch (y) {
                        g.noAudio = !0
                    }
                else
                    g.noAudio = !0;
            try {
                r = new Audio,
                r.muted && (g.noAudio = !0)
            } catch (y) {}
            g.noAudio || g._setupCodecs();
            return g
        },
        _setupCodecs: function() {
            var g = this || c
              , r = null;
            try {
                r = "undefined" !== typeof Audio ? new Audio : null
            } catch (G) {
                return g
            }
            if (!r || "function" !== typeof r.canPlayType)
                return g;
            var y = r.canPlayType("audio/mpeg;").replace(/^no$/, "")
              , F = g._navigator && g._navigator.userAgent.match(/OPR\/([0-6].)/g);
            F = F && 33 > parseInt(F[0].split("/")[1], 10);
            g._codecs = {
                mp3: !(F || !y && !r.canPlayType("audio/mp3;").replace(/^no$/, "")),
                mpeg: !!y,
                opus: !!r.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ""),
                ogg: !!r.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
                oga: !!r.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
                wav: !!r.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
                aac: !!r.canPlayType("audio/aac;").replace(/^no$/, ""),
                caf: !!r.canPlayType("audio/x-caf;").replace(/^no$/, ""),
                m4a: !!(r.canPlayType("audio/x-m4a;") || r.canPlayType("audio/m4a;") || r.canPlayType("audio/aac;")).replace(/^no$/, ""),
                m4b: !!(r.canPlayType("audio/x-m4b;") || r.canPlayType("audio/m4b;") || r.canPlayType("audio/aac;")).replace(/^no$/, ""),
                mp4: !!(r.canPlayType("audio/x-mp4;") || r.canPlayType("audio/mp4;") || r.canPlayType("audio/aac;")).replace(/^no$/, ""),
                weba: !!r.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, ""),
                webm: !!r.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, ""),
                dolby: !!r.canPlayType('audio/mp4; codecs="ec-3"').replace(/^no$/, ""),
                flac: !!(r.canPlayType("audio/x-flac;") || r.canPlayType("audio/flac;")).replace(/^no$/, "")
            };
            return g
        },
        _unlockAudio: function() {
            var g = this || c;
            if (!g._audioUnlocked && g.ctx) {
                g._audioUnlocked = !1;
                g.autoUnlock = !1;
                g._mobileUnloaded || 44100 === g.ctx.sampleRate || (g._mobileUnloaded = !0,
                g.unload());
                g._scratchBuffer = g.ctx.createBuffer(1, 1, 22050);
                var r = function(y) {
                    for (; g._html5AudioPool.length < g.html5PoolSize; )
                        try {
                            var F = new Audio;
                            F._unlocked = !0;
                            g._releaseHtml5Audio(F)
                        } catch (V) {
                            g.noAudio = !0;
                            break
                        }
                    for (y = 0; y < g._howls.length; y++)
                        if (!g._howls[y]._webAudio) {
                            F = g._howls[y]._getSoundIds();
                            for (var G = 0; G < F.length; G++) {
                                var M = g._howls[y]._soundById(F[G]);
                                M && M._node && !M._node._unlocked && (M._node._unlocked = !0,
                                M._node.load())
                            }
                        }
                    g._autoResume();
                    var O = g.ctx.createBufferSource();
                    O.buffer = g._scratchBuffer;
                    O.connect(g.ctx.destination);
                    "undefined" === typeof O.start ? O.noteOn(0) : O.start(0);
                    "function" === typeof g.ctx.resume && g.ctx.resume();
                    O.onended = function() {
                        O.disconnect(0);
                        g._audioUnlocked = !0;
                        document.removeEventListener("touchstart", r, !0);
                        document.removeEventListener("touchend", r, !0);
                        document.removeEventListener("click", r, !0);
                        for (var V = 0; V < g._howls.length; V++)
                            g._howls[V]._emit("unlock")
                    }
                };
                document.addEventListener("touchstart", r, !0);
                document.addEventListener("touchend", r, !0);
                document.addEventListener("click", r, !0);
                return g
            }
        },
        _obtainHtml5Audio: function() {
            var g = this || c;
            if (g._html5AudioPool.length)
                return g._html5AudioPool.pop();
            (g = (new Audio).play()) && "undefined" !== typeof Promise && (g instanceof Promise || "function" === typeof g.then) && g.catch(function() {
                console.warn("HTML5 Audio pool exhausted, returning potentially locked audio object.")
            });
            return new Audio
        },
        _releaseHtml5Audio: function(g) {
            var r = this || c;
            g._unlocked && r._html5AudioPool.push(g);
            return r
        },
        _autoSuspend: function() {
            var g = this;
            if (g.autoSuspend && g.ctx && "undefined" !== typeof g.ctx.suspend && c.usingWebAudio) {
                for (var r = 0; r < g._howls.length; r++)
                    if (g._howls[r]._webAudio)
                        for (var y = 0; y < g._howls[r]._sounds.length; y++)
                            if (!g._howls[r]._sounds[y]._paused)
                                return g;
                g._suspendTimer && clearTimeout(g._suspendTimer);
                g._suspendTimer = setTimeout(function() {
                    if (g.autoSuspend) {
                        g._suspendTimer = null;
                        g.state = "suspending";
                        var F = function() {
                            g.state = "suspended";
                            g._resumeAfterSuspend && (delete g._resumeAfterSuspend,
                            g._autoResume())
                        };
                        g.ctx.suspend().then(F, F)
                    }
                }, 3E4);
                return g
            }
        },
        _autoResume: function() {
            var g = this;
            if (g.ctx && "undefined" !== typeof g.ctx.resume && c.usingWebAudio)
                return "running" === g.state && "interrupted" !== g.ctx.state && g._suspendTimer ? (clearTimeout(g._suspendTimer),
                g._suspendTimer = null) : "suspended" === g.state || "running" === g.state && "interrupted" === g.ctx.state ? (g.ctx.resume().then(function() {
                    g.state = "running";
                    for (var r = 0; r < g._howls.length; r++)
                        g._howls[r]._emit("resume")
                }),
                g._suspendTimer && (clearTimeout(g._suspendTimer),
                g._suspendTimer = null)) : "suspending" === g.state && (g._resumeAfterSuspend = !0),
                g
        }
    };
    var c = new b
      , e = function(g) {
        g.src && 0 !== g.src.length ? this.init(g) : console.error("An array of source files must be passed with any new Howl.")
    };
    e.prototype = {
        init: function(g) {
            var r = this;
            c.ctx || N();
            r._autoplay = g.autoplay || !1;
            r._format = "string" !== typeof g.format ? g.format : [g.format];
            r._html5 = g.html5 || !1;
            r._muted = g.mute || !1;
            r._loop = g.loop || !1;
            r._pool = g.pool || 5;
            r._preload = "boolean" === typeof g.preload || "metadata" === g.preload ? g.preload : !0;
            r._rate = g.rate || 1;
            r._sprite = g.sprite || {};
            r._src = "string" !== typeof g.src ? g.src : [g.src];
            r._volume = void 0 !== g.volume ? g.volume : 1;
            r._xhr = {
                method: g.xhr && g.xhr.method ? g.xhr.method : "GET",
                headers: g.xhr && g.xhr.headers ? g.xhr.headers : null,
                withCredentials: g.xhr && g.xhr.withCredentials ? g.xhr.withCredentials : !1
            };
            r._duration = 0;
            r._state = "unloaded";
            r._sounds = [];
            r._endTimers = {};
            r._queue = [];
            r._playLock = !1;
            r._onend = g.onend ? [{
                fn: g.onend
            }] : [];
            r._onfade = g.onfade ? [{
                fn: g.onfade
            }] : [];
            r._onload = g.onload ? [{
                fn: g.onload
            }] : [];
            r._onloaderror = g.onloaderror ? [{
                fn: g.onloaderror
            }] : [];
            r._onplayerror = g.onplayerror ? [{
                fn: g.onplayerror
            }] : [];
            r._onpause = g.onpause ? [{
                fn: g.onpause
            }] : [];
            r._onplay = g.onplay ? [{
                fn: g.onplay
            }] : [];
            r._onstop = g.onstop ? [{
                fn: g.onstop
            }] : [];
            r._onmute = g.onmute ? [{
                fn: g.onmute
            }] : [];
            r._onvolume = g.onvolume ? [{
                fn: g.onvolume
            }] : [];
            r._onrate = g.onrate ? [{
                fn: g.onrate
            }] : [];
            r._onseek = g.onseek ? [{
                fn: g.onseek
            }] : [];
            r._onunlock = g.onunlock ? [{
                fn: g.onunlock
            }] : [];
            r._onresume = [];
            r._webAudio = c.usingWebAudio && !r._html5;
            "undefined" !== typeof c.ctx && c.ctx && c.autoUnlock && c._unlockAudio();
            c._howls.push(r);
            r._autoplay && r._queue.push({
                event: "play",
                action: function() {
                    r.play()
                }
            });
            r._preload && "none" !== r._preload && r.load();
            return r
        },
        load: function() {
            var g = null;
            if (c.noAudio)
                this._emit("loaderror", null, "No audio support.");
            else {
                "string" === typeof this._src && (this._src = [this._src]);
                for (var r = 0; r < this._src.length; r++) {
                    if (this._format && this._format[r])
                        var y = this._format[r];
                    else {
                        var F = this._src[r];
                        if ("string" !== typeof F) {
                            this._emit("loaderror", null, "Non-string found in selected audio sources - ignoring.");
                            continue
                        }
                        (y = /^data:audio\/([^;,]+);/i.exec(F)) || (y = /\.([^.]+)$/.exec(F.split("?", 1)[0]));
                        y &&= y[1].toLowerCase()
                    }
                    y || console.warn('No file extension was found. Consider using the "format" property or specify an extension.');
                    if (y && c.codecs(y)) {
                        g = this._src[r];
                        break
                    }
                }
                if (g)
                    return this._src = g,
                    this._state = "loading",
                    "https:" === window.location.protocol && "http:" === g.slice(0, 5) && (this._html5 = !0,
                    this._webAudio = !1),
                    new l(this),
                    this._webAudio && x(this),
                    this;
                this._emit("loaderror", null, "No codec support for selected audio sources.")
            }
        },
        play: function(g, r) {
            var y = this
              , F = null;
            if ("number" === typeof g)
                F = g,
                g = null;
            else {
                if ("string" === typeof g && "loaded" === y._state && !y._sprite[g])
                    return null;
                if ("undefined" === typeof g && (g = "__default",
                !y._playLock)) {
                    for (var G = 0, M = 0; M < y._sounds.length; M++)
                        y._sounds[M]._paused && !y._sounds[M]._ended && (G++,
                        F = y._sounds[M]._id);
                    1 === G ? g = null : F = null
                }
            }
            var O = F ? y._soundById(F) : y._inactiveSound();
            if (!O)
                return null;
            F && !g && (g = O._sprite || "__default");
            if ("loaded" !== y._state) {
                O._sprite = g;
                O._ended = !1;
                var V = O._id;
                y._queue.push({
                    event: "play",
                    action: function() {
                        y.play(V)
                    }
                });
                return V
            }
            if (F && !O._paused)
                return r || y._loadQueue("play"),
                O._id;
            y._webAudio && c._autoResume();
            var fa = Math.max(0, 0 < O._seek ? O._seek : y._sprite[g][0] / 1E3)
              , va = Math.max(0, (y._sprite[g][0] + y._sprite[g][1]) / 1E3 - fa)
              , Fa = 1E3 * va / Math.abs(O._rate)
              , qb = y._sprite[g][0] / 1E3
              , ib = (y._sprite[g][0] + y._sprite[g][1]) / 1E3;
            O._sprite = g;
            O._ended = !1;
            var nb = function() {
                O._paused = !1;
                O._seek = fa;
                O._start = qb;
                O._stop = ib;
                O._loop = !(!O._loop && !y._sprite[g][2])
            };
            if (fa >= ib)
                y._ended(O);
            else {
                var pa = O._node;
                if (y._webAudio)
                    F = function() {
                        y._playLock = !1;
                        nb();
                        y._refreshBuffer(O);
                        pa.gain.setValueAtTime(O._muted || y._muted ? 0 : O._volume, c.ctx.currentTime);
                        O._playStart = c.ctx.currentTime;
                        "undefined" === typeof pa.bufferSource.start ? O._loop ? pa.bufferSource.noteGrainOn(0, fa, 86400) : pa.bufferSource.noteGrainOn(0, fa, va) : O._loop ? pa.bufferSource.start(0, fa, 86400) : pa.bufferSource.start(0, fa, va);
                        Infinity !== Fa && (y._endTimers[O._id] = setTimeout(y._ended.bind(y, O), Fa));
                        r || setTimeout(function() {
                            y._emit("play", O._id);
                            y._loadQueue()
                        }, 0)
                    }
                    ,
                    "running" === c.state && "interrupted" !== c.ctx.state ? F() : (y._playLock = !0,
                    y.once("resume", F),
                    y._clearTimer(O._id));
                else {
                    var Wa = function() {
                        pa.currentTime = fa;
                        pa.muted = O._muted || y._muted || c._muted || pa.muted;
                        pa.volume = O._volume * c.volume();
                        pa.playbackRate = O._rate;
                        try {
                            var Ca = pa.play();
                            Ca && "undefined" !== typeof Promise && (Ca instanceof Promise || "function" === typeof Ca.then) ? (y._playLock = !0,
                            nb(),
                            Ca.then(function() {
                                y._playLock = !1;
                                pa._unlocked = !0;
                                r || (y._emit("play", O._id),
                                y._loadQueue())
                            }).catch(function() {
                                y._playLock = !1;
                                y._emit("playerror", O._id, "Playback was unable to start. This is most commonly an issue on mobile devices and Chrome where playback was not within a user interaction.");
                                O._ended = !0;
                                O._paused = !0
                            })) : r || (y._playLock = !1,
                            nb(),
                            y._emit("play", O._id),
                            y._loadQueue());
                            pa.playbackRate = O._rate;
                            pa.paused ? y._emit("playerror", O._id, "Playback was unable to start. This is most commonly an issue on mobile devices and Chrome where playback was not within a user interaction.") : "__default" !== g || O._loop ? y._endTimers[O._id] = setTimeout(y._ended.bind(y, O), Fa) : (y._endTimers[O._id] = function() {
                                y._ended(O);
                                pa.removeEventListener("ended", y._endTimers[O._id], !1)
                            }
                            ,
                            pa.addEventListener("ended", y._endTimers[O._id], !1))
                        } catch (Ta) {
                            y._emit("playerror", O._id, Ta)
                        }
                    };
                    "data:audio/wav;base64,UklGRigAAABXQVZFZm10IBIAAAABAAEARKwAAIhYAQACABAAAABkYXRhAgAAAAEA" === pa.src && (pa.src = y._src,
                    pa.load());
                    F = window && window.ejecta || !pa.readyState && c._navigator.isCocoonJS;
                    if (3 <= pa.readyState || F)
                        Wa();
                    else {
                        y._playLock = !0;
                        var Sa = function() {
                            Wa();
                            pa.removeEventListener(c._canPlayEvent, Sa, !1)
                        };
                        pa.addEventListener(c._canPlayEvent, Sa, !1);
                        y._clearTimer(O._id)
                    }
                }
                return O._id
            }
        },
        pause: function(g, r) {
            var y = this;
            if ("loaded" !== y._state || y._playLock)
                return y._queue.push({
                    event: "pause",
                    action: function() {
                        y.pause(g)
                    }
                }),
                y;
            for (var F = y._getSoundIds(g), G = 0; G < F.length; G++) {
                y._clearTimer(F[G]);
                var M = y._soundById(F[G]);
                if (M && !M._paused && (M._seek = y.seek(F[G]),
                M._rateSeek = 0,
                M._paused = !0,
                y._stopFade(F[G]),
                M._node))
                    if (y._webAudio) {
                        if (!M._node.bufferSource)
                            continue;
                        "undefined" === typeof M._node.bufferSource.stop ? M._node.bufferSource.noteOff(0) : M._node.bufferSource.stop(0);
                        y._cleanBuffer(M._node)
                    } else
                        isNaN(M._node.duration) && Infinity !== M._node.duration || M._node.pause();
                r || y._emit("pause", M ? M._id : null)
            }
            return y
        },
        stop: function(g, r) {
            var y = this;
            if ("loaded" !== y._state || y._playLock)
                return y._queue.push({
                    event: "stop",
                    action: function() {
                        y.stop(g)
                    }
                }),
                y;
            for (var F = y._getSoundIds(g), G = 0; G < F.length; G++) {
                y._clearTimer(F[G]);
                var M = y._soundById(F[G]);
                M && (M._seek = M._start || 0,
                M._rateSeek = 0,
                M._paused = !0,
                M._ended = !0,
                y._stopFade(F[G]),
                M._node && (y._webAudio ? M._node.bufferSource && ("undefined" === typeof M._node.bufferSource.stop ? M._node.bufferSource.noteOff(0) : M._node.bufferSource.stop(0),
                y._cleanBuffer(M._node)) : isNaN(M._node.duration) && Infinity !== M._node.duration || (M._node.currentTime = M._start || 0,
                M._node.pause(),
                Infinity === M._node.duration && y._clearSound(M._node))),
                r || y._emit("stop", M._id))
            }
            return y
        },
        mute: function(g, r) {
            var y = this;
            if ("loaded" !== y._state || y._playLock)
                return y._queue.push({
                    event: "mute",
                    action: function() {
                        y.mute(g, r)
                    }
                }),
                y;
            if ("undefined" === typeof r)
                if ("boolean" === typeof g)
                    y._muted = g;
                else
                    return y._muted;
            for (var F = y._getSoundIds(r), G = 0; G < F.length; G++) {
                var M = y._soundById(F[G]);
                M && (M._muted = g,
                M._interval && y._stopFade(M._id),
                y._webAudio && M._node ? M._node.gain.setValueAtTime(g ? 0 : M._volume, c.ctx.currentTime) : M._node && (M._node.muted = c._muted ? !0 : g),
                y._emit("mute", M._id))
            }
            return y
        },
        volume: function() {
            var g = this, r = arguments, y, F;
            if (0 === r.length)
                return g._volume;
            1 === r.length || 2 === r.length && "undefined" === typeof r[1] ? 0 <= g._getSoundIds().indexOf(r[0]) ? F = parseInt(r[0], 10) : y = parseFloat(r[0]) : 2 <= r.length && (y = parseFloat(r[0]),
            F = parseInt(r[1], 10));
            var G;
            if ("undefined" !== typeof y && 0 <= y && 1 >= y) {
                if ("loaded" !== g._state || g._playLock)
                    return g._queue.push({
                        event: "volume",
                        action: function() {
                            g.volume.apply(g, r)
                        }
                    }),
                    g;
                "undefined" === typeof F && (g._volume = y);
                F = g._getSoundIds(F);
                for (var M = 0; M < F.length; M++)
                    if (G = g._soundById(F[M]))
                        G._volume = y,
                        r[2] || g._stopFade(F[M]),
                        g._webAudio && G._node && !G._muted ? G._node.gain.setValueAtTime(y, c.ctx.currentTime) : G._node && !G._muted && (G._node.volume = y * c.volume()),
                        g._emit("volume", G._id)
            } else
                return (G = F ? g._soundById(F) : g._sounds[0]) ? G._volume : 0;
            return g
        },
        fade: function(g, r, y, F) {
            var G = this;
            if ("loaded" !== G._state || G._playLock)
                return G._queue.push({
                    event: "fade",
                    action: function() {
                        G.fade(g, r, y, F)
                    }
                }),
                G;
            g = Math.min(Math.max(0, parseFloat(g)), 1);
            r = Math.min(Math.max(0, parseFloat(r)), 1);
            y = parseFloat(y);
            G.volume(g, F);
            for (var M = G._getSoundIds(F), O = 0; O < M.length; O++) {
                var V = G._soundById(M[O]);
                if (V) {
                    F || G._stopFade(M[O]);
                    if (G._webAudio && !V._muted) {
                        var fa = c.ctx.currentTime
                          , va = fa + y / 1E3;
                        V._volume = g;
                        V._node.gain.setValueAtTime(g, fa);
                        V._node.gain.linearRampToValueAtTime(r, va)
                    }
                    G._startFadeInterval(V, g, r, y, M[O], "undefined" === typeof F)
                }
            }
            return G
        },
        _startFadeInterval: function(g, r, y, F, G, M) {
            var O = this
              , V = r
              , fa = y - r;
            G = Math.abs(fa / .01);
            G = Math.max(4, 0 < G ? F / G : F);
            var va = Date.now();
            g._fadeTo = y;
            g._interval = setInterval(function() {
                var Fa = (Date.now() - va) / F;
                va = Date.now();
                V += fa * Fa;
                V = 0 > fa ? Math.max(y, V) : Math.min(y, V);
                V = Math.round(100 * V) / 100;
                O._webAudio ? g._volume = V : O.volume(V, g._id, !0);
                M && (O._volume = V);
                if (y < r && V <= y || y > r && V >= y)
                    clearInterval(g._interval),
                    g._interval = null,
                    g._fadeTo = null,
                    O.volume(y, g._id),
                    O._emit("fade", g._id)
            }, G)
        },
        _stopFade: function(g) {
            var r = this._soundById(g);
            r && r._interval && (this._webAudio && r._node.gain.cancelScheduledValues(c.ctx.currentTime),
            clearInterval(r._interval),
            r._interval = null,
            this.volume(r._fadeTo, g),
            r._fadeTo = null,
            this._emit("fade", g));
            return this
        },
        loop: function() {
            var g = arguments, r;
            if (0 === g.length)
                return this._loop;
            if (1 === g.length)
                if ("boolean" === typeof g[0])
                    this._loop = r = g[0];
                else
                    return (g = this._soundById(parseInt(g[0], 10))) ? g._loop : !1;
            else if (2 === g.length) {
                r = g[0];
                var y = parseInt(g[1], 10)
            }
            y = this._getSoundIds(y);
            for (var F = 0; F < y.length; F++)
                if (g = this._soundById(y[F]))
                    if (g._loop = r,
                    this._webAudio && g._node && g._node.bufferSource && (g._node.bufferSource.loop = r))
                        g._node.bufferSource.loopStart = g._start || 0,
                        g._node.bufferSource.loopEnd = g._stop;
            return this
        },
        rate: function() {
            var g = this, r = arguments, y;
            if (0 === r.length)
                var F = g._sounds[0]._id;
            else
                1 === r.length ? 0 <= g._getSoundIds().indexOf(r[0]) ? F = parseInt(r[0], 10) : y = parseFloat(r[0]) : 2 === r.length && (y = parseFloat(r[0]),
                F = parseInt(r[1], 10));
            var G;
            if ("number" === typeof y) {
                if ("loaded" !== g._state || g._playLock)
                    return g._queue.push({
                        event: "rate",
                        action: function() {
                            g.rate.apply(g, r)
                        }
                    }),
                    g;
                "undefined" === typeof F && (g._rate = y);
                F = g._getSoundIds(F);
                for (var M = 0; M < F.length; M++)
                    if (G = g._soundById(F[M])) {
                        g.playing(F[M]) && (G._rateSeek = g.seek(F[M]),
                        G._playStart = g._webAudio ? c.ctx.currentTime : G._playStart);
                        G._rate = y;
                        g._webAudio && G._node && G._node.bufferSource ? G._node.bufferSource.playbackRate.setValueAtTime(y, c.ctx.currentTime) : G._node && (G._node.playbackRate = y);
                        var O = g.seek(F[M]);
                        O = 1E3 * ((g._sprite[G._sprite][0] + g._sprite[G._sprite][1]) / 1E3 - O) / Math.abs(G._rate);
                        if (g._endTimers[F[M]] || !G._paused)
                            g._clearTimer(F[M]),
                            g._endTimers[F[M]] = setTimeout(g._ended.bind(g, G), O);
                        g._emit("rate", G._id)
                    }
            } else
                return (G = g._soundById(F)) ? G._rate : g._rate;
            return g
        },
        seek: function() {
            var g = this
              , r = arguments;
            if (0 === r.length)
                var y = g._sounds[0]._id;
            else if (1 === r.length)
                if (0 <= g._getSoundIds().indexOf(r[0]))
                    y = parseInt(r[0], 10);
                else {
                    if (g._sounds.length) {
                        y = g._sounds[0]._id;
                        var F = parseFloat(r[0])
                    }
                }
            else
                2 === r.length && (F = parseFloat(r[0]),
                y = parseInt(r[1], 10));
            if ("undefined" === typeof y)
                return g;
            if ("loaded" !== g._state || g._playLock)
                return g._queue.push({
                    event: "seek",
                    action: function() {
                        g.seek.apply(g, r)
                    }
                }),
                g;
            var G = g._soundById(y);
            if (G)
                if ("number" === typeof F && 0 <= F) {
                    var M = g.playing(y);
                    M && g.pause(y, !0);
                    G._seek = F;
                    G._ended = !1;
                    g._clearTimer(y);
                    g._webAudio || !G._node || isNaN(G._node.duration) || (G._node.currentTime = F);
                    var O = function() {
                        g._emit("seek", y);
                        M && g.play(y, !0)
                    };
                    if (M && !g._webAudio) {
                        var V = function() {
                            g._playLock ? setTimeout(V, 0) : O()
                        };
                        setTimeout(V, 0)
                    } else
                        O()
                } else
                    return g._webAudio ? (F = g.playing(y) ? c.ctx.currentTime - G._playStart : 0,
                    G._seek + ((G._rateSeek ? G._rateSeek - G._seek : 0) + F * Math.abs(G._rate))) : G._node.currentTime;
            return g
        },
        playing: function(g) {
            if ("number" === typeof g)
                return (g = this._soundById(g)) ? !g._paused : !1;
            for (g = 0; g < this._sounds.length; g++)
                if (!this._sounds[g]._paused)
                    return !0;
            return !1
        },
        duration: function(g) {
            var r = this._duration;
            (g = this._soundById(g)) && (r = this._sprite[g._sprite][1] / 1E3);
            return r
        },
        state: function() {
            return this._state
        },
        unload: function() {
            for (var g = this._sounds, r = 0; r < g.length; r++)
                g[r]._paused || this.stop(g[r]._id),
                this._webAudio || (this._clearSound(g[r]._node),
                g[r]._node.removeEventListener("error", g[r]._errorFn, !1),
                g[r]._node.removeEventListener(c._canPlayEvent, g[r]._loadFn, !1),
                c._releaseHtml5Audio(g[r]._node)),
                delete g[r]._node,
                this._clearTimer(g[r]._id);
            r = c._howls.indexOf(this);
            0 <= r && c._howls.splice(r, 1);
            g = !0;
            for (r = 0; r < c._howls.length; r++)
                if (c._howls[r]._src === this._src || 0 <= this._src.indexOf(c._howls[r]._src)) {
                    g = !1;
                    break
                }
            q && g && delete q[this._src];
            c.noAudio = !1;
            this._state = "unloaded";
            this._sounds = [];
            return null
        },
        on: function(g, r, y, F) {
            g = this["_on" + g];
            "function" === typeof r && g.push(F ? {
                id: y,
                fn: r,
                once: F
            } : {
                id: y,
                fn: r
            });
            return this
        },
        off: function(g, r, y) {
            var F = this["_on" + g];
            "number" === typeof r && (y = r,
            r = null);
            if (r || y)
                for (g = 0; g < F.length; g++) {
                    var G = y === F[g].id;
                    if (r === F[g].fn && G || !r && G) {
                        F.splice(g, 1);
                        break
                    }
                }
            else if (g)
                this["_on" + g] = [];
            else
                for (r = Object.keys(this),
                g = 0; g < r.length; g++)
                    0 === r[g].indexOf("_on") && Array.isArray(this[r[g]]) && (this[r[g]] = []);
            return this
        },
        once: function(g, r, y) {
            this.on(g, r, y, 1);
            return this
        },
        _emit: function(g, r, y) {
            for (var F = this["_on" + g], G = F.length - 1; 0 <= G; G--)
                F[G].id && F[G].id !== r && "load" !== g || (setTimeout(function(M) {
                    M.call(this, r, y)
                }
                .bind(this, F[G].fn), 0),
                F[G].once && this.off(g, F[G].fn, F[G].id));
            this._loadQueue(g);
            return this
        },
        _loadQueue: function(g) {
            if (0 < this._queue.length) {
                var r = this._queue[0];
                r.event === g && (this._queue.shift(),
                this._loadQueue());
                g || r.action()
            }
            return this
        },
        _ended: function(g) {
            var r = g._sprite;
            if (!this._webAudio && g._node && !g._node.paused && !g._node.ended && g._node.currentTime < g._stop)
                return setTimeout(this._ended.bind(this, g), 100),
                this;
            r = !(!g._loop && !this._sprite[r][2]);
            this._emit("end", g._id);
            !this._webAudio && r && this.stop(g._id, !0).play(g._id);
            if (this._webAudio && r) {
                this._emit("play", g._id);
                g._seek = g._start || 0;
                g._rateSeek = 0;
                g._playStart = c.ctx.currentTime;
                var y = 1E3 * (g._stop - g._start) / Math.abs(g._rate);
                this._endTimers[g._id] = setTimeout(this._ended.bind(this, g), y)
            }
            this._webAudio && !r && (g._paused = !0,
            g._ended = !0,
            g._seek = g._start || 0,
            g._rateSeek = 0,
            this._clearTimer(g._id),
            this._cleanBuffer(g._node),
            c._autoSuspend());
            this._webAudio || r || this.stop(g._id, !0);
            return this
        },
        _clearTimer: function(g) {
            if (this._endTimers[g]) {
                if ("function" !== typeof this._endTimers[g])
                    clearTimeout(this._endTimers[g]);
                else {
                    var r = this._soundById(g);
                    r && r._node && r._node.removeEventListener("ended", this._endTimers[g], !1)
                }
                delete this._endTimers[g]
            }
            return this
        },
        _soundById: function(g) {
            for (var r = 0; r < this._sounds.length; r++)
                if (g === this._sounds[r]._id)
                    return this._sounds[r];
            return null
        },
        _inactiveSound: function() {
            this._drain();
            for (var g = 0; g < this._sounds.length; g++)
                if (this._sounds[g]._ended)
                    return this._sounds[g].reset();
            return new l(this)
        },
        _drain: function() {
            var g = this._pool, r = 0, y;
            if (!(this._sounds.length < g)) {
                for (y = 0; y < this._sounds.length; y++)
                    this._sounds[y]._ended && r++;
                for (y = this._sounds.length - 1; 0 <= y && !(r <= g); y--)
                    this._sounds[y]._ended && (this._webAudio && this._sounds[y]._node && this._sounds[y]._node.disconnect(0),
                    this._sounds.splice(y, 1),
                    r--)
            }
        },
        _getSoundIds: function(g) {
            if ("undefined" === typeof g) {
                g = [];
                for (var r = 0; r < this._sounds.length; r++)
                    g.push(this._sounds[r]._id);
                return g
            }
            return [g]
        },
        _refreshBuffer: function(g) {
            g._node.bufferSource = c.ctx.createBufferSource();
            g._node.bufferSource.buffer = q[this._src];
            g._panner ? g._node.bufferSource.connect(g._panner) : g._node.bufferSource.connect(g._node);
            if (g._node.bufferSource.loop = g._loop)
                g._node.bufferSource.loopStart = g._start || 0,
                g._node.bufferSource.loopEnd = g._stop || 0;
            g._node.bufferSource.playbackRate.setValueAtTime(g._rate, c.ctx.currentTime);
            return this
        },
        _cleanBuffer: function(g) {
            var r = c._navigator && 0 <= c._navigator.vendor.indexOf("Apple");
            if (c._scratchBuffer && g.bufferSource && (g.bufferSource.onended = null,
            g.bufferSource.disconnect(0),
            r))
                try {
                    g.bufferSource.buffer = c._scratchBuffer
                } catch (y) {}
            g.bufferSource = null;
            return this
        },
        _clearSound: function(g) {
            /MSIE |Trident\//.test(c._navigator && c._navigator.userAgent) || (g.src = "data:audio/wav;base64,UklGRigAAABXQVZFZm10IBIAAAABAAEARKwAAIhYAQACABAAAABkYXRhAgAAAAEA")
        }
    };
    var l = function(g) {
        this._parent = g;
        this.init()
    };
    l.prototype = {
        init: function() {
            var g = this._parent;
            this._muted = g._muted;
            this._loop = g._loop;
            this._volume = g._volume;
            this._rate = g._rate;
            this._seek = 0;
            this._ended = this._paused = !0;
            this._sprite = "__default";
            this._id = ++c._counter;
            g._sounds.push(this);
            this.create();
            return this
        },
        create: function() {
            var g = this._parent
              , r = c._muted || this._muted || this._parent._muted ? 0 : this._volume;
            g._webAudio ? (this._node = "undefined" === typeof c.ctx.createGain ? c.ctx.createGainNode() : c.ctx.createGain(),
            this._node.gain.setValueAtTime(r, c.ctx.currentTime),
            this._node.paused = !0,
            this._node.connect(c.masterGain)) : c.noAudio || (this._node = c._obtainHtml5Audio(),
            this._errorFn = this._errorListener.bind(this),
            this._node.addEventListener("error", this._errorFn, !1),
            this._loadFn = this._loadListener.bind(this),
            this._node.addEventListener(c._canPlayEvent, this._loadFn, !1),
            this._node.src = g._src,
            this._node.preload = !0 === g._preload ? "auto" : g._preload,
            this._node.volume = r * c.volume(),
            this._node.load());
            return this
        },
        reset: function() {
            var g = this._parent;
            this._muted = g._muted;
            this._loop = g._loop;
            this._volume = g._volume;
            this._rate = g._rate;
            this._rateSeek = this._seek = 0;
            this._ended = this._paused = !0;
            this._sprite = "__default";
            this._id = ++c._counter;
            return this
        },
        _errorListener: function() {
            this._parent._emit("loaderror", this._id, this._node.error ? this._node.error.code : 0);
            this._node.removeEventListener("error", this._errorFn, !1)
        },
        _loadListener: function() {
            var g = this._parent;
            g._duration = Math.ceil(10 * this._node.duration) / 10;
            0 === Object.keys(g._sprite).length && (g._sprite = {
                __default: [0, 1E3 * g._duration]
            });
            "loaded" !== g._state && (g._state = "loaded",
            g._emit("load"),
            g._loadQueue());
            this._node.removeEventListener(c._canPlayEvent, this._loadFn, !1)
        }
    };
    var q = {}
      , x = function(g) {
        var r = g._src;
        if (q[r])
            g._duration = q[r].duration,
            E(g);
        else if (/^data:[^;]+;base64,/.test(r)) {
            for (var y = atob(r.split(",")[1]), F = new Uint8Array(y.length), G = 0; G < y.length; ++G)
                F[G] = y.charCodeAt(G);
            A(F.buffer, g)
        } else {
            var M = new XMLHttpRequest;
            M.open(g._xhr.method, r, !0);
            M.withCredentials = g._xhr.withCredentials;
            M.responseType = "arraybuffer";
            g._xhr.headers && Object.keys(g._xhr.headers).forEach(function(O) {
                M.setRequestHeader(O, g._xhr.headers[O])
            });
            M.onload = function() {
                var O = (M.status + "")[0];
                "0" !== O && "2" !== O && "3" !== O ? g._emit("loaderror", null, "Failed loading audio file with status: " + M.status + ".") : A(M.response, g)
            }
            ;
            M.onerror = function() {
                g._webAudio && (g._html5 = !0,
                g._webAudio = !1,
                g._sounds = [],
                delete q[r],
                g.load())
            }
            ;
            C(M)
        }
    }
      , C = function(g) {
        try {
            g.send()
        } catch (r) {
            g.onerror()
        }
    }
      , A = function(g, r) {
        var y = function() {
            r._emit("loaderror", null, "Decoding audio data failed.")
        }
          , F = function(G) {
            G && 0 < r._sounds.length ? (q[r._src] = G,
            E(r, G)) : y()
        };
        "undefined" !== typeof Promise && 1 === c.ctx.decodeAudioData.length ? c.ctx.decodeAudioData(g).then(F).catch(y) : c.ctx.decodeAudioData(g, F, y)
    }
      , E = function(g, r) {
        r && !g._duration && (g._duration = r.duration);
        0 === Object.keys(g._sprite).length && (g._sprite = {
            __default: [0, 1E3 * g._duration]
        });
        "loaded" !== g._state && (g._state = "loaded",
        g._emit("load"),
        g._loadQueue())
    }
      , N = function() {
        if (c.usingWebAudio) {
            try {
                "undefined" !== typeof AudioContext ? c.ctx = new AudioContext : "undefined" !== typeof webkitAudioContext ? c.ctx = new webkitAudioContext : c.usingWebAudio = !1
            } catch (y) {
                c.usingWebAudio = !1
            }
            c.ctx || (c.usingWebAudio = !1);
            var g = /iP(hone|od|ad)/.test(c._navigator && c._navigator.platform)
              , r = c._navigator && c._navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
            r = r ? parseInt(r[1], 10) : null;
            g && r && 9 > r && (g = /safari/.test(c._navigator && c._navigator.userAgent.toLowerCase()),
            c._navigator && !g && (c.usingWebAudio = !1));
            c.usingWebAudio && (c.masterGain = "undefined" === typeof c.ctx.createGain ? c.ctx.createGainNode() : c.ctx.createGain(),
            c.masterGain.gain.setValueAtTime(c._muted ? 0 : c._volume, c.ctx.currentTime),
            c.masterGain.connect(c.ctx.destination));
            c._setup()
        }
    };
    "function" === typeof define && define.amd && define([], function() {
        return {
            Howler: c,
            Howl: e
        }
    });
    "undefined" !== typeof exports && (exports.Howler = c,
    exports.Howl = e);
    "undefined" !== typeof global ? (global.HowlerGlobal = b,
    global.Howler = c,
    global.Howl = e,
    global.Sound = l) : "undefined" !== typeof window && (window.HowlerGlobal = b,
    window.Howler = c,
    window.Howl = e,
    window.Sound = l)
}
)();
(function() {
    HowlerGlobal.prototype._pos = [0, 0, 0];
    HowlerGlobal.prototype._orientation = [0, 0, -1, 0, 1, 0];
    HowlerGlobal.prototype.stereo = function(c) {
        if (!this.ctx || !this.ctx.listener)
            return this;
        for (var e = this._howls.length - 1; 0 <= e; e--)
            this._howls[e].stereo(c);
        return this
    }
    ;
    HowlerGlobal.prototype.pos = function(c, e, l) {
        if (!this.ctx || !this.ctx.listener)
            return this;
        e = "number" !== typeof e ? this._pos[1] : e;
        l = "number" !== typeof l ? this._pos[2] : l;
        if ("number" === typeof c)
            this._pos = [c, e, l],
            "undefined" !== typeof this.ctx.listener.positionX ? (this.ctx.listener.positionX.setTargetAtTime(this._pos[0], Howler.ctx.currentTime, .1),
            this.ctx.listener.positionY.setTargetAtTime(this._pos[1], Howler.ctx.currentTime, .1),
            this.ctx.listener.positionZ.setTargetAtTime(this._pos[2], Howler.ctx.currentTime, .1)) : this.ctx.listener.setPosition(this._pos[0], this._pos[1], this._pos[2]);
        else
            return this._pos;
        return this
    }
    ;
    HowlerGlobal.prototype.orientation = function(c, e, l, q, x, C) {
        if (!this.ctx || !this.ctx.listener)
            return this;
        var A = this._orientation;
        e = "number" !== typeof e ? A[1] : e;
        l = "number" !== typeof l ? A[2] : l;
        q = "number" !== typeof q ? A[3] : q;
        x = "number" !== typeof x ? A[4] : x;
        C = "number" !== typeof C ? A[5] : C;
        if ("number" === typeof c)
            this._orientation = [c, e, l, q, x, C],
            "undefined" !== typeof this.ctx.listener.forwardX ? (this.ctx.listener.forwardX.setTargetAtTime(c, Howler.ctx.currentTime, .1),
            this.ctx.listener.forwardY.setTargetAtTime(e, Howler.ctx.currentTime, .1),
            this.ctx.listener.forwardZ.setTargetAtTime(l, Howler.ctx.currentTime, .1),
            this.ctx.listener.upX.setTargetAtTime(q, Howler.ctx.currentTime, .1),
            this.ctx.listener.upY.setTargetAtTime(x, Howler.ctx.currentTime, .1),
            this.ctx.listener.upZ.setTargetAtTime(C, Howler.ctx.currentTime, .1)) : this.ctx.listener.setOrientation(c, e, l, q, x, C);
        else
            return A;
        return this
    }
    ;
    Howl.prototype.init = function(c) {
        return function(e) {
            this._orientation = e.orientation || [1, 0, 0];
            this._stereo = e.stereo || null;
            this._pos = e.pos || null;
            this._pannerAttr = {
                coneInnerAngle: "undefined" !== typeof e.coneInnerAngle ? e.coneInnerAngle : 360,
                coneOuterAngle: "undefined" !== typeof e.coneOuterAngle ? e.coneOuterAngle : 360,
                coneOuterGain: "undefined" !== typeof e.coneOuterGain ? e.coneOuterGain : 0,
                distanceModel: "undefined" !== typeof e.distanceModel ? e.distanceModel : "inverse",
                maxDistance: "undefined" !== typeof e.maxDistance ? e.maxDistance : 1E4,
                panningModel: "undefined" !== typeof e.panningModel ? e.panningModel : "HRTF",
                refDistance: "undefined" !== typeof e.refDistance ? e.refDistance : 1,
                rolloffFactor: "undefined" !== typeof e.rolloffFactor ? e.rolloffFactor : 1
            };
            this._onstereo = e.onstereo ? [{
                fn: e.onstereo
            }] : [];
            this._onpos = e.onpos ? [{
                fn: e.onpos
            }] : [];
            this._onorientation = e.onorientation ? [{
                fn: e.onorientation
            }] : [];
            return c.call(this, e)
        }
    }(Howl.prototype.init);
    Howl.prototype.stereo = function(c, e) {
        var l = this;
        if (!l._webAudio)
            return l;
        if ("loaded" !== l._state)
            return l._queue.push({
                event: "stereo",
                action: function() {
                    l.stereo(c, e)
                }
            }),
            l;
        var q = "undefined" === typeof Howler.ctx.createStereoPanner ? "spatial" : "stereo";
        if ("undefined" === typeof e)
            if ("number" === typeof c)
                l._stereo = c,
                l._pos = [c, 0, 0];
            else
                return l._stereo;
        for (var x = l._getSoundIds(e), C = 0; C < x.length; C++) {
            var A = l._soundById(x[C]);
            if (A)
                if ("number" === typeof c)
                    A._stereo = c,
                    A._pos = [c, 0, 0],
                    A._node && (A._pannerAttr.panningModel = "equalpower",
                    A._panner && A._panner.pan || b(A, q),
                    "spatial" === q ? "undefined" !== typeof A._panner.positionX ? (A._panner.positionX.setValueAtTime(c, Howler.ctx.currentTime),
                    A._panner.positionY.setValueAtTime(0, Howler.ctx.currentTime),
                    A._panner.positionZ.setValueAtTime(0, Howler.ctx.currentTime)) : A._panner.setPosition(c, 0, 0) : A._panner.pan.setValueAtTime(c, Howler.ctx.currentTime)),
                    l._emit("stereo", A._id);
                else
                    return A._stereo
        }
        return l
    }
    ;
    Howl.prototype.pos = function(c, e, l, q) {
        var x = this;
        if (!x._webAudio)
            return x;
        if ("loaded" !== x._state)
            return x._queue.push({
                event: "pos",
                action: function() {
                    x.pos(c, e, l, q)
                }
            }),
            x;
        e = "number" !== typeof e ? 0 : e;
        l = "number" !== typeof l ? -.5 : l;
        if ("undefined" === typeof q)
            if ("number" === typeof c)
                x._pos = [c, e, l];
            else
                return x._pos;
        for (var C = x._getSoundIds(q), A = 0; A < C.length; A++) {
            var E = x._soundById(C[A]);
            if (E)
                if ("number" === typeof c)
                    E._pos = [c, e, l],
                    E._node && (E._panner && !E._panner.pan || b(E, "spatial"),
                    "undefined" !== typeof E._panner.positionX ? (E._panner.positionX.setValueAtTime(c, Howler.ctx.currentTime),
                    E._panner.positionY.setValueAtTime(e, Howler.ctx.currentTime),
                    E._panner.positionZ.setValueAtTime(l, Howler.ctx.currentTime)) : E._panner.setPosition(c, e, l)),
                    x._emit("pos", E._id);
                else
                    return E._pos
        }
        return x
    }
    ;
    Howl.prototype.orientation = function(c, e, l, q) {
        var x = this;
        if (!x._webAudio)
            return x;
        if ("loaded" !== x._state)
            return x._queue.push({
                event: "orientation",
                action: function() {
                    x.orientation(c, e, l, q)
                }
            }),
            x;
        e = "number" !== typeof e ? x._orientation[1] : e;
        l = "number" !== typeof l ? x._orientation[2] : l;
        if ("undefined" === typeof q)
            if ("number" === typeof c)
                x._orientation = [c, e, l];
            else
                return x._orientation;
        for (var C = x._getSoundIds(q), A = 0; A < C.length; A++) {
            var E = x._soundById(C[A]);
            if (E)
                if ("number" === typeof c)
                    E._orientation = [c, e, l],
                    E._node && (E._panner || (E._pos || (E._pos = x._pos || [0, 0, -.5]),
                    b(E, "spatial")),
                    "undefined" !== typeof E._panner.orientationX ? (E._panner.orientationX.setValueAtTime(c, Howler.ctx.currentTime),
                    E._panner.orientationY.setValueAtTime(e, Howler.ctx.currentTime),
                    E._panner.orientationZ.setValueAtTime(l, Howler.ctx.currentTime)) : E._panner.setOrientation(c, e, l)),
                    x._emit("orientation", E._id);
                else
                    return E._orientation
        }
        return x
    }
    ;
    Howl.prototype.pannerAttr = function() {
        var c = arguments;
        if (!this._webAudio)
            return this;
        if (0 === c.length)
            return this._pannerAttr;
        if (1 === c.length)
            if ("object" === typeof c[0]) {
                var e = c[0];
                "undefined" === typeof l && (e.pannerAttr || (e.pannerAttr = {
                    coneInnerAngle: e.coneInnerAngle,
                    coneOuterAngle: e.coneOuterAngle,
                    coneOuterGain: e.coneOuterGain,
                    distanceModel: e.distanceModel,
                    maxDistance: e.maxDistance,
                    refDistance: e.refDistance,
                    rolloffFactor: e.rolloffFactor,
                    panningModel: e.panningModel
                }),
                this._pannerAttr = {
                    coneInnerAngle: "undefined" !== typeof e.pannerAttr.coneInnerAngle ? e.pannerAttr.coneInnerAngle : this._coneInnerAngle,
                    coneOuterAngle: "undefined" !== typeof e.pannerAttr.coneOuterAngle ? e.pannerAttr.coneOuterAngle : this._coneOuterAngle,
                    coneOuterGain: "undefined" !== typeof e.pannerAttr.coneOuterGain ? e.pannerAttr.coneOuterGain : this._coneOuterGain,
                    distanceModel: "undefined" !== typeof e.pannerAttr.distanceModel ? e.pannerAttr.distanceModel : this._distanceModel,
                    maxDistance: "undefined" !== typeof e.pannerAttr.maxDistance ? e.pannerAttr.maxDistance : this._maxDistance,
                    refDistance: "undefined" !== typeof e.pannerAttr.refDistance ? e.pannerAttr.refDistance : this._refDistance,
                    rolloffFactor: "undefined" !== typeof e.pannerAttr.rolloffFactor ? e.pannerAttr.rolloffFactor : this._rolloffFactor,
                    panningModel: "undefined" !== typeof e.pannerAttr.panningModel ? e.pannerAttr.panningModel : this._panningModel
                })
            } else
                return (c = this._soundById(parseInt(c[0], 10))) ? c._pannerAttr : this._pannerAttr;
        else if (2 === c.length) {
            e = c[0];
            var l = parseInt(c[1], 10)
        }
        l = this._getSoundIds(l);
        for (var q = 0; q < l.length; q++)
            if (c = this._soundById(l[q])) {
                var x = c._pannerAttr;
                x = {
                    coneInnerAngle: "undefined" !== typeof e.coneInnerAngle ? e.coneInnerAngle : x.coneInnerAngle,
                    coneOuterAngle: "undefined" !== typeof e.coneOuterAngle ? e.coneOuterAngle : x.coneOuterAngle,
                    coneOuterGain: "undefined" !== typeof e.coneOuterGain ? e.coneOuterGain : x.coneOuterGain,
                    distanceModel: "undefined" !== typeof e.distanceModel ? e.distanceModel : x.distanceModel,
                    maxDistance: "undefined" !== typeof e.maxDistance ? e.maxDistance : x.maxDistance,
                    refDistance: "undefined" !== typeof e.refDistance ? e.refDistance : x.refDistance,
                    rolloffFactor: "undefined" !== typeof e.rolloffFactor ? e.rolloffFactor : x.rolloffFactor,
                    panningModel: "undefined" !== typeof e.panningModel ? e.panningModel : x.panningModel
                };
                var C = c._panner;
                C ? (C.coneInnerAngle = x.coneInnerAngle,
                C.coneOuterAngle = x.coneOuterAngle,
                C.coneOuterGain = x.coneOuterGain,
                C.distanceModel = x.distanceModel,
                C.maxDistance = x.maxDistance,
                C.refDistance = x.refDistance,
                C.rolloffFactor = x.rolloffFactor,
                C.panningModel = x.panningModel) : (c._pos || (c._pos = this._pos || [0, 0, -.5]),
                b(c, "spatial"))
            }
        return this
    }
    ;
    Sound.prototype.init = function(c) {
        return function() {
            var e = this._parent;
            this._orientation = e._orientation;
            this._stereo = e._stereo;
            this._pos = e._pos;
            this._pannerAttr = e._pannerAttr;
            c.call(this);
            this._stereo ? e.stereo(this._stereo) : this._pos && e.pos(this._pos[0], this._pos[1], this._pos[2], this._id)
        }
    }(Sound.prototype.init);
    Sound.prototype.reset = function(c) {
        return function() {
            var e = this._parent;
            this._orientation = e._orientation;
            this._stereo = e._stereo;
            this._pos = e._pos;
            this._pannerAttr = e._pannerAttr;
            this._stereo ? e.stereo(this._stereo) : this._pos ? e.pos(this._pos[0], this._pos[1], this._pos[2], this._id) : this._panner && (this._panner.disconnect(0),
            this._panner = void 0,
            e._refreshBuffer(this));
            return c.call(this)
        }
    }(Sound.prototype.reset);
    var b = function(c, e) {
        "spatial" === (e || "spatial") ? (c._panner = Howler.ctx.createPanner(),
        c._panner.coneInnerAngle = c._pannerAttr.coneInnerAngle,
        c._panner.coneOuterAngle = c._pannerAttr.coneOuterAngle,
        c._panner.coneOuterGain = c._pannerAttr.coneOuterGain,
        c._panner.distanceModel = c._pannerAttr.distanceModel,
        c._panner.maxDistance = c._pannerAttr.maxDistance,
        c._panner.refDistance = c._pannerAttr.refDistance,
        c._panner.rolloffFactor = c._pannerAttr.rolloffFactor,
        c._panner.panningModel = c._pannerAttr.panningModel,
        "undefined" !== typeof c._panner.positionX ? (c._panner.positionX.setValueAtTime(c._pos[0], Howler.ctx.currentTime),
        c._panner.positionY.setValueAtTime(c._pos[1], Howler.ctx.currentTime),
        c._panner.positionZ.setValueAtTime(c._pos[2], Howler.ctx.currentTime)) : c._panner.setPosition(c._pos[0], c._pos[1], c._pos[2]),
        "undefined" !== typeof c._panner.orientationX ? (c._panner.orientationX.setValueAtTime(c._orientation[0], Howler.ctx.currentTime),
        c._panner.orientationY.setValueAtTime(c._orientation[1], Howler.ctx.currentTime),
        c._panner.orientationZ.setValueAtTime(c._orientation[2], Howler.ctx.currentTime)) : c._panner.setOrientation(c._orientation[0], c._orientation[1], c._orientation[2])) : (c._panner = Howler.ctx.createStereoPanner(),
        c._panner.pan.setValueAtTime(c._stereo, Howler.ctx.currentTime));
        c._panner.connect(c._node);
        c._paused || c._parent.pause(c._id, !0).play(c._id, !0)
    }
}
)();
!function(b, c) {
    "object" == typeof exports && "undefined" != typeof module ? c() : "function" == typeof define && define.amd ? define(c) : c()
}(0, function() {
    function b(g) {
        var r = this.constructor;
        return this.then(function(y) {
            return r.resolve(g()).then(function() {
                return y
            })
        }, function(y) {
            return r.resolve(g()).then(function() {
                return r.reject(y)
            })
        })
    }
    function c() {}
    function e(g) {
        if (!(this instanceof e))
            throw new TypeError("Promises must be constructed via new");
        if ("function" != typeof g)
            throw new TypeError("not a function");
        this._state = 0;
        this._handled = !1;
        this._value = void 0;
        this._deferreds = [];
        A(g, this)
    }
    function l(g, r) {
        for (; 3 === g._state; )
            g = g._value;
        0 !== g._state ? (g._handled = !0,
        e._immediateFn(function() {
            var y = 1 === g._state ? r.onFulfilled : r.onRejected;
            if (null !== y) {
                try {
                    var F = y(g._value)
                } catch (G) {
                    return void x(r.promise, G)
                }
                q(r.promise, F)
            } else
                (1 === g._state ? q : x)(r.promise, g._value)
        })) : g._deferreds.push(r)
    }
    function q(g, r) {
        try {
            if (r === g)
                throw new TypeError("A promise cannot be resolved with itself.");
            if (r && ("object" == typeof r || "function" == typeof r)) {
                var y = r.then;
                if (r instanceof e)
                    return g._state = 3,
                    g._value = r,
                    void C(g);
                if ("function" == typeof y)
                    return void A(function(F, G) {
                        return function() {
                            F.apply(G, arguments)
                        }
                    }(y, r), g)
            }
            g._state = 1;
            g._value = r;
            C(g)
        } catch (F) {
            x(g, F)
        }
    }
    function x(g, r) {
        g._state = 2;
        g._value = r;
        C(g)
    }
    function C(g) {
        2 === g._state && 0 === g._deferreds.length && e._immediateFn(function() {
            g._handled || e._unhandledRejectionFn(g._value)
        });
        for (var r = 0, y = g._deferreds.length; y > r; r++)
            l(g, g._deferreds[r]);
        g._deferreds = null
    }
    function A(g, r) {
        var y = !1;
        try {
            g(function(F) {
                y || (y = !0,
                q(r, F))
            }, function(F) {
                y || (y = !0,
                x(r, F))
            })
        } catch (F) {
            y || (y = !0,
            x(r, F))
        }
    }
    var E = setTimeout;
    e.prototype["catch"] = function(g) {
        return this.then(null, g)
    }
    ;
    e.prototype.then = function(g, r) {
        var y = new this.constructor(c);
        return l(this, new function(F, G, M) {
            this.onFulfilled = "function" == typeof F ? F : null;
            this.onRejected = "function" == typeof G ? G : null;
            this.promise = M
        }
        (g,r,y)),
        y
    }
    ;
    e.prototype["finally"] = b;
    e.all = function(g) {
        return new e(function(r, y) {
            function F(V, fa) {
                try {
                    if (fa && ("object" == typeof fa || "function" == typeof fa)) {
                        var va = fa.then;
                        if ("function" == typeof va)
                            return void va.call(fa, function(Fa) {
                                F(V, Fa)
                            }, y)
                    }
                    G[V] = fa;
                    0 == --M && r(G)
                } catch (Fa) {
                    y(Fa)
                }
            }
            if (!g || "undefined" == typeof g.length)
                throw new TypeError("Promise.all accepts an array");
            var G = Array.prototype.slice.call(g);
            if (0 === G.length)
                return r([]);
            for (var M = G.length, O = 0; G.length > O; O++)
                F(O, G[O])
        }
        )
    }
    ;
    e.resolve = function(g) {
        return g && "object" == typeof g && g.constructor === e ? g : new e(function(r) {
            r(g)
        }
        )
    }
    ;
    e.reject = function(g) {
        return new e(function(r, y) {
            y(g)
        }
        )
    }
    ;
    e.race = function(g) {
        return new e(function(r, y) {
            for (var F = 0, G = g.length; G > F; F++)
                g[F].then(r, y)
        }
        )
    }
    ;
    e._immediateFn = "function" == typeof setImmediate && function(g) {
        setImmediate(g)
    }
    || function(g) {
        E(g, 0)
    }
    ;
    e._unhandledRejectionFn = function(g) {
        void 0 !== console && console && console.warn("Possible Unhandled Promise Rejection:", g)
    }
    ;
    var N = function() {
        if ("undefined" != typeof self)
            return self;
        if ("undefined" != typeof window)
            return window;
        if ("undefined" != typeof global)
            return global;
        throw Error("unable to locate global object");
    }();
    "Promise"in N ? N.Promise.prototype["finally"] || (N.Promise.prototype["finally"] = b) : N.Promise = e
});
(function() {
    function b(G, M) {
        document.addEventListener ? G.addEventListener("scroll", M, !1) : G.attachEvent("scroll", M)
    }
    function c(G) {
        document.body ? G() : document.addEventListener ? document.addEventListener("DOMContentLoaded", function O() {
            document.removeEventListener("DOMContentLoaded", O);
            G()
        }) : document.attachEvent("onreadystatechange", function V() {
            if ("interactive" == document.readyState || "complete" == document.readyState)
                document.detachEvent("onreadystatechange", V),
                G()
        })
    }
    function e(G) {
        this.a = document.createElement("div");
        this.a.setAttribute("aria-hidden", "true");
        this.a.appendChild(document.createTextNode(G));
        this.b = document.createElement("span");
        this.c = document.createElement("span");
        this.h = document.createElement("span");
        this.f = document.createElement("span");
        this.g = -1;
        this.b.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
        this.c.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
        this.f.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
        this.h.style.cssText = "display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";
        this.b.appendChild(this.h);
        this.c.appendChild(this.f);
        this.a.appendChild(this.b);
        this.a.appendChild(this.c)
    }
    function l(G, M) {
        G.a.style.cssText = "max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;white-space:nowrap;font-synthesis:none;font:" + M + ";"
    }
    function q(G) {
        var M = G.a.offsetWidth
          , O = M + 100;
        G.f.style.width = O + "px";
        G.c.scrollLeft = O;
        G.b.scrollLeft = G.b.scrollWidth + 100;
        return G.g !== M ? (G.g = M,
        !0) : !1
    }
    function x(G, M) {
        function O() {
            var fa = V;
            q(fa) && fa.a.parentNode && M(fa.g)
        }
        var V = G;
        b(G.b, O);
        b(G.c, O);
        q(G)
    }
    function C(G, M) {
        M = M || {};
        this.family = G;
        this.style = M.style || "normal";
        this.weight = M.weight || "normal";
        this.stretch = M.stretch || "normal"
    }
    function A() {
        if (null === r)
            if (E() && /Apple/.test(window.navigator.vendor)) {
                var G = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))(?:\.([0-9]+))/.exec(window.navigator.userAgent);
                r = !!G && 603 > parseInt(G[1], 10)
            } else
                r = !1;
        return r
    }
    function E() {
        null === F && (F = !!document.fonts);
        return F
    }
    function N(G, M) {
        var O = G.style
          , V = G.weight;
        if (null === y) {
            var fa = document.createElement("div");
            try {
                fa.style.font = "condensed 100px sans-serif"
            } catch (va) {}
            y = "" !== fa.style.font
        }
        return [O, V, y ? G.stretch : "", "100px", M].join(" ")
    }
    var g = null
      , r = null
      , y = null
      , F = null;
    C.prototype.load = function(G, M) {
        var O = this
          , V = G || "BESbswy"
          , fa = 0
          , va = M || 3E3
          , Fa = (new Date).getTime();
        return new Promise(function(qb, ib) {
            if (E() && !A()) {
                var nb = new Promise(function(Wa, Sa) {
                    function Ca() {
                        (new Date).getTime() - Fa >= va ? Sa(Error("" + va + "ms timeout exceeded")) : document.fonts.load(N(O, '"' + O.family + '"'), V).then(function(Ta) {
                            1 <= Ta.length ? Wa() : setTimeout(Ca, 25)
                        }, Sa)
                    }
                    Ca()
                }
                )
                  , pa = new Promise(function(Wa, Sa) {
                    fa = setTimeout(function() {
                        Sa(Error("" + va + "ms timeout exceeded"))
                    }, va)
                }
                );
                Promise.race([pa, nb]).then(function() {
                    clearTimeout(fa);
                    qb(O)
                }, ib)
            } else
                c(function() {
                    function Wa() {
                        var xa;
                        if (xa = -1 != Ya && -1 != Za || -1 != Ya && -1 != $a || -1 != Za && -1 != $a)
                            (xa = Ya != Za && Ya != $a && Za != $a) || (null === g && (xa = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent),
                            g = !!xa && (536 > parseInt(xa[1], 10) || 536 === parseInt(xa[1], 10) && 11 >= parseInt(xa[2], 10))),
                            xa = g && (Ya == rb && Za == rb && $a == rb || Ya == tb && Za == tb && $a == tb || Ya == Ga && Za == Ga && $a == Ga)),
                            xa = !xa;
                        xa && (Na.parentNode && Na.parentNode.removeChild(Na),
                        clearTimeout(fa),
                        qb(O))
                    }
                    function Sa() {
                        if ((new Date).getTime() - Fa >= va)
                            Na.parentNode && Na.parentNode.removeChild(Na),
                            ib(Error("" + va + "ms timeout exceeded"));
                        else {
                            var xa = document.hidden;
                            if (!0 === xa || void 0 === xa)
                                Ya = Ca.a.offsetWidth,
                                Za = Ta.a.offsetWidth,
                                $a = Xa.a.offsetWidth,
                                Wa();
                            fa = setTimeout(Sa, 50)
                        }
                    }
                    var Ca = new e(V)
                      , Ta = new e(V)
                      , Xa = new e(V)
                      , Ya = -1
                      , Za = -1
                      , $a = -1
                      , rb = -1
                      , tb = -1
                      , Ga = -1
                      , Na = document.createElement("div");
                    Na.dir = "ltr";
                    l(Ca, N(O, "sans-serif"));
                    l(Ta, N(O, "serif"));
                    l(Xa, N(O, "monospace"));
                    Na.appendChild(Ca.a);
                    Na.appendChild(Ta.a);
                    Na.appendChild(Xa.a);
                    document.body.appendChild(Na);
                    rb = Ca.a.offsetWidth;
                    tb = Ta.a.offsetWidth;
                    Ga = Xa.a.offsetWidth;
                    Sa();
                    x(Ca, function(xa) {
                        Ya = xa;
                        Wa()
                    });
                    l(Ca, N(O, '"' + O.family + '",sans-serif'));
                    x(Ta, function(xa) {
                        Za = xa;
                        Wa()
                    });
                    l(Ta, N(O, '"' + O.family + '",serif'));
                    x(Xa, function(xa) {
                        $a = xa;
                        Wa()
                    });
                    l(Xa, N(O, '"' + O.family + '",monospace'))
                })
        }
        )
    }
    ;
    "object" === typeof module ? module.exports = C : (window.FontFaceObserver = C,
    window.FontFaceObserver.prototype.load = C.prototype.load)
}
)();
(function(b) {
    Number.prototype.map = function(A, E, N, g) {
        return N + (this - A) / (E - A) * (g - N)
    }
    ;
    Number.prototype.limit = function(A, E) {
        return Math.min(E, Math.max(A, this))
    }
    ;
    Number.prototype.round = function(A) {
        A = Math.pow(10, A || 0);
        return Math.round(this * A) / A
    }
    ;
    Number.prototype.floor = function() {
        return Math.floor(this)
    }
    ;
    Number.prototype.ceil = function() {
        return Math.ceil(this)
    }
    ;
    Number.prototype.toInt = function() {
        return this | 0
    }
    ;
    Number.prototype.toRad = function() {
        return this / 180 * Math.PI
    }
    ;
    Number.prototype.toDeg = function() {
        return 180 * this / Math.PI
    }
    ;
    Array.prototype.erase = function(A) {
        for (var E = this.length; E--; )
            this[E] === A && this.splice(E, 1);
        return this
    }
    ;
    Array.prototype.random = function() {
        return this[Math.floor(Math.random() * this.length)]
    }
    ;
    Function.prototype.bind = Function.prototype.bind || function(A) {
        if ("function" !== typeof this)
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        var E = Array.prototype.slice.call(arguments, 1)
          , N = this
          , g = function() {}
          , r = function() {
            return N.apply(this instanceof g && A ? this : A, E.concat(Array.prototype.slice.call(arguments)))
        };
        g.prototype = this.prototype;
        r.prototype = new g;
        return r
    }
    ;
    b.ig = {
        game: null,
        debug: null,
        version: "1.23",
        global: b,
        modules: {},
        resources: [],
        ready: !1,
        baked: !1,
        nocache: "",
        ua: {},
        prefix: b.ImpactPrefix || "",
        lib: "lib/",
        _current: null,
        _loadQueue: [],
        _waitForOnload: 0,
        $: function(A) {
            return "#" == A.charAt(0) ? document.getElementById(A.substr(1)) : document.getElementsByTagName(A)
        },
        $new: function(A) {
            return document.createElement(A)
        },
        copy: function(A) {
            if (!A || "object" != typeof A || A instanceof HTMLElement || A instanceof ig.Class)
                return A;
            if (A instanceof Array)
                for (var E = [], N = 0, g = A.length; N < g; N++)
                    E[N] = ig.copy(A[N]);
            else
                for (N in E = {},
                A)
                    E[N] = ig.copy(A[N]);
            return E
        },
        merge: function(A, E) {
            for (var N in E) {
                var g = E[N];
                "object" != typeof g || g instanceof HTMLElement || g instanceof ig.Class || null === g ? A[N] = g : (A[N] && "object" == typeof A[N] || (A[N] = g instanceof Array ? [] : {}),
                ig.merge(A[N], g))
            }
            return A
        },
        ksort: function(A) {
            if (!A || "object" != typeof A)
                return [];
            var E = [], N = [], g;
            for (g in A)
                E.push(g);
            E.sort();
            for (g = 0; g < E.length; g++)
                N.push(A[E[g]]);
            return N
        },
        setVendorAttribute: function(A, E, N) {
            var g = E.charAt(0).toUpperCase() + E.substr(1);
            A[E] = "undefined" !== typeof A.imageSmoothingEnabled ? A["ms" + g] = A["moz" + g] = A["o" + g] = N : A["ms" + g] = A["moz" + g] = A["webkit" + g] = A["o" + g] = N
        },
        getVendorAttribute: function(A, E) {
            var N = E.charAt(0).toUpperCase() + E.substr(1);
            return "undefined" !== typeof A.imageSmoothingEnabled ? A[E] || A["ms" + N] || A["moz" + N] || A["o" + N] : A[E] || A["ms" + N] || A["moz" + N] || A["webkit" + N] || A["o" + N]
        },
        normalizeVendorAttribute: function(A, E) {
            var N = ig.getVendorAttribute(A, E);
            !A[E] && N && (A[E] = N)
        },
        getImagePixels: function(A, E, N, g, r) {
            var y = ig.$new("canvas");
            y.width = A.width;
            y.height = A.height;
            var F = y.getContext("2d");
            ig.System.SCALE.CRISP(y, F);
            var G = ig.getVendorAttribute(F, "backingStorePixelRatio") || 1;
            ig.normalizeVendorAttribute(F, "getImageDataHD");
            var M = A.width / G
              , O = A.height / G;
            y.width = Math.ceil(M);
            y.height = Math.ceil(O);
            F.drawImage(A, 0, 0, M, O);
            return 1 === G ? F.getImageData(E, N, g, r) : F.getImageDataHD(E, N, g, r)
        },
        module: function(A) {
            if (ig._current)
                throw "Module '" + ig._current.name + "' defines nothing";
            if (ig.modules[A] && ig.modules[A].body)
                throw "Module '" + A + "' is already defined";
            ig._current = {
                name: A,
                requires: [],
                loaded: !1,
                body: null
            };
            ig.modules[A] = ig._current;
            ig._loadQueue.push(ig._current);
            return ig
        },
        requires: function() {
            ig._current.requires = Array.prototype.slice.call(arguments);
            return ig
        },
        defines: function(A) {
            ig._current.body = A;
            ig._current = null;
            ig._initDOMReady()
        },
        addResource: function(A) {
            ig.resources.push(A)
        },
        setNocache: function(A) {
            ig.nocache = A ? "?" + Date.now() : ""
        },
        log: function() {},
        assert: function(A, E) {},
        show: function(A, E) {},
        mark: function(A, E) {},
        _loadScript: function(A, E) {
            ig.modules[A] = {
                name: A,
                requires: [],
                loaded: !1,
                body: null
            };
            ig._waitForOnload++;
            var N = ig.prefix + ig.lib + A.replace(/\./g, "/") + ".js" + ig.nocache
              , g = ig.$new("script");
            g.type = "text/javascript";
            g.src = N;
            g.onload = function() {
                ig._waitForOnload--;
                ig._execModules()
            }
            ;
            g.onerror = function() {
                throw "Failed to load module " + A + " at " + N + " required from " + E;
            }
            ;
            ig.$("head")[0].appendChild(g)
        },
        _execModules: function() {
            for (var A = !1, E = 0; E < ig._loadQueue.length; E++) {
                for (var N = ig._loadQueue[E], g = !0, r = 0; r < N.requires.length; r++) {
                    var y = N.requires[r];
                    ig.modules[y] ? ig.modules[y].loaded || (g = !1) : (g = !1,
                    ig._loadScript(y, N.name))
                }
                g && N.body && (ig._loadQueue.splice(E, 1),
                N.loaded = !0,
                N.body(),
                A = !0,
                E--)
            }
            if (A)
                ig._execModules();
            else if (!ig.baked && 0 == ig._waitForOnload && 0 != ig._loadQueue.length) {
                A = [];
                for (E = 0; E < ig._loadQueue.length; E++) {
                    g = [];
                    y = ig._loadQueue[E].requires;
                    for (r = 0; r < y.length; r++)
                        (N = ig.modules[y[r]]) && N.loaded || g.push(y[r]);
                    A.push(ig._loadQueue[E].name + " (requires: " + g.join(", ") + ")")
                }
                throw "Unresolved (or circular?) dependencies. Most likely there's a name/path mismatch for one of the listed modules or a previous syntax error prevents a module from loading:\n" + A.join("\n");
            }
        },
        _DOMReady: function() {
            if (!ig.modules["dom.ready"].loaded) {
                if (!document.body)
                    return setTimeout(ig._DOMReady, 13);
                ig.modules["dom.ready"].loaded = !0;
                ig._waitForOnload--;
                ig._execModules()
            }
            return 0
        },
        _boot: function() {
            document.location.href.match(/\?nocache/) && ig.setNocache(!0);
            ig.ua.pixelRatio = b.devicePixelRatio || 1;
            ig.ua.viewport = {
                width: b.innerWidth,
                height: b.innerHeight
            };
            ig.ua.screen = {
                width: b.screen.availWidth * ig.ua.pixelRatio,
                height: b.screen.availHeight * ig.ua.pixelRatio
            };
            ig.ua.iPhone = /iPhone/i.test(navigator.userAgent);
            ig.ua.iPhone4 = ig.ua.iPhone && 2 == ig.ua.pixelRatio;
            ig.ua.iPad = /iPad/i.test(navigator.userAgent);
            ig.ua.android = /android/i.test(navigator.userAgent);
            ig.ua.winPhone = /Windows Phone/i.test(navigator.userAgent);
            ig.ua.is_uiwebview = /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent);
            ig.ua.is_safari_or_uiwebview = /(iPhone|iPod|iPad).*AppleWebKit/i.test(navigator.userAgent);
            ig.ua.iOS = ig.ua.iPhone || ig.ua.iPad;
            ig.ua.iOS6_tag = /OS 6_/i.test(navigator.userAgent);
            ig.ua.iOS6 = (ig.ua.iPhone || ig.ua.iPad) && ig.ua.iOS6_tag;
            ig.ua.iOSgt5 = ig.ua.iOS && 5 < parseInt(navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/)[1]);
            ig.ua.HTCONE = /HTC_One/i.test(navigator.userAgent);
            ig.ua.winPhone = /Windows Phone/i.test(navigator.userAgent);
            ig.ua.Kindle = /Silk/i.test(navigator.userAgent);
            ig.ua.touchDevice = "ontouchstart"in b || b.navigator.msMaxTouchPoints;
            ig.ua.mobile = ig.ua.iOS || ig.ua.android || ig.ua.iOS6 || ig.ua.winPhone || ig.ua.Kindle || /mobile/i.test(navigator.userAgent)
        },
        _initDOMReady: function() {
            ig.modules["dom.ready"] ? ig._execModules() : (ig._boot(),
            ig.modules["dom.ready"] = {
                requires: [],
                loaded: !1,
                body: null
            },
            ig._waitForOnload++,
            "complete" === document.readyState ? ig._DOMReady() : (document.addEventListener("DOMContentLoaded", ig._DOMReady, !1),
            b.addEventListener("load", ig._DOMReady, !1)))
        }
    };
    ig.normalizeVendorAttribute(b, "requestAnimationFrame");
    if (b.requestAnimationFrame) {
        var c = 1
          , e = {};
        b.ig.setAnimation = function(A) {
            var E = c++;
            e[E] = !0;
            var N = function() {
                e[E] && (b.requestAnimationFrame(N),
                A())
            };
            b.requestAnimationFrame(N);
            return E
        }
        ;
        b.ig.clearAnimation = function(A) {
            delete e[A]
        }
    } else
        b.ig.setAnimation = function(A) {
            return b.setInterval(A, 1E3 / 60)
        }
        ,
        b.ig.clearAnimation = function(A) {
            b.clearInterval(A)
        }
        ;
    var l = !1
      , q = /xyz/.test(function() {
        xyz
    }) ? /\bparent\b/ : /.*/
      , x = 0;
    b.ig.Class = function() {}
    ;
    var C = function(A) {
        var E = this.prototype, N = {}, g;
        for (g in A)
            "function" == typeof A[g] && "function" == typeof E[g] && q.test(A[g]) ? (N[g] = E[g],
            E[g] = function(r, y) {
                return function() {
                    var F = this.parent;
                    this.parent = N[r];
                    var G = y.apply(this, arguments);
                    this.parent = F;
                    return G
                }
            }(g, A[g])) : E[g] = A[g]
    };
    b.ig.Class.extend = function(A) {
        function E() {
            if (!l) {
                if (this.staticInstantiate) {
                    var y = this.staticInstantiate.apply(this, arguments);
                    if (y)
                        return y
                }
                for (var F in this)
                    "object" == typeof this[F] && (this[F] = ig.copy(this[F]));
                this.init && this.init.apply(this, arguments)
            }
            return this
        }
        var N = this.prototype;
        l = !0;
        var g = new this;
        l = !1;
        for (var r in A)
            "function" == typeof A[r] && "function" == typeof N[r] && q.test(A[r]) ? g[r] = function(y, F) {
                return function() {
                    var G = this.parent;
                    this.parent = N[y];
                    var M = F.apply(this, arguments);
                    this.parent = G;
                    return M
                }
            }(r, A[r]) : g[r] = A[r];
        E.prototype = g;
        E.prototype.constructor = E;
        E.extend = b.ig.Class.extend;
        E.inject = C;
        E.classId = g.classId = ++x;
        return E
    }
    ;
    b.ImpactMixin && ig.merge(ig, b.ImpactMixin)
}
)(window);
ig.baked = !0;
ig.module("impact.image").defines(function() {
    ig.Image = ig.Class.extend({
        data: null,
        width: 0,
        height: 0,
        loaded: !1,
        failed: !1,
        loadCallback: null,
        path: "",
        staticInstantiate: function(b) {
            return ig.Image.cache[b] || null
        },
        init: function(b) {
            this.path = b;
            this.load()
        },
        load: function(b) {
            this.loaded ? b && b(this.path, !0) : (!this.loaded && ig.ready ? (this.loadCallback = b || null,
            this.data = new Image,
            this.data.onload = this.onload.bind(this),
            this.data.onerror = this.onerror.bind(this),
            this.data.src = ig.prefix + this.path + ig.nocache) : ig.addResource(this),
            ig.Image.cache[this.path] = this)
        },
        reload: function() {
            this.loaded = !1;
            this.data = new Image;
            this.data.onload = this.onload.bind(this);
            this.data.src = this.path + "?" + Date.now()
        },
        onload: function(b) {
            this.width = this.data.width;
            this.height = this.data.height;
            this.loaded = !0;
            1 != ig.system.scale && this.resize(ig.system.scale);
            this.loadCallback && this.loadCallback(this.path, !0)
        },
        onerror: function(b) {
            this.failed = !0;
            this.loadCallback && this.loadCallback(this.path, !1)
        },
        resize: function(b) {
            var c = ig.getImagePixels(this.data, 0, 0, this.width, this.height)
              , e = this.width * b
              , l = this.height * b
              , q = ig.$new("canvas");
            q.width = e;
            q.height = l;
            for (var x = q.getContext("2d"), C = x.getImageData(0, 0, e, l), A = 0; A < l; A++)
                for (var E = 0; E < e; E++) {
                    var N = 4 * (Math.floor(A / b) * this.width + Math.floor(E / b))
                      , g = 4 * (A * e + E);
                    C.data[g] = c.data[N];
                    C.data[g + 1] = c.data[N + 1];
                    C.data[g + 2] = c.data[N + 2];
                    C.data[g + 3] = c.data[N + 3]
                }
            x.putImageData(C, 0, 0);
            this.data = q
        },
        draw: function(b, c, e, l, q, x) {
            if (this.loaded) {
                var C = ig.system.scale;
                e = e ? e * C : 0;
                l = l ? l * C : 0;
                q = (q ? q : this.width) * C;
                x = (x ? x : this.height) * C;
                ig.system.context.drawImage(this.data, e, l, q, x, ig.system.getDrawPos(b), ig.system.getDrawPos(c), q, x);
                ig.Image.drawCount++
            }
        },
        drawTile: function(b, c, e, l, q, x, C) {
            q = q ? q : l;
            if (!(!this.loaded || l > this.width || q > this.height)) {
                var A = ig.system.scale
                  , E = Math.floor(l * A)
                  , N = Math.floor(q * A)
                  , g = x ? -1 : 1
                  , r = C ? -1 : 1;
                if (x || C)
                    ig.system.context.save(),
                    ig.system.context.scale(g, r);
                ig.system.context.drawImage(this.data, Math.floor(e * l) % this.width * A, Math.floor(e * l / this.width) * q * A, E, N, ig.system.getDrawPos(b) * g - (x ? E : 0), ig.system.getDrawPos(c) * r - (C ? N : 0), E, N);
                (x || C) && ig.system.context.restore();
                ig.Image.drawCount++
            }
        }
    });
    ig.Image.drawCount = 0;
    ig.Image.cache = {};
    ig.Image.reloadCache = function() {
        for (var b in ig.Image.cache)
            ig.Image.cache[b].reload()
    }
});
ig.baked = !0;
ig.module("impact.font").requires("impact.image").defines(function() {
    ig.Font = ig.Image.extend({
        widthMap: [],
        indices: [],
        firstChar: 32,
        alpha: 1,
        letterSpacing: 1,
        lineSpacing: 0,
        onload: function(b) {
            this._loadMetrics(this.data);
            this.parent(b)
        },
        widthForString: function(b) {
            if (-1 !== b.indexOf("\n")) {
                b = b.split("\n");
                for (var c = 0, e = 0; e < b.length; e++)
                    c = Math.max(c, this._widthForLine(b[e]));
                return c
            }
            return this._widthForLine(b)
        },
        _widthForLine: function(b) {
            for (var c = 0, e = 0; e < b.length; e++)
                c += this.widthMap[b.charCodeAt(e) - this.firstChar] + this.letterSpacing;
            return c
        },
        heightForString: function(b) {
            return b.split("\n").length * (this.height + this.lineSpacing)
        },
        draw: function(b, c, e, l) {
            "string" != typeof b && (b = b.toString());
            if (-1 !== b.indexOf("\n")) {
                b = b.split("\n");
                for (var q = this.height + this.lineSpacing, x = 0; x < b.length; x++)
                    this.draw(b[x], c, e + x * q, l)
            } else {
                if (l == ig.Font.ALIGN.RIGHT || l == ig.Font.ALIGN.CENTER)
                    x = this._widthForLine(b),
                    c -= l == ig.Font.ALIGN.CENTER ? x / 2 : x;
                1 !== this.alpha && (ig.system.context.globalAlpha = this.alpha);
                for (x = 0; x < b.length; x++)
                    l = b.charCodeAt(x),
                    c += this._drawChar(l - this.firstChar, c, e);
                1 !== this.alpha && (ig.system.context.globalAlpha = 1);
                ig.Image.drawCount += b.length
            }
        },
        _drawChar: function(b, c, e) {
            if (!this.loaded || 0 > b || b >= this.indices.length)
                return 0;
            var l = ig.system.scale
              , q = this.widthMap[b] * l
              , x = (this.height - 2) * l;
            ig.system.context.drawImage(this.data, this.indices[b] * l, 0, q, x, ig.system.getDrawPos(c), ig.system.getDrawPos(e), q, x);
            return this.widthMap[b] + this.letterSpacing
        },
        _loadMetrics: function(b) {
            this.height = b.height - 1;
            this.widthMap = [];
            this.indices = [];
            for (var c = ig.getImagePixels(b, 0, b.height - 1, b.width, 1), e = 0, l = 0, q = 0; q < b.width; q++) {
                var x = 4 * q + 3;
                127 < c.data[x] ? l++ : 128 > c.data[x] && l && (this.widthMap.push(l),
                this.indices.push(q - l),
                e++,
                l = 0)
            }
            this.widthMap.push(l);
            this.indices.push(q - l)
        }
    });
    ig.Font.ALIGN = {
        LEFT: 0,
        RIGHT: 1,
        CENTER: 2
    }
});
ig.baked = !0;
ig.module("impact.sound").defines(function() {
    ig.SoundManager = ig.Class.extend({
        clips: {},
        volume: 1,
        format: null,
        init: function() {
            if (ig.Sound.enabled && window.Audio) {
                for (var b = new Audio, c = 0; c < ig.Sound.use.length; c++) {
                    var e = ig.Sound.use[c];
                    if (b.canPlayType(e.mime)) {
                        this.format = e;
                        break
                    }
                }
                this.format || (ig.Sound.enabled = !1)
            } else
                ig.Sound.enabled = !1
        },
        load: function(b, c, e) {
            var l = ig.prefix + b.replace(/[^\.]+$/, this.format.ext) + ig.nocache;
            if (this.clips[b]) {
                if (c && this.clips[b].length < ig.Sound.channels)
                    for (c = this.clips[b].length; c < ig.Sound.channels; c++) {
                        var q = new Audio(l);
                        q.load();
                        this.clips[b].push(q)
                    }
                return this.clips[b][0]
            }
            var x = new Audio(l);
            e && (x.addEventListener("canplaythrough", function E(A) {
                x.removeEventListener("canplaythrough", E, !1);
                e(b, !0, A)
            }, !1),
            x.addEventListener("error", function(A) {
                e(b, !1, A)
            }, !1));
            x.preload = "auto";
            x.load();
            this.clips[b] = [x];
            if (c)
                for (c = 1; c < ig.Sound.channels; c++)
                    q = new Audio(l),
                    q.load(),
                    this.clips[b].push(q);
            return x
        },
        get: function(b) {
            b = this.clips[b];
            for (var c = 0, e; e = b[c++]; )
                if (e.paused || e.ended)
                    return e.ended && (e.currentTime = 0),
                    e;
            b[0].pause();
            b[0].currentTime = 0;
            return b[0]
        }
    });
    ig.Music = ig.Class.extend({
        tracks: [],
        namedTracks: {},
        currentTrack: null,
        currentIndex: 0,
        random: !1,
        _volume: 1,
        _loop: !1,
        _fadeInterval: 0,
        _fadeTimer: null,
        _endedCallbackBound: null,
        init: function() {
            this._endedCallbackBound = this._endedCallback.bind(this);
            Object.defineProperty ? (Object.defineProperty(this, "volume", {
                get: this.getVolume.bind(this),
                set: this.setVolume.bind(this)
            }),
            Object.defineProperty(this, "loop", {
                get: this.getLooping.bind(this),
                set: this.setLooping.bind(this)
            })) : this.__defineGetter__ && (this.__defineGetter__("volume", this.getVolume.bind(this)),
            this.__defineSetter__("volume", this.setVolume.bind(this)),
            this.__defineGetter__("loop", this.getLooping.bind(this)),
            this.__defineSetter__("loop", this.setLooping.bind(this)))
        },
        add: function(b, c) {
            ig.Sound.enabled && (b = ig.soundManager.load(b instanceof ig.Sound ? b.path : b, !1),
            b.loop = this._loop,
            b.volume = this._volume,
            b.addEventListener("ended", this._endedCallbackBound, !1),
            this.tracks.push(b),
            c && (this.namedTracks[c] = b),
            this.currentTrack || (this.currentTrack = b))
        },
        next: function() {
            this.tracks.length && (this.stop(),
            this.currentIndex = this.random ? Math.floor(Math.random() * this.tracks.length) : (this.currentIndex + 1) % this.tracks.length,
            this.currentTrack = this.tracks[this.currentIndex],
            this.play())
        },
        pause: function() {
            this.currentTrack && this.currentTrack.pause()
        },
        stop: function() {
            this.currentTrack && (this.currentTrack.pause(),
            this.currentTrack.currentTime = 0)
        },
        play: function(b) {
            if (b && this.namedTracks[b])
                b = this.namedTracks[b],
                b != this.currentTrack && (this.stop(),
                this.currentTrack = b);
            else if (!this.currentTrack)
                return;
            this.currentTrack.play()
        },
        getLooping: function() {
            return this._loop
        },
        setLooping: function(b) {
            this._loop = b;
            for (var c in this.tracks)
                this.tracks[c].loop = b
        },
        getVolume: function() {
            return this._volume
        },
        setVolume: function(b) {
            this._volume = b.limit(0, 1);
            for (var c in this.tracks)
                this.tracks[c].volume = this._volume
        },
        fadeOut: function(b) {
            this.currentTrack && (clearInterval(this._fadeInterval),
            this.fadeTimer = new ig.Timer(b),
            this._fadeInterval = setInterval(this._fadeStep.bind(this), 50))
        },
        _fadeStep: function() {
            var b = this.fadeTimer.delta().map(-this.fadeTimer.target, 0, 1, 0).limit(0, 1) * this._volume;
            .01 >= b ? (this.stop(),
            this.currentTrack.volume = this._volume,
            clearInterval(this._fadeInterval)) : this.currentTrack.volume = b
        },
        _endedCallback: function() {
            this._loop ? this.play() : this.next()
        }
    });
    ig.Sound = ig.Class.extend({
        path: "",
        volume: 1,
        currentClip: null,
        multiChannel: !0,
        init: function(b, c) {
            this.path = b;
            this.multiChannel = !1 !== c;
            this.load()
        },
        load: function(b) {
            ig.Sound.enabled ? ig.ready ? ig.soundManager.load(this.path, this.multiChannel, b) : ig.addResource(this) : b && b(this.path, !0)
        },
        play: function() {
            ig.Sound.enabled && (this.currentClip = ig.soundManager.get(this.path),
            this.currentClip.volume = ig.soundManager.volume * this.volume,
            this.currentClip.play())
        },
        stop: function() {
            this.currentClip && (this.currentClip.pause(),
            this.currentClip.currentTime = 0)
        }
    });
    ig.Sound.FORMAT = {
        MP3: {
            ext: "mp3",
            mime: "audio/mpeg"
        },
        M4A: {
            ext: "m4a",
            mime: "audio/mp4; codecs=mp4a"
        },
        OGG: {
            ext: "ogg",
            mime: "audio/ogg; codecs=vorbis"
        },
        WEBM: {
            ext: "webm",
            mime: "audio/webm; codecs=vorbis"
        },
        CAF: {
            ext: "caf",
            mime: "audio/x-caf"
        }
    };
    ig.Sound.use = [ig.Sound.FORMAT.OGG, ig.Sound.FORMAT.MP3];
    ig.Sound.channels = 4;
    ig.Sound.enabled = !0
});
ig.baked = !0;
ig.module("impact.loader").requires("impact.image", "impact.font", "impact.sound").defines(function() {
    ig.Loader = ig.Class.extend({
        resources: [],
        gameClass: null,
        status: 0,
        done: !1,
        _unloaded: [],
        _drawStatus: 0,
        _intervalId: 0,
        _loadCallbackBound: null,
        init: function(b, c) {
            this.gameClass = b;
            this.resources = c;
            this._loadCallbackBound = this._loadCallback.bind(this);
            for (b = 0; b < this.resources.length; b++)
                this._unloaded.push(this.resources[b].path)
        },
        load: function() {
            ig.system.clear("#000");
            if (this.resources.length) {
                for (var b = 0; b < this.resources.length; b++)
                    this.loadResource(this.resources[b]);
                this._intervalId = setInterval(this.draw.bind(this), 16)
            } else
                this.end()
        },
        loadResource: function(b) {
            b.load(this._loadCallbackBound)
        },
        end: function() {
            this.done || (this.done = !0,
            clearInterval(this._intervalId))
        },
        draw: function() {},
        _loadCallback: function(b, c) {
            if (c)
                this._unloaded.erase(b);
            else
                throw "Failed to load resource: " + b;
            this.status = 1 - this._unloaded.length / this.resources.length;
            0 == this._unloaded.length && setTimeout(this.end.bind(this), 250)
        }
    })
});
ig.baked = !0;
ig.module("impact.timer").defines(function() {
    ig.Timer = ig.Class.extend({
        target: 0,
        base: 0,
        last: 0,
        pausedAt: 0,
        init: function(b) {
            this.last = this.base = ig.Timer.time;
            this.target = b || 0
        },
        set: function(b) {
            this.target = b || 0;
            this.base = ig.Timer.time;
            this.pausedAt = 0
        },
        reset: function() {
            this.base = ig.Timer.time;
            this.pausedAt = 0
        },
        tick: function() {
            var b = ig.Timer.time - this.last;
            this.last = ig.Timer.time;
            return this.pausedAt ? 0 : b
        },
        delta: function() {
            return (this.pausedAt || ig.Timer.time) - this.base - this.target
        },
        pause: function() {
            this.pausedAt || (this.pausedAt = ig.Timer.time)
        },
        unpause: function() {
            this.pausedAt && (this.base += ig.Timer.time - this.pausedAt,
            this.pausedAt = 0)
        }
    });
    ig.Timer._last = 0;
    ig.Timer.time = Number.MIN_VALUE;
    ig.Timer.timeScale = 1;
    ig.Timer.maxStep = .05;
    ig.Timer.step = function() {
        var b = Date.now();
        ig.Timer.time += Math.min((b - ig.Timer._last) / 1E3, ig.Timer.maxStep) * ig.Timer.timeScale;
        ig.Timer._last = b
    }
});
ig.baked = !0;
ig.module("impact.system").requires("impact.timer", "impact.image").defines(function() {
    ig.System = ig.Class.extend({
        fps: 30,
        width: 320,
        height: 240,
        realWidth: 320,
        realHeight: 240,
        scale: 1,
        tick: 0,
        animationId: 0,
        newGameClass: null,
        running: !1,
        delegate: null,
        clock: null,
        canvas: null,
        context: null,
        init: function(b, c, e, l, q) {
            this.fps = c;
            this.clock = new ig.Timer;
            this.canvas = ig.$(b);
            this.resize(e, l, q);
            this.context = this.canvas.getContext("2d");
            this.getDrawPos = ig.System.drawMode;
            1 != this.scale && (ig.System.scaleMode = ig.System.SCALE.CRISP);
            ig.System.scaleMode(this.canvas, this.context)
        },
        resize: function(b, c, e) {
            this.width = b;
            this.height = c;
            this.scale = e || this.scale;
            this.realWidth = this.width * this.scale;
            this.realHeight = this.height * this.scale;
            this.canvas.width = this.realWidth;
            this.canvas.height = this.realHeight
        },
        setGame: function(b) {
            this.running ? this.newGameClass = b : this.setGameNow(b)
        },
        setGameNow: function(b) {
            ig.game = new b;
            ig.system.setDelegate(ig.game)
        },
        setDelegate: function(b) {
            if ("function" == typeof b.run)
                this.delegate = b,
                this.startRunLoop();
            else
                throw "System.setDelegate: No run() function in object";
        },
        stopRunLoop: function() {
            ig.clearAnimation(this.animationId);
            this.running = !1
        },
        startRunLoop: function() {
            this.stopRunLoop();
            this.animationId = ig.setAnimation(this.run.bind(this));
            this.running = !0
        },
        clear: function(b) {
            this.context.fillStyle = b;
            this.context.fillRect(0, 0, this.realWidth, this.realHeight)
        },
        run: function() {
            ig.Timer.step();
            this.tick = this.clock.tick();
            this.delegate.run();
            ig.input.clearPressed();
            this.newGameClass && (this.setGameNow(this.newGameClass),
            this.newGameClass = null)
        },
        getDrawPos: null
    });
    ig.System.DRAW = {
        AUTHENTIC: function(b) {
            return Math.round(b) * this.scale
        },
        SMOOTH: function(b) {
            return Math.round(b * this.scale)
        },
        SUBPIXEL: function(b) {
            return b * this.scale
        }
    };
    ig.System.drawMode = ig.System.DRAW.SMOOTH;
    ig.System.SCALE = {
        CRISP: function(b, c) {
            ig.setVendorAttribute(c, "imageSmoothingEnabled", !1);
            b.style.imageRendering = "-moz-crisp-edges";
            b.style.imageRendering = "-o-crisp-edges";
            b.style.imageRendering = "-webkit-optimize-contrast";
            b.style.imageRendering = "crisp-edges";
            b.style.msInterpolationMode = "nearest-neighbor"
        },
        SMOOTH: function(b, c) {
            ig.setVendorAttribute(c, "imageSmoothingEnabled", !0);
            b.style.imageRendering = "";
            b.style.msInterpolationMode = ""
        }
    };
    ig.System.scaleMode = ig.System.SCALE.SMOOTH
});
ig.baked = !0;
ig.module("impact.input").defines(function() {
    ig.KEY = {
        MOUSE1: -1,
        MOUSE2: -3,
        MWHEEL_UP: -4,
        MWHEEL_DOWN: -5,
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        PAUSE: 19,
        CAPS: 20,
        ESC: 27,
        SPACE: 32,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        END: 35,
        HOME: 36,
        LEFT_ARROW: 37,
        UP_ARROW: 38,
        RIGHT_ARROW: 39,
        DOWN_ARROW: 40,
        INSERT: 45,
        DELETE: 46,
        _0: 48,
        _1: 49,
        _2: 50,
        _3: 51,
        _4: 52,
        _5: 53,
        _6: 54,
        _7: 55,
        _8: 56,
        _9: 57,
        A: 65,
        B: 66,
        C: 67,
        D: 68,
        E: 69,
        F: 70,
        G: 71,
        H: 72,
        I: 73,
        J: 74,
        K: 75,
        L: 76,
        M: 77,
        N: 78,
        O: 79,
        P: 80,
        Q: 81,
        R: 82,
        S: 83,
        T: 84,
        U: 85,
        V: 86,
        W: 87,
        X: 88,
        Y: 89,
        Z: 90,
        NUMPAD_0: 96,
        NUMPAD_1: 97,
        NUMPAD_2: 98,
        NUMPAD_3: 99,
        NUMPAD_4: 100,
        NUMPAD_5: 101,
        NUMPAD_6: 102,
        NUMPAD_7: 103,
        NUMPAD_8: 104,
        NUMPAD_9: 105,
        MULTIPLY: 106,
        ADD: 107,
        SUBSTRACT: 109,
        DECIMAL: 110,
        DIVIDE: 111,
        F1: 112,
        F2: 113,
        F3: 114,
        F4: 115,
        F5: 116,
        F6: 117,
        F7: 118,
        F8: 119,
        F9: 120,
        F10: 121,
        F11: 122,
        F12: 123,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        PLUS: 187,
        COMMA: 188,
        MINUS: 189,
        PERIOD: 190
    };
    ig.Input = ig.Class.extend({
        bindings: {},
        actions: {},
        presses: {},
        locks: {},
        delayedKeyup: {},
        isUsingMouse: !1,
        isUsingKeyboard: !1,
        isUsingAccelerometer: !1,
        mouse: {
            x: 0,
            y: 0
        },
        accel: {
            x: 0,
            y: 0,
            z: 0
        },
        initMouse: function() {
            if (!this.isUsingMouse) {
                this.isUsingMouse = !0;
                var b = this.mousewheel.bind(this);
                ig.system.canvas.addEventListener("wheel", b, !1);
                ig.system.canvas.addEventListener("mousewheel", b, !1);
                ig.system.canvas.addEventListener("DOMMouseScroll", b, !1);
                ig.system.canvas.addEventListener("contextmenu", this.contextmenu.bind(this), !1);
                ig.system.canvas.addEventListener("mousedown", this.keydown.bind(this), !1);
                ig.system.canvas.addEventListener("mouseup", this.keyup.bind(this), !1);
                ig.system.canvas.addEventListener("mousemove", this.mousemove.bind(this), !1);
                ig.ua.touchDevice && (ig.system.canvas.addEventListener("touchstart", this.keydown.bind(this), !1),
                ig.system.canvas.addEventListener("touchend", this.keyup.bind(this), !1),
                ig.system.canvas.addEventListener("touchmove", this.mousemove.bind(this), !1),
                ig.system.canvas.addEventListener("MSPointerDown", this.keydown.bind(this), !1),
                ig.system.canvas.addEventListener("MSPointerUp", this.keyup.bind(this), !1),
                ig.system.canvas.addEventListener("MSPointerMove", this.mousemove.bind(this), !1),
                ig.system.canvas.style.msTouchAction = "none")
            }
        },
        initKeyboard: function() {
            this.isUsingKeyboard || (this.isUsingKeyboard = !0,
            window.addEventListener("keydown", this.keydown.bind(this), !1),
            window.addEventListener("keyup", this.keyup.bind(this), !1))
        },
        initAccelerometer: function() {
            this.isUsingAccelerometer || window.addEventListener("devicemotion", this.devicemotion.bind(this), !1)
        },
        mousewheel: function(b) {
            var c = this.bindings[0 < (b.wheelDelta ? b.wheelDelta : -1 * b.detail) ? ig.KEY.MWHEEL_UP : ig.KEY.MWHEEL_DOWN];
            c && (this.actions[c] = !0,
            this.presses[c] = !0,
            this.delayedKeyup[c] = !0,
            b.stopPropagation(),
            b.preventDefault())
        },
        mousemove: function(b) {
            var c = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth;
            c = c / ig.system.realWidth * ig.system.scale;
            var e = {
                left: 0,
                top: 0
            };
            ig.system.canvas.getBoundingClientRect && (e = ig.system.canvas.getBoundingClientRect());
            b = b.touches ? b.touches[0] : b;
            this.mouse.x = (b.clientX - e.left) / c;
            this.mouse.y = (b.clientY - e.top) / c
        },
        contextmenu: function(b) {
            this.bindings[ig.KEY.MOUSE2] && (b.stopPropagation(),
            b.preventDefault())
        },
        keydown: function(b) {
            var c = b.target.tagName;
            "INPUT" != c && "TEXTAREA" != c && (c = "keydown" == b.type ? b.keyCode : 2 == b.button ? ig.KEY.MOUSE2 : ig.KEY.MOUSE1,
            "touchstart" != b.type && "mousedown" != b.type || this.mousemove(b),
            c = this.bindings[c]) && (this.actions[c] = !0,
            this.locks[c] || (this.presses[c] = !0,
            this.locks[c] = !0),
            b.stopPropagation(),
            b.preventDefault())
        },
        keyup: function(b) {
            var c = b.target.tagName;
            "INPUT" != c && "TEXTAREA" != c && (c = this.bindings["keyup" == b.type ? b.keyCode : 2 == b.button ? ig.KEY.MOUSE2 : ig.KEY.MOUSE1]) && (this.delayedKeyup[c] = !0,
            b.stopPropagation(),
            b.preventDefault())
        },
        devicemotion: function(b) {
            this.accel = b.accelerationIncludingGravity
        },
        bind: function(b, c) {
            0 > b ? this.initMouse() : 0 < b && this.initKeyboard();
            this.bindings[b] = c
        },
        bindTouch: function(b, c) {
            b = ig.$(b);
            var e = this;
            b.addEventListener("touchstart", function(l) {
                e.touchStart(l, c)
            }, !1);
            b.addEventListener("touchend", function(l) {
                e.touchEnd(l, c)
            }, !1);
            b.addEventListener("MSPointerDown", function(l) {
                e.touchStart(l, c)
            }, !1);
            b.addEventListener("MSPointerUp", function(l) {
                e.touchEnd(l, c)
            }, !1)
        },
        unbind: function(b) {
            this.delayedKeyup[this.bindings[b]] = !0;
            this.bindings[b] = null
        },
        unbindAll: function() {
            this.bindings = {};
            this.actions = {};
            this.presses = {};
            this.locks = {};
            this.delayedKeyup = {}
        },
        state: function(b) {
            return this.actions[b]
        },
        pressed: function(b) {
            return this.presses[b]
        },
        released: function(b) {
            return !!this.delayedKeyup[b]
        },
        clearPressed: function() {
            for (var b in this.delayedKeyup)
                this.actions[b] = !1,
                this.locks[b] = !1;
            this.delayedKeyup = {};
            this.presses = {}
        },
        touchStart: function(b, c) {
            this.actions[c] = !0;
            this.presses[c] = !0;
            b.stopPropagation();
            b.preventDefault();
            return !1
        },
        touchEnd: function(b, c) {
            this.delayedKeyup[c] = !0;
            b.stopPropagation();
            b.preventDefault();
            return !1
        }
    })
});
ig.baked = !0;
ig.module("impact.impact").requires("dom.ready", "impact.loader", "impact.system", "impact.input", "impact.sound").defines(function() {
    ig.main = function(b, c, e, l, q, x, C) {
        ig.system = new ig.System(b,e,l,q,x || 1);
        ig.input = new ig.Input;
        ig.soundManager = new ig.SoundManager;
        ig.music = new ig.Music;
        ig.ready = !0;
        (new (C || ig.Loader)(c,ig.resources)).load()
    }
});
ig.baked = !0;
ig.module("impact.animation").requires("impact.timer", "impact.image").defines(function() {
    ig.AnimationSheet = ig.Class.extend({
        width: 8,
        height: 8,
        image: null,
        init: function(b, c, e) {
            this.width = c;
            this.height = e;
            this.image = new ig.Image(b)
        }
    });
    ig.Animation = ig.Class.extend({
        sheet: null,
        timer: null,
        sequence: [],
        flip: {
            x: !1,
            y: !1
        },
        pivot: {
            x: 0,
            y: 0
        },
        frame: 0,
        tile: 0,
        loopCount: 0,
        alpha: 1,
        angle: 0,
        init: function(b, c, e, l) {
            this.sheet = b;
            this.pivot = {
                x: b.width / 2,
                y: b.height / 2
            };
            this.timer = new ig.Timer;
            this.frameTime = c;
            this.sequence = e;
            this.stop = !!l;
            this.tile = this.sequence[0]
        },
        rewind: function() {
            this.timer.set();
            this.frame = this.loopCount = 0;
            this.tile = this.sequence[0];
            return this
        },
        gotoFrame: function(b) {
            this.timer.set(this.frameTime * -b - 1E-4);
            this.update()
        },
        gotoRandomFrame: function() {
            this.gotoFrame(Math.floor(Math.random() * this.sequence.length))
        },
        update: function() {
            var b = Math.floor(this.timer.delta() / this.frameTime);
            this.loopCount = Math.floor(b / this.sequence.length);
            this.frame = this.stop && 0 < this.loopCount ? this.sequence.length - 1 : b % this.sequence.length;
            this.tile = this.sequence[this.frame]
        },
        draw: function(b, c) {
            var e = Math.max(this.sheet.width, this.sheet.height);
            b > ig.system.width || c > ig.system.height || 0 > b + e || 0 > c + e || (1 != this.alpha && (ig.system.context.globalAlpha = this.alpha),
            0 == this.angle ? this.sheet.image.drawTile(b, c, this.tile, this.sheet.width, this.sheet.height, this.flip.x, this.flip.y) : (ig.system.context.save(),
            ig.system.context.translate(ig.system.getDrawPos(b + this.pivot.x), ig.system.getDrawPos(c + this.pivot.y)),
            ig.system.context.rotate(this.angle),
            this.sheet.image.drawTile(-this.pivot.x, -this.pivot.y, this.tile, this.sheet.width, this.sheet.height, this.flip.x, this.flip.y),
            ig.system.context.restore()),
            1 != this.alpha && (ig.system.context.globalAlpha = 1))
        }
    })
});
ig.baked = !0;
ig.module("impact.entity").requires("impact.animation", "impact.impact").defines(function() {
    ig.Entity = ig.Class.extend({
        id: 0,
        settings: {},
        size: {
            x: 16,
            y: 16
        },
        offset: {
            x: 0,
            y: 0
        },
        pos: {
            x: 0,
            y: 0
        },
        last: {
            x: 0,
            y: 0
        },
        vel: {
            x: 0,
            y: 0
        },
        accel: {
            x: 0,
            y: 0
        },
        friction: {
            x: 0,
            y: 0
        },
        maxVel: {
            x: 100,
            y: 100
        },
        zIndex: 0,
        gravityFactor: 1,
        standing: !1,
        bounciness: 0,
        minBounceVelocity: 40,
        anims: {},
        animSheet: null,
        currentAnim: null,
        health: 10,
        type: 0,
        checkAgainst: 0,
        collides: 0,
        _killed: !1,
        slopeStanding: {
            min: (44).toRad(),
            max: (136).toRad()
        },
        init: function(b, c, e) {
            this.id = ++ig.Entity._lastId;
            this.pos.x = this.last.x = b;
            this.pos.y = this.last.y = c;
            ig.merge(this, e)
        },
        reset: function(b, c, e) {
            var l = this.constructor.prototype;
            this.pos.x = b;
            this.pos.y = c;
            this.last.x = b;
            this.last.y = c;
            this.vel.x = l.vel.x;
            this.vel.y = l.vel.y;
            this.accel.x = l.accel.x;
            this.accel.y = l.accel.y;
            this.health = l.health;
            this._killed = l._killed;
            this.standing = l.standing;
            this.type = l.type;
            this.checkAgainst = l.checkAgainst;
            this.collides = l.collides;
            ig.merge(this, e)
        },
        addAnim: function(b, c, e, l) {
            if (!this.animSheet)
                throw "No animSheet to add the animation " + b + " to.";
            c = new ig.Animation(this.animSheet,c,e,l);
            this.anims[b] = c;
            this.currentAnim || (this.currentAnim = c);
            return c
        },
        update: function() {
            if (null != ig && null != ig.game) {
                this.last.x = this.pos.x;
                this.last.y = this.pos.y;
                this.vel.y += ig.game.gravity * ig.system.tick * this.gravityFactor;
                this.vel.x = this.getNewVelocity(this.vel.x, this.accel.x, this.friction.x, this.maxVel.x);
                this.vel.y = this.getNewVelocity(this.vel.y, this.accel.y, this.friction.y, this.maxVel.y);
                var b = ig.game.collisionMap.trace(this.pos.x, this.pos.y, this.vel.x * ig.system.tick, this.vel.y * ig.system.tick, this.size.x, this.size.y);
                this.handleMovementTrace(b);
                this.currentAnim && this.currentAnim.update()
            }
        },
        getNewVelocity: function(b, c, e, l) {
            return c ? (b + c * ig.system.tick).limit(-l, l) : e ? (c = e * ig.system.tick,
            0 < b - c ? b - c : 0 > b + c ? b + c : 0) : b.limit(-l, l)
        },
        handleMovementTrace: function(b) {
            this.standing = !1;
            b.collision.y && (0 < this.bounciness && Math.abs(this.vel.y) > this.minBounceVelocity ? this.vel.y *= -this.bounciness : (0 < this.vel.y && (this.standing = !0),
            this.vel.y = 0));
            b.collision.x && (this.vel.x = 0 < this.bounciness && Math.abs(this.vel.x) > this.minBounceVelocity ? this.vel.x * -this.bounciness : 0);
            if (b.collision.slope) {
                var c = b.collision.slope;
                if (0 < this.bounciness) {
                    var e = this.vel.x * c.nx + this.vel.y * c.ny;
                    this.vel.x = (this.vel.x - c.nx * e * 2) * this.bounciness;
                    this.vel.y = (this.vel.y - c.ny * e * 2) * this.bounciness
                } else
                    e = (this.vel.x * c.x + this.vel.y * c.y) / (c.x * c.x + c.y * c.y),
                    this.vel.x = c.x * e,
                    this.vel.y = c.y * e,
                    c = Math.atan2(c.x, c.y),
                    c > this.slopeStanding.min && c < this.slopeStanding.max && (this.standing = !0)
            }
            this.pos = b.pos
        },
        draw: function() {
            this.currentAnim && this.currentAnim.draw(this.pos.x - this.offset.x - ig.game._rscreen.x, this.pos.y - this.offset.y - ig.game._rscreen.y)
        },
        kill: function() {
            ig.game.removeEntity(this)
        },
        receiveDamage: function(b, c) {
            this.health -= b;
            0 >= this.health && this.kill()
        },
        touches: function(b) {
            return !(this.pos.x >= b.pos.x + b.size.x || this.pos.x + this.size.x <= b.pos.x || this.pos.y >= b.pos.y + b.size.y || this.pos.y + this.size.y <= b.pos.y)
        },
        distanceTo: function(b) {
            var c = this.pos.x + this.size.x / 2 - (b.pos.x + b.size.x / 2);
            b = this.pos.y + this.size.y / 2 - (b.pos.y + b.size.y / 2);
            return Math.sqrt(c * c + b * b)
        },
        angleTo: function(b) {
            return Math.atan2(b.pos.y + b.size.y / 2 - (this.pos.y + this.size.y / 2), b.pos.x + b.size.x / 2 - (this.pos.x + this.size.x / 2))
        },
        check: function(b) {},
        collideWith: function(b, c) {},
        ready: function() {},
        erase: function() {}
    });
    ig.Entity._lastId = 0;
    ig.Entity.COLLIDES = {
        NEVER: 0,
        LITE: 1,
        PASSIVE: 2,
        ACTIVE: 4,
        FIXED: 8
    };
    ig.Entity.TYPE = {
        NONE: 0,
        A: 1,
        B: 2,
        BOTH: 3
    };
    ig.Entity.checkPair = function(b, c) {
        b.checkAgainst & c.type && b.check(c);
        c.checkAgainst & b.type && c.check(b);
        b.collides && c.collides && b.collides + c.collides > ig.Entity.COLLIDES.ACTIVE && ig.Entity.solveCollision(b, c)
    }
    ;
    ig.Entity.solveCollision = function(b, c) {
        var e = null;
        if (b.collides == ig.Entity.COLLIDES.LITE || c.collides == ig.Entity.COLLIDES.FIXED)
            e = b;
        else if (c.collides == ig.Entity.COLLIDES.LITE || b.collides == ig.Entity.COLLIDES.FIXED)
            e = c;
        b.last.x + b.size.x > c.last.x && b.last.x < c.last.x + c.size.x ? (b.last.y < c.last.y ? ig.Entity.seperateOnYAxis(b, c, e) : ig.Entity.seperateOnYAxis(c, b, e),
        b.collideWith(c, "y"),
        c.collideWith(b, "y")) : b.last.y + b.size.y > c.last.y && b.last.y < c.last.y + c.size.y && (b.last.x < c.last.x ? ig.Entity.seperateOnXAxis(b, c, e) : ig.Entity.seperateOnXAxis(c, b, e),
        b.collideWith(c, "x"),
        c.collideWith(b, "x"))
    }
    ;
    ig.Entity.seperateOnXAxis = function(b, c, e) {
        var l = b.pos.x + b.size.x - c.pos.x;
        e ? (e.vel.x = -e.vel.x * e.bounciness + (b === e ? c : b).vel.x,
        c = ig.game.collisionMap.trace(e.pos.x, e.pos.y, e == b ? -l : l, 0, e.size.x, e.size.y),
        e.pos.x = c.pos.x) : (e = (b.vel.x - c.vel.x) / 2,
        b.vel.x = -e,
        c.vel.x = e,
        e = ig.game.collisionMap.trace(b.pos.x, b.pos.y, -l / 2, 0, b.size.x, b.size.y),
        b.pos.x = Math.floor(e.pos.x),
        b = ig.game.collisionMap.trace(c.pos.x, c.pos.y, l / 2, 0, c.size.x, c.size.y),
        c.pos.x = Math.ceil(b.pos.x))
    }
    ;
    ig.Entity.seperateOnYAxis = function(b, c, e) {
        var l = b.pos.y + b.size.y - c.pos.y;
        if (e) {
            c = b === e ? c : b;
            e.vel.y = -e.vel.y * e.bounciness + c.vel.y;
            var q = 0;
            e == b && Math.abs(e.vel.y - c.vel.y) < e.minBounceVelocity && (e.standing = !0,
            q = c.vel.x * ig.system.tick);
            b = ig.game.collisionMap.trace(e.pos.x, e.pos.y, q, e == b ? -l : l, e.size.x, e.size.y);
            e.pos.y = b.pos.y;
            e.pos.x = b.pos.x
        } else
            ig.game.gravity && (c.standing || 0 < b.vel.y) ? (e = ig.game.collisionMap.trace(b.pos.x, b.pos.y, 0, -(b.pos.y + b.size.y - c.pos.y), b.size.x, b.size.y),
            b.pos.y = e.pos.y,
            0 < b.bounciness && b.vel.y > b.minBounceVelocity ? b.vel.y *= -b.bounciness : (b.standing = !0,
            b.vel.y = 0)) : (e = (b.vel.y - c.vel.y) / 2,
            b.vel.y = -e,
            c.vel.y = e,
            q = c.vel.x * ig.system.tick,
            e = ig.game.collisionMap.trace(b.pos.x, b.pos.y, q, -l / 2, b.size.x, b.size.y),
            b.pos.y = e.pos.y,
            b = ig.game.collisionMap.trace(c.pos.x, c.pos.y, 0, l / 2, c.size.x, c.size.y),
            c.pos.y = b.pos.y)
    }
});
ig.baked = !0;
ig.module("impact.map").defines(function() {
    ig.Map = ig.Class.extend({
        tilesize: 8,
        width: 1,
        height: 1,
        data: [[]],
        name: null,
        init: function(b, c) {
            this.tilesize = b;
            this.data = c;
            this.height = c.length;
            this.width = c[0].length;
            this.pxWidth = this.width * this.tilesize;
            this.pxHeight = this.height * this.tilesize
        },
        getTile: function(b, c) {
            b = Math.floor(b / this.tilesize);
            c = Math.floor(c / this.tilesize);
            return 0 <= b && b < this.width && 0 <= c && c < this.height ? this.data[c][b] : 0
        },
        setTile: function(b, c, e) {
            b = Math.floor(b / this.tilesize);
            c = Math.floor(c / this.tilesize);
            0 <= b && b < this.width && 0 <= c && c < this.height && (this.data[c][b] = e)
        }
    })
});
ig.baked = !0;
ig.module("impact.collision-map").requires("impact.map").defines(function() {
    ig.CollisionMap = ig.Map.extend({
        lastSlope: 1,
        tiledef: null,
        init: function(e, l, q) {
            this.parent(e, l);
            this.tiledef = q || ig.CollisionMap.defaultTileDef;
            for (var x in this.tiledef)
                x | 0 > this.lastSlope && (this.lastSlope = x | 0)
        },
        trace: function(e, l, q, x, C, A) {
            var E = {
                collision: {
                    x: !1,
                    y: !1,
                    slope: !1
                },
                pos: {
                    x: e,
                    y: l
                },
                tile: {
                    x: 0,
                    y: 0
                }
            }
              , N = Math.ceil(Math.max(Math.abs(q), Math.abs(x)) / this.tilesize);
            if (1 < N)
                for (var g = q / N, r = x / N, y = 0; y < N && (g || r) && (this._traceStep(E, e, l, g, r, C, A, q, x, y),
                e = E.pos.x,
                l = E.pos.y,
                E.collision.x && (q = g = 0),
                E.collision.y && (x = r = 0),
                !E.collision.slope); y++)
                    ;
            else
                this._traceStep(E, e, l, q, x, C, A, q, x, 0);
            return E
        },
        _traceStep: function(e, l, q, x, C, A, E, N, g, r) {
            e.pos.x += x;
            e.pos.y += C;
            if (x) {
                var y = 0 < x ? A : 0
                  , F = 0 > x ? this.tilesize : 0
                  , G = Math.max(Math.floor(q / this.tilesize), 0)
                  , M = Math.min(Math.ceil((q + E) / this.tilesize), this.height);
                x = Math.floor((e.pos.x + y) / this.tilesize);
                var O = Math.floor((l + y) / this.tilesize);
                if (0 < r || x == O || 0 > O || O >= this.width)
                    O = -1;
                if (0 <= x && x < this.width)
                    for (; G < M; G++) {
                        if (-1 != O) {
                            var V = this.data[G][O];
                            if (1 < V && V <= this.lastSlope && this._checkTileDef(e, V, l, q, N, g, A, E, O, G))
                                break
                        }
                        V = this.data[G][x];
                        if (1 == V || V > this.lastSlope || 1 < V && this._checkTileDef(e, V, l, q, N, g, A, E, x, G)) {
                            if (1 < V && V <= this.lastSlope && e.collision.slope)
                                break;
                            e.collision.x = !0;
                            e.tile.x = V;
                            l = e.pos.x = x * this.tilesize - y + F;
                            N = 0;
                            break
                        }
                    }
            }
            if (C) {
                y = 0 < C ? E : 0;
                C = 0 > C ? this.tilesize : 0;
                x = Math.max(Math.floor(e.pos.x / this.tilesize), 0);
                F = Math.min(Math.ceil((e.pos.x + A) / this.tilesize), this.width);
                G = Math.floor((e.pos.y + y) / this.tilesize);
                M = Math.floor((q + y) / this.tilesize);
                if (0 < r || G == M || 0 > M || M >= this.height)
                    M = -1;
                if (0 <= G && G < this.height)
                    for (; x < F && !(-1 != M && (V = this.data[M][x],
                    1 < V && V <= this.lastSlope && this._checkTileDef(e, V, l, q, N, g, A, E, x, M))); x++)
                        if (V = this.data[G][x],
                        1 == V || V > this.lastSlope || 1 < V && this._checkTileDef(e, V, l, q, N, g, A, E, x, G)) {
                            if (1 < V && V <= this.lastSlope && e.collision.slope)
                                break;
                            e.collision.y = !0;
                            e.tile.y = V;
                            e.pos.y = G * this.tilesize - y + C;
                            break
                        }
            }
        },
        _checkTileDef: function(e, l, q, x, C, A, E, N, g, r) {
            var y = this.tiledef[l];
            if (!y)
                return !1;
            l = (y[2] - y[0]) * this.tilesize;
            var F = (y[3] - y[1]) * this.tilesize
              , G = y[4];
            E = q + C + (0 > F ? E : 0) - (g + y[0]) * this.tilesize;
            N = x + A + (0 < l ? N : 0) - (r + y[1]) * this.tilesize;
            if (0 < l * N - F * E) {
                if (0 > C * -F + A * l)
                    return G;
                g = Math.sqrt(l * l + F * F);
                r = F / g;
                g = -l / g;
                var M = E * r + N * g;
                y = r * M;
                M *= g;
                if (y * y + M * M >= C * C + A * A)
                    return G || .5 > l * (N - A) - F * (E - C);
                e.pos.x = q + C - y;
                e.pos.y = x + A - M;
                e.collision.slope = {
                    x: l,
                    y: F,
                    nx: r,
                    ny: g
                };
                return !0
            }
            return !1
        }
    });
    var b = 1 / 3
      , c = 2 / 3;
    ig.CollisionMap.defaultTileDef = {
        5: [0, 1, 1, c, !0],
        6: [0, c, 1, b, !0],
        7: [0, b, 1, 0, !0],
        3: [0, 1, 1, .5, !0],
        4: [0, .5, 1, 0, !0],
        2: [0, 1, 1, 0, !0],
        10: [.5, 1, 1, 0, !0],
        21: [0, 1, .5, 0, !0],
        32: [c, 1, 1, 0, !0],
        43: [b, 1, c, 0, !0],
        54: [0, 1, b, 0, !0],
        27: [0, 0, 1, b, !0],
        28: [0, b, 1, c, !0],
        29: [0, c, 1, 1, !0],
        25: [0, 0, 1, .5, !0],
        26: [0, .5, 1, 1, !0],
        24: [0, 0, 1, 1, !0],
        11: [0, 0, .5, 1, !0],
        22: [.5, 0, 1, 1, !0],
        33: [0, 0, b, 1, !0],
        44: [b, 0, c, 1, !0],
        55: [c, 0, 1, 1, !0],
        16: [1, b, 0, 0, !0],
        17: [1, c, 0, b, !0],
        18: [1, 1, 0, c, !0],
        14: [1, .5, 0, 0, !0],
        15: [1, 1, 0, .5, !0],
        13: [1, 1, 0, 0, !0],
        8: [.5, 1, 0, 0, !0],
        19: [1, 1, .5, 0, !0],
        30: [b, 1, 0, 0, !0],
        41: [c, 1, b, 0, !0],
        52: [1, 1, c, 0, !0],
        38: [1, c, 0, 1, !0],
        39: [1, b, 0, c, !0],
        40: [1, 0, 0, b, !0],
        36: [1, .5, 0, 1, !0],
        37: [1, 0, 0, .5, !0],
        35: [1, 0, 0, 1, !0],
        9: [1, 0, .5, 1, !0],
        20: [.5, 0, 0, 1, !0],
        31: [1, 0, c, 1, !0],
        42: [c, 0, b, 1, !0],
        53: [b, 0, 0, 1, !0],
        12: [0, 0, 1, 0, !1],
        23: [1, 1, 0, 1, !1],
        34: [1, 0, 1, 1, !1],
        45: [0, 1, 0, 0, !1]
    };
    ig.CollisionMap.staticNoCollision = {
        trace: function(e, l, q, x) {
            return {
                collision: {
                    x: !1,
                    y: !1,
                    slope: !1
                },
                pos: {
                    x: e + q,
                    y: l + x
                },
                tile: {
                    x: 0,
                    y: 0
                }
            }
        }
    }
});
ig.baked = !0;
ig.module("impact.background-map").requires("impact.map", "impact.image").defines(function() {
    ig.BackgroundMap = ig.Map.extend({
        tiles: null,
        scroll: {
            x: 0,
            y: 0
        },
        distance: 1,
        repeat: !1,
        tilesetName: "",
        foreground: !1,
        enabled: !0,
        preRender: !1,
        preRenderedChunks: null,
        chunkSize: 512,
        debugChunks: !1,
        anims: {},
        init: function(b, c, e) {
            this.parent(b, c);
            this.setTileset(e)
        },
        setTileset: function(b) {
            this.tilesetName = b instanceof ig.Image ? b.path : b;
            this.tiles = new ig.Image(this.tilesetName);
            this.preRenderedChunks = null
        },
        setScreenPos: function(b, c) {
            this.scroll.x = b / this.distance;
            this.scroll.y = c / this.distance
        },
        preRenderMapToChunks: function() {
            var b = this.width * this.tilesize * ig.system.scale
              , c = this.height * this.tilesize * ig.system.scale;
            this.chunkSize = Math.min(Math.max(b, c), this.chunkSize);
            var e = Math.ceil(b / this.chunkSize)
              , l = Math.ceil(c / this.chunkSize);
            this.preRenderedChunks = [];
            for (var q = 0; q < l; q++) {
                this.preRenderedChunks[q] = [];
                for (var x = 0; x < e; x++)
                    this.preRenderedChunks[q][x] = this.preRenderChunk(x, q, x == e - 1 ? b - x * this.chunkSize : this.chunkSize, q == l - 1 ? c - q * this.chunkSize : this.chunkSize)
            }
        },
        preRenderChunk: function(b, c, e, l) {
            var q = e / this.tilesize / ig.system.scale + 1
              , x = l / this.tilesize / ig.system.scale + 1
              , C = b * this.chunkSize / ig.system.scale % this.tilesize
              , A = c * this.chunkSize / ig.system.scale % this.tilesize;
            b = Math.floor(b * this.chunkSize / this.tilesize / ig.system.scale);
            c = Math.floor(c * this.chunkSize / this.tilesize / ig.system.scale);
            var E = ig.$new("canvas");
            E.width = e;
            E.height = l;
            E.retinaResolutionEnabled = !1;
            l = E.getContext("2d");
            ig.System.scaleMode(E, l);
            e = ig.system.context;
            ig.system.context = l;
            for (l = 0; l < q; l++)
                for (var N = 0; N < x; N++)
                    if (l + b < this.width && N + c < this.height) {
                        var g = this.data[N + c][l + b];
                        g && this.tiles.drawTile(l * this.tilesize - C, N * this.tilesize - A, g - 1, this.tilesize)
                    }
            ig.system.context = e;
            return E
        },
        draw: function() {
            this.tiles.loaded && this.enabled && (this.preRender ? this.drawPreRendered() : this.drawTiled())
        },
        drawPreRendered: function() {
            this.preRenderedChunks || this.preRenderMapToChunks();
            var b = ig.system.getDrawPos(this.scroll.x)
              , c = ig.system.getDrawPos(this.scroll.y);
            if (this.repeat) {
                var e = this.width * this.tilesize * ig.system.scale;
                b = (b % e + e) % e;
                e = this.height * this.tilesize * ig.system.scale;
                c = (c % e + e) % e
            }
            e = Math.max(Math.floor(b / this.chunkSize), 0);
            var l = Math.max(Math.floor(c / this.chunkSize), 0)
              , q = Math.ceil((b + ig.system.realWidth) / this.chunkSize)
              , x = Math.ceil((c + ig.system.realHeight) / this.chunkSize)
              , C = this.preRenderedChunks[0].length
              , A = this.preRenderedChunks.length;
            this.repeat || (q = Math.min(q, C),
            x = Math.min(x, A));
            for (var E = 0; l < x; l++) {
                for (var N = 0, g = e; g < q; g++) {
                    var r = this.preRenderedChunks[l % A][g % C]
                      , y = -b + g * this.chunkSize - N
                      , F = -c + l * this.chunkSize - E;
                    ig.system.context.drawImage(r, y, F);
                    ig.Image.drawCount++;
                    this.debugChunks && (ig.system.context.strokeStyle = "#f0f",
                    ig.system.context.strokeRect(y, F, this.chunkSize, this.chunkSize));
                    this.repeat && r.width < this.chunkSize && y + r.width < ig.system.realWidth && (N += this.chunkSize - r.width,
                    q++)
                }
                this.repeat && r.height < this.chunkSize && F + r.height < ig.system.realHeight && (E += this.chunkSize - r.height,
                x++)
            }
        },
        drawTiled: function() {
            var b, c = (this.scroll.x / this.tilesize).toInt(), e = (this.scroll.y / this.tilesize).toInt(), l = this.scroll.x % this.tilesize, q = this.scroll.y % this.tilesize, x = -l - this.tilesize;
            l = ig.system.width + this.tilesize - l;
            var C = ig.system.height + this.tilesize - q
              , A = -1;
            for (q = -q - this.tilesize; q < C; A++,
            q += this.tilesize) {
                var E = A + e;
                if (E >= this.height || 0 > E) {
                    if (!this.repeat)
                        continue;
                    E = (E % this.height + this.height) % this.height
                }
                for (var N = -1, g = x; g < l; N++,
                g += this.tilesize) {
                    var r = N + c;
                    if (r >= this.width || 0 > r) {
                        if (!this.repeat)
                            continue;
                        r = (r % this.width + this.width) % this.width
                    }
                    if (r = this.data[E][r])
                        (b = this.anims[r - 1]) ? b.draw(g, q) : this.tiles.drawTile(g, q, r - 1, this.tilesize)
                }
            }
        }
    })
});
ig.baked = !0;
ig.module("impact.game").requires("impact.impact", "impact.entity", "impact.collision-map", "impact.background-map").defines(function() {
    ig.Game = ig.Class.extend({
        clearColor: "#000000",
        gravity: 0,
        screen: {
            x: 0,
            y: 0
        },
        _rscreen: {
            x: 0,
            y: 0
        },
        entities: [],
        namedEntities: {},
        collisionMap: ig.CollisionMap.staticNoCollision,
        backgroundMaps: [],
        backgroundAnims: {},
        autoSort: !1,
        sortBy: null,
        cellSize: 64,
        _deferredKill: [],
        _levelToLoad: null,
        _doSortEntities: !1,
        staticInstantiate: function() {
            this.sortBy = this.sortBy || ig.Game.SORT.Z_INDEX;
            ig.game = this;
            return null
        },
        loadLevel: function(b) {
            this.screen = {
                x: 0,
                y: 0
            };
            this.entities = [];
            this.namedEntities = {};
            for (var c = 0; c < b.entities.length; c++) {
                var e = b.entities[c];
                this.spawnEntity(e.type, e.x, e.y, e.settings)
            }
            this.sortEntities();
            this.collisionMap = ig.CollisionMap.staticNoCollision;
            this.backgroundMaps = [];
            for (c = 0; c < b.layer.length; c++)
                if (e = b.layer[c],
                "collision" == e.name)
                    this.collisionMap = new ig.CollisionMap(e.tilesize,e.data);
                else {
                    var l = new ig.BackgroundMap(e.tilesize,e.data,e.tilesetName);
                    l.anims = this.backgroundAnims[e.tilesetName] || {};
                    l.repeat = e.repeat;
                    l.distance = e.distance;
                    l.foreground = !!e.foreground;
                    l.preRender = !!e.preRender;
                    l.name = e.name;
                    this.backgroundMaps.push(l)
                }
            for (c = 0; c < this.entities.length; c++)
                this.entities[c].ready()
        },
        loadLevelDeferred: function(b) {
            this._levelToLoad = b
        },
        getMapByName: function(b) {
            if ("collision" == b)
                return this.collisionMap;
            for (var c = 0; c < this.backgroundMaps.length; c++)
                if (this.backgroundMaps[c].name == b)
                    return this.backgroundMaps[c];
            return null
        },
        getEntityByName: function(b) {
            return this.namedEntities[b]
        },
        getEntitiesByType: function(b) {
            b = "string" === typeof b ? ig.global[b] : b;
            for (var c = [], e = 0; e < this.entities.length; e++) {
                var l = this.entities[e];
                l instanceof b && !l._killed && c.push(l)
            }
            return c
        },
        spawnEntity: function(b, c, e, l) {
            var q = "string" === typeof b ? ig.global[b] : b;
            if (!q)
                throw "Can't spawn entity of type " + b;
            b = new q(c,e,l || {});
            this.entities.push(b);
            b.name && (this.namedEntities[b.name] = b);
            return b
        },
        sortEntities: function() {
            this.entities.sort(this.sortBy)
        },
        sortEntitiesDeferred: function() {
            this._doSortEntities = !0
        },
        removeEntity: function(b) {
            b.name && delete this.namedEntities[b.name];
            b._killed = !0;
            b.type = ig.Entity.TYPE.NONE;
            b.checkAgainst = ig.Entity.TYPE.NONE;
            b.collides = ig.Entity.COLLIDES.NEVER;
            this._deferredKill.push(b)
        },
        run: function() {
            this.update();
            this.draw()
        },
        update: function() {
            this._levelToLoad && (this.loadLevel(this._levelToLoad),
            this._levelToLoad = null);
            this.updateEntities();
            this.checkEntities();
            for (var b = 0; b < this._deferredKill.length; b++)
                this._deferredKill[b].erase(),
                this.entities.erase(this._deferredKill[b]);
            this._deferredKill = [];
            if (this._doSortEntities || this.autoSort)
                this.sortEntities(),
                this._doSortEntities = !1;
            for (var c in this.backgroundAnims) {
                b = this.backgroundAnims[c];
                for (var e in b)
                    b[e].update()
            }
        },
        updateEntities: function() {
            for (var b = 0; b < this.entities.length; b++) {
                var c = this.entities[b];
                c._killed || c.update()
            }
        },
        draw: function() {
            this.clearColor && ig.system.clear(this.clearColor);
            this._rscreen.x = ig.system.getDrawPos(this.screen.x) / ig.system.scale;
            this._rscreen.y = ig.system.getDrawPos(this.screen.y) / ig.system.scale;
            var b;
            for (b = 0; b < this.backgroundMaps.length; b++) {
                var c = this.backgroundMaps[b];
                if (c.foreground)
                    break;
                c.setScreenPos(this.screen.x, this.screen.y);
                c.draw()
            }
            this.drawEntities();
            for (b; b < this.backgroundMaps.length; b++)
                c = this.backgroundMaps[b],
                c.setScreenPos(this.screen.x, this.screen.y),
                c.draw()
        },
        drawEntities: function() {
            for (var b = 0; b < this.entities.length; b++)
                this.entities[b].draw()
        },
        checkEntities: function() {
            for (var b = {}, c = 0; c < this.entities.length; c++) {
                var e = this.entities[c];
                if (e.type != ig.Entity.TYPE.NONE || e.checkAgainst != ig.Entity.TYPE.NONE || e.collides != ig.Entity.COLLIDES.NEVER)
                    for (var l = {}, q = Math.floor(e.pos.y / this.cellSize), x = Math.floor((e.pos.x + e.size.x) / this.cellSize) + 1, C = Math.floor((e.pos.y + e.size.y) / this.cellSize) + 1, A = Math.floor(e.pos.x / this.cellSize); A < x; A++)
                        for (var E = q; E < C; E++)
                            if (b[A])
                                if (b[A][E]) {
                                    for (var N = b[A][E], g = 0; g < N.length; g++)
                                        e.touches(N[g]) && !l[N[g].id] && (l[N[g].id] = !0,
                                        ig.Entity.checkPair(e, N[g]));
                                    N.push(e)
                                } else
                                    b[A][E] = [e];
                            else
                                b[A] = {},
                                b[A][E] = [e]
            }
        }
    });
    ig.Game.SORT = {
        Z_INDEX: function(b, c) {
            return b.zIndex - c.zIndex
        },
        POS_X: function(b, c) {
            return b.pos.x + b.size.x - (c.pos.x + c.size.x)
        },
        POS_Y: function(b, c) {
            return b.pos.y + b.size.y - (c.pos.y + c.size.y)
        }
    }
});
ig.baked = !0;
ig.module("plugins.patches.fps-limit-patch").requires("impact.impact").defines(function() {
    ig.normalizeVendorAttribute(window, "requestAnimationFrame");
    if (window.requestAnimationFrame) {
        var b = 1
          , c = {};
        window.ig.setAnimation = function(e) {
            var l = b++;
            c[l] = !0;
            var q = ig.system.fps || 60;
            performance.now();
            var x = function(C) {
                if (c[l])
                    for (window.requestAnimationFrame(x),
                    e(); performance.now() - C < 1E3 / q; )
                        ;
            };
            window.requestAnimationFrame(x);
            return l
        }
        ;
        window.ig.clearAnimation = function(e) {
            delete c[e]
        }
    }
});
ig.baked = !0;
ig.module("plugins.patches.timer-patch").requires("impact.timer").defines(function() {
    ig.Timer.step = function() {
        var b = Date.now()
          , c = (b - ig.Timer._last) / 1E3;
        0 > c && (c = 0);
        ig.Timer.time += Math.min(c, ig.Timer.maxStep) * ig.Timer.timeScale;
        ig.Timer._last = b
    }
});
ig.baked = !0;
ig.module("plugins.patches.user-agent-patch").defines(function() {
    ig.ua.touchDevice = "ontouchstart"in window || window.navigator.msMaxTouchPoints || window.navigator.maxTouchPoints;
    ig.ua.is_mac = "MacIntel" === navigator.platform;
    ig.ua.iOS = ig.ua.touchDevice && ig.ua.is_mac || ig.ua.iOS;
    ig.ua.mobile = ig.ua.iOS || ig.ua.mobile
});
ig.baked = !0;
ig.module("plugins.patches.webkit-image-smoothing-patch").defines(function() {
    ig.System && (ig.System.SCALE = {
        CRISP: function(b, c) {
            c.imageSmoothingEnabled = c.msImageSmoothingEnabled = c.mozImageSmoothingEnabled = c.oImageSmoothingEnabled = !1;
            b.style.imageRendering = "-moz-crisp-edges";
            b.style.imageRendering = "-o-crisp-edges";
            b.style.imageRendering = "-webkit-optimize-contrast";
            b.style.imageRendering = "crisp-edges";
            b.style.msInterpolationMode = "nearest-neighbor"
        },
        SMOOTH: function(b, c) {
            c.imageSmoothingEnabled = c.msImageSmoothingEnabled = c.mozImageSmoothingEnabled = c.oImageSmoothingEnabled = !0;
            b.style.imageRendering = "";
            b.style.msInterpolationMode = ""
        }
    },
    ig.System.scaleMode = ig.System.SCALE.SMOOTH)
});
ig.baked = !0;
ig.module("plugins.patches.windowfocus-onMouseDown-patch").requires("impact.input").defines(function() {
    var b = !1;
    try {
        b = window.self !== window.top,
        !1 === b && (b = 0 < window.frames.length)
    } catch (c) {
        b = !0
    }
    ig.Input.inject({
        keydown: function(c) {
            var e = c.target.tagName;
            "INPUT" != e && "TEXTAREA" != e && (e = "keydown" == c.type ? c.keyCode : 2 == c.button ? ig.KEY.MOUSE2 : ig.KEY.MOUSE1,
            b && 0 > e && window.focus(),
            "touchstart" != c.type && "mousedown" != c.type || this.mousemove(c),
            e = this.bindings[e]) && (this.actions[e] = !0,
            this.locks[e] || (this.presses[e] = !0,
            this.locks[e] = !0),
            c.stopPropagation(),
            c.preventDefault())
        }
    })
});
ig.baked = !0;
ig.module("plugins.patches.input-patch").requires("impact.input").defines(function() {
    ig.Input.inject({
        mousemove: function(b) {
            var c = ig.system.realWidth / ig.system.realWidth * ig.system.scale
              , e = {
                left: 0,
                top: 0
            };
            ig.system.canvas.getBoundingClientRect && (e = ig.system.canvas.getBoundingClientRect());
            b = b.touches ? b.touches[0] : b;
            this.mouse.x = (b.clientX - e.left) / c;
            this.mouse.y = (b.clientY - e.top) / c;
            try {
                ig.soundHandler.unlockWebAudio()
            } catch (l) {}
        },
        keyup: function(b) {
            var c = b.target.tagName;
            if ("INPUT" != c && "TEXTAREA" != c && (c = this.bindings["keyup" == b.type ? b.keyCode : 2 == b.button ? ig.KEY.MOUSE2 : ig.KEY.MOUSE1])) {
                this.delayedKeyup[c] = !0;
                b.stopPropagation();
                b.preventDefault();
                if (ig.visibilityHandler)
                    ig.visibilityHandler.onChange("focus");
                try {
                    ig.soundHandler.unlockWebAudio()
                } catch (e) {}
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.data.vector").defines(function() {
    Vector2 = function(b, c) {
        this.x = b || 0;
        this.y = c || 0
    }
    ;
    Vector2.prototype = {
        valType: "number",
        neg: function() {
            this.x = -this.x;
            this.y = -this.y;
            return this
        },
        row: function(b) {
            typeof b === this.valType && (this.y = b);
            return this.y
        },
        col: function(b) {
            typeof b === this.valType && (this.x = b);
            return this.x
        },
        add: function(b) {
            b instanceof Vector2 ? (this.x += b.x,
            this.y += b.y) : (this.x += b,
            this.y += b);
            return this
        },
        sub: function(b) {
            b instanceof Vector2 ? (this.x -= b.x,
            this.y -= b.y) : (this.x -= b,
            this.y -= b);
            return this
        },
        mul: function(b) {
            b instanceof Vector2 ? (this.x *= b.x,
            this.y *= b.y) : (this.x *= b,
            this.y *= b);
            return this
        },
        div: function(b) {
            b instanceof Vector2 ? (0 != b.x && (this.x /= b.x),
            0 != b.y && (this.y /= b.y)) : 0 != b && (this.x /= b,
            this.y /= b);
            return this
        },
        equals: function(b) {
            return this.x == b.x && this.y == b.y
        },
        dot: function(b) {
            return this.x * b.x + this.y * b.y
        },
        cross: function(b) {
            return this.x * b.y - this.y * b.x
        },
        length: function() {
            return Math.sqrt(this.dot(this))
        },
        norm: function() {
            return this.divide(this.length())
        },
        min: function() {
            return Math.min(this.x, this.y)
        },
        max: function() {
            return Math.max(this.x, this.y)
        },
        toAngles: function() {
            return -Math.atan2(-this.y, this.x)
        },
        angleTo: function(b) {
            return Math.acos(this.dot(b) / (this.length() * b.length()))
        },
        toArray: function(b) {
            return [this.x, this.y].slice(0, b || 2)
        },
        clone: function() {
            return new Vector2(this.x,this.y)
        },
        set: function(b, c) {
            this.x = b;
            this.y = c;
            return this
        },
        unit: function() {
            var b = this.length();
            if (0 < b)
                return new Vector2(this.x / b,this.y / b);
            throw "Divide by 0 error in unitVector function of vector:" + this;
        },
        turnRight: function() {
            var b = this.x;
            this.x = -this.y;
            this.y = b;
            return this
        },
        turnLeft: function() {
            var b = this.x;
            this.x = this.y;
            this.y = -b;
            return this
        },
        rotate: function(b) {
            var c = this.clone();
            this.x = c.x * Math.cos(b) - c.y * Math.sin(b);
            this.y = c.x * Math.sin(b) + c.y * Math.cos(b);
            return this
        }
    };
    Vector2.negative = function(b) {
        return new Vector2(-b.x,-b.y)
    }
    ;
    Vector2.add = function(b, c) {
        return c instanceof Vector2 ? new Vector2(b.x + c.x,b.y + c.y) : new Vector2(b.x + v,b.y + v)
    }
    ;
    Vector2.subtract = function(b, c) {
        return c instanceof Vector2 ? new Vector2(b.x - c.x,b.y - c.y) : new Vector2(b.x - v,b.y - v)
    }
    ;
    Vector2.multiply = function(b, c) {
        return c instanceof Vector2 ? new Vector2(b.x * c.x,b.y * c.y) : new Vector2(b.x * v,b.y * v)
    }
    ;
    Vector2.divide = function(b, c) {
        return c instanceof Vector2 ? new Vector2(b.x / c.x,b.y / c.y) : new Vector2(b.x / v,b.y / v)
    }
    ;
    Vector2.equals = function(b, c) {
        return b.x == c.x && b.y == c.y
    }
    ;
    Vector2.dot = function(b, c) {
        return b.x * c.x + b.y * c.y
    }
    ;
    Vector2.cross = function(b, c) {
        return b.x * c.y - b.y * c.x
    }
});
ig.baked = !0;
ig.module("plugins.data.color-rgb").defines(function() {
    ColorRGB = function(b, c, e, l) {
        this.r = b || 0;
        this.g = c || 0;
        this.b = e || 0;
        this.a = l || 0
    }
    ;
    ColorRGB.prototype = {
        setRandomColor: function() {
            this.r = Math.round(255 * Math.random());
            this.g = Math.round(255 * Math.random());
            this.b = Math.round(255 * Math.random())
        },
        getStyle: function() {
            return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")"
        },
        getHex: function() {
            for (var b = this.r.toString(16), c = this.g.toString(16), e = this.b.toString(16); 2 > b.length; )
                b = "0" + b;
            for (; 2 > c.length; )
                c = "0" + c;
            for (; 2 > e.length; )
                e = "0" + e;
            return "#" + b + c + e
        },
        getInvertedColor: function() {
            return new ColorRGB(255 - this.r,255 - this.g,255 - this.b,255 - this.a)
        },
        clone: function() {
            return new ColorRGB(this.r,this.g,this.b,this.a)
        }
    }
});
ig.baked = !0;
ig.module("plugins.font.font-info").requires("impact.impact").defines(function() {
    ig.FontInfo = ig.Class.extend({
        fonts: [{
            name: "montserrat",
            source: "/static/spin-arena/v1/media/fonts/montserrat"
        }, {
            name: "metropolis",
            source: "/static/spin-arena/v1/media/fonts/metropolis-black"
        }, {
            name: "oxanium",
            source: "/static/spin-arena/v1/media/fonts/oxanium-medium"
        }],
        init: function() {
            this.registerCssFont()
        },
        registerCssFont: function() {
            if (0 < this.fonts.length) {
                var b = document.createElement("style");
                b.type = "text/css";
                for (var c = "", e = 0; e < this.fonts.length; e++) {
                    var l = this.fonts[e];
                    c += "@font-face {font-family: '" + l.name + "';src: url('" + l.source + ".eot');src: url('" + l.source + ".eot?#iefix') format('embedded-opentype'),url('" + l.source + ".woff2') format('woff2'),url('" + l.source + ".woff') format('woff'),url('" + l.source + ".ttf') format('truetype'),url('" + l.source + ".svg#svgFontName') format('svg')}"
                }
                b.appendChild(document.createTextNode(c));
                document.head.appendChild(b)
            }
        },
        isValid: function() {
            for (var b = 0; b < this.fonts.length; b++)
                if (!this._isValidName(this.fonts[b].source))
                    return !1;
            return !0
        },
        _isValidName: function(b) {
            return -1 == b.search(/^[a-z0-9-\/]+$/) ? !1 : !0
        }
    })
});
ig.baked = !0;
ig.module("plugins.font.font-loader").requires("impact.impact", "plugins.font.font-info", "impact.loader").defines(function() {
    ig.FontLoader = ig.Class.extend({
        fontInfo: new ig.FontInfo,
        onload: !1,
        onerror: !1,
        init: function(b, c) {
            this.onload = b;
            this.onerror = c
        },
        load: function() {
            this.fontInfo.isValid() ? this._loadByLib() : console.error("Only lowercased alphanumeric and dash are allowed for font file name!. Please check the font path")
        },
        _loadByLib: function() {
            for (var b = [], c = 0; c < this.fontInfo.fonts.length; c++) {
                var e = new FontFaceObserver(this.fontInfo.fonts[c].name);
                b.push(e.load())
            }
            Promise.all(b).then(this.onload).catch(this.onerror)
        }
    });
    ig.Loader.inject({
        parentLoad: !1,
        load: function() {
            this.parentLoad = this.parent;
            (new ig.FontLoader(this.onFontLoad.bind(this),this.onFontError.bind(this))).load()
        },
        onFontLoad: function(b) {},
        onFontError: function(b) {
            console.error("Font is not loaded")
        }
    })
});
ig.baked = !0;
ig.module("plugins.handlers.dom-handler").defines(function() {
    ig.DomHandler = ig.Class.extend({
        JQUERYAVAILABLE: !1,
        init: function() {
            this.JQUERYAVAILABLE = this._jqueryAvailable()
        },
        _jqueryAvailable: function() {
            return "undefined" !== typeof jQuery
        },
        addEvent: function(b, c, e, l) {
            if (this.JQUERYAVAILABLE)
                b.on(c, e);
            else
                b.addEventListener(c, e, l)
        },
        create: function(b) {
            return this.JQUERYAVAILABLE ? $("<" + b + ">") : ig.$new(b)
        },
        getElementByClass: function(b) {
            return this.JQUERYAVAILABLE ? $("." + b) : document.getElementsByClassName(b)
        },
        getElementById: function(b) {
            return this.JQUERYAVAILABLE ? 0 < $(b).length ? $(b) : null : ig.$(b)
        },
        appendChild: function(b, c) {
            this.JQUERYAVAILABLE ? b.append(c) : b.appendChild(c)
        },
        appendToBody: function(b) {
            this.JQUERYAVAILABLE ? $("body").append(b) : document.body.appendChild(b)
        },
        resize: function(b, c, e) {
            if (this.JQUERYAVAILABLE)
                b.width(c.toFixed(2)),
                b.height(e.toFixed(2));
            else {
                var l = b.style.visibility;
                c = "width:" + c.toFixed(2) + "px; height:" + e.toFixed(2) + "px;";
                this.attr(b, "style", c);
                b.style.visibility = l
            }
        },
        resizeOffsetLeft: function(b, c, e, l) {
            if (this.JQUERYAVAILABLE)
                b.width(c.toFixed(2)),
                b.height(e.toFixed(2)),
                b.css("left", l);
            else {
                var q = b.style.visibility;
                c = "width:" + c.toFixed(2) + "px; height:" + e.toFixed(2) + "px; left: " + l.toFixed(2) + "px;";
                this.attr(b, "style", c);
                b.style.visibility = q
            }
        },
        resizeOffset: function(b, c, e, l, q) {
            if (this.JQUERYAVAILABLE)
                b.width(c.toFixed(2)),
                b.height(e.toFixed(2)),
                b.css("left", l),
                b.css("top", q);
            else {
                var x = b.style.visibility;
                c = "width:" + c.toFixed(2) + "px; height:" + e.toFixed(2) + "px; left: " + l.toFixed(2) + "px; top: " + q.toFixed(2) + "px;";
                this.attr(b, "style", c);
                b.style.visibility = x
            }
        },
        css: function(b, c) {
            if (this.JQUERYAVAILABLE)
                b.css(c);
            else {
                var e = "", l;
                for (l in c)
                    e += l + ":" + c[l] + ";";
                this.attr(b, "style", e)
            }
        },
        getOffsets: function(b) {
            return this.JQUERYAVAILABLE ? (b = b.offset(),
            {
                left: b.left,
                top: b.top
            }) : {
                left: b.offsetLeft,
                top: b.offsetTop
            }
        },
        attr: function(b, c, e) {
            if ("undefined" === typeof e)
                return this.JQUERYAVAILABLE ? b.attr(c) : b.getAttribute(c);
            this.JQUERYAVAILABLE ? b.attr(c, e) : b.setAttribute(c, e)
        },
        show: function(b) {
            b && "undefined" !== typeof b && (this.JQUERYAVAILABLE ? (b.show(),
            b.css("visibility", "visible")) : b && (b.style ? b.style.visibility = "visible" : b[0] && (b[0].style.visibility = "visible")))
        },
        hide: function(b) {
            b && "undefined" !== typeof b && (this.JQUERYAVAILABLE ? (b.hide(),
            b.css("visibility", "hidden")) : b && (b.style ? b.style.visibility = "hidden" : b[0] && (b[0].style.visibility = "hidden")))
        },
        getQueryVariable: function(b) {
            for (var c = window.location.search.substring(1).split("&"), e = 0; e < c.length; e++) {
                var l = c[e].match(/([^=]+?)=(.+)/);
                if (l && decodeURIComponent(l[1]) == b)
                    return decodeURIComponent(l[2])
            }
        },
        forcedDeviceDetection: function() {
            var b = this.getQueryVariable("device");
            if (b)
                switch (b) {
                case "mobile":
                    console.log("serving mobile version ...");
                    ig.ua.mobile = !0;
                    break;
                case "desktop":
                    console.log("serving desktop version ...");
                    ig.ua.mobile = !1;
                    break;
                default:
                    console.log("serving universal version ...")
                }
            else
                console.log("serving universal version ...")
        },
        forcedDeviceRotation: function() {
            var b = this.getQueryVariable("force-rotate");
            if (b)
                switch (b) {
                case "portrait":
                    console.log("force rotate to portrait");
                    window.orientation = 0;
                    break;
                case "landscape":
                    console.log("force rotate to horizontal");
                    window.orientation = 90;
                    break;
                default:
                    alert("wrong command/type in param force-rotate. Defaulting value to portrait"),
                    window.orientation = 0
                }
        }
    })
});
ig.baked = !0;
ig.module("plugins.handlers.size-handler").requires("plugins.data.vector").defines(function() {
    ig.SizeHandler = ig.Class.extend({
        portraitMode: !0,
        disableStretchToFitOnMobileFlag: !0,
        enableStretchToFitOnAntiPortraitModeFlag: !0,
        enableScalingLimitsOnMobileFlag: !1,
        minScalingOnMobile: 0,
        maxScalingOnMobile: 1,
        enableStretchToFitOnDesktopFlag: !1,
        enableScalingLimitsOnDesktopFlag: !1,
        minScalingOnDesktop: 0,
        maxScalingOnDesktop: 1,
        desktop: {
            actualSize: new Vector2(window.innerWidth,window.innerHeight),
            actualResolution: new Vector2(1920,1080)
        },
        mobile: {
            actualSize: new Vector2(window.innerWidth,window.innerHeight),
            actualResolution: new Vector2(1080,1920)
        },
        windowSize: new Vector2(window.innerWidth,window.innerHeight),
        scaleRatioMultiplier: new Vector2(1,1),
        sizeRatio: new Vector2(1,1),
        scale: 1,
        domHandler: null,
        dynamicClickableEntityDivs: {},
        coreDivsToResize: ["#canvas", "#play", "#orientate"],
        adsToResize: {
            MobileAdInGamePreroll: {
                "box-width": _SETTINGS.Ad.Mobile.Preroll.Width + 2,
                "box-height": _SETTINGS.Ad.Mobile.Preroll.Height + 20
            },
            MobileAdInGameEnd: {
                "box-width": _SETTINGS.Ad.Mobile.End.Width + 2,
                "box-height": _SETTINGS.Ad.Mobile.End.Height + 20
            },
            MobileAdInGamePreroll2: {
                "box-width": _SETTINGS.Ad.Mobile.Preroll.Width + 2,
                "box-height": _SETTINGS.Ad.Mobile.Preroll.Height + 20
            },
            MobileAdInGameEnd2: {
                "box-width": _SETTINGS.Ad.Mobile.End.Width + 2,
                "box-height": _SETTINGS.Ad.Mobile.End.Height + 20
            },
            MobileAdInGamePreroll3: {
                "box-width": _SETTINGS.Ad.Mobile.Preroll.Width + 2,
                "box-height": _SETTINGS.Ad.Mobile.Preroll.Height + 20
            },
            MobileAdInGameEnd3: {
                "box-width": _SETTINGS.Ad.Mobile.End.Width + 2,
                "box-height": _SETTINGS.Ad.Mobile.End.Height + 20
            }
        },
        init: function(b) {
            this.domHandler = b;
            if ("undefined" === typeof b)
                throw "undefined Dom Handler for Size Handler";
            this.sizeCalcs();
            this.eventListenerSetup();
            this.samsungFix()
        },
        sizeCalcs: function() {
            this.windowSize = new Vector2(window.innerWidth,window.innerHeight);
            if (ig.ua.mobile) {
                this.mobile.actualSize = new Vector2(window.innerWidth,window.innerHeight);
                var b = new Vector2(this.mobile.actualResolution.x,this.mobile.actualResolution.y);
                this.scaleRatioMultiplier = new Vector2(this.mobile.actualSize.x / b.x,this.mobile.actualSize.y / b.y);
                if (this.disableStretchToFitOnMobileFlag) {
                    var c = Math.min(this.scaleRatioMultiplier.x, this.scaleRatioMultiplier.y);
                    this.enableScalingLimitsOnMobileFlag && (c = c.limit(this.minScalingOnMobile, this.maxScalingOnMobile));
                    this.mobile.actualSize.x = b.x * c;
                    this.mobile.actualSize.y = b.y * c;
                    this.scaleRatioMultiplier.x = c;
                    this.scaleRatioMultiplier.y = c
                } else
                    this.sizeRatio.x = this.scaleRatioMultiplier.x,
                    this.sizeRatio.y = this.scaleRatioMultiplier.y,
                    this.scaleRatioMultiplier.x = 1,
                    this.scaleRatioMultiplier.y = 1
            } else
                this.desktop.actualSize = new Vector2(window.innerWidth,window.innerHeight),
                b = new Vector2(this.desktop.actualResolution.x,this.desktop.actualResolution.y),
                this.scaleRatioMultiplier = new Vector2(this.desktop.actualSize.x / b.x,this.desktop.actualSize.y / b.y),
                this.enableStretchToFitOnDesktopFlag ? (this.sizeRatio.x = this.scaleRatioMultiplier.x,
                this.sizeRatio.y = this.scaleRatioMultiplier.y,
                this.scaleRatioMultiplier.x = 1,
                this.scaleRatioMultiplier.y = 1) : (c = Math.min(this.scaleRatioMultiplier.x, this.scaleRatioMultiplier.y),
                this.enableScalingLimitsOnDesktopFlag && (c = c.limit(this.minScalingOnDesktop, this.maxScalingOnDesktop)),
                this.desktop.actualSize.x = b.x * c,
                this.desktop.actualSize.y = b.y * c,
                this.scaleRatioMultiplier.x = c,
                this.scaleRatioMultiplier.y = c)
        },
        resizeLayers: function(b, c) {
            for (b = 0; b < this.coreDivsToResize.length; b++)
                if (c = ig.domHandler.getElementById(this.coreDivsToResize[b]),
                ig.ua.mobile)
                    if (this.disableStretchToFitOnMobileFlag) {
                        var e = Math.floor(ig.sizeHandler.windowSize.x / 2 - ig.sizeHandler.mobile.actualSize.x / 2)
                          , l = Math.floor(ig.sizeHandler.windowSize.y / 2 - ig.sizeHandler.mobile.actualSize.y / 2);
                        0 > e && (e = 0);
                        0 > l && (l = 0);
                        ig.domHandler.resizeOffset(c, Math.floor(ig.sizeHandler.mobile.actualSize.x), Math.floor(ig.sizeHandler.mobile.actualSize.y), e, l);
                        if (this.portraitMode ? window.innerHeight < window.innerWidth : window.innerHeight > window.innerWidth)
                            if (this.enableStretchToFitOnAntiPortraitModeFlag)
                                ig.domHandler.resizeOffset(c, Math.floor(window.innerWidth), Math.floor(window.innerHeight), 0, 0);
                            else {
                                var q = new Vector2(window.innerWidth / this.mobile.actualResolution.y,window.innerHeight / this.mobile.actualResolution.x);
                                e = Math.min(q.x, q.y);
                                q = this.mobile.actualResolution.y * e;
                                var x = this.mobile.actualResolution.x * e;
                                e = Math.floor(ig.sizeHandler.windowSize.x / 2 - q / 2);
                                l = Math.floor(ig.sizeHandler.windowSize.y / 2 - x / 2);
                                0 > e && (e = 0);
                                0 > l && (l = 0);
                                ig.domHandler.resizeOffset(c, Math.floor(q), Math.floor(x), e, l)
                            }
                    } else
                        ig.domHandler.resize(c, Math.floor(ig.sizeHandler.mobile.actualSize.x), Math.floor(ig.sizeHandler.mobile.actualSize.y));
                else
                    this.enableStretchToFitOnDesktopFlag ? ig.domHandler.resize(c, Math.floor(ig.sizeHandler.desktop.actualSize.x), Math.floor(ig.sizeHandler.desktop.actualSize.y)) : (e = Math.floor(ig.sizeHandler.windowSize.x / 2 - ig.sizeHandler.desktop.actualSize.x / 2),
                    l = Math.floor(ig.sizeHandler.windowSize.y / 2 - ig.sizeHandler.desktop.actualSize.y / 2),
                    0 > e && (e = 0),
                    0 > l && (l = 0),
                    ig.domHandler.resizeOffset(c, Math.floor(ig.sizeHandler.desktop.actualSize.x), Math.floor(ig.sizeHandler.desktop.actualSize.y), e, l));
            for (var C in this.adsToResize)
                b = ig.domHandler.getElementById("#" + C),
                c = ig.domHandler.getElementById("#" + C + "-Box"),
                q = (window.innerWidth - this.adsToResize[C]["box-width"]) / 2 + "px",
                e = (window.innerHeight - this.adsToResize[C]["box-height"]) / 2 + "px",
                b && ig.domHandler.css(b, {
                    width: window.innerWidth,
                    height: window.innerHeight
                }),
                c && ig.domHandler.css(c, {
                    left: q,
                    top: e
                });
            b = ig.domHandler.getElementById("#canvas");
            c = ig.domHandler.getOffsets(b);
            b = c.left;
            c = c.top;
            q = Math.min(ig.sizeHandler.scaleRatioMultiplier.x, ig.sizeHandler.scaleRatioMultiplier.y);
            for (C in this.dynamicClickableEntityDivs) {
                e = ig.domHandler.getElementById("#" + C);
                if (ig.ua.mobile) {
                    x = this.dynamicClickableEntityDivs[C].entity_pos_x;
                    var A = this.dynamicClickableEntityDivs[C].entity_pos_y
                      , E = this.dynamicClickableEntityDivs[C].width;
                    l = this.dynamicClickableEntityDivs[C].height;
                    this.disableStretchToFitOnMobileFlag ? (x = Math.floor(b + x * this.scaleRatioMultiplier.x) + "px",
                    A = Math.floor(c + A * this.scaleRatioMultiplier.y) + "px",
                    E = Math.floor(E * this.scaleRatioMultiplier.x) + "px",
                    l = Math.floor(l * this.scaleRatioMultiplier.y) + "px") : (x = Math.floor(x * this.sizeRatio.x) + "px",
                    A = Math.floor(A * this.sizeRatio.y) + "px",
                    E = Math.floor(E * this.sizeRatio.x) + "px",
                    l = Math.floor(l * this.sizeRatio.y) + "px")
                } else
                    x = this.dynamicClickableEntityDivs[C].entity_pos_x,
                    A = this.dynamicClickableEntityDivs[C].entity_pos_y,
                    E = this.dynamicClickableEntityDivs[C].width,
                    l = this.dynamicClickableEntityDivs[C].height,
                    this.enableStretchToFitOnDesktopFlag ? (x = Math.floor(x * this.sizeRatio.x) + "px",
                    A = Math.floor(A * this.sizeRatio.y) + "px",
                    E = Math.floor(E * this.sizeRatio.x) + "px",
                    l = Math.floor(l * this.sizeRatio.y) + "px") : (x = Math.floor(b + x * this.scaleRatioMultiplier.x) + "px",
                    A = Math.floor(c + A * this.scaleRatioMultiplier.y) + "px",
                    E = Math.floor(E * this.scaleRatioMultiplier.x) + "px",
                    l = Math.floor(l * this.scaleRatioMultiplier.y) + "px");
                ig.domHandler.css(e, {
                    float: "left",
                    position: "absolute",
                    left: x,
                    top: A,
                    width: E,
                    height: l,
                    "z-index": 3
                });
                this.dynamicClickableEntityDivs[C]["font-size"] && ig.domHandler.css(e, {
                    "font-size": this.dynamicClickableEntityDivs[C]["font-size"] * q + "px"
                })
            }
            $("#ajaxbar").width(this.windowSize.x);
            $("#ajaxbar").height(this.windowSize.y)
        },
        resize: function() {
            this.sizeCalcs();
            this.resizeLayers()
        },
        reorient: function() {
            console.log("changing orientation ...");
            if (ig.ua.mobile) {
                var b = this.portraitMode ? window.innerHeight < window.innerWidth : window.innerHeight > window.innerWidth
                  , c = this.domHandler.getElementById("#orientate")
                  , e = this.domHandler.getElementById("#game");
                b ? (this.domHandler.show(c),
                this.domHandler.hide(e),
                console.log("portrait" + window.innerWidth + "," + window.innerHeight)) : (this.domHandler.show(e),
                this.domHandler.hide(c),
                console.log("landscape" + window.innerWidth + "," + window.innerHeight))
            }
            ig.ua.mobile ? (this.resize(),
            this.resizeAds()) : this.resize()
        },
        resizeAds: function() {
            for (var b in this.adsToResize) {
                var c = ig.domHandler.getElementById("#" + b)
                  , e = ig.domHandler.getElementById("#" + b + "-Box")
                  , l = (window.innerWidth - this.adsToResize[b]["box-width"]) / 2 + "px"
                  , q = (window.innerHeight - this.adsToResize[b]["box-height"]) / 2 + "px";
                c && ig.domHandler.css(c, {
                    width: window.innerWidth,
                    height: window.innerHeight
                });
                e && ig.domHandler.css(e, {
                    left: l,
                    top: q
                })
            }
        },
        samsungFix: function() {
            !ig.ua.android || 4.2 > parseFloat(navigator.userAgent.slice(navigator.userAgent.indexOf("Android") + 8, navigator.userAgent.indexOf("Android") + 11)) || 0 > navigator.userAgent.indexOf("GT") || 0 < navigator.userAgent.indexOf("Chrome") || 0 < navigator.userAgent.indexOf("Firefox") || (document.addEventListener("touchstart", function(b) {
                b.preventDefault();
                return !1
            }, !1),
            document.addEventListener("touchmove", function(b) {
                b.preventDefault();
                return !1
            }, !1),
            document.addEventListener("touchend", function(b) {
                b.preventDefault();
                return !1
            }, !1))
        },
        orientationInterval: null,
        orientationTimeout: null,
        orientationHandler: function() {
            this.reorient();
            window.scrollTo(0, 1)
        },
        orientationDelayHandler: function() {
            null == this.orientationInterval && (this.orientationInterval = window.setInterval(this.orientationHandler.bind(this), 100));
            null == this.orientationTimeout && (this.orientationTimeout = window.setTimeout(function() {
                this.clearAllIntervals()
            }
            .bind(this), 2E3))
        },
        clearAllIntervals: function() {
            window.clearInterval(this.orientationInterval);
            this.orientationInterval = null;
            window.clearTimeout(this.orientationTimeout);
            this.orientationTimeout = null
        },
        eventListenerSetup: function() {
            ig.ua.iOS ? (window.addEventListener("orientationchange", this.orientationDelayHandler.bind(this)),
            window.addEventListener("resize", this.orientationDelayHandler.bind(this))) : (window.addEventListener("orientationchange", this.orientationHandler.bind(this)),
            window.addEventListener("resize", this.orientationHandler.bind(this)));
            document.ontouchmove = function(b) {
                window.scrollTo(0, 1);
                b.preventDefault()
            }
            ;
            this.chromePullDownRefreshFix()
        },
        chromePullDownRefreshFix: function() {
            var b = window.chrome || navigator.userAgent.match("CriOS")
              , c = "ontouchstart"in document.documentElement;
            if (b && c) {
                var e = b = !1
                  , l = 0
                  , q = !1;
                try {
                    CSS.supports("overscroll-behavior-y", "contain") && (b = !0)
                } catch (x) {}
                try {
                    if (b)
                        return document.body.style.overscrollBehaviorY = "contain"
                } catch (x) {}
                b = document.head || document.body;
                c = document.createElement("style");
                c.type = "text/css";
                c.styleSheet ? c.styleSheet.cssText = "\n      ::-webkit-scrollbar {\n        width: 500x;\n      }\n      ::-webkit-scrollbar-thumb {\n        border-radius: 500px;\n        background-color: rgba(0, 0, 0, 0.2);\n      }\n      body {\n        -webkit-overflow-scrolling: auto!important;\n      }\n    " : c.appendChild(document.createTextNode("\n      ::-webkit-scrollbar {\n        width: 500px;\n      }\n      ::-webkit-scrollbar-thumb {\n        border-radius: 500px;\n        background-color: rgba(0, 0, 0, 0.2);\n      }\n      body {\n        -webkit-overflow-scrolling: auto!important;\n      }\n    "));
                b.appendChild(c);
                try {
                    addEventListener("test", null, {
                        get passive() {
                            e = !0
                        }
                    })
                } catch (x) {}
                document.addEventListener("touchstart", function(x) {
                    1 === x.touches.length && (l = x.touches[0].clientY,
                    q = 0 === window.pageYOffset)
                }, !!e && {
                    passive: !0
                });
                document.addEventListener("touchmove", function(x) {
                    var C;
                    if (C = q) {
                        q = !1;
                        C = x.touches[0].clientY;
                        var A = C - l;
                        C = (l = C,
                        0 < A)
                    }
                    if (C)
                        return x.preventDefault()
                }, !!e && {
                    passive: !1
                })
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.handlers.api-handler").defines(function() {
    ig.ApiHandler = ig.Class.extend({
        apiAvailable: {
            MJSPreroll: function() {
                ig.ua.mobile && ig.domHandler.JQUERYAVAILABLE && _SETTINGS && _SETTINGS.Ad.Mobile.Preroll.Enabled && MobileAdInGamePreroll.Initialize()
            },
            MJSHeader: function() {
                ig.ua.mobile && ig.domHandler.JQUERYAVAILABLE && _SETTINGS.Ad.Mobile.Header.Enabled && MobileAdInGameHeader.Initialize()
            },
            MJSFooter: function() {
                ig.ua.mobile && ig.domHandler.JQUERYAVAILABLE && _SETTINGS.Ad.Mobile.Footer.Enabled && MobileAdInGameFooter.Initialize()
            },
            MJSEnd: function() {
                ig.ua.mobile && ig.domHandler.JQUERYAVAILABLE && _SETTINGS.Ad.Mobile.End.Enabled && MobileAdInGameEnd.Initialize()
            }
        },
        run: function(b, c) {
            if (this.apiAvailable[b])
                this.apiAvailable[b](c)
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.sound-player").defines(function() {
    SoundPlayer = ig.Class.extend({
        tagName: "SoundPlayer",
        stayMuteFlag: !1,
        debug: !1,
        init: function(b, c) {
            this.debug && console.log(this.tagName)
        },
        play: function(b) {
            this.debug && console.log("play sound ", b)
        },
        stop: function(b) {
            this.debug && console.log("stop sound ")
        },
        volume: function(b) {
            this.debug && console.log("set volume")
        },
        mute: function(b) {
            this.debug && console.log("mute");
            "undefined" === typeof b ? this.stayMuteFlag = !0 : b && (this.stayMuteFlag = !0)
        },
        unmute: function(b) {
            this.debug && console.log("unmute");
            "undefined" === typeof b ? this.stayMuteFlag = !1 : b && (this.stayMuteFlag = !1)
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.impact-music-player").requires("plugins.audio.sound-player").defines(function() {
    ImpactMusicPlayer = SoundPlayer.extend({
        tagName: "ImpactMusicPlayer",
        bgmPlaying: !1,
        soundList: {},
        init: function(b, c) {
            this.parent(b, c);
            for (var e in b)
                this.soundList[e] = e,
                ig.music.add(b[e].path + ".*", e);
            c && c.loop && (ig.music.loop = c.loop)
        },
        play: function(b) {
            this.stayMuteFlag || (this.bgmPlaying = !0,
            "undefined" === typeof b ? ig.music.play(b) : ig.music.play())
        },
        stop: function(b) {
            this.bgmPlaying = !1;
            ig.music.pause()
        },
        volume: function(b) {
            console.log("impactmusic:", b);
            0 > b ? ig.music.volume = 0 : isNaN(b) ? ig.music.volume = 1 : ig.music.volume = 1 < b ? 1 : b
        },
        getVolume: function() {
            return ig.music.volume
        },
        mute: function(b) {
            this.parent(b);
            this.bgmPlaying && this.stop()
        },
        unmute: function(b) {
            this.parent(b);
            this.play()
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.impact-sound-player").requires("plugins.audio.sound-player").defines(function() {
    ImpactSoundPlayer = SoundPlayer.extend({
        tagName: "ImpactSoundPlayer",
        soundList: {},
        init: function(b, c) {
            this.parent(b, c);
            for (var e in b)
                c = new ig.Sound(b[e].path + ".*"),
                this.soundList[e] = c
        },
        play: function(b) {
            this.stayMuteFlag || ("object" === typeof b ? (console.log(b + " exists"),
            b.play()) : "string" === typeof b && this.soundList[b].play())
        },
        stop: function(b) {
            this.parent(b);
            b.stop()
        },
        volume: function(b) {
            0 > b ? ig.soundManager.volume = 0 : isNaN(b) ? ig.soundManager.volume = 1 : ig.soundManager.volume = 1 < b ? 1 : b
        },
        getVolume: function() {
            return ig.soundManager.volume
        },
        mute: function(b) {
            this.parent(b);
            ig.Sound.enabled = !1
        },
        unmute: function(b) {
            this.parent(b);
            ig.Sound.enabled = !0
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.howler-player").requires("plugins.audio.sound-player").defines(function() {
    HowlerPlayer = SoundPlayer.extend({
        tagName: "HowlerPlayer",
        soundList: {},
        init: function(b, c) {
            this.parent(b, c);
            for (var e in b)
                c = b[e].path,
                c = new Howl({
                    src: [c + "." + ig.Sound.FORMAT.OGG.ext, c + "." + ig.Sound.FORMAT.MP3.ext]
                }),
                this.soundList[e] = c
        },
        play: function(b) {
            if (Howler.ctx && "running" !== Howler.ctx.state)
                return Howler.ctx.resume();
            this.stayMuteFlag || ("object" === typeof b ? b.play() : "string" === typeof b && this.soundList[b].play())
        },
        stop: function(b) {
            this.parent(b);
            "object" === typeof b ? b.stop() : "string" === typeof b && this.soundList[b].stop()
        },
        volume: function(b) {
            for (var c in this.soundList) {
                if (0 > b) {
                    this.soundList[c].volume(0);
                    break
                }
                isNaN(b) ? this.soundList[c].volume(1) : 1 < b ? this.soundList[c].volume(1) : this.soundList[c].volume(b)
            }
        },
        getVolume: function() {
            for (var b in this.soundList)
                return this.soundList[b].volume()
        },
        mute: function(b) {
            this.parent(b);
            Howler.mute(!0)
        },
        unmute: function(b) {
            this.parent(b);
            Howler.mute(!1)
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.howler-music-player").requires("plugins.audio.sound-player").defines(function() {
    HowlerMusicPlayer = SoundPlayer.extend({
        tagName: "HowlerMusicPlayer",
        bgmPlaying: !1,
        soundList: {},
        init: function(b, c) {
            this.parent(b, c);
            for (var e in b)
                c = b[e].path,
                c = new Howl({
                    src: [c + "." + ig.Sound.FORMAT.OGG.ext, c + "." + ig.Sound.FORMAT.MP3.ext],
                    loop: !0,
                    autoplay: !1,
                    onend: function() {}
                    .bind(this)
                }),
                this.soundList[e] = c
        },
        play: function(b) {
            if (!this.stayMuteFlag && !this.bgmPlaying)
                if ("object" === typeof b)
                    this.bgmPlaying = !0,
                    b.play();
                else if ("string" === typeof b)
                    this.bgmPlaying = !0,
                    this.soundList[b].play();
                else
                    for (var c in this.soundList) {
                        this.soundList[c].play();
                        this.bgmPlaying = !0;
                        break
                    }
        },
        stop: function(b) {
            this.parent(b);
            if (this.bgmPlaying) {
                for (var c in this.soundList)
                    this.soundList[c].stop();
                this.bgmPlaying = !1
            }
        },
        volume: function(b) {
            console.log("howler", b);
            for (var c in this.soundList) {
                if (0 > b) {
                    this.soundList[c].volume(0);
                    break
                }
                isNaN(b) ? this.soundList[c].volume(1) : 1 < b ? this.soundList[c].volume(1) : this.soundList[c].volume(b)
            }
        },
        getVolume: function() {
            for (var b in this.soundList)
                return this.soundList[b].volume()
        },
        mute: function(b) {
            this.parent(b);
            Howler.mute(!0)
        },
        unmute: function(b) {
            this.parent(b);
            Howler.mute(!1)
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.jukebox-player").requires("plugins.audio.sound-player").defines(function() {
    JukeboxPlayer = SoundPlayer.extend({
        tagName: "JukeboxPlayer",
        bgmPlaying: !1,
        soundList: {},
        jukeboxPlayer: null,
        pausePosition: 0,
        premuteVolume: 0,
        minVolume: .001,
        init: function(b, c) {
            this.parent(b, c);
            for (var e in b)
                this.soundList[e] = e,
                c = b[e].path,
                this.jukeboxPlayer = new jukebox.Player({
                    resources: [c + "." + ig.Sound.FORMAT.OGG.ext, c + "." + ig.Sound.FORMAT.MP3.ext],
                    autoplay: !1,
                    spritemap: {
                        music: {
                            start: b[e].startMp3,
                            end: b[e].endMp3,
                            loop: !0
                        }
                    }
                })
        },
        play: function(b) {
            this.stayMuteFlag || (this.bgmPlaying = !0,
            this.pausePosition ? (console.log("resume"),
            this.jukeboxPlayer.resume(this.pausePosition)) : (console.log("play"),
            this.jukeboxPlayer.play(this.jukeboxPlayer.settings.spritemap.music.start, !0)),
            this.premuteVolume = this.getVolume())
        },
        stop: function(b) {
            this.bgmPlaying = !1;
            this.pausePosition = this.jukeboxPlayer.pause()
        },
        volume: function(b) {
            console.log("jukebox:", b);
            0 >= b ? this.jukeboxPlayer.setVolume(this.minVolume) : isNaN(b) ? this.jukeboxPlayer.setVolume(1) : 1 < b ? this.jukeboxPlayer.setVolume(1) : this.jukeboxPlayer.setVolume(b)
        },
        getVolume: function() {
            return this.jukeboxPlayer.getVolume()
        },
        mute: function(b) {
            this.parent(b);
            this.bgmPlaying && (console.log("jukebox", this.premuteVolume),
            this.stayMuteFlag || (this.premuteVolume = this.getVolume()),
            this.jukeboxPlayer.pause(),
            this.jukeboxPlayer.setVolume(this.minVolume))
        },
        unmute: function(b) {
            this.parent(b);
            this.stayMuteFlag || (console.log("jukebox", this.premuteVolume),
            this.jukeboxPlayer.setVolume(this.premuteVolume),
            this.jukeboxPlayer.resume())
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.webaudio-music-player").requires("plugins.audio.sound-player").defines(function() {
    WebaudioMusicPlayer = SoundPlayer.extend({
        tagName: "WebaudioMusicPlayer",
        bgmPlaying: !1,
        isSupported: !1,
        muteFlag: !1,
        pausedTime: 0,
        webaudio: null,
        useHTML5Audio: !1,
        audio: null,
        inactiveAudio: null,
        codecs: null,
        reinitOnPlay: !1,
        inputList: null,
        _volume: 1,
        soundList: {},
        init: function(b) {
            this.webaudio = {
                compatibility: {},
                gainNode: null,
                buffer: null,
                source_loop: {},
                source_once: {}
            };
            try {
                Howler && Howler.ctx ? this.webaudio.context = Howler.ctx : ig && ig.webaudio_ctx ? this.webaudio.context = ig.webaudio_ctx : (this.AudioContext = window.AudioContext || window.webkitAudioContext,
                this.webaudio.context = new this.AudioContext,
                ig.webaudio_ctx = this.webaudio.context),
                this.isSupported = !0
            } catch (c) {
                console.log("Web Audio API not supported in this browser."),
                this.webaudio = null,
                this.useHTML5Audio = !0
            }
            if (this.useHTML5Audio)
                if ("undefined" !== typeof Audio)
                    try {
                        new Audio
                    } catch (c) {
                        this.useHTML5Audio = !1
                    }
                else
                    this.useHTML5Audio = !1;
            this.useHTML5Audio && (this.audio = new Audio,
            this.isSupported = !0,
            this.initHTML5Audio(b));
            if (!this.isSupported)
                return null;
            this.webaudio && (this.inputList = b,
            this.initWebAudio(b))
        },
        initWebAudio: function(b) {
            ig.ua.iOS && this.initIOSWebAudioUnlock();
            this.webaudio.gainNode = "undefined" === typeof this.webaudio.context.createGain ? this.webaudio.context.createGainNode() : this.webaudio.context.createGain();
            this.webaudio.gainNode.connect(this.webaudio.context.destination);
            this.webaudio.gainNode.gain.value = this._volume;
            this.webaudio.buffer = null;
            var c = "start"
              , e = "stop"
              , l = this.webaudio.context.createBufferSource();
            "function" !== typeof l.start && (c = "noteOn");
            this.webaudio.compatibility.start = c;
            "function" !== typeof l.stop && (e = "noteOff");
            this.webaudio.compatibility.stop = e;
            for (var q in b) {
                this.soundList[q] = q;
                e = b[q].path;
                c = e + "." + ig.Sound.FORMAT.MP3.ext;
                var x = e + "." + ig.Sound.FORMAT.OGG.ext;
                ig.ua.mobile ? ig.ua.iOS && (x = c) : (e = navigator.userAgent.toLowerCase(),
                -1 != e.indexOf("safari") && -1 >= e.indexOf("chrome") && (x = c),
                e.indexOf("win64") && (x = c));
                var C = new XMLHttpRequest;
                C.open("GET", x, !0);
                C.responseType = "arraybuffer";
                C.onload = function() {
                    this.webaudio.context.decodeAudioData(C.response, function(A) {
                        this.webaudio.buffer = A;
                        this.webaudio.source_loop = {};
                        this.bgmPlaying ? this.play(null, !0) : this.stop()
                    }
                    .bind(this), function() {
                        console.log('Error decoding audio "' + x + '".')
                    })
                }
                .bind(this);
                C.send();
                if (4 == C.readyState && "undefined" !== typeof Audio) {
                    this.useHTML5Audio = !0;
                    try {
                        new Audio
                    } catch (A) {
                        this.useHTML5Audio = !1
                    }
                    this.useHTML5Audio && (console.log("Using HTML5 Audio"),
                    this.webaudio = null,
                    this.audio = new Audio,
                    this.isSupported = !0,
                    this.initHTML5Audio(b))
                }
                break
            }
        },
        initIOSWebAudioUnlock: function() {
            if (this.webaudio) {
                webaudio = this.webaudio;
                var b = function() {
                    var c = webaudio.context
                      , e = c.createBuffer(1, 1, 22050)
                      , l = c.createBufferSource();
                    l.buffer = e;
                    l.connect(c.destination);
                    "undefined" === typeof l.start ? l.noteOn(0) : l.start(0);
                    setTimeout(function() {
                        l.playbackState !== l.PLAYING_STATE && l.playbackState !== l.FINISHED_STATE || window.removeEventListener("touchend", b, !1)
                    }
                    .bind(this), 0)
                };
                window.addEventListener("touchend", b, !1)
            }
        },
        initHTML5Audio: function(b) {
            if (this.useHTML5Audio && this.audio) {
                var c = this.audio;
                this.codecs = {};
                this.codecs = {
                    mp3: !!c.canPlayType("audio/mpeg;").replace(/^no$/, ""),
                    opus: !!c.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ""),
                    ogg: !!c.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
                    wav: !!c.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
                    aac: !!c.canPlayType("audio/aac;").replace(/^no$/, ""),
                    m4a: !!(c.canPlayType("audio/x-m4a;") || c.canPlayType("audio/m4a;") || c.canPlayType("audio/aac;")).replace(/^no$/, ""),
                    mp4: !!(c.canPlayType("audio/x-mp4;") || c.canPlayType("audio/mp4;") || c.canPlayType("audio/aac;")).replace(/^no$/, ""),
                    weba: !!c.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, "")
                };
                this.is = {
                    ff: !(null == window.mozInnerScreenX || !/firefox/.test(navigator.userAgent.toLowerCase())),
                    ie: !(!document.all || window.opera),
                    opera: !!window.opera,
                    chrome: !!window.chrome,
                    safari: !(window.chrome || !/safari/.test(navigator.userAgent.toLowerCase()) || !window.getComputedStyle || window.globalStorage || window.opera)
                };
                this.playDelay = -60;
                this.stopDelay = 30;
                this.is.chrome && (this.playDelay = -25);
                this.is.chrome && (this.stopDelay = 25);
                this.is.ff && (this.playDelay = -25);
                this.is.ff && (this.stopDelay = 85);
                this.is.opera && (this.playDelay = 5);
                this.is.opera && (this.stopDelay = 0);
                for (var e in b) {
                    this.soundList[e] = e;
                    var l = b[e].path;
                    c = l + "." + ig.Sound.FORMAT.OGG.ext;
                    l = l + "." + ig.Sound.FORMAT.MP3.ext;
                    var q = null;
                    this.codecs[ig.Sound.FORMAT.OGG.ext.toLowerCase()] ? q = c : this.codecs[ig.Sound.FORMAT.MP3.ext.toLowerCase()] && (q = l);
                    if (q) {
                        ig.ua.mobile ? ig.ua.iOS && (q = l) : (b = navigator.userAgent.toLowerCase(),
                        -1 != b.indexOf("safari") && -1 >= b.indexOf("chrome") && (q = l));
                        this.audio.addEventListener("error", function() {
                            this.audio.error && 4 === this.audio.error.code && (this.isSupported = !1)
                        }, !1);
                        this.audio.src = q;
                        this.audio._pos = 0;
                        this.audio.preload = "auto";
                        this.audio.volume = this._volume;
                        this.inactiveAudio = new Audio;
                        this.inactiveAudio.src = q;
                        this.inactiveAudio._pos = 0;
                        this.inactiveAudio.preload = "auto";
                        this.inactiveAudio.volume = this._volume;
                        this.inactiveAudio.load();
                        var x = function() {
                            this._duration = this.audio.duration;
                            this._loaded || (this._loaded = !0);
                            this.bgmPlaying ? this.play(null, !0) : this.stop();
                            this.audio.removeEventListener("canplaythrough", x, !1)
                        }
                        .bind(this);
                        this.audio.addEventListener("canplaythrough", x, !1);
                        this.audio.load();
                        break
                    }
                }
            }
        },
        play: function(b, c) {
            if (this.isSupported)
                if (this.bgmPlaying = !0,
                this.webaudio) {
                    if (!c && this.reinitOnPlay && this.webaudio.source_loop.buffer == this.webaudio.buffer) {
                        if (this.webaudio.source_loop._playing && (this.webaudio.source_loop[this.webaudio.compatibility.stop](0),
                        this.webaudio.source_loop._playing = !1,
                        this.pausedTime += this.webaudio.context.currentTime - this.webaudio.source_loop._startTime,
                        this.pausedTime %= this.webaudio.source_loop.buffer.duration,
                        this.webaudio.source_loop._startTime = 0,
                        "noteOn" === this.webaudio.compatibility.start))
                            this.webaudio.source_once[this.webaudio.compatibility.stop](0);
                        try {
                            this.webaudio.context.close();
                            this.webaudio.context = new this.AudioContext;
                            this.webaudio.gainNode = this.webaudio.context.createGain();
                            this.webaudio.gainNode.connect(this.webaudio.context.destination);
                            this.webaudio.gainNode.gain.value = this._volume;
                            c = "start";
                            var e = "stop"
                              , l = this.webaudio.context.createBufferSource();
                            "function" !== typeof l.start && (c = "noteOn");
                            this.webaudio.compatibility.start = c;
                            "function" !== typeof l.stop && (e = "noteOff");
                            this.webaudio.compatibility.stop = e;
                            this.webaudio.source_loop = {};
                            this.play(null, !0)
                        } catch (x) {}
                    }
                    if (!this.webaudio.buffer)
                        this.bgmPlaying = !0;
                    else if (!this.muteFlag && (this.bgmPlaying = !0,
                    !this.webaudio.source_loop._playing)) {
                        this.webaudio.source_loop = this.webaudio.context.createBufferSource();
                        this.webaudio.source_loop.buffer = this.webaudio.buffer;
                        this.webaudio.source_loop.loop = !0;
                        this.webaudio.source_loop.connect(this.webaudio.gainNode);
                        if (null == b || isNaN(b))
                            b = 0,
                            this.pausedTime && (b = this.pausedTime);
                        this.webaudio.source_loop._startTime = this.webaudio.context.currentTime;
                        if ("noteOn" === this.webaudio.compatibility.start)
                            this.webaudio.source_once = this.webaudio.context.createBufferSource(),
                            this.webaudio.source_once.buffer = this.webaudio.buffer,
                            this.webaudio.source_once.connect(this.webaudio.gainNode),
                            this.webaudio.source_once.noteGrainOn(0, b, this.webaudio.buffer.duration - b),
                            this.webaudio.source_loop[this.webaudio.compatibility.start](this.webaudio.context.currentTime + (this.webaudio.buffer.duration - b));
                        else
                            this.webaudio.source_loop[this.webaudio.compatibility.start](0, b);
                        this.webaudio.source_loop._playing = !0
                    }
                } else if (this.audio) {
                    var q = this.audio;
                    this.muteFlag || (this.bgmPlaying = !0,
                    isNaN(b) && (b = 0,
                    this.pausedTime && (b = this.pausedTime)),
                    l = this._duration - b,
                    this._onEndTimer && (clearTimeout(this._onEndTimer),
                    this._onEndTimer = null),
                    this._onEndTimer = setTimeout(function() {
                        this.audio.currentTime = 0;
                        this.audio.pause();
                        this.pausedTime = 0;
                        if (this.inactiveAudio) {
                            var x = this.audio;
                            this.audio = this.inactiveAudio;
                            this.inactiveAudio = x
                        }
                        this.play()
                    }
                    .bind(this), 1E3 * l + this.playDelay),
                    4 === q.readyState || !q.readyState && navigator.isCocoonJS ? (q.readyState = 4,
                    q.currentTime = b,
                    q.muted = this.muteFlag || q.muted,
                    q.volume = this._volume,
                    setTimeout(function() {
                        q.play()
                    }, 0)) : (clearTimeout(this._onEndTimer),
                    this._onEndTimer = null,
                    function() {
                        var x = function() {
                            typeof ("function" == this.play) && (this.play(),
                            q.removeEventListener("canplaythrough", x, !1))
                        }
                        .bind(this);
                        q.addEventListener("canplaythrough", x, !1)
                    }
                    .call(this)))
                }
        },
        stop: function() {
            this.bgmPlaying = !1;
            if (this.isSupported)
                if (this.webaudio) {
                    if (this.webaudio.source_loop._playing && (this.webaudio.source_loop[this.webaudio.compatibility.stop](0),
                    this.webaudio.source_loop._playing = !1,
                    this.pausedTime += this.webaudio.context.currentTime - this.webaudio.source_loop._startTime,
                    this.pausedTime %= this.webaudio.source_loop.buffer.duration,
                    this.webaudio.source_loop._startTime = 0,
                    "noteOn" === this.webaudio.compatibility.start))
                        this.webaudio.source_once[this.webaudio.compatibility.stop](0)
                } else if (this.audio) {
                    var b = this.audio;
                    4 == b.readyState && (this.pausedTime = b.currentTime,
                    b.currentTime = 0,
                    b.pause(),
                    clearTimeout(this._onEndTimer),
                    this._onEndTimer = null)
                }
        },
        volume: function(b) {
            if (isNaN(b) || null == b)
                return this.getVolume();
            this.isSupported && (this._volume = b,
            0 > this._volume ? this._volume = 0 : 1 < this._volume && (this._volume = 1),
            this.webaudio ? this.webaudio.gainNode && (this.webaudio.gainNode.gain.value = this._volume) : this.audio && (this.audio.volume = this._volume,
            this.inactiveAudio && (this.inactiveAudio.volume = this._volume)))
        },
        getVolume: function() {
            return this.isSupported ? this._volume : 0
        },
        mute: function(b) {
            this.parent(b);
            0 == this.muteFlag && (this.muteFlag = !0,
            this.bgmPlaying && (this.stop(),
            this.bgmPlaying = !0))
        },
        unmute: function(b) {
            this.parent(b);
            this.stayMuteFlag || 1 != this.muteFlag || (this.muteFlag = !1,
            this.bgmPlaying && this.play())
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.sound-info").defines(function() {
    SoundInfo = ig.Class.extend({
        FORMATS: {
            OGG: ".ogg",
            MP3: ".mp3"
        },
        sfx: {
            kittyopeningSound: {
                path: "/static/spin-arena/v1/media/audio/opening/kittyopening"
            },
            staticSound: {
                path: "/static/spin-arena/v1/media/audio/play/static"
            },
            openingSound: {
                path: "/static/spin-arena/v1/media/audio/opening/opening"
            },
            button: {
                path: "/static/spin-arena/v1/media/audio/play/button"
            },
            tick: {
                path: "/static/spin-arena/v1/media/audio/play/tick"
            },
            lose: {
                path: "/static/spin-arena/v1/media/audio/play/lose"
            },
            win: {
                path: "/static/spin-arena/v1/media/audio/play/win"
            },
            win2: {
                path: "/static/spin-arena/v1/media/audio/play/win2"
            }
        },
        bgm1: {
            idle: {
                path: "/static/spin-arena/v1/media/audio/idle",
                startOgg: 0,
                endOgg: 9.104,
                startMp3: 0,
                endMp3: 9.104
            }
        },
        bgm2: {
            background: {
                path: "/static/spin-arena/v1/media/audio/bgm",
                startOgg: 0,
                endOgg: 21.463,
                startMp3: 0,
                endMp3: 21.463
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.audio.sound-handler").requires("plugins.audio.impact-music-player", "plugins.audio.impact-sound-player", "plugins.audio.howler-player", "plugins.audio.howler-music-player", "plugins.audio.jukebox-player", "plugins.audio.webaudio-music-player", "plugins.audio.sound-info").defines(function() {
    ig.SoundHandler = ig.Class.extend({
        bgmPlayer: null,
        sfxPlayer: null,
        focusBlurMute: !1,
        soundInfo: new SoundInfo,
        init: function() {
            console.log("Initiating sound handler");
            ig.ua.mobile ? (this.sfxPlayer = new HowlerPlayer(this.soundInfo.sfx),
            this.bgmPlayer1 = new WebaudioMusicPlayer(this.soundInfo.bgm1,{
                loop: !0,
                reinitOnPlay: !0
            }),
            this.bgmPlayer1.isSupported || (this.bgmPlayer1 = new JukeboxPlayer(this.soundInfo.bgm1,{
                loop: !0,
                reinitOnPlay: !0
            })),
            this.bgmPlayer2 = new WebaudioMusicPlayer(this.soundInfo.bgm2,{
                loop: !0,
                reinitOnPlay: !0
            }),
            this.bgmPlayer2.isSupported || (this.bgmPlayer2 = new JukeboxPlayer(this.soundInfo.bgm2,{
                loop: !0,
                reinitOnPlay: !0
            })),
            this.bgmPlayer3 = new WebaudioMusicPlayer(this.soundInfo.bgm3,{
                loop: !0,
                reinitOnPlay: !0
            }),
            this.bgmPlayer3.isSupported || (this.bgmPlayer3 = new JukeboxPlayer(this.soundInfo.bgm3,{
                loop: !0,
                reinitOnPlay: !0
            }))) : (this.sfxPlayer = new HowlerPlayer(this.soundInfo.sfx),
            this.bgmPlayer1 = new WebaudioMusicPlayer(this.soundInfo.bgm1,{
                loop: !0,
                reinitOnPlay: !0
            }),
            this.bgmPlayer1.isSupported || (this.bgmPlayer1 = new ImpactMusicPlayer(this.soundInfo.bgm1,{
                loop: !0,
                reinitOnPlay: !0
            })),
            this.bgmPlayer2 = new WebaudioMusicPlayer(this.soundInfo.bgm2,{
                loop: !0,
                reinitOnPlay: !0
            }),
            this.bgmPlayer2.isSupported || (this.bgmPlayer2 = new ImpactMusicPlayer(this.soundInfo.bgm2,{
                loop: !0,
                reinitOnPlay: !0
            })),
            this.bgmPlayer3 = new WebaudioMusicPlayer(this.soundInfo.bgm3,{
                loop: !0,
                reinitOnPlay: !0
            }),
            this.bgmPlayer3.isSupported || (this.bgmPlayer3 = new ImpactMusicPlayer(this.soundInfo.bgm3,{
                loop: !0,
                reinitOnPlay: !0
            })))
        },
        unlockWebAudio: function() {
            Howler && (Howler.ctx && "running" !== Howler.ctx.state && Howler.ctx.resume(),
            Howler._audioUnlocked || "function" === typeof Howler._unlockAudio && Howler._unlockAudio());
            ig && ig.webaudio_ctx && ig.webaudio_ctx.state && "running" !== ig.webaudio_ctx.state && ig.webaudio_ctx.resume();
            this.bgmPlayer1.webaudio && this.bgmPlayer1.webaudio.context && this.bgmPlayer1.bgmPlaying && this.bgmPlayer1.webaudio.context.state && "running" !== this.bgmPlayer1.webaudio.context.state && this.bgmPlayer1.webaudio.context.resume()
        },
        checkBGM: function() {
            return this.bgmPlayer1.stayMuteFlag
        },
        checkSFX: function() {
            return this.sfxPlayer.stayMuteFlag
        },
        muteSFX: function(b) {
            this.sfxPlayer && this.sfxPlayer.mute(b)
        },
        muteBGM: function(b) {
            this.bgmPlayer1 && this.bgmPlayer1.mute(b);
            this.bgmPlayer2 && this.bgmPlayer2.mute(b);
            this.bgmPlayer3 && this.bgmPlayer3.mute(b)
        },
        unmuteSFX: function(b) {
            this.sfxPlayer && this.sfxPlayer.unmute(b)
        },
        unmuteBGM: function(b) {
            this.bgmPlayer1 && this.bgmPlayer1.unmute(b);
            this.bgmPlayer2 && this.bgmPlayer2.unmute(b);
            this.bgmPlayer3 && this.bgmPlayer3.unmute(b)
        },
        muteAll: function(b) {
            this.muteSFX(b);
            this.muteBGM(b)
        },
        unmuteAll: function(b) {
            this.unlockWebAudio();
            this.unmuteSFX(b);
            this.unmuteBGM(b)
        },
        forceMuteAll: function() {
            this.focusBlurMute || this.muteAll(!1);
            this.focusBlurMute = !0
        },
        forceUnMuteAll: function() {
            this.focusBlurMute && (this.unmuteAll(!1),
            this.focusBlurMute = !1)
        },
        saveVolume: function() {
            this.sfxPlayer && ig.game.io.storageSet("soundVolume", this.sfxPlayer.getVolume());
            this.bgmPlayer1 && ig.game.io.storageSet("musicVolume", this.bgmPlayer1.getVolume())
        },
        forceLoopBGM: function() {
            this.focusBlurMute || (this.bgmPlayer1 && this.bgmPlayer1.bgmPlaying && this.forceLoop(this.bgmPlayer1),
            this.bgmPlayer2 && this.bgmPlayer2.bgmPlaying && this.forceLoop(this.bgmPlayer2),
            this.bgmPlayer3 && this.bgmPlayer3.bgmPlaying && this.forceLoop(this.bgmPlayer3))
        },
        forceLoop: function(b) {
            var c = b.jukeboxPlayer;
            if (c) {
                null != window.mozInnerScreenX && /firefox/.test(navigator.userAgent.toLowerCase());
                var e = !!window.chrome;
                !window.chrome && /safari/.test(navigator.userAgent.toLowerCase());
                b = .1;
                ig.ua.mobile && (b = .115,
                ig.ua.android && (b = .45,
                e && (b = .3)));
                c.settings.spritemap.music && (e = c.settings.spritemap.music.end - b,
                c.getCurrentTime() >= e && (e = c.settings.spritemap.music.start,
                ig.ua.android ? this.forcelooped || (c.play(e, !0),
                this.forcelooped = !0,
                setTimeout(function() {
                    ig.soundHandler.forcelooped = !1
                }, b)) : c.setCurrentTime(e)))
            } else
                "ImpactMusicPlayer" == b.tagName && (null != window.mozInnerScreenX && /firefox/.test(navigator.userAgent.toLowerCase()),
                e = !!window.chrome,
                !window.chrome && /safari/.test(navigator.userAgent.toLowerCase()),
                b = .1,
                ig.ua.mobile && (b = .115,
                ig.ua.android && (b = .45,
                e && (b = .3))),
                c = 0,
                "mp3" == ig.soundManager.format.ext && (c = .05),
                ig.music.currentTrack && (e = ig.music.currentTrack.duration - b,
                ig.music.currentTrack.currentTime >= e && (ig.ua.android ? this.forcelooped || (ig.music.currentTrack.pause(),
                ig.music.currentTrack.currentTime = c,
                ig.music.currentTrack.play(),
                this.forcelooped = !0,
                setTimeout(function() {
                    ig.soundHandler.forcelooped = !1
                }, b)) : ig.music.currentTrack.currentTime = c)))
        }
    })
});
ig.baked = !0;
ig.module("plugins.handlers.visibility-handler").requires("plugins.audio.sound-handler").defines(function() {
    ig.VisibilityHandler = ig.Class.extend({
        version: "1.0.3",
        config: {
            allowResumeWithoutFocus: {
                desktop: !0,
                mobile: {
                    kaios: !1,
                    default: !0
                }
            },
            handlerDelay: {
                desktop: 0,
                mobile: {
                    kaios: 0,
                    default: 0
                }
            },
            autoFocusOnResume: {
                desktop: !0,
                mobile: {
                    kaios: !1,
                    default: !0
                }
            },
            autoFocusAfterResume: {
                desktop: !0,
                mobile: {
                    kaios: !1,
                    default: !0
                }
            }
        },
        browserPrefixes: ["o", "ms", "moz", "webkit"],
        browserPrefix: null,
        hiddenPropertyName: null,
        visibilityEventName: null,
        visibilityStateName: null,
        isShowingOverlay: !1,
        isFocused: !1,
        isPaused: !1,
        init: function() {
            this.initVisibilityHandler();
            this.initFocusHandler();
            this.initPageTransitionHandler();
            ig.visibilityHandler = this
        },
        pauseHandler: function() {
            ig.game && ig.game.pauseGame();
            ig.soundHandler && ig.soundHandler.forceMuteAll()
        },
        resumeHandler: function() {
            ig.game && ig.game.resumeGame();
            ig.soundHandler && ig.soundHandler.forceUnMuteAll()
        },
        initVisibilityHandler: function() {
            this.browserPrefix = this.getBrowserPrefix();
            this.hiddenPropertyName = this.getHiddenPropertyName(this.browserPrefix);
            this.visibilityEventName = this.getVisibilityEventName(this.browserPrefix);
            this.visibilityStateName = this.getVisibilityStateName(this.browserPrefix);
            this.visibilityEventName && document.addEventListener(this.visibilityEventName, this.onChange.bind(this), !0)
        },
        initFocusHandler: function() {
            window.addEventListener("blur", this.onChange.bind(this), !0);
            document.addEventListener("blur", this.onChange.bind(this), !0);
            document.addEventListener("focusout", this.onChange.bind(this), !0);
            window.addEventListener("focus", this.onChange.bind(this), !0);
            document.addEventListener("focus", this.onChange.bind(this), !0);
            document.addEventListener("focusin", this.onChange.bind(this), !0)
        },
        initPageTransitionHandler: function() {
            window.addEventListener("pagehide", this.onChange.bind(this), !0);
            window.addEventListener("pageshow", this.onChange.bind(this), !0)
        },
        getBrowserPrefix: function() {
            var b = null;
            this.browserPrefixes.forEach(function(c) {
                if (this.getHiddenPropertyName(c)in document)
                    return b = c
            }
            .bind(this));
            return b
        },
        getHiddenPropertyName: function(b) {
            return b ? b + "Hidden" : "hidden"
        },
        getVisibilityEventName: function(b) {
            return (b ? b : "") + "visibilitychange"
        },
        getVisibilityStateName: function(b) {
            return b ? b + "VisibilityState" : "visibilityState"
        },
        hasView: function() {
            return !(document[this.hiddenPropertyName] || "visible" !== document[this.visibilityStateName])
        },
        hasFocus: function() {
            return document.hasFocus() || this.isFocused
        },
        onOverlayShow: function() {
            this.systemPaused();
            this.isShowingOverlay = !0
        },
        onOverlayHide: function() {
            this.isShowingOverlay = !1;
            this.systemResumed()
        },
        systemPaused: function(b) {
            if (this.isPaused)
                return !1;
            this.pauseHandler();
            return this.isPaused = !0
        },
        systemResumed: function(b) {
            if (!this.isPaused || !this.hasView() || this.isShowingOverlay)
                return !1;
            if (!this.hasFocus())
                if (ig.ua.mobile)
                    if (this.isKaiOS()) {
                        if (!this.config.allowResumeWithoutFocus.mobile.kaios)
                            return !1
                    } else {
                        if (!this.config.allowResumeWithoutFocus.mobile.default)
                            return !1
                    }
                else if (!this.config.allowResumeWithoutFocus.desktop)
                    return !1;
            this.focusOnResume();
            this.resumeHandler();
            this.focusAfterResume();
            this.isPaused = !1;
            return !0
        },
        isKaiOS: function() {
            return /KAIOS/.test(navigator.userAgent) || !1
        },
        focusOnResume: function() {
            return ig.ua.mobile ? this.isKaiOS() ? this.config.autoFocusOnResume.mobile.kaios : this.config.autoFocusOnResume.mobile.default : this.config.autoFocusOnResume.desktop
        },
        focusAfterResume: function() {
            return ig.ua.mobile ? this.isKaiOS() ? this.config.autoFocusAfterResume.mobile.kaios : this.config.autoFocusAfterResume.mobile.default : this.config.autoFocusAfterResume.desktop
        },
        focus: function(b) {
            window.focus && b && window.focus()
        },
        handleDelayedEvent: function(b) {
            if (this.hasView() && "pause" !== b.type && "pageHide" !== b.type && "blur" !== b.type && "focusout" !== b.type) {
                if ("focus" === b.type || "focusin" === b.type)
                    this.isFocused = !0;
                return this.systemResumed(b)
            }
        },
        startDelayedEventHandler: function(b) {
            ig.ua.mobile ? this.isKaiOS() ? 0 < this.config.handlerDelay.mobile.kaios ? window.setTimeout(function(c) {
                this.handleDelayedEvent(c)
            }
            .bind(this, b), this.config.handlerDelay.mobile) : this.handleDelayedEvent(b) : 0 < this.config.handlerDelay.mobile.default ? window.setTimeout(function(c) {
                this.handleDelayedEvent(c)
            }
            .bind(this, b), this.config.handlerDelay.mobile) : this.handleDelayedEvent(b) : 0 < this.config.handlerDelay.desktop ? window.setTimeout(function(c) {
                this.handleDelayedEvent(c)
            }
            .bind(this, b), this.config.handlerDelay.desktop) : this.handleDelayedEvent(b)
        },
        onChange: function(b) {
            this.startDelayedEventHandler(b)
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.storage").defines(function() {
    ig.Storage = ig.Class.extend({
        staticInstantiate: function(b) {
            return ig.Storage.instance ? ig.Storage.instance : null
        },
        init: function() {
            ig.Storage.instance = this
        },
        isCapable: function() {
            try {
                return "undefined" !== typeof window.localStorage
            } catch (b) {
                return !1
            }
        },
        isSet: function(b) {
            return null !== this.get(b)
        },
        initUnset: function(b, c) {
            null === this.get(b) && this.set(b, c)
        },
        get: function(b) {
            if (!this.isCapable())
                return null;
            try {
                return JSON.parse(localStorage.getItem(b))
            } catch (c) {
                return window.localStorage.getItem(b)
            }
        },
        getInt: function(b) {
            return ~~this.get(b)
        },
        getFloat: function(b) {
            return parseFloat(this.get(b))
        },
        getBool: function(b) {
            return !!this.get(b)
        },
        key: function(b) {
            return this.isCapable() ? window.localStorage.key(b) : null
        },
        set: function(b, c) {
            if (!this.isCapable())
                return null;
            try {
                window.localStorage.setItem(b, JSON.stringify(c))
            } catch (e) {
                console.log(e)
            }
        },
        setHighest: function(b, c) {
            c > this.getFloat(b) && this.set(b, c)
        },
        remove: function(b) {
            if (!this.isCapable())
                return null;
            window.localStorage.removeItem(b)
        },
        clear: function() {
            if (!this.isCapable())
                return null;
            window.localStorage.clear()
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.mouse").requires("plugins.data.vector").defines(function() {
    Mouse = ig.Class.extend({
        pos: new Vector2(0,0),
        bindings: {
            click: [ig.KEY.MOUSE1]
        },
        init: function() {
            ig.input.initMouse();
            for (var b in this.bindings) {
                this[b] = b;
                for (var c = 0; c < this.bindings[b].length; c++)
                    ig.input.bind(this.bindings[b][c], b)
            }
        },
        getLast: function() {
            return this.pos
        },
        getPos: function() {
            this.pos.set(ig.input.mouse.x / ig.sizeHandler.sizeRatio.x / ig.sizeHandler.scaleRatioMultiplier.x, ig.input.mouse.y / ig.sizeHandler.sizeRatio.y / ig.sizeHandler.scaleRatioMultiplier.y);
            return this.pos.clone()
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.keyboard").defines(function() {
    Keyboard = ig.Class.extend({
        bindings: {
            jump: [ig.KEY.W, ig.KEY.UP_ARROW],
            moveright: [ig.KEY.D, ig.KEY.RIGHT_ARROW],
            moveleft: [ig.KEY.A, ig.KEY.LEFT_ARROW],
            shoot: [ig.KEY.S, ig.KEY.DOWN_ARROW, ig.KEY.SPACE]
        },
        init: function() {
            for (var b in this.bindings) {
                this[b] = b;
                for (var c = 0; c < this.bindings[b].length; c++)
                    ig.input.bind(this.bindings[b][c], b)
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.gamepad-input").defines(function() {
    ig.PADKEY = {
        BUTTON_0: 0,
        PADBUTTON_1: 1,
        BUTTON_2: 2,
        BUTTON_3: 3,
        BUTTON_LEFT_BUMPER: 4,
        BUTTON_RIGHT_BUMPER: 5,
        BUTTON_LEFT_TRIGGER: 6,
        BUTTON_RIGHT_TRIGGER: 7,
        BUTTON_LEFT_JOYSTICK: 10,
        BUTTON_RIGHT_JOYSTICK: 11,
        BUTTON_DPAD_UP: 12,
        BUTTON_DPAD_DOWN: 13,
        BUTTON_DPAD_LEFT: 14,
        BUTTON_DPAD_RIGHT: 15,
        BUTTON_MENU: 16,
        AXIS_LEFT_JOYSTICK_X: 0,
        AXIS_LEFT_JOYSTICK_Y: 1,
        AXIS_RIGHT_JOYSTICK_X: 2,
        AXIS_RIGHT_JOYSTICK_Y: 3
    };
    ig.GamepadInput = ig.Class.extend({
        isInit: !1,
        isSupported: !1,
        list: [],
        bindings: {},
        states: {},
        presses: {},
        releases: {},
        downLocks: {},
        upLocks: {},
        leftStick: {
            x: 0,
            y: 0
        },
        rightStick: {
            x: 0,
            y: 0
        },
        start: function() {
            if (!this.isInit) {
                this.isInit = !0;
                var b = navigator.getGamepads || navigator.webkitGetGamepads;
                b && (!navigator.getGamepads && navigator.webkitGetGamepads && (navigator.getGamepads = navigator.webkitGetGamepads),
                this.list = navigator.getGamepads());
                this.isSupported = b
            }
        },
        isAvailable: function() {
            return this.isInit && this.isSupported
        },
        buttonPressed: function(b) {
            return "object" == typeof b ? b.pressed : 1 == b
        },
        buttonDown: function(b) {
            if (b = this.bindings[b])
                this.states[b] = !0,
                this.downLocks[b] || (this.presses[b] = !0,
                this.downLocks[b] = !0)
        },
        buttonUp: function(b) {
            (b = this.bindings[b]) && this.downLocks[b] && !this.upLocks[b] && (this.states[b] = !1,
            this.releases[b] = !0,
            this.upLocks[b] = !0)
        },
        clearPressed: function() {
            for (var b in this.releases)
                this.states[b] = !1,
                this.downLocks[b] = !1;
            this.releases = {};
            this.presses = {};
            this.upLocks = {}
        },
        bind: function(b, c) {
            this.bindings[b] = c
        },
        unbind: function(b) {
            this.releases[this.bindings[b]] = !0;
            this.bindings[b] = null
        },
        unbindAll: function() {
            this.bindings = {};
            this.states = {};
            this.presses = {};
            this.releases = {};
            this.downLocks = {};
            this.upLocks = {}
        },
        state: function(b) {
            return this.states[b]
        },
        pressed: function(b) {
            return this.presses[b]
        },
        released: function(b) {
            return this.releases[b]
        },
        clamp: function(b, c, e) {
            return b < c ? c : b > e ? e : b
        },
        pollGamepads: function() {
            if (this.isSupported) {
                this.leftStick.x = 0;
                this.leftStick.y = 0;
                this.rightStick.x = 0;
                this.rightStick.y = 0;
                this.list = navigator.getGamepads();
                for (var b in this.bindings) {
                    for (var c = !1, e = 0; e < this.list.length; e++) {
                        var l = this.list[e];
                        if (l && l.buttons && this.buttonPressed(l.buttons[b])) {
                            c = !0;
                            break
                        }
                    }
                    c ? this.buttonDown(b) : this.buttonUp(b)
                }
                for (e = 0; e < this.list.length; e++)
                    if ((l = this.list[e]) && l.axes) {
                        b = l.axes[ig.GAMEPADINPUT.AXIS_LEFT_JOYSTICK_X];
                        c = l.axes[ig.GAMEPADINPUT.AXIS_LEFT_JOYSTICK_Y];
                        var q = l.axes[ig.GAMEPADINPUT.AXIS_RIGHT_JOYSTICK_X];
                        l = l.axes[ig.GAMEPADINPUT.AXIS_RIGHT_JOYSTICK_Y];
                        this.leftStick.x += isNaN(b) ? 0 : b;
                        this.leftStick.y += isNaN(c) ? 0 : c;
                        this.rightStick.x += isNaN(q) ? 0 : q;
                        this.rightStick.y += isNaN(l) ? 0 : l
                    }
                0 < this.list.length && (this.leftStick.x = this.clamp(this.leftStick.x, -1, 1),
                this.leftStick.y = this.clamp(this.leftStick.y, -1, 1),
                this.rightStick.x = this.clamp(this.rightStick.x, -1, 1),
                this.rightStick.y = this.clamp(this.rightStick.y, -1, 1))
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.gamepad").requires("plugins.io.gamepad-input").defines(function() {
    Gamepad = ig.Class.extend({
        bindings: {
            padJump: [ig.PADKEY.BUTTON_0]
        },
        init: function() {
            ig.gamepadInput.start();
            for (var b in this.bindings)
                for (var c = 0; c < this.bindings[b].length; c++)
                    ig.gamepadInput.bind(this.bindings[b][c], b)
        },
        press: function(b) {},
        held: function(b) {},
        release: function(b) {}
    })
});
ig.baked = !0;
ig.module("plugins.io.multitouch").defines(function() {
    Multitouch = ig.Class.extend({
        init: function() {
            ig.multitouchInput.start()
        },
        getTouchesPos: function() {
            if (ig.ua.mobile) {
                if (0 < ig.multitouchInput.touches.length) {
                    for (var b = [], c = 0; c < ig.multitouchInput.touches.length; c++) {
                        var e = ig.multitouchInput.touches[c];
                        b.push({
                            x: e.x,
                            y: e.y
                        })
                    }
                    return b
                }
                return null
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.multitouch-input").defines(function() {
    ig.MultitouchInput = ig.Class.extend({
        isStart: !1,
        touches: [],
        multitouchCapable: !1,
        lastEventUp: null,
        start: function() {
            this.isStart || (this.isStart = !0,
            navigator.maxTouchPoints && 1 < navigator.maxTouchPoints && (this.multitouchCapable = !0),
            ig.ua.touchDevice && (window.navigator.msPointerEnabled && (ig.system.canvas.addEventListener("MSPointerDown", this.touchdown.bind(this), !1),
            ig.system.canvas.addEventListener("MSPointerUp", this.touchup.bind(this), !1),
            ig.system.canvas.addEventListener("MSPointerMove", this.touchmove.bind(this), !1),
            ig.system.canvas.style.msContentZooming = "none",
            ig.system.canvas.style.msTouchAction = "none"),
            ig.system.canvas.addEventListener("touchstart", this.touchdown.bind(this), !1),
            ig.system.canvas.addEventListener("touchend", this.touchup.bind(this), !1),
            ig.system.canvas.addEventListener("touchmove", this.touchmove.bind(this), !1)))
        },
        touchmove: function(b) {
            if (ig.ua.touchDevice) {
                var c = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth
                  , e = parseInt(ig.system.canvas.offsetHeight) || ig.system.realHeight;
                c = c / ig.system.realWidth * ig.system.scale;
                e = e / ig.system.realHeight * ig.system.scale;
                if (b.touches) {
                    for (; 0 < this.touches.length; )
                        this.touches.pop();
                    !this.multitouchCapable && 1 < b.touches.length && (this.multitouchCapable = !0);
                    var l = {
                        left: 0,
                        top: 0
                    };
                    ig.system.canvas.getBoundingClientRect && (l = ig.system.canvas.getBoundingClientRect());
                    for (var q = 0; q < b.touches.length; q++) {
                        var x = b.touches[q];
                        x && this.touches.push({
                            x: (x.clientX - l.left) / c,
                            y: (x.clientY - l.top) / e
                        })
                    }
                } else
                    this.windowMove(b)
            }
            try {
                ig.soundHandler.unlockWebAudio()
            } catch (C) {}
        },
        touchdown: function(b) {
            var c = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth
              , e = parseInt(ig.system.canvas.offsetHeight) || ig.system.realHeight;
            c = c / ig.system.realWidth * ig.system.scale;
            e = e / ig.system.realHeight * ig.system.scale;
            if (window.navigator.msPointerEnabled)
                this.windowKeyDown(b);
            else if (ig.ua.touchDevice && b.touches) {
                for (; 0 < this.touches.length; )
                    this.touches.pop();
                !this.multitouchCapable && 1 < b.touches.length && (this.multitouchCapable = !0);
                var l = {
                    left: 0,
                    top: 0
                };
                ig.system.canvas.getBoundingClientRect && (l = ig.system.canvas.getBoundingClientRect());
                for (var q = 0; q < b.touches.length; q++) {
                    var x = b.touches[q];
                    x && this.touches.push({
                        x: (x.clientX - l.left) / c,
                        y: (x.clientY - l.top) / e
                    })
                }
            }
        },
        touchup: function(b) {
            var c = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth;
            parseInt(ig.system.canvas.offsetHeight);
            c = c / ig.system.realWidth * ig.system.scale;
            if (window.navigator.msPointerEnabled)
                this.windowKeyUp(b);
            else {
                this.lastEventUp = b;
                var e = {
                    left: 0,
                    top: 0
                };
                ig.system.canvas.getBoundingClientRect && (e = ig.system.canvas.getBoundingClientRect());
                if (ig.ua.touchDevice)
                    for (b = (b.changedTouches[0].clientX - e.left) / c,
                    c = 0; c < this.touches.length; c++)
                        this.touches[c].x >= b - 40 && this.touches[c].x <= b + 40 && this.touches.splice(c, 1)
            }
            if (ig.visibilityHandler)
                ig.visibilityHandler.onChange("focus");
            try {
                ig.soundHandler.unlockWebAudio()
            } catch (l) {}
        },
        windowKeyDown: function(b) {
            var c = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth
              , e = parseInt(ig.system.canvas.offsetHeight) || ig.system.realHeight;
            c = c / ig.system.realWidth * ig.system.scale;
            e = e / ig.system.realHeight * ig.system.scale;
            if (window.navigator.msPointerEnabled) {
                var l = {
                    left: 0,
                    top: 0
                };
                ig.system.canvas.getBoundingClientRect && (l = ig.system.canvas.getBoundingClientRect());
                b = b.changedTouches ? b.changedTouches : [b];
                for (var q = 0; q < b.length; ++q) {
                    var x = b[q]
                      , C = "undefined" != typeof x.identifier ? x.identifier : "undefined" != typeof x.pointerId ? x.pointerId : 1
                      , A = (x.clientX - l.left) / c;
                    x = (x.clientY - l.top) / e;
                    for (var E = 0; E < this.touches.length; ++E)
                        this.touches[E].identifier == C && this.touches.splice(E, 1);
                    this.touches.push({
                        x: A,
                        y: x,
                        identifier: C
                    })
                }
                for (c = 0; c < this.touches.length; c++)
                    ;
            }
        },
        windowKeyUp: function(b) {
            b = "undefined" != typeof b.identifier ? b.identifier : "undefined" != typeof b.pointerId ? b.pointerId : 1;
            for (var c = 0; c < this.touches.length; ++c)
                this.touches[c].identifier == b && this.touches.splice(c, 1);
            for (; 0 < this.touches.length; )
                this.touches.pop();
            if (ig.visibilityHandler)
                ig.visibilityHandler.onChange("focus");
            try {
                ig.soundHandler.unlockWebAudio()
            } catch (e) {}
        },
        windowMove: function(b) {
            var c = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth
              , e = parseInt(ig.system.canvas.offsetHeight) || ig.system.realHeight;
            c = c / ig.system.realWidth * ig.system.scale;
            e = e / ig.system.realHeight * ig.system.scale;
            var l = {
                left: 0,
                top: 0
            };
            ig.system.canvas.getBoundingClientRect && (l = ig.system.canvas.getBoundingClientRect());
            if (window.navigator.msPointerEnabled)
                for (var q = "undefined" != typeof b.identifier ? b.identifier : "undefined" != typeof b.pointerId ? b.pointerId : 1, x = 0; x < this.touches.length; ++x)
                    if (this.touches[x].identifier == q) {
                        var C = (b.clientY - l.top) / e;
                        this.touches[x].x = (b.clientX - l.left) / c;
                        this.touches[x].y = C
                    }
            try {
                ig.soundHandler.unlockWebAudio()
            } catch (A) {}
        },
        clear: function() {
            for (var b = 0; b < this.released.length; ++b)
                this.released[b] && (this.released.splice(b, 1),
                b--)
        },
        pollMultitouch: function(b) {
            !this.multitouchCapable && 1 < b && (this.multitouchCapable = !0)
        },
        spliceFromArray: function(b, c) {
            for (var e = 0; e < c.length; e++)
                for (var l = 0; l < b.length; l++)
                    b[l].identifier === c[e].identifier && (b.splice(l, 1),
                    l--)
        },
        updateSizeProperties: function() {
            var b = parseInt(ig.system.canvas.offsetWidth) || ig.system.realWidth
              , c = parseInt(ig.system.canvas.offsetHeight) || ig.system.realHeight;
            this.scaleX = b / ig.system.realWidth * ig.system.scale;
            this.scaleY = c / ig.system.realHeight * ig.system.scale
        },
        upgrade: function(b, c, e) {
            var l = {
                left: 0,
                top: 0
            };
            ig.system.canvas.getBoundingClientRect && (l = ig.system.canvas.getBoundingClientRect());
            var q = (e.clientX - l.left) / this.scaleX;
            l = (e.clientY - l.top) / this.scaleY;
            for (var x = 0; x < b.length; x++)
                if (e.identifier === b[x].identifier) {
                    b.splice(x, 1);
                    c.push({
                        identifier: e.identifier,
                        x: q,
                        y: l
                    });
                    break
                }
        },
        updateArray: function(b, c) {
            var e = {
                left: 0,
                top: 0
            };
            ig.system.canvas.getBoundingClientRect && (e = ig.system.canvas.getBoundingClientRect());
            var l = (c.clientX - e.left) / this.scaleX;
            e = (c.clientY - e.top) / this.scaleY;
            for (var q = 0; q < b.length; q++)
                if (c.identifier === b[q].identifier) {
                    b[q].x = l;
                    b[q].y = e;
                    break
                }
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.fake-storage").requires("impact.game").defines(function() {
    ig.FakeStorage = ig.Class.extend({
        tempData: {},
        init: function() {
            ig.FakeStorage.instance = this
        },
        initUnset: function(b, c) {
            null === this.get(b) && this.set(b, c)
        },
        set: function(b, c) {
            this.tempData[b] = JSON.stringify(c)
        },
        setHighest: function(b, c) {
            c > this.getFloat(b) && this.set(b, c)
        },
        get: function(b) {
            return "undefined" == typeof this.tempData[b] ? null : JSON.parse(this.tempData[b])
        },
        getInt: function(b) {
            return ~~this.get(b)
        },
        getFloat: function(b) {
            return parseFloat(this.get(b))
        },
        getBool: function(b) {
            return !!this.get(b)
        },
        isSet: function(b) {
            return null !== this.get(b)
        },
        remove: function(b) {
            delete this.tempData[b]
        },
        clear: function() {
            this.tempData = {}
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.io-manager").requires("plugins.io.storage", "plugins.io.mouse", "plugins.io.keyboard", "plugins.io.gamepad", "plugins.io.multitouch", "plugins.io.multitouch-input", "plugins.io.gamepad-input", "plugins.io.fake-storage").defines(function() {
    IoManager = ig.Class.extend({
        version: "1.0.0",
        storage: null,
        localStorageSupport: !1,
        mouse: null,
        keyboard: null,
        multitouch: null,
        gamepad: null,
        init: function() {
            ig.multitouchInput = new ig.MultitouchInput;
            ig.gamepadInput = new ig.GamepadInput;
            this.unbindAll();
            this.initStorage();
            this.initMouse();
            this.initKeyboard()
        },
        unbindAll: function() {
            ig.input.unbindAll();
            ig.gamepadInput.unbindAll()
        },
        initStorage: function() {
            try {
                window.localStorage.setItem("test", "test"),
                window.localStorage.removeItem("test"),
                this.storage = new ig.Storage
            } catch (b) {
                console.log("using fake storage"),
                this.storage = new ig.FakeStorage
            }
        },
        initMouse: function() {
            this.mouse = new Mouse
        },
        initKeyboard: function() {
            this.keyboard = new Keyboard
        },
        initMultitouch: function() {
            this.multitouch = new Multitouch
        },
        initGamepad: function() {
            this.gamepad = new Gamepad
        },
        press: function(b) {
            return ig.input.pressed(b) || this.gamepad && this.gamepad.press(b) ? !0 : !1
        },
        held: function(b) {
            return ig.input.state(b) || this.gamepad && this.gamepad.state(b) ? !0 : !1
        },
        release: function(b) {
            return ig.input.released(b) || this.gamepad && this.gamepad.released(b) ? !0 : !1
        },
        getClickPos: function() {
            return this.mouse.getPos()
        },
        getLastClickPos: function() {
            return this.mouse.getLast()
        },
        getTouchesPos: function() {
            return this.multitouch.getTouchesPos()
        },
        checkOverlap: function(b, c, e, l, q) {
            return b.x > c + l || b.x < c || b.y > e + q || b.y < e ? !1 : !0
        },
        clear: function() {
            ig.multitouchInput.clear()
        },
        _supportsLocalStorage: function() {
            try {
                return localStorage.setItem("test", "test"),
                localStorage.removeItem("test"),
                this.localStorageSupport = "localStorage"in window && null !== window.localStorage
            } catch (b) {
                return this.localStorageSupport
            }
        },
        storageIsSet: function(b) {
            return "function" === typeof this.storage.isSet ? this.storage.isSet(b) : null
        },
        storageGet: function(b) {
            return "function" === typeof this.storage.get ? this.storage.get(b) : null
        },
        storageSet: function(b, c) {
            return "function" === typeof this.storage.set ? this.storage.set(b, c) : null
        },
        assert: function(b, c, e) {
            if (c !== e)
                throw "actualValue:" + c + " not equal to testValue:" + e + " at " + b;
        }
    })
});
ig.baked = !0;
ig.module("plugins.io.storage-manager").requires("impact.game", "plugins.io.io-manager").defines(function() {
    ig.Game.prototype.name = "MJS-Game";
    ig.Game.prototype.version = "1.0.0";
    ig.Game.prototype.sessionData = {};
    ig.Game.prototype.initData = function() {
        return this.sessionData = {
            sound: .5,
            music: .5,
            level: 1,
            score: 0
        }
    }
    ;
    ig.Game.prototype.setupStorageManager = function() {
        "undefined" === typeof this.name ? console.error("Cannot found Game Name, Storage Manager Cancelled.") : "undefined" === typeof this.version ? console.error("Cannot found Game Version, Storage Manager Cancelled.") : (this.io || (this.io = new IoManager,
        console.log("IO Manager doesn't existed. Initialize...")),
        console.log("Plug in Storage Manager"),
        this.storage = this.io.storage,
        this.storageName = this.name + "-v" + this.version,
        this.loadAll())
    }
    ;
    ig.Game.prototype.loadAll = function() {
        var b = this.storage.get(this.storageName);
        if (null === b || "undefined" === typeof b)
            b = this.initData();
        for (var c in b)
            this.sessionData[c] = b[c];
        this.storage.set(this.storageName, b)
    }
    ;
    ig.Game.prototype.saveAll = function() {
        var b = this.storage.get(this.storageName), c;
        for (c in b)
            b[c] = this.sessionData[c];
        this.storage.set(this.storageName, b)
    }
    ;
    ig.Game.prototype.load = function(b) {
        return this.storage.get(this.storageName)[b]
    }
    ;
    ig.Game.prototype.save = function(b, c) {
        var e = this.storage.get(this.storageName);
        e[b] = c;
        this.storage.set(this.storageName, e)
    }
});
ig.baked = !0;
ig.module("plugins.splash-loader").requires("impact.loader", "impact.animation").defines(function() {
    ig.SplashLoader = ig.Loader.extend({
        tapToStartDivId: "tap-to-start",
        loadingBar: new ig.Image("/static/spin-arena/v1/media/graphics/game/loading-bar.png"),
        loadingFrame: new ig.Image("/static/spin-arena/v1/media/graphics/game/loading-frame.png"),
        init: async function(b, c) {
            await GameConfiguration.initGame().then(e=>{
                console.log(e);
                ig.gameData = e;
                for (e = 0; e < ig.gameData.prizes.length; e++) {
                    var l = new ig.Image(ig.gameData.prizes[e].image_url);
                    c.push(l)
                }
                this.gameClass = b;
                this.resources = c;
                this._loadCallbackBound = this._loadCallback.bind(this);
                for (e = 0; e < this.resources.length; e++)
                    this._unloaded.push(this.resources[e].path);
                this.done = !1;
                this.parentLoad()
            }
            );
            ig.apiHandler.run("MJSPreroll")
        },
        end: function() {
            this.parent();
            this._drawStatus = 1;
            _SETTINGS.TapToStartAudioUnlock.Enabled ? this.tapToStartDiv(function() {
                ig.system.setGame(MyGame)
            }) : ig.system.setGame(MyGame);
            this.draw()
        },
        tapToStartDiv: function(b) {
            this.desktopCoverDIV = document.getElementById(this.tapToStartDivId);
            if (!this.desktopCoverDIV) {
                this.desktopCoverDIV = document.createElement("div");
                this.desktopCoverDIV.id = this.tapToStartDivId;
                this.desktopCoverDIV.setAttribute("class", "play");
                this.desktopCoverDIV.setAttribute("style", "position: absolute; display: block; z-index: 999999; background-color: rgba(23, 32, 53, 0.7); visibility: visible; font-size: 10vmin; text-align: center; vertical-align: middle; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;");
                this.desktopCoverDIV.innerHTML = "<div style='color:white;background-color: rgba(255, 255, 255, 0.3); border: 2px solid #fff; font-size:20px; border-radius: 5px; position: relative; float: left; top: 50%; left: 50%; transform: translate(-50%, -50%);'><div style='padding:20px 50px; font-family: oxanium;'>" + _STRINGS.Splash.TapToStart + "</div></div>";
                (document.getElementById("play").parentNode || document.getElementById("ajaxbar")).appendChild(this.desktopCoverDIV);
                try {
                    "undefined" !== typeof ig.sizeHandler ? "undefined" !== typeof ig.sizeHandler.coreDivsToResize && (ig.sizeHandler.coreDivsToResize.push("#" + this.tapToStartDivId),
                    "function" === typeof ig.sizeHandler.reorient && ig.sizeHandler.reorient()) : "undefined" !== typeof coreDivsToResize && (coreDivsToResize.push(this.tapToStartDivId),
                    "function" === typeof sizeHandler && sizeHandler())
                } catch (c) {
                    console.log(c)
                }
                this.desktopCoverDIV.addEventListener("click", function() {
                    ig.soundHandler.unlockWebAudio();
                    this.setAttribute("style", "visibility: hidden;");
                    "function" === typeof b && b()
                })
            }
        },
        drawCheck: 0,
        draw: function() {
            this._drawStatus += (this.status - this._drawStatus) / 5;
            1 === this.drawCheck && console.log("Font should be loaded before loader draw loop");
            2 > this.drawCheck && this.drawCheck++;
            var b = ig.system.context;
            b.save();
            b.fillStyle = "#f4f4f4";
            b.fillRect(0, 0, ig.system.width, ig.system.height);
            b.restore();
            this.loadingFrame.draw(.5 * (ig.system.width - this.loadingFrame.width), .5 * (ig.system.height - this.loadingFrame.height) + 156);
            this.loadingBar.draw(.5 * (ig.system.width - this.loadingBar.width), .5 * (ig.system.height - this.loadingBar.height) + 156, 0, 0, this.loadingBar.width * this._drawStatus, this.loadingBar.height)
        }
    })
});
ig.baked = !0;
ig.module("plugins.tween").requires("impact.entity").defines(function() {
    Array.prototype.indexOf || (Array.prototype.indexOf = function(b, c) {
        for (c = 0; c < this.length; ++c)
            if (this[c] === b)
                return c;
        return -1
    }
    );
    ig.Entity.prototype.tweens = [];
    ig.Entity.prototype._preTweenUpdate = ig.Entity.prototype.update;
    ig.Entity.prototype.update = function() {
        this._preTweenUpdate();
        if (0 < this.tweens.length) {
            for (var b = [], c = 0; c < this.tweens.length; c++)
                this.tweens[c].update(),
                this.tweens[c].complete || b.push(this.tweens[c]);
            this.tweens = b
        }
    }
    ;
    ig.Entity.prototype.tween = function(b, c, e) {
        b = new ig.Tween(this,b,c,e);
        this.tweens.push(b);
        return b
    }
    ;
    ig.Entity.prototype.pauseTweens = function() {
        for (var b = 0; b < this.tweens.length; b++)
            this.tweens[b].pause()
    }
    ;
    ig.Entity.prototype.resumeTweens = function() {
        for (var b = 0; b < this.tweens.length; b++)
            this.tweens[b].resume()
    }
    ;
    ig.Entity.prototype.stopTweens = function(b) {
        for (var c = 0; c < this.tweens.length; c++)
            this.tweens[c].stop(b)
    }
    ;
    ig.Tween = function(b, c, e, l) {
        var q = {}
          , x = {}
          , C = {}
          , A = 0
          , E = !1
          , N = !1
          , g = !1;
        this.duration = e;
        this.paused = this.complete = !1;
        this.easing = ig.Tween.Easing.Linear.EaseNone;
        this.onComplete = !1;
        this.loop = this.delay = 0;
        this.loopCount = -1;
        ig.merge(this, l);
        this.loopNum = this.loopCount;
        this.chain = function(r) {
            g = r
        }
        ;
        this.initEnd = function(r, y, F) {
            if ("object" !== typeof y[r])
                F[r] = y[r];
            else
                for (subprop in y[r])
                    F[r] || (F[r] = {}),
                    this.initEnd(subprop, y[r], F[r])
        }
        ;
        this.initStart = function(r, y, F, G) {
            if ("object" !== typeof F[r])
                "undefined" !== typeof y[r] && (G[r] = F[r]);
            else
                for (subprop in F[r])
                    G[r] || (G[r] = {}),
                    "undefined" !== typeof y[r] && this.initStart(subprop, y[r], F[r], G[r])
        }
        ;
        this.start = function() {
            this.paused = this.complete = !1;
            this.loopNum = this.loopCount;
            A = 0;
            -1 == b.tweens.indexOf(this) && b.tweens.push(this);
            N = !0;
            E = new ig.Timer;
            for (var r in c)
                this.initEnd(r, c, x);
            for (r in x)
                this.initStart(r, x, b, q),
                this.initDelta(r, C, b, x)
        }
        ;
        this.initDelta = function(r, y, F, G) {
            if ("object" !== typeof G[r])
                y[r] = G[r] - F[r];
            else
                for (subprop in G[r])
                    y[r] || (y[r] = {}),
                    this.initDelta(subprop, y[r], F[r], G[r])
        }
        ;
        this.propUpdate = function(r, y, F, G, M) {
            if ("object" !== typeof F[r])
                y[r] = "undefined" != typeof F[r] ? F[r] + G[r] * M : y[r];
            else
                for (subprop in F[r])
                    this.propUpdate(subprop, y[r], F[r], G[r], M)
        }
        ;
        this.propSet = function(r, y, F) {
            if ("object" !== typeof y[r])
                F[r] = y[r];
            else
                for (subprop in y[r])
                    F[r] || (F[r] = {}),
                    this.propSet(subprop, y[r], F[r])
        }
        ;
        this.update = function() {
            if (!N)
                return !1;
            if (this.delay) {
                if (E.delta() < this.delay)
                    return;
                this.delay = 0;
                E.reset()
            }
            if (this.paused || this.complete)
                return !1;
            var r = (E.delta() + A) / this.duration;
            r = 1 < r ? 1 : r;
            var y = this.easing(r);
            for (property in C)
                this.propUpdate(property, b, q, C, y);
            if (1 <= r) {
                if (0 == this.loopNum || !this.loop) {
                    this.complete = !0;
                    if (this.onComplete)
                        this.onComplete();
                    g && g.start();
                    return !1
                }
                if (this.loop == ig.Tween.Loop.Revert) {
                    for (property in q)
                        this.propSet(property, q, b);
                    A = 0;
                    E.reset();
                    -1 != this.loopNum && this.loopNum--
                } else if (this.loop == ig.Tween.Loop.Reverse) {
                    r = {};
                    y = {};
                    ig.merge(r, x);
                    ig.merge(y, q);
                    ig.merge(q, r);
                    ig.merge(x, y);
                    for (property in x)
                        this.initDelta(property, C, b, x);
                    A = 0;
                    E.reset();
                    -1 != this.loopNum && this.loopNum--
                }
            }
        }
        ;
        this.pause = function() {
            this.paused = !0;
            E && E.delta && (A += E.delta())
        }
        ;
        this.resume = function() {
            this.paused = !1;
            E && E.reset && E.reset()
        }
        ;
        this.stop = function(r) {
            r && (this.loop = this.complete = this.paused = !1,
            A += e,
            this.update());
            this.complete = !0
        }
    }
    ;
    ig.Tween.Loop = {
        Revert: 1,
        Reverse: 2
    };
    ig.Tween.Easing = {
        Linear: {},
        Quadratic: {},
        Cubic: {},
        Quartic: {},
        Quintic: {},
        Sinusoidal: {},
        Exponential: {},
        Circular: {},
        Elastic: {},
        Back: {},
        Bounce: {}
    };
    ig.Tween.Easing.Linear.EaseNone = function(b) {
        return b
    }
    ;
    ig.Tween.Easing.Quadratic.EaseIn = function(b) {
        return b * b
    }
    ;
    ig.Tween.Easing.Quadratic.EaseOut = function(b) {
        return -b * (b - 2)
    }
    ;
    ig.Tween.Easing.Quadratic.EaseInOut = function(b) {
        return 1 > (b *= 2) ? .5 * b * b : -.5 * (--b * (b - 2) - 1)
    }
    ;
    ig.Tween.Easing.Cubic.EaseIn = function(b) {
        return b * b * b
    }
    ;
    ig.Tween.Easing.Cubic.EaseOut = function(b) {
        return --b * b * b + 1
    }
    ;
    ig.Tween.Easing.Cubic.EaseInOut = function(b) {
        return 1 > (b *= 2) ? .5 * b * b * b : .5 * ((b -= 2) * b * b + 2)
    }
    ;
    ig.Tween.Easing.Quartic.EaseIn = function(b) {
        return b * b * b * b
    }
    ;
    ig.Tween.Easing.Quartic.EaseOut = function(b) {
        return -(--b * b * b * b - 1)
    }
    ;
    ig.Tween.Easing.Quartic.EaseInOut = function(b) {
        return 1 > (b *= 2) ? .5 * b * b * b * b : -.5 * ((b -= 2) * b * b * b - 2)
    }
    ;
    ig.Tween.Easing.Quintic.EaseIn = function(b) {
        return b * b * b * b * b
    }
    ;
    ig.Tween.Easing.Quintic.EaseOut = function(b) {
        return --b * b * b * b * b + 1
    }
    ;
    ig.Tween.Easing.Quintic.EaseInOut = function(b) {
        return 1 > (b *= 2) ? .5 * b * b * b * b * b : .5 * ((b -= 2) * b * b * b * b + 2)
    }
    ;
    ig.Tween.Easing.Sinusoidal.EaseIn = function(b) {
        return -Math.cos(b * Math.PI / 2) + 1
    }
    ;
    ig.Tween.Easing.Sinusoidal.EaseOut = function(b) {
        return Math.sin(b * Math.PI / 2)
    }
    ;
    ig.Tween.Easing.Sinusoidal.EaseInOut = function(b) {
        return -.5 * (Math.cos(Math.PI * b) - 1)
    }
    ;
    ig.Tween.Easing.Exponential.EaseIn = function(b) {
        return 0 == b ? 0 : Math.pow(2, 10 * (b - 1))
    }
    ;
    ig.Tween.Easing.Exponential.EaseOut = function(b) {
        return 1 == b ? 1 : -Math.pow(2, -10 * b) + 1
    }
    ;
    ig.Tween.Easing.Exponential.EaseInOut = function(b) {
        return 0 == b ? 0 : 1 == b ? 1 : 1 > (b *= 2) ? .5 * Math.pow(2, 10 * (b - 1)) : .5 * (-Math.pow(2, -10 * (b - 1)) + 2)
    }
    ;
    ig.Tween.Easing.Circular.EaseIn = function(b) {
        return -(Math.sqrt(1 - b * b) - 1)
    }
    ;
    ig.Tween.Easing.Circular.EaseOut = function(b) {
        return Math.sqrt(1 - --b * b)
    }
    ;
    ig.Tween.Easing.Circular.EaseInOut = function(b) {
        return 1 > (b /= .5) ? -.5 * (Math.sqrt(1 - b * b) - 1) : .5 * (Math.sqrt(1 - (b -= 2) * b) + 1)
    }
    ;
    ig.Tween.Easing.Elastic.EaseIn = function(b) {
        var c = .1
          , e = .4;
        if (0 == b)
            return 0;
        if (1 == b)
            return 1;
        e ||= .3;
        if (!c || 1 > c) {
            c = 1;
            var l = e / 4
        } else
            l = e / (2 * Math.PI) * Math.asin(1 / c);
        return -(c * Math.pow(2, 10 * --b) * Math.sin(2 * (b - l) * Math.PI / e))
    }
    ;
    ig.Tween.Easing.Elastic.EaseOut = function(b) {
        var c = .1
          , e = .4;
        if (0 == b)
            return 0;
        if (1 == b)
            return 1;
        e ||= .3;
        if (!c || 1 > c) {
            c = 1;
            var l = e / 4
        } else
            l = e / (2 * Math.PI) * Math.asin(1 / c);
        return c * Math.pow(2, -10 * b) * Math.sin(2 * (b - l) * Math.PI / e) + 1
    }
    ;
    ig.Tween.Easing.Elastic.EaseInOut = function(b) {
        var c = .1
          , e = .4;
        if (0 == b)
            return 0;
        if (1 == b)
            return 1;
        e ||= .3;
        if (!c || 1 > c) {
            c = 1;
            var l = e / 4
        } else
            l = e / (2 * Math.PI) * Math.asin(1 / c);
        return 1 > (b *= 2) ? -.5 * c * Math.pow(2, 10 * --b) * Math.sin(2 * (b - l) * Math.PI / e) : c * Math.pow(2, -10 * --b) * Math.sin(2 * (b - l) * Math.PI / e) * .5 + 1
    }
    ;
    ig.Tween.Easing.Back.EaseIn = function(b) {
        return b * b * (2.70158 * b - 1.70158)
    }
    ;
    ig.Tween.Easing.Back.EaseOut = function(b) {
        return --b * b * (2.70158 * b + 1.70158) + 1
    }
    ;
    ig.Tween.Easing.Back.EaseInOut = function(b) {
        return 1 > (b *= 2) ? .5 * b * b * (3.5949095 * b - 2.5949095) : .5 * ((b -= 2) * b * (3.5949095 * b + 2.5949095) + 2)
    }
    ;
    ig.Tween.Easing.Bounce.EaseIn = function(b) {
        return 1 - ig.Tween.Easing.Bounce.EaseOut(1 - b)
    }
    ;
    ig.Tween.Easing.Bounce.EaseOut = function(b) {
        return (b /= 1) < 1 / 2.75 ? 7.5625 * b * b : b < 2 / 2.75 ? 7.5625 * (b -= 1.5 / 2.75) * b + .75 : b < 2.5 / 2.75 ? 7.5625 * (b -= 2.25 / 2.75) * b + .9375 : 7.5625 * (b -= 2.625 / 2.75) * b + .984375
    }
    ;
    ig.Tween.Easing.Bounce.EaseInOut = function(b) {
        return .5 > b ? .5 * ig.Tween.Easing.Bounce.EaseIn(2 * b) : .5 * ig.Tween.Easing.Bounce.EaseOut(2 * b - 1) + .5
    }
    ;
    ig.Tween.Interpolation = {
        Linear: function(b, c) {
            var e = b.length - 1
              , l = e * c
              , q = Math.floor(l)
              , x = TWEEN.Interpolation.Utils.Linear;
            return 0 > c ? x(b[0], b[1], l) : 1 < c ? x(b[e], b[e - 1], e - l) : x(b[q], b[q + 1 > e ? e : q + 1], l - q)
        }
    }
});
ig.baked = !0;
ig.module("plugins.patches.entity-patch").requires("impact.entity").defines(function() {
    ig.Entity.inject({
        handleMovementTrace: function(b) {
            this.standing = !1;
            b.collision.y && (0 < this.bounciness && Math.abs(this.vel.y) > this.minBounceVelocity ? this.vel.y *= -this.bounciness : (0 < this.vel.y && (this.standing = !0),
            this.vel.y = 0));
            b.collision.x && (this.vel.x = 0 < this.bounciness && Math.abs(this.vel.x) > this.minBounceVelocity ? this.vel.x * -this.bounciness : 0);
            if (b.collision.slope) {
                var c = b.collision.slope;
                if (0 < this.bounciness) {
                    var e = this.vel.x * c.nx + this.vel.y * c.ny;
                    this.vel.x = (this.vel.x - c.nx * e * 2) * this.bounciness;
                    this.vel.y = (this.vel.y - c.ny * e * 2) * this.bounciness
                } else
                    e = (this.vel.x * c.x + this.vel.y * c.y) / (c.x * c.x + c.y * c.y),
                    this.vel.x = c.x * e,
                    this.vel.y = c.y * e,
                    c = Math.atan2(c.x, c.y),
                    c > this.slopeStanding.min && c < this.slopeStanding.max && (this.standing = !0)
            }
            this.pos.x = b.pos.x;
            this.pos.y = b.pos.y
        }
    })
});
ig.baked = !0;
ig.module("plugins.tweens-handler").requires("impact.entity", "plugins.tween", "plugins.patches.entity-patch").defines(function() {
    Array.prototype.indexOf || (Array.prototype.indexOf = function(b, c) {
        for (c = 0; c < this.length; ++c)
            if (this[c] === b)
                return c;
        return -1
    }
    );
    ig.TweensHandler = ig.Class.extend({
        _tweens: [],
        _systemPausedTweens: [],
        init: function() {},
        getAll: function() {
            return this._tweens
        },
        removeAll: function() {
            this._tweens = []
        },
        add: function(b) {
            this._tweens.push(b)
        },
        remove: function(b) {
            b = this._tweens.indexOf(b);
            -1 !== b && this._tweens.splice(b, 1)
        },
        onSystemPause: function() {
            if (0 === this._tweens.length)
                return !1;
            for (var b = 0, c; b < this._tweens.length; )
                c = this._tweens[b],
                c._isPlaying && (this._systemPausedTweens.push(c),
                c.pause()),
                b++;
            return !0
        },
        onSystemResume: function() {
            if (0 === this._systemPausedTweens.length)
                return !1;
            for (var b = 0; b < this._systemPausedTweens.length; )
                this._systemPausedTweens[b].resume(),
                b++;
            this._systemPausedTweens = [];
            return !0
        },
        update: function(b, c) {
            if (0 === this._tweens.length)
                return !1;
            var e = 0;
            for (b = void 0 !== b ? b : ig.game.tweens.now(); e < this._tweens.length; )
                this._tweens[e].update(b) || c ? e++ : this._tweens.splice(e, 1);
            return !0
        },
        now: function() {
            return Date.now()
        }
    });
    ig.TweenDef = ig.Class.extend({
        _ent: null,
        _valuesStart: {},
        _valuesEnd: {},
        _valuesStartRepeat: {},
        _duration: 1E3,
        _repeat: 0,
        _yoyo: !1,
        _isPlaying: !1,
        _reversed: !1,
        _delayTime: 0,
        _startTime: null,
        _pauseTime: null,
        _easingFunction: ig.Tween.Easing.Linear.EaseNone,
        _interpolationFunction: ig.Tween.Interpolation.Linear,
        _chainedTweens: [],
        _onStartCallback: null,
        _onStartCallbackFired: !1,
        _onUpdateCallback: null,
        _onCompleteCallback: null,
        _onStopCallback: null,
        _onPauseCallback: null,
        _onResumeCallback: null,
        _currentElapsed: 0,
        init: function(b) {
            this._object = b
        },
        to: function(b, c) {
            this._valuesEnd = b;
            void 0 !== c && (this._duration = c);
            return this
        },
        start: function(b) {
            if (this._isPlaying)
                return this;
            ig.game.tweens.add(this);
            this._isPlaying = !0;
            this._onStartCallbackFired = !1;
            this._startTime = void 0 !== b ? b : ig.game.tweens.now();
            this._startTime += this._delayTime;
            for (var c in this._valuesEnd) {
                if (this._valuesEnd[c]instanceof Array) {
                    if (0 === this._valuesEnd[c].length)
                        continue;
                    this._valuesEnd[c] = [this._object[c]].concat(this._valuesEnd[c])
                }
                void 0 !== this._object[c] && (this._valuesStart[c] = this._object[c],
                !1 === this._valuesStart[c]instanceof Array && (this._valuesStart[c] *= 1),
                this._valuesStartRepeat[c] = this._valuesStart[c] || 0)
            }
            return this
        },
        stop: function() {
            if (!this._isPlaying)
                return this;
            ig.game.tweens.remove(this);
            this._isPlaying = !1;
            null !== this._onStopCallback && this._onStopCallback.call(this._object, this._object);
            this.stopChainedTweens();
            return this
        },
        pause: function() {
            if (!this._isPlaying)
                return this;
            ig.game.tweens.remove(this);
            this._isPlaying = !1;
            this._pauseTime = ig.game.tweens.now();
            null !== this._onPauseCallback && this._onPauseCallback.call(this._object, this._object);
            return this
        },
        resume: function() {
            if (this._isPlaying || !this._pauseTime)
                return this;
            var b = ig.game.tweens.now() - this._pauseTime;
            this._startTime += b;
            ig.game.tweens.add(this);
            this._isPlaying = !0;
            null !== this._onResumeCallback && this._onResumeCallback.call(this._object, this._object);
            this._pauseTime = null;
            return this
        },
        end: function() {
            this.update(this._startTime + this._duration);
            return this
        },
        stopChainedTweens: function() {
            for (var b = 0, c = this._chainedTweens.length; b < c; b++)
                this._chainedTweens[b].stop()
        },
        delay: function(b) {
            this._delayTime = b;
            return this
        },
        repeat: function(b) {
            this._repeat = b;
            return this
        },
        repeatDelay: function(b) {
            this._repeatDelayTime = b;
            return this
        },
        yoyo: function(b) {
            this._yoyo = b;
            return this
        },
        easing: function(b) {
            this._easingFunction = b;
            return this
        },
        interpolation: function(b) {
            this._interpolationFunction = b;
            return this
        },
        chain: function() {
            this._chainedTweens = arguments;
            return this
        },
        onStart: function(b) {
            this._onStartCallback = b;
            return this
        },
        onUpdate: function(b) {
            this._onUpdateCallback = b;
            return this
        },
        onComplete: function(b) {
            this._onCompleteCallback = b;
            return this
        },
        onStop: function(b) {
            this._onStopCallback = b;
            return this
        },
        onPause: function(b) {
            this._onPauseCallback = b;
            return this
        },
        onResume: function(b) {
            this._onResumeCallback = b;
            return this
        },
        update: function(b) {
            var c;
            if (b < this._startTime)
                return !0;
            !1 === this._onStartCallbackFired && (null !== this._onStartCallback && this._onStartCallback.call(this._object, this._object),
            this._onStartCallbackFired = !0);
            var e = (b - this._startTime) / this._duration;
            this._currentElapsed = e = 1 < e ? 1 : e;
            var l = this._easingFunction(e);
            for (c in this._valuesEnd)
                if (void 0 !== this._valuesStart[c]) {
                    var q = this._valuesStart[c] || 0
                      , x = this._valuesEnd[c];
                    x instanceof Array ? this._object[c] = this._interpolationFunction(x, l) : ("string" === typeof x && (x = "+" === x.charAt(0) || "-" === x.charAt(0) ? q + parseFloat(x) : parseFloat(x)),
                    "number" === typeof x && (this._object[c] = q + (x - q) * l))
                }
            null !== this._onUpdateCallback && this._onUpdateCallback.call(this._object, this._object, l);
            if (1 === e)
                if (0 < this._repeat) {
                    isFinite(this._repeat) && this._repeat--;
                    for (c in this._valuesStartRepeat)
                        "string" === typeof this._valuesEnd[c] && (this._valuesStartRepeat[c] = _valuesStartRepeat[c] + parseFloat(_valuesEnd[c])),
                        this._yoyo && (e = this._valuesStartRepeat[c],
                        this._valuesStartRepeat[c] = this._valuesEnd[c],
                        this._valuesEnd[c] = e),
                        this._valuesStart[c] = this._valuesStartRepeat[c];
                    this._yoyo && (this._reversed = !this._reversed);
                    this._startTime = void 0 !== this._repeatDelayTime ? b + this._repeatDelayTime : b + this._delayTime
                } else {
                    null !== this._onCompleteCallback && this._onCompleteCallback.call(this._object, this._object);
                    b = 0;
                    for (c = this._chainedTweens.length; b < c; b++)
                        this._chainedTweens[b].start(this._startTime + this._duration);
                    return !1
                }
            return !0
        }
    });
    ig.Tween.Interpolation = {
        Linear: function(b, c) {
            var e = b.length - 1
              , l = e * c
              , q = Math.floor(l)
              , x = ig.Tween.Interpolation.Utils.Linear;
            return 0 > c ? x(b[0], b[1], l) : 1 < c ? x(b[e], b[e - 1], e - l) : x(b[q], b[q + 1 > e ? e : q + 1], l - q)
        },
        Bezier: function(b, c) {
            for (var e = 0, l = b.length - 1, q = Math.pow, x = ig.Tween.Interpolation.Utils.Bernstein, C = 0; C <= l; C++)
                e += q(1 - c, l - C) * q(c, C) * b[C] * x(l, C);
            return e
        },
        CatmullRom: function(b, c) {
            var e = b.length - 1
              , l = e * c
              , q = Math.floor(l)
              , x = ig.Tween.Interpolation.Utils.CatmullRom;
            return b[0] === b[e] ? (0 > c && (q = Math.floor(l = e * (1 + c))),
            x(b[(q - 1 + e) % e], b[q], b[(q + 1) % e], b[(q + 2) % e], l - q)) : 0 > c ? b[0] - (x(b[0], b[0], b[1], b[1], -l) - b[0]) : 1 < c ? b[e] - (x(b[e], b[e], b[e - 1], b[e - 1], l - e) - b[e]) : x(b[q ? q - 1 : 0], b[q], b[e < q + 1 ? e : q + 1], b[e < q + 2 ? e : q + 2], l - q)
        },
        Utils: {
            Linear: function(b, c, e) {
                return (c - b) * e + b
            },
            Bernstein: function(b, c) {
                var e = ig.Tween.Interpolation.Utils.Factorial;
                return e(b) / e(c) / e(b - c)
            },
            Factorial: function() {
                var b = [1];
                return function(c) {
                    var e = 1;
                    if (b[c])
                        return b[c];
                    for (var l = c; 1 < l; l--)
                        e *= l;
                    return b[c] = e
                }
            }(),
            CatmullRom: function(b, c, e, l, q) {
                b = .5 * (e - b);
                l = .5 * (l - c);
                var x = q * q;
                return (2 * c - 2 * e + b + l) * q * x + (-3 * c + 3 * e - 2 * b - l) * x + b * q + c
            }
        }
    }
});
ig.baked = !0;
ig.module("plugins.scale").requires("impact.entity").defines(function() {
    ig.Entity.inject({
        scale: {
            x: 1,
            y: 1
        },
        _offset: {
            x: 0,
            y: 0
        },
        _scale: {
            x: 1,
            y: 1
        },
        _size: {
            x: 0,
            y: 0
        },
        init: function(b, c, e) {
            this.parent(b, c, e);
            this._offset.x = this.offset.x;
            this._offset.y = this.offset.y;
            this._size.x = this.size.x;
            this._size.y = this.size.y;
            this.setScale(this.scale.x, this.scale.y)
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            b.translate(ig.system.getDrawPos(this.pos.x.round() - this.offset.x - ig.game.screen.x), ig.system.getDrawPos(this.pos.y.round() - this.offset.y - ig.game.screen.y));
            b.scale(this._scale.x, this._scale.y);
            null != this.currentAnim && this.currentAnim.draw(0, 0);
            b.restore()
        },
        setScale: function(b, c) {
            var e = this.size.x
              , l = this.size.y;
            this.scale.x = b || this.scale.x;
            this.scale.y = c || this.scale.y;
            this._scale.x = this.scale.x / ig.system.scale;
            this._scale.y = this.scale.y / ig.system.scale;
            this.offset.x = this._offset.x * this._scale.x;
            this.offset.y = this._offset.y * this._scale.y;
            this.size.x = this._size.x * this._scale.x;
            this.size.y = this._size.y * this._scale.y;
            this.pos.x += (e - this.size.x) / 2;
            this.pos.y += (l - this.size.y) / 2
        },
        setSize: function(b, c) {
            this._size.x = b;
            this._size.y = c;
            this.size.x = b;
            this.size.y = c
        }
    })
});
ig.baked = !0;
ig.module("plugins.url-parameters").defines(function() {
    ig.UrlParameters = ig.Class.extend({
        init: function() {
            switch (getQueryVariable("iphone")) {
            case "true":
                ig.ua.iPhone = !0,
                console.log("iPhone mode")
            }
            var b = getQueryVariable("webview");
            if (b)
                switch (b) {
                case "true":
                    ig.ua.is_uiwebview = !0,
                    console.log("webview mode")
                }
            if (b = getQueryVariable("debug"))
                switch (b) {
                case "true":
                    ig.game.showDebugMenu(),
                    console.log("debug mode")
                }
            switch (getQueryVariable("view")) {
            case "stats":
                ig.game.resetPlayerStats(),
                ig.game.endGame()
            }
            getQueryVariable("ad")
        }
    })
});
ig.baked = !0;
ig.module("plugins.director").requires("impact.impact").defines(function() {
    ig.Director = ig.Class.extend({
        init: function(b, c) {
            this.game = b;
            this.levels = [];
            this.currentLevel = 0;
            this.append(c)
        },
        loadLevel: function(b) {
            for (var c in ig.sizeHandler.dynamicClickableEntityDivs) {
                var e = ig.domHandler.getElementById("#" + c);
                ig.domHandler.hide(e)
            }
            this.currentLevel = b;
            this.game.loadLevel(this.levels[b]);
            return !0
        },
        loadLevelWithoutEntities: function(b) {
            this.currentLevel = b;
            this.game.loadLevelWithoutEntities(this.levels[b]);
            return !0
        },
        append: function(b) {
            newLevels = [];
            return "object" === typeof b ? (b.constructor === [].constructor ? newLevels = b : newLevels[0] = b,
            this.levels = this.levels.concat(newLevels),
            !0) : !1
        },
        nextLevel: function() {
            return this.currentLevel + 1 < this.levels.length ? this.loadLevel(this.currentLevel + 1) : !1
        },
        previousLevel: function() {
            return 0 <= this.currentLevel - 1 ? this.loadLevel(this.currentLevel - 1) : !1
        },
        jumpTo: function(b) {
            var c = null;
            for (i = 0; i < this.levels.length; i++)
                this.levels[i] == b && (c = i);
            return 0 <= c ? this.loadLevel(c) : !1
        },
        firstLevel: function() {
            return this.loadLevel(0)
        },
        lastLevel: function() {
            return this.loadLevel(this.levels.length - 1)
        },
        reloadLevel: function() {
            return this.loadLevel(this.currentLevel)
        }
    })
});
ig.baked = !0;
ig.module("plugins.impact-storage").requires("impact.game").defines(function() {
    ig.Storage = ig.Class.extend({
        staticInstantiate: function(b) {
            return ig.Storage.instance ? ig.Storage.instance : null
        },
        init: function() {
            ig.Storage.instance = this
        },
        isCapable: function() {
            try {
                return "undefined" !== typeof window.localStorage
            } catch (b) {
                return !1
            }
        },
        isSet: function(b) {
            return null !== this.get(b)
        },
        initUnset: function(b, c) {
            null === this.get(b) && this.set(b, c)
        },
        get: function(b) {
            if (!this.isCapable())
                return null;
            try {
                return JSON.parse(localStorage.getItem(b))
            } catch (c) {
                return window.localStorage.getItem(b)
            }
        },
        getInt: function(b) {
            return ~~this.get(b)
        },
        getFloat: function(b) {
            return parseFloat(this.get(b))
        },
        getBool: function(b) {
            return !!this.get(b)
        },
        key: function(b) {
            return this.isCapable() ? window.localStorage.key(b) : null
        },
        set: function(b, c) {
            if (!this.isCapable())
                return null;
            try {
                window.localStorage.setItem(b, JSON.stringify(c))
            } catch (e) {
                console.log(e)
            }
        },
        setHighest: function(b, c) {
            c > this.getFloat(b) && this.set(b, c)
        },
        remove: function(b) {
            if (!this.isCapable())
                return null;
            window.localStorage.removeItem(b)
        },
        clear: function() {
            if (!this.isCapable())
                return null;
            window.localStorage.clear()
        }
    })
});
ig.baked = !0;
ig.module("plugins.fullscreen").requires("impact.entity", "plugins.handlers.size-handler", "plugins.director").defines(function() {
    ig.Fullscreen = {
        enableFullscreenButton: !0,
        _isEnabled: "notChecked",
        isEnabled: function() {
            "notChecked" == this._isEnabled && (this._isEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled || document.msFullscreenEnabled ? !0 : !1);
            return this._isEnabled
        },
        isFullscreen: function() {
            return ig.Fullscreen.isEnabled() && (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement) ? !0 : !1
        },
        toggleFullscreen: function() {
            ig.Fullscreen.isFullscreen() ? ig.Fullscreen.exitFullscreen() : ig.Fullscreen.requestFullscreen();
            ig.sizeHandler.orientationDelayHandler();
            if (ig && ig.visibilityHandler)
                ig.visibilityHandler.onChange("focus");
            try {
                ig.soundHandler.unlockWebAudio()
            } catch (b) {}
        },
        requestFullscreen: function() {
            var b = document.documentElement;
            b.requestFullscreen ? b.requestFullscreen() : b.webkitRequestFullscreen ? b.webkitRequestFullscreen() : b.mozRequestFullScreen ? b.mozRequestFullScreen() : b.msRequestFullscreen ? b.msRequestFullscreen() : console.log("no request fullscreen method available")
        },
        exitFullscreen: function() {
            document.exitFullscreen ? document.exitFullscreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.msExitFullscreen ? document.msExitFullscreen() : console.log("no exit fullscreen method available")
        },
        divs: {}
    };
    ig.Director.inject({
        loadLevel: function(b) {
            var c = ig.Fullscreen.divs, e;
            for (e in c)
                c = ig.domHandler.getElementById("#" + e),
                ig.domHandler.hide(c);
            return this.parent(b)
        }
    });
    ig.SizeHandler.inject({
        resize: function() {
            this.parent();
            var b = ig.Fullscreen.divs, c;
            for (c in b) {
                var e = Math.min(ig.sizeHandler.scaleRatioMultiplier.x, ig.sizeHandler.scaleRatioMultiplier.y)
                  , l = ig.domHandler.getElementById("#" + c)
                  , q = b[c].entity_pos_x
                  , x = b[c].entity_pos_y
                  , C = b[c].width
                  , A = b[c].height
                  , E = ig.domHandler.getElementById("#canvas")
                  , N = ig.domHandler.getOffsets(E);
                ig.ua.mobile ? (E = N.left,
                N = N.top,
                ig.sizeHandler.disableStretchToFitOnMobileFlag ? (q = Math.floor(E + q * ig.sizeHandler.scaleRatioMultiplier.x) + "px",
                x = Math.floor(N + x * ig.sizeHandler.scaleRatioMultiplier.y) + "px",
                C = Math.floor(C * ig.sizeHandler.scaleRatioMultiplier.x) + "px",
                A = Math.floor(A * ig.sizeHandler.scaleRatioMultiplier.y) + "px") : (q = Math.floor(q * ig.sizeHandler.sizeRatio.x) + "px",
                x = Math.floor(x * ig.sizeHandler.sizeRatio.y) + "px",
                C = Math.floor(C * ig.sizeHandler.sizeRatio.x) + "px",
                A = Math.floor(A * ig.sizeHandler.sizeRatio.y) + "px")) : (E = N.left,
                N = N.top,
                q = Math.floor(E + q * e) + "px",
                x = Math.floor(N + x * e) + "px",
                C = Math.floor(C * e) + "px",
                A = Math.floor(A * e) + "px");
                ig.domHandler.css(l, {
                    float: "left",
                    position: "absolute",
                    left: q,
                    top: x,
                    width: C,
                    height: A,
                    "z-index": 3
                });
                b[c]["font-size"] && ig.domHandler.css(l, {
                    "font-size": b[c]["font-size"] * e + "px"
                })
            }
        }
    });
    ig.FullscreenButton = ig.Entity.extend({
        enterImage: null,
        exitImage: null,
        isReady: !1,
        zIndex: 999999,
        identifier: null,
        prevPos: {
            x: 0,
            y: 0
        },
        invisImagePath: "/static/spin-arena/v1/media/graphics/misc/invisible.png",
        init: function(b, c, e) {
            this.parent(b, c, e);
            ig.Fullscreen.isEnabled() && ig.Fullscreen.enableFullscreenButton ? this.enterImage.loaded ? this.initSize() : this.enterImage.loadCallback = function() {
                this.initSize()
            }
            .bind(this) : this.kill()
        },
        kill: function() {
            this.parent()
        },
        destroy: function() {
            this.parent();
            console.log("destroy")
        },
        initSize: function() {
            this.size.x = this.enterImage.width;
            this.size.y = this.enterImage.height;
            this.createClickableLayer();
            this.isReady = !0
        },
        createClickableLayer: function() {
            this.identifier = "fullscreen-button-layer";
            var b = ig.domHandler.getElementById("#" + this.identifier);
            b ? (ig.domHandler.show(b),
            ig.domHandler.attr(b, "onclick", "ig.Fullscreen.toggleFullscreen()")) : this.createClickableOutboundLayer()
        },
        update: function(b, c) {
            b = this.pos.x;
            c = this.pos.y;
            !this.isReady || this.prevPos.x === b && this.prevPos.y === c || (ig.Fullscreen.divs[this.identifier] = {},
            ig.Fullscreen.divs[this.identifier].width = this.size.x,
            ig.Fullscreen.divs[this.identifier].height = this.size.y,
            ig.Fullscreen.divs[this.identifier].entity_pos_x = this.pos.x,
            ig.Fullscreen.divs[this.identifier].entity_pos_y = this.pos.y,
            this.prevPos.x = b,
            this.prevPos.y = c)
        },
        draw: function() {
            this.isReady && (ig.Fullscreen.isFullscreen() ? this.exitImage.draw(this.pos.x, this.pos.y) : this.enterImage.draw(this.pos.x, this.pos.y))
        },
        createClickableOutboundLayer: function() {
            var b = this.identifier
              , c = this.invisImagePath
              , e = ig.domHandler.create("div");
            ig.domHandler.attr(e, "id", b);
            ig.domHandler.attr(e, "onclick", "ig.Fullscreen.toggleFullscreen()");
            var l = ig.domHandler.create("a")
              , q = ig.domHandler.create("img");
            ig.domHandler.css(q, {
                width: "100%",
                height: "100%"
            });
            ig.domHandler.attr(q, "src", c);
            c = Math.min(ig.sizeHandler.scaleRatioMultiplier.x, ig.sizeHandler.scaleRatioMultiplier.y);
            if (ig.ua.mobile) {
                var x = ig.domHandler.getElementById("#canvas");
                x = ig.domHandler.getOffsets(x);
                var C = x.left
                  , A = x.top;
                console.log(x.left);
                ig.sizeHandler.disableStretchToFitOnMobileFlag ? (x = Math.floor(C + this.pos.x * ig.sizeHandler.scaleRatioMultiplier.x) + "px",
                A = Math.floor(A + this.pos.y * ig.sizeHandler.scaleRatioMultiplier.y) + "px",
                C = Math.floor(this.size.x * ig.sizeHandler.scaleRatioMultiplier.x) + "px",
                c = Math.floor(this.size.y * ig.sizeHandler.scaleRatioMultiplier.y) + "px") : (x = Math.floor(this.pos.x * ig.sizeHandler.sizeRatio.x) + "px",
                A = Math.floor(this.pos.y * ig.sizeHandler.sizeRatio.y) + "px",
                C = Math.floor(this.size.x * ig.sizeHandler.sizeRatio.x) + "px",
                c = Math.floor(this.size.y * ig.sizeHandler.sizeRatio.y) + "px")
            } else
                x = ig.domHandler.getElementById("#canvas"),
                x = ig.domHandler.getOffsets(x),
                C = x.left,
                A = x.top,
                ig.sizeHandler.enableStretchToFitOnDesktopFlag ? (x = Math.floor(C + this.pos.x * ig.sizeHandler.sizeRatio.x) + "px",
                A = Math.floor(A + this.pos.y * ig.sizeHandler.sizeRatio.y) + "px",
                C = Math.floor(this.size.x * ig.sizeHandler.sizeRatio.x) + "px",
                c = Math.floor(this.size.y * ig.sizeHandler.sizeRatio.y) + "px") : (x = Math.floor(C + this.pos.x * c) + "px",
                A = Math.floor(A + this.pos.y * c) + "px",
                C = Math.floor(this.size.x * c) + "px",
                c = Math.floor(this.size.y * c) + "px");
            ig.domHandler.css(e, {
                float: "left",
                position: "absolute",
                left: x,
                top: A,
                width: C,
                height: c,
                "z-index": 3
            });
            ig.domHandler.addEvent(e, "mousemove", ig.input.mousemove.bind(ig.input), !1);
            ig.domHandler.appendChild(l, q);
            ig.domHandler.appendChild(e, l);
            ig.domHandler.appendToBody(e);
            ig.Fullscreen.divs[b] = {};
            ig.Fullscreen.divs[b].width = this.size.x;
            ig.Fullscreen.divs[b].height = this.size.y;
            ig.Fullscreen.divs[b].entity_pos_x = this.pos.x;
            ig.Fullscreen.divs[b].entity_pos_y = this.pos.y
        },
        show: function() {
            var b = ig.domHandler.getElementById("#" + this.identifier);
            b && ig.domHandler.show(b)
        },
        hide: function() {
            var b = ig.domHandler.getElementById("#" + this.identifier);
            b && ig.domHandler.hide(b)
        }
    })
});
ig.baked = !0;
ig.module("plugins.responsive.responsive-utils").requires("impact.system").defines(function() {
    ig.responsive = {
        width: 0,
        height: 0,
        halfWidth: 0,
        halfHeight: 0,
        originalWidth: 0,
        originalHeight: 0,
        fillScale: 1,
        scaleX: 1,
        scaleY: 1,
        toAnchor: function(b, c, e) {
            switch (e) {
            case "top-left":
            case "left-top":
                return this.toAnchorTopLeft(b, c);
            case "top-center":
            case "center-top":
            case "top":
                return this.toAnchorTopCenter(b, c);
            case "top-right":
            case "right-top":
                return this.toAnchorTopRight(b, c);
            case "left-middle":
            case "middle-left":
            case "left":
                return this.toAnchorLeftMiddle(b, c);
            case "center-middle":
            case "middle-center":
            case "middle":
            case "center":
                return this.toAnchorCenterMiddle(b, c);
            case "right-middle":
            case "middle-right":
            case "right":
                return this.toAnchorRightMiddle(b, c);
            case "bottom-left":
            case "left-bottom":
                return this.toAnchorBottomLeft(b, c);
            case "bottom-center":
            case "center-bottom":
            case "bottom":
                return this.toAnchorBottomCenter(b, c);
            case "bottom-right":
            case "right-bottom":
                return this.toAnchorBottomRight(b, c);
            default:
                return this.toAnchorDefault(b, c)
            }
        },
        toAnchorDefault: function(b, c) {
            return {
                x: b + (this.width - this.originalWidth) / 2,
                y: c + (this.height - this.originalHeight) / 2
            }
        },
        toAnchorCenterMiddle: function(b, c) {
            return {
                x: b + this.halfWidth,
                y: c + this.halfHeight
            }
        },
        toAnchorLeftMiddle: function(b, c) {
            return {
                x: b,
                y: c + this.halfHeight
            }
        },
        toAnchorTopCenter: function(b, c) {
            return {
                x: b + this.halfWidth,
                y: c
            }
        },
        toAnchorRightMiddle: function(b, c) {
            return {
                x: b + this.width,
                y: c + this.halfHeight
            }
        },
        toAnchorBottomCenter: function(b, c) {
            return {
                x: b + this.halfWidth,
                y: c + this.height
            }
        },
        toAnchorTopLeft: function(b, c) {
            return {
                x: b,
                y: c
            }
        },
        toAnchorBottomLeft: function(b, c) {
            return {
                x: b,
                y: c + this.height
            }
        },
        toAnchorTopRight: function(b, c) {
            return {
                x: b + this.width,
                y: c
            }
        },
        toAnchorBottomRight: function(b, c) {
            return {
                x: b + this.width,
                y: c + this.height
            }
        },
        drawScaledImage: function(b, c, e, l, q, x, C) {
            x ||= 0;
            C ||= 0;
            var A = ig.system.context;
            A.save();
            A.translate(c, e);
            1 == l && 1 == q || A.scale(l, q);
            b.draw(-b.width * x, -b.height * C);
            A.restore()
        },
        drawScaledRotateImage: function(b, c, e, l, q, x, C, A) {
            x ||= 0;
            C ||= 0;
            A ||= 0;
            var E = ig.system.context;
            E.save();
            E.translate(c, e);
            1 == l && 1 == q || E.scale(l, q);
            E.rotate(A);
            b.draw(-b.width * x, -b.height * C);
            E.restore()
        },
        drawRoundRect: function(b, c, e, l, q, x, C) {
            var A = ig.system.context;
            A.save();
            "undefined" == typeof C && (C = !0);
            "undefined" === typeof q && (q = 5);
            A.beginPath();
            A.moveTo(b + q, c);
            A.lineTo(b + e - q, c);
            A.quadraticCurveTo(b + e, c, b + e, c + q);
            A.lineTo(b + e, c + l - q);
            A.quadraticCurveTo(b + e, c + l, b + e - q, c + l);
            A.lineTo(b + q, c + l);
            A.quadraticCurveTo(b, c + l, b, c + l - q);
            A.lineTo(b, c + q);
            A.quadraticCurveTo(b, c, b + q, c);
            A.closePath();
            C && A.stroke();
            x && A.fill();
            A.restore()
        }
    }
});
ig.baked = !0;
ig.module("plugins.responsive.responsive-plugin").requires("impact.system", "impact.entity", "plugins.handlers.size-handler", "plugins.responsive.responsive-utils").defines(function() {
    ig.SizeHandler.inject({
        resize: function() {
            this.parent()
        },
        sizeCalcs: function() {
            this.originalResolution || (this.originalResolution = new Vector2(this.desktop.actualResolution.x,this.desktop.actualResolution.y),
            ig.responsive.originalWidth = this.desktop.actualResolution.x,
            ig.responsive.originalHeight = this.desktop.actualResolution.y);
            var b = window.innerWidth
              , c = window.innerHeight
              , e = b / c
              , l = this.originalResolution.x / this.originalResolution.y;
            this.windowSize = new Vector2(b,c);
            if (e < l) {
                l = this.originalResolution.x;
                var q = l / e;
                ig.responsive.scaleX = 1;
                ig.responsive.scaleY = q / this.originalResolution.y
            } else
                q = this.originalResolution.y,
                l = q * e,
                ig.responsive.scaleX = l / this.originalResolution.x,
                ig.responsive.scaleY = 1;
            this.scaleRatioMultiplier = new Vector2(b / l,c / q);
            this.desktop.actualResolution = new Vector2(l,q);
            this.mobile.actualResolution = new Vector2(l,q);
            this.desktop.actualSize = new Vector2(b,c);
            this.mobile.actualSize = new Vector2(b,c);
            ig.responsive.width = l;
            ig.responsive.height = q;
            ig.responsive.halfWidth = l / 2;
            ig.responsive.halfHeight = q / 2;
            ig.responsive.fillScale = Math.max(ig.responsive.scaleX, ig.responsive.scaleY)
        },
        resizeLayers: function(b, c) {
            ig.system.resize(ig.sizeHandler.desktop.actualResolution.x, ig.sizeHandler.desktop.actualResolution.y);
            for (b = 0; b < this.coreDivsToResize.length; b++) {
                c = ig.domHandler.getElementById(this.coreDivsToResize[b]);
                var e = Math.floor(ig.sizeHandler.windowSize.x / 2 - ig.sizeHandler.desktop.actualSize.x / 2)
                  , l = Math.floor(ig.sizeHandler.windowSize.y / 2 - ig.sizeHandler.desktop.actualSize.y / 2);
                0 > e && (e = 0);
                0 > l && (l = 0);
                ig.domHandler.resizeOffset(c, Math.floor(ig.sizeHandler.desktop.actualSize.x), Math.floor(ig.sizeHandler.desktop.actualSize.y), e, l)
            }
            for (var q in this.adsToResize)
                b = ig.domHandler.getElementById("#" + q),
                c = ig.domHandler.getElementById("#" + q + "-Box"),
                e = (window.innerWidth - this.adsToResize[q]["box-width"]) / 2 + "px",
                l = (window.innerHeight - this.adsToResize[q]["box-height"]) / 2 + "px",
                b && ig.domHandler.css(b, {
                    width: window.innerWidth,
                    height: window.innerHeight
                }),
                c && ig.domHandler.css(c, {
                    left: e,
                    top: l
                });
            b = ig.domHandler.getElementById("#canvas");
            c = ig.domHandler.getOffsets(b);
            b = c.left;
            c = c.top;
            e = Math.min(ig.sizeHandler.scaleRatioMultiplier.x, ig.sizeHandler.scaleRatioMultiplier.y);
            for (q in this.dynamicClickableEntityDivs)
                l = ig.domHandler.getElementById("#" + q),
                ig.domHandler.css(l, {
                    float: "left",
                    position: "absolute",
                    left: Math.floor(b + this.dynamicClickableEntityDivs[q].entity_pos_x * this.scaleRatioMultiplier.x) + "px",
                    top: Math.floor(c + this.dynamicClickableEntityDivs[q].entity_pos_y * this.scaleRatioMultiplier.y) + "px",
                    width: Math.floor(this.dynamicClickableEntityDivs[q].width * this.scaleRatioMultiplier.x) + "px",
                    height: Math.floor(this.dynamicClickableEntityDivs[q].height * this.scaleRatioMultiplier.y) + "px",
                    "z-index": 3
                }),
                this.dynamicClickableEntityDivs[q]["font-size"] && ig.domHandler.css(l, {
                    "font-size": this.dynamicClickableEntityDivs[q]["font-size"] * e + "px"
                });
            $("#ajaxbar").width(this.windowSize.x);
            $("#ajaxbar").height(this.windowSize.y)
        },
        reorient: function() {
            ig.ua.mobile ? (this.resize(),
            this.resizeAds()) : this.resize();
            try {
                BABYLON && this.resizeBabylon()
            } catch (b) {}
            ig.game && (ig.game.update(),
            ig.game.draw());
            ig.domHandler.getElementById("#play")[0].dispatchEvent(new CustomEvent("reorientate",{
                bubbles: !0,
                detail: {
                    orientation: window.innerHeight > window.innerWidth ? "portrait" : "landscape"
                }
            }))
        },
        resizeBabylon: function() {
            var b = window.innerWidth
              , c = window.innerHeight
              , e = ig.responsive.originalWidth
              , l = ig.responsive.originalHeight
              , q = Math.max(e, l);
            ig.ua.mobile && (q = 640);
            minSize = Math.min(e, l);
            b / c > e / l ? (c > l && (c = l),
            b = Math.floor(window.innerWidth / window.innerHeight * c),
            b > q && (b = q),
            c = Math.floor(window.innerHeight / window.innerWidth * b)) : (b > e && (b = e),
            c = Math.floor(window.innerHeight / window.innerWidth * b),
            c > q && (c = q),
            b = Math.floor(window.innerWidth / window.innerHeight * c));
            l = e = 1;
            window.innerWidth > q && (e = window.innerWidth / q);
            window.innerHeight > q && (l = window.innerHeight / q);
            wgl.system.engine.setSize(b, c);
            wgl.system.engine.resize();
            wgl.system.engine.setHardwareScalingLevel(Math.max(e, l));
            ig.gameScene.zoomFactor = 1;
            ig.ua.mobile && minSize < q && (ig.gameScene.zoomFactor = q / minSize);
            ig.wglW = b;
            ig.wglH = c;
            ig.wglInnerW = window.innerWidth;
            ig.wglInnerH = window.innerHeight;
            console.log("babylon renderSize : ", wgl.system.engine.getRenderWidth(), wgl.system.engine.getRenderHeight(), "hwScalingLevel : ", wgl.system.engine.getHardwareScalingLevel())
        }
    });
    ig.Entity.inject({
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.anchorType || (this.anchorType = "none");
            this.anchoredPositionX = b;
            this.anchoredPositionY = c
        },
        setAnchoredPosition: function(b, c, e) {
            e ||= "default";
            this.anchorType = e;
            this.anchoredPositionX = b;
            this.anchoredPositionY = c
        },
        update: function() {
            this.parent();
            if ("" != this.anchorType && "none" != this.anchorType) {
                var b = ig.responsive.toAnchor(this.anchoredPositionX, this.anchoredPositionY, this.anchorType);
                this.pos.x = b.x;
                this.pos.y = b.y
            }
        }
    })
});
ig.baked = !0;
ig.module("plugins.responsive.responsive-anchor-pivot").requires("impact.entity", "plugins.responsive.responsive-plugin").defines(function() {
    ig.Entity.inject({
        anchorPivot: {
            x: 0,
            y: 0
        },
        update: function() {
            this.parent();
            this.refreshAnchoredPosition()
        },
        refreshAnchoredPosition: function() {
            if ("" != this.anchorType && "none" != this.anchorType) {
                var b = ig.responsive.toAnchor(this.anchoredPositionX - this.size.x * this.anchorPivot.x, this.anchoredPositionY - this.size.y * this.anchorPivot.y, this.anchorType);
                this.pos.x = b.x;
                this.pos.y = b.y
            }
        }
    })
});
this.START_BRANDING_SPLASH;
ig.baked = !0;
ig.module("plugins.branding.splash").requires("impact.impact", "impact.entity").defines(function() {
    ig.BrandingSplash = ig.Class.extend({
        init: function() {
            ig.game.spawnEntity(EntityBranding, 0, 0);
            console.log("spawn branding")
        }
    });
    EntityBranding = ig.Entity.extend({
        autoUpdateScale: !1,
        gravityFactor: 0,
        size: {
            x: 32,
            y: 32
        },
        splash: new ig.Image("/static/spin-arena/v1/branding/splash1.png"),
        init: function(b, c, e) {
            this.parent(b, c, e);
            320 >= ig.system.width ? (this.size.x = 320,
            this.size.y = 200) : (this.size.x = 480,
            this.size.y = 240);
            this.pos.x = (ig.system.width - this.size.x) / 2;
            this.pos.y = -this.size.y - 200;
            this.endPosY = (ig.system.height - this.size.y) / 2;
            b = this.tween({
                pos: {
                    y: this.endPosY
                }
            }, .5, {
                easing: ig.Tween.Easing.Bounce.EaseIn
            });
            c = this.tween({}, 2.5, {
                onComplete: function() {
                    ig.game.director.loadLevel(ig.game.director.currentLevel)
                }
            });
            b.chain(c);
            b.start();
            this.currentAnim = this.anims.idle
        },
        createClickableLayer: function() {
            console.log("Build clickable layer");
            this.checkClickableLayer("branding-splash", _SETTINGS.Branding.Logo.Link, _SETTINGS.Branding.Logo.NewWindow)
        },
        doesClickableLayerExist: function(b) {
            for (k in dynamicClickableEntityDivs)
                if (k == b)
                    return !0;
            return !1
        },
        checkClickableLayer: function(b, c, e) {
            "undefined" == typeof wm && (this.doesClickableLayerExist(b) ? (ig.game.showOverlay([b]),
            $("#" + b).find("[href]").attr("href", c)) : this.createClickableOutboundLayer(b, c, "/static/spin-arena/v1/media/graphics/misc/invisible.png", e))
        },
        createClickableOutboundLayer: function(b, c, e, l) {
            var q = ig.$new("div");
            q.id = b;
            document.body.appendChild(q);
            q = $("#" + q.id);
            q.css("float", "left");
            q.css("position", "absolute");
            if (ig.ua.mobile) {
                var x = window.innerHeight / mobileHeight
                  , C = window.innerWidth / mobileWidth;
                q.css("left", this.pos.x * C);
                q.css("top", this.pos.y * x);
                q.css("width", this.size.x * C);
                q.css("height", this.size.y * x)
            } else
                x = w / 2 - destW / 2,
                C = h / 2 - destH / 2,
                console.log(x, C),
                q.css("left", x + this.pos.x * multiplier),
                q.css("top", C + this.pos.y * multiplier),
                q.css("width", this.size.x * multiplier),
                q.css("height", this.size.y * multiplier);
            l ? q.html("<a target='_blank' href='" + c + "'><img style='width:100%;height:100%' src='" + e + "'></a>") : q.html("<a href='" + c + "'><img style='width:100%;height:100%' src='" + e + "'></a>");
            dynamicClickableEntityDivs[b] = {};
            dynamicClickableEntityDivs[b].width = this.size.x * multiplier;
            dynamicClickableEntityDivs[b].height = this.size.y * multiplier;
            dynamicClickableEntityDivs[b].entity_pos_x = this.pos.x;
            dynamicClickableEntityDivs[b].entity_pos_y = this.pos.y
        },
        draw: function() {
            ig.system.context.fillStyle = "#ffffff";
            ig.system.context.fillRect(0, 0, ig.system.width, ig.system.height);
            ig.system.context.fillStyle = "#000";
            ig.system.context.font = "12px Arial";
            ig.system.context.textAlign = "left";
            320 >= ig.system.width ? ig.system.context.fillText("powered by MarketJS.com", ig.system.width - 150, ig.system.height - 15) : ig.system.context.fillText("powered by MarketJS.com", ig.system.width - 160, ig.system.height - 15);
            this.parent();
            this.splash && ig.system.context.drawImage(this.splash.data, 0, 0, this.splash.data.width, this.splash.data.height, this.pos.x, this.pos.y, this.size.x, this.size.y)
        }
    })
});
this.END_BRANDING_SPLASH;
ig.baked = !0;
ig.module("game.entities.buttons.button").requires("impact.entity").defines(function() {
    EntityButton = ig.Entity.extend({
        collides: ig.Entity.COLLIDES.NEVER,
        type: ig.Entity.TYPE.A,
        zIndex: 1E3,
        anchorPivot: {
            x: .5,
            y: .5
        },
        isClicked: !1,
        isLocked: !1,
        label: !1,
        fontColor: "#000000",
        fontSize: 64,
        buttonScale: 1,
        baseScale: 1,
        highlightScale: 1.05,
        clickScale: .95,
        fontPreFix: "",
        textOffset: {
            x: 0,
            y: 0
        },
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            e.animations && (b = e.animations,
            b.normal && (this.anims.normal = new ig.Animation(b.normal.animSheet,b.normal.frameTime,b.normal.sequence,b.normal.stop),
            this.currentAnim = this.anims.normal),
            b.hover && (this.anims.hover = new ig.Animation(b.hover.animSheet,b.hover.frameTime,b.hover.sequence,b.hover.stop)),
            b.clicked && (this.anims.clicked = new ig.Animation(b.clicked.animSheet,b.clicked.frameTime,b.clicked.sequence,b.clicked.stop)))
        },
        update: function() {
            this.setScale(this.baseScale, this.baseScale);
            this.parent();
            this.setScale(this.buttonScale, this.buttonScale)
        },
        draw: function() {
            this.parent();
            if (this.label) {
                var b = ig.system.context;
                b.save();
                b.globalAlpha = this.currentAnim.alpha;
                b.font = this.fontPreFix + this.fontSize * this.scale.y + "px oxanium";
                b.fillStyle = this.fontColor;
                b.textAlign = "center";
                b.textBaseline = "middle";
                b.fillText(this.label, this.pos.x + .5 * this.size.x + this.textOffset.x, this.pos.y + .5 * this.size.y + this.textOffset.y);
                b.restore()
            }
        },
        clicked: function() {
            this.isLocked || (this.isClicked = !0,
            this.buttonScale = this.baseScale * this.clickScale,
            this.anims.clicked && (this.currentAnim = this.anims.clicked.rewind()),
            this.onClicked())
        },
        clicking: function() {},
        released: function() {
            if (this.isClicked)
                this.onReleased();
            this.isClicked = !1;
            this.over()
        },
        releasedOutside: function() {
            this.isClicked = !1
        },
        onClicked: function() {},
        onReleased: function() {},
        onLeaved: function() {},
        over: function() {
            this.isLocked || (this.buttonScale = this.baseScale * this.highlightScale,
            this.anims.hover && (this.currentAnim = this.anims.hover.rewind()))
        },
        leave: function() {
            this.isLocked || (this.buttonScale = this.baseScale,
            this.anims.normal && this.currentAnim !== this.anims.normal && (this.currentAnim = this.anims.normal.rewind()),
            this.onLeaved())
        }
    })
});
ig.baked = !0;
ig.module("plugins.clickable-div-layer").requires("plugins.data.vector").defines(function() {
    ClickableDivLayer = ig.Class.extend({
        pos: new Vector2(0,0),
        size: new Vector2(0,0),
        identifier: null,
        invisImagePath: "/static/spin-arena/v1/media/graphics/misc/invisible.png",
        init: function(b) {
            this.pos = new Vector2(b.pos.x,b.pos.y);
            this.size = new Vector2(b.size.x,b.size.y);
            var c = "more-games"
              , e = "www.google.com"
              , l = !1;
            b.div_layer_name && (c = b.div_layer_name);
            b.link && (e = b.link);
            b.newWindow && (l = b.newWindow);
            this.createClickableLayer(c, e, l)
        },
        createClickableLayer: function(b, c, e) {
            this.identifier = b;
            var l = ig.domHandler.getElementById("#" + b);
            l ? (ig.domHandler.show(l),
            ig.domHandler.attr(l, "href", c)) : this.createClickableOutboundLayer(b, c, this.invisImagePath, e)
        },
        update: function(b, c) {
            if (this.pos.x !== b || this.pos.y !== c)
                ig.sizeHandler.dynamicClickableEntityDivs[this.identifier] = {},
                ig.sizeHandler.dynamicClickableEntityDivs[this.identifier].width = this.size.x,
                ig.sizeHandler.dynamicClickableEntityDivs[this.identifier].height = this.size.y,
                ig.sizeHandler.dynamicClickableEntityDivs[this.identifier].entity_pos_x = this.pos.x,
                ig.sizeHandler.dynamicClickableEntityDivs[this.identifier].entity_pos_y = this.pos.y
        },
        createClickableOutboundLayer: function(b, c, e, l) {
            var q = ig.domHandler.create("div");
            ig.domHandler.attr(q, "id", b);
            var x = ig.domHandler.create("a");
            ig.domHandler.addEvent(q, "mousedown", function(E) {
                E.preventDefault()
            });
            l ? (ig.domHandler.attr(x, "href", c),
            ig.domHandler.attr(x, "target", "_blank")) : ig.domHandler.attr(x, "href", c);
            c = ig.domHandler.create("img");
            ig.domHandler.css(c, {
                width: "100%",
                height: "100%"
            });
            ig.domHandler.attr(c, "src", e);
            e = Math.min(ig.sizeHandler.scaleRatioMultiplier.x, ig.sizeHandler.scaleRatioMultiplier.y);
            if (ig.ua.mobile) {
                l = ig.domHandler.getElementById("#canvas");
                l = ig.domHandler.getOffsets(l);
                var C = l.left
                  , A = l.top;
                console.log(l.left);
                ig.sizeHandler.disableStretchToFitOnMobileFlag ? (l = Math.floor(C + this.pos.x * ig.sizeHandler.scaleRatioMultiplier.x) + "px",
                A = Math.floor(A + this.pos.y * ig.sizeHandler.scaleRatioMultiplier.y) + "px",
                C = Math.floor(this.size.x * ig.sizeHandler.scaleRatioMultiplier.x) + "px",
                e = Math.floor(this.size.y * ig.sizeHandler.scaleRatioMultiplier.y) + "px") : (l = Math.floor(this.pos.x * ig.sizeHandler.sizeRatio.x) + "px",
                A = Math.floor(this.pos.y * ig.sizeHandler.sizeRatio.y) + "px",
                C = Math.floor(this.size.x * ig.sizeHandler.sizeRatio.x) + "px",
                e = Math.floor(this.size.y * ig.sizeHandler.sizeRatio.y) + "px")
            } else
                l = ig.domHandler.getElementById("#canvas"),
                l = ig.domHandler.getOffsets(l),
                C = l.left,
                A = l.top,
                ig.sizeHandler.enableStretchToFitOnDesktopFlag ? (l = Math.floor(C + this.pos.x * ig.sizeHandler.sizeRatio.x) + "px",
                A = Math.floor(A + this.pos.y * ig.sizeHandler.sizeRatio.y) + "px",
                C = Math.floor(this.size.x * ig.sizeHandler.sizeRatio.x) + "px",
                e = Math.floor(this.size.y * ig.sizeHandler.sizeRatio.y) + "px") : (l = Math.floor(C + this.pos.x * e) + "px",
                A = Math.floor(A + this.pos.y * e) + "px",
                C = Math.floor(this.size.x * e) + "px",
                e = Math.floor(this.size.y * e) + "px");
            ig.domHandler.css(q, {
                float: "left",
                position: "absolute",
                left: l,
                top: A,
                width: C,
                height: e,
                "z-index": 3
            });
            ig.domHandler.addEvent(q, "mousemove", ig.input.mousemove.bind(ig.input), !1);
            ig.domHandler.appendChild(x, c);
            ig.domHandler.appendChild(q, x);
            ig.domHandler.appendToBody(q);
            ig.sizeHandler.dynamicClickableEntityDivs[b] = {};
            ig.sizeHandler.dynamicClickableEntityDivs[b].width = this.size.x;
            ig.sizeHandler.dynamicClickableEntityDivs[b].height = this.size.y;
            ig.sizeHandler.dynamicClickableEntityDivs[b].entity_pos_x = this.pos.x;
            ig.sizeHandler.dynamicClickableEntityDivs[b].entity_pos_y = this.pos.y
        }
    })
});
ig.baked = !0;
ig.module("game.entities.buttons.button-branding-logo").requires("game.entities.buttons.button", "plugins.clickable-div-layer").defines(function() {
    EntityButtonBrandingLogo = EntityButton.extend({
        type: ig.Entity.TYPE.A,
        gravityFactor: 0,
        logo: new ig.AnimationSheet("/static/spin-arena/v1/branding/logo.png",_SETTINGS.Branding.Logo.Width,_SETTINGS.Branding.Logo.Height),
        zIndex: 10001,
        size: {
            x: 64,
            y: 66
        },
        clickableLayer: null,
        link: null,
        newWindow: !1,
        div_layer_name: "branding-logo",
        name: "brandinglogo",
        init: function(b, c, e) {
            this.parent(b, c, e);
            if (!ig.global.wm) {
                if ("undefined" == typeof wm)
                    if (_SETTINGS.Branding.Logo.Enabled)
                        this.size.x = _SETTINGS.Branding.Logo.Width,
                        this.size.y = _SETTINGS.Branding.Logo.Height,
                        this.anims.idle = new ig.Animation(this.logo,0,[0],!0),
                        this.currentAnim = this.anims.idle,
                        e && e.centralize && (this.pos.x = ig.system.width / 2 - this.size.x / 2,
                        console.log("centralize true ... centering branded logo ...")),
                        _SETTINGS.Branding.Logo.LinkEnabled && (this.link = _SETTINGS.Branding.Logo.Link,
                        this.newWindow = _SETTINGS.Branding.Logo.NewWindow,
                        this.clickableLayer = new ClickableDivLayer(this));
                    else {
                        this.kill();
                        return
                    }
                this.div_layer_name = e.div_layer_name ? e.div_layer_name : "branding-logo"
            }
        },
        show: function() {
            var b = ig.domHandler.getElementById("#" + this.div_layer_name);
            ig.domHandler.show(b)
        },
        hide: function() {
            var b = ig.domHandler.getElementById("#" + this.div_layer_name);
            ig.domHandler.hide(b)
        },
        clicked: function() {},
        clicking: function() {},
        released: function() {}
    })
});
ig.baked = !0;
ig.module("game.entities.branding-logo-placeholder").requires("impact.entity", "game.entities.buttons.button-branding-logo").defines(function() {
    EntityBrandingLogoPlaceholder = ig.Entity.extend({
        gravityFactor: 0,
        size: {
            x: 32,
            y: 32
        },
        _wmDrawBox: !0,
        _wmBoxColor: "rgba(0, 0, 255, 0.7)",
        init: function(b, c, e) {
            this.parent(b, c, e);
            if (e)
                switch (console.log("settings found ... using that div layer name"),
                b = e.div_layer_name,
                console.log("settings.centralize:", e.centralize),
                e.centralize) {
                case "true":
                    console.log("centralize true");
                    centralize = !0;
                    break;
                case "false":
                    console.log("centralize false");
                    centralize = !1;
                    break;
                default:
                    console.log("default ... centralize false"),
                    centralize = !1
                }
            else
                b = "branding-logo",
                centralize = !1;
            if ("undefined" == typeof wm) {
                if (_SETTINGS.Branding.Logo.Enabled)
                    try {
                        ig.game.spawnEntity(EntityButtonBrandingLogo, this.pos.x, this.pos.y, {
                            div_layer_name: b,
                            centralize
                        })
                    } catch (l) {
                        console.log(l)
                    }
                this.kill()
            }
        }
    })
});
ig.baked = !0;
ig.module("game.entities.buttons.button-more-games").requires("game.entities.buttons.button", "plugins.clickable-div-layer").defines(function() {
    EntityButtonMoreGames = EntityButton.extend({
        type: ig.Entity.TYPE.A,
        gravityFactor: 0,
        size: {
            x: 64,
            y: 66
        },
        zIndex: 750,
        clickableLayer: null,
        link: null,
        newWindow: !1,
        div_layer_name: "more-games",
        name: "moregames",
        init: function(b, c, e) {
            this.parent(b, c, e);
            ig.global.wm || (this.div_layer_name = e.div_layer_name ? e.div_layer_name : "more-games",
            _SETTINGS.MoreGames.Enabled ? (this.anims.idle = new ig.Animation(this.logo,0,[0],!0),
            this.currentAnim = this.anims.idle,
            _SETTINGS.MoreGames.Link && (this.link = _SETTINGS.MoreGames.Link),
            _SETTINGS.MoreGames.NewWindow && (this.newWindow = _SETTINGS.MoreGames.NewWindow),
            this.clickableLayer = new ClickableDivLayer(this)) : this.kill())
        },
        show: function() {
            var b = ig.domHandler.getElementById("#" + this.div_layer_name);
            b && ig.domHandler.show(b)
        },
        hide: function() {
            var b = ig.domHandler.getElementById("#" + this.div_layer_name);
            b && ig.domHandler.hide(b)
        },
        clicked: function() {},
        clicking: function() {},
        released: function() {}
    })
});
ig.baked = !0;
ig.module("game.entities.opening-kitty").requires("impact.entity").defines(function() {
    EntityOpeningKitty = ig.Entity.extend({
        size: {
            x: 48,
            y: 48
        },
        kittyAnim: -1,
        kittyImage: new ig.Image("/static/spin-arena/v1/media/graphics/opening/kitty.png"),
        kittyTitleImage: new ig.Image("/static/spin-arena/v1/media/graphics/opening/kittytitle.png"),
        soundKey: "kittyopeningSound",
        init: function(b, c, e) {
            this.parent(b, c, e)
        },
        ready: function() {
            !ig.wm && _SETTINGS.DeveloperBranding.Splash.Enabled && (this.initTimer = new ig.Timer(.1))
        },
        update: function() {
            this.parent();
            this.updateKittyOpening();
            this.unlockWebAudio()
        },
        unlockWebAudio: function() {
            if (ig.input.released("click"))
                try {
                    ig.soundHandler.unlockWebAudio()
                } catch (b) {}
        },
        draw: function() {
            this.parent();
            ig.global.wm || (this.nextLevelTimer && 0 > this.nextLevelTimer.delta() && (ig.system.context.globalAlpha = -this.nextLevelTimer.delta()),
            this.drawKittyOpening())
        },
        updateKittyOpening: function() {
            if (!ig.wm)
                if (_SETTINGS.DeveloperBranding.Splash.Enabled) {
                    if (this.initTimer && 0 < this.initTimer.delta()) {
                        this.initTimer = null;
                        try {
                            ig.soundHandler.sfxPlayer.play(this.soundKey)
                        } catch (b) {
                            console.log(b)
                        }
                        this.kittyTimer = new ig.Timer(.15)
                    }
                    this.kittyTimer && 0 < this.kittyTimer.delta() && (7 > this.kittyAnim ? (this.kittyAnim++,
                    this.kittyTimer.reset()) : (this.kittyTimer = null,
                    this.nextLevelTimer = new ig.Timer(2)));
                    this.nextLevelTimer && 0 < this.nextLevelTimer.delta() && (this.nextLevelTimer = null,
                    ig.game.director.nextLevel(),
                    ig.system.context.globalAlpha = 1)
                } else
                    ig.game.director.nextLevel(),
                    ig.system.context.globalAlpha = 1,
                    this.kill()
        },
        drawKittyOpening: function() {
            var b = ig.system.context.createLinearGradient(0, 0, 0, ig.system.height);
            b.addColorStop(0, "#ffed94");
            b.addColorStop(1, "#ffcd85");
            ig.system.context.fillStyle = b;
            ig.system.context.fillRect(0, 0, ig.system.width, ig.system.height);
            0 <= this.kittyAnim && (this.kittyImage.drawTile(ig.system.width / 2 - this.kittyImage.width / 8, ig.system.height / 2 - this.kittyImage.height / 4, this.kittyAnim, 218, 325),
            this.kittyTitleImage.drawTile(ig.system.width / 2 - this.kittyTitleImage.width / 2, ig.system.height / 2 + this.kittyImage.height / 4 + 10, this.kittyAnim, 380, 37));
            ig.system.context.globalAlpha = 1
        }
    })
});
ig.baked = !0;
ig.module("game.entities.pointer").requires("impact.entity").defines(function() {
    EntityPointer = ig.Entity.extend({
        checkAgainst: ig.Entity.TYPE.BOTH,
        size: new Vector2(1,1),
        isFirstPressed: !1,
        isPressed: !1,
        isReleased: !1,
        isHovering: !1,
        hoveringItem: null,
        objectArray: [],
        clickedObjectList: [],
        ignorePause: !0,
        zIndex: 5500,
        init: function(b, c, e) {
            this.parent(b, c, e);
            ig.global.wm || (ig.game.pointer && ig.game.pointer.kill(),
            ig.game.pointer = this)
        },
        check: function(b) {
            this.objectArray.push(b)
        },
        clickObject: function(b) {
            this.isFirstPressed && "function" == typeof b.clicked && (b.clicked(),
            this.addToClickedObjectList(b));
            this.isPressed && !this.isReleased && "function" == typeof b.clicking && b.clicking();
            this.isReleased && "function" == typeof b.released && (b.released(),
            this.removeFromClickedObjectList(b))
        },
        refreshPos: function() {
            this.pos = ig.game.io.getClickPos()
        },
        update: function() {
            this.parent();
            this.refreshPos();
            var b = null
              , c = -1;
            for (a = this.objectArray.length - 1; -1 < a; a--)
                this.objectArray[a].zIndex > c && (c = this.objectArray[a].zIndex,
                b = this.objectArray[a]);
            if (null != b)
                null != this.hoveringItem ? this.hoveringItem != b && ("function" == typeof this.hoveringItem.leave && this.hoveringItem.leave(),
                "function" == typeof b.over && b.over()) : "function" == typeof b.over && b.over(),
                this.hoveringItem = b,
                this.clickObject(b),
                this.objectArray = [];
            else if (null != this.hoveringItem && "function" == typeof this.hoveringItem.leave && (this.hoveringItem.leave(),
            this.hoveringItem = null),
            this.isReleased) {
                for (b = 0; b < this.clickedObjectList.length; b++)
                    c = this.clickedObjectList[b],
                    "function" == typeof c.releasedOutside && c.releasedOutside();
                this.clickedObjectList = []
            }
            this.isFirstPressed = ig.input.pressed("click");
            this.isReleased = ig.input.released("click");
            this.isPressed = ig.input.state("click")
        },
        addToClickedObjectList: function(b) {
            this.clickedObjectList.push(b)
        },
        removeFromClickedObjectList: function(b) {
            for (var c = [], e = 0; e < this.clickedObjectList.length; e++) {
                var l = this.clickedObjectList[e];
                l != b && c.push(l)
            }
            this.clickedObjectList = c
        }
    })
});
ig.baked = !0;
ig.module("game.entities.pointer-selector").requires("game.entities.pointer").defines(function() {
    EntityPointerSelector = EntityPointer.extend({
        zIndex: 1E3,
        _wmDrawBox: !0,
        _wmBoxColor: "rgba(0, 0, 255, 0.7)",
        size: {
            x: 20,
            y: 20
        },
        init: function(b, c, e) {
            this.parent(b, c, e)
        }
    })
});
ig.baked = !0;
ig.module("game.entities.select").requires("impact.entity").defines(function() {
    EntitySelect = ig.Entity.extend({
        type: ig.Entity.TYPE.B,
        checkAgainst: ig.Entity.TYPE.A,
        collides: ig.Entity.COLLIDES.NEVER,
        canSelect: !1,
        canSelectTimerDuration: .35,
        zIndex: 99999,
        isHovering: !1,
        isSelected: !1,
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.canSelectTimer = new ig.Timer(this.canSelectTimerDuration)
        },
        doesClickableLayerExist: function(b) {
            for (k in dynamicClickableEntityDivs)
                if (k == b)
                    return !0;
            return !1
        },
        checkClickableLayer: function(b, c, e) {
            "undefined" == typeof wm && (this.doesClickableLayerExist(b) ? (ig.game.showOverlay([b]),
            $("#" + b).find("[href]").attr("href", c)) : this.createClickableOutboundLayer(b, c, "/static/spin-arena/v1/media/graphics/misc/invisible.png", e))
        },
        createClickableOutboundLayer: function(b, c, e, l) {
            var q = ig.$new("div");
            q.id = b;
            document.body.appendChild(q);
            $("#" + q.id).css("float", "left");
            $("#" + q.id).css("width", this.size.x * multiplier);
            $("#" + q.id).css("height", this.size.y * multiplier);
            $("#" + q.id).css("position", "absolute");
            var x = w / 2 - destW / 2
              , C = h / 2 - destH / 2;
            w == mobileWidth ? ($("#" + q.id).css("left", this.pos.x),
            $("#" + q.id).css("top", this.pos.y)) : ($("#" + q.id).css("left", x + this.pos.x * multiplier),
            $("#" + q.id).css("top", C + this.pos.y * multiplier));
            l ? $("#" + q.id).html("<a target='_blank' href='" + c + "'><img style='width:100%;height:100%' src='" + e + "'></a>") : $("#" + q.id).html("<a href='" + c + "'><img style='width:100%;height:100%' src='" + e + "'></a>");
            dynamicClickableEntityDivs[b] = {};
            dynamicClickableEntityDivs[b].width = $("#" + q.id).width();
            dynamicClickableEntityDivs[b].height = $("#" + q.id).height();
            dynamicClickableEntityDivs[b].entity_pos_x = this.pos.x;
            dynamicClickableEntityDivs[b].entity_pos_y = this.pos.y
        },
        hovered: function() {
            this.isHovering = !0;
            this.dehoverOthers()
        },
        dehoverOthers: function() {
            var b = ig.game.getEntitiesByType(EntitySelect);
            for (i = 0; i < b.length; i++)
                b[i] != this && (b[i].isHovering = !1)
        },
        deselectOthers: function() {
            var b = ig.game.getEntitiesByType(EntitySelect);
            for (i = 0; i < b.length; i++)
                b[i] != this && (b[i].isSelected = !1)
        },
        update: function() {
            this.parent();
            this.canSelectTimer && 0 < this.canSelectTimer.delta() && (this.canSelect = !0,
            this.canSelectTimer = null)
        }
    })
});
ig.baked = !0;
ig.module("game.entities.buttons.button-toggle").requires("game.entities.buttons.button").defines(function() {
    EntityButtonToggle = EntityButton.extend({
        isOn: !1,
        label_on: !1,
        label_off: !1,
        highlightScale: 1,
        clickScale: 1,
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.animations && (b = e.animations,
            b.on_normal && (this.anims.on_normal = new ig.Animation(b.on_normal.animSheet,b.on_normal.frameTime,b.on_normal.sequence,b.on_normal.stop)),
            b.off_normal && (this.anims.off_normal = new ig.Animation(b.off_normal.animSheet,b.off_normal.frameTime,b.off_normal.sequence,b.off_normal.stop)),
            b.on_hover && (this.anims.on_hover = new ig.Animation(b.on_hover.animSheet,b.on_hover.frameTime,b.on_hover.sequence,b.on_hover.stop)),
            b.off_hover && (this.anims.off_hover = new ig.Animation(b.off_hover.animSheet,b.off_hover.frameTime,b.off_hover.sequence,b.off_hover.stop)),
            b.on_clicked && (this.anims.on_clicked = new ig.Animation(b.on_clicked.animSheet,b.on_clicked.frameTime,b.on_clicked.sequence,b.on_clicked.stop)),
            b.off_clicked && (this.anims.off_clicked = new ig.Animation(b.off_clicked.animSheet,b.off_clicked.frameTime,b.off_clicked.sequence,b.off_clicked.stop)));
            this.isOn && this.anims.on_normal ? this.currentAnim = this.anims.on_normal.rewind() : !this.isOn && this.anims.off_normal && (this.currentAnim = this.anims.off_normal.rewind());
            this.updateLabel()
        },
        clicked: function() {
            this.isLocked || (this.parent(),
            this.isOn && this.anims.on_clicked ? this.currentAnim = this.anims.on_clicked.rewind() : !this.isOn && this.anims.off_clicked && (this.currentAnim = this.anims.off_clicked.rewind()),
            this.updateLabel())
        },
        onClicked: function() {},
        onReleased: function() {},
        over: function() {
            this.isLocked || (this.parent(),
            this.isOn && this.anims.on_hover ? this.currentAnim = this.anims.on_hover.rewind() : !this.isOn && this.anims.off_hover && (this.currentAnim = this.anims.off_hover.rewind()),
            this.updateLabel())
        },
        leave: function() {
            this.isLocked || (this.parent(),
            this.isOn && this.anims.on_normal ? this.currentAnim = this.anims.on_normal.rewind() : !this.isOn && this.anims.off_normal && (this.currentAnim = this.anims.off_normal.rewind()),
            this.updateLabel())
        },
        updateLabel: function() {
            this.label = this.isOn ? this.label_on : this.label_off;
            this.isOn && this.anims.on_normal ? this.currentAnim = this.anims.on_normal.rewind() : !this.isOn && this.anims.off_normal && (this.currentAnim = this.anims.off_normal.rewind())
        }
    })
});
ig.baked = !0;
ig.module("game.entities.controllers.form-controller").requires("impact.entity").defines(function() {
    EntityFormController = ig.Entity.extend({
        init: function(b, c, e) {
            this.parent(b, c, e);
            try {
                grecaptcha.reset(),
                document.getElementById("register-tos-agree-error").style.display = "none",
                MarketJSPlatformLogoutAPI.initialize()
            } catch (q) {}
            try {
                if (!ig.game.prizeImagePreloaded) {
                    for (b = 0; b < _STRINGS.WheelSlices.length; b++) {
                        var l = _STRINGS.WheelSlices[b];
                        "string" == typeof l.slicePrizeImage && 0 < l.slicePrizeImage.length && new ig.Image(l.slicePrizeImage);
                        "string" == typeof l.prizeImage && 0 < l.prizeImage.length && new ig.Image(l.prizeImage)
                    }
                    ig.game.prizeImagePreloaded = !0
                }
            } catch (q) {}
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            b.fillStyle = "#f4f4f4";
            b.fillRect(0, 0, ig.system.width, ig.system.height);
            b.restore()
        }
    })
});
ig.baked = !0;
ig.module("game.entities.objects.wheel-prize").requires("impact.entity").defines(function() {
    EntityWheelPrize = ig.Entity.extend({
        zIndex: 100,
        anchorPivot: {
            x: .5,
            y: .5
        },
        lineWidth: 10,
        borderAlpha: 1,
        baseX: 0,
        baseY: 0,
        isBlurred: !1,
        selectedScale: 0,
        curSelected: !1,
        name: "",
        names: [],
        solidBg: {
            common: new ig.Image("/static/spin-arena/v1/media/graphics/game/common-solidbg.png"),
            rare: new ig.Image("/static/spin-arena/v1/media/graphics/game/rare-solidbg.png"),
            grandprize: new ig.Image("/static/spin-arena/v1/media/graphics/game/grandprize-solidbg.png")
        },
        borderBg: {
            common: new ig.Image("/static/spin-arena/v1/media/graphics/game/common-border.png"),
            rare: new ig.Image("/static/spin-arena/v1/media/graphics/game/rare-border.png"),
            grandprize: new ig.Image("/static/spin-arena/v1/media/graphics/game/grandprize-border.png")
        },
        borderSelectedBg: {
            common: new ig.Image("/static/spin-arena/v1/media/graphics/game/common-border-selected.png"),
            rare: new ig.Image("/static/spin-arena/v1/media/graphics/game/rare-border-selected.png"),
            grandprize: new ig.Image("/static/spin-arena/v1/media/graphics/game/grandprize-border-selected.png")
        },
        init: function(b, c, e) {
            var l = (200 / e.sheet.width).toFixed(3)
              , q = (200 / e.sheet.height).toFixed(3);
            this.scale = {
                x: l,
                y: q
            };
            this.size = {
                x: e.sheet.width,
                y: e.sheet.height
            };
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            this.baseX = b;
            this.baseY = c;
            this.anims.idle = new ig.Animation(e.sheet,1,[0],!0);
            this.currentAnim = this.anims.idle;
            this.variant = e.displayConfig.variant;
            this.id = e.id;
            this.selected(!1);
            this.names = this.getLines(ig.system.context, this.name, 87)
        },
        getLines: function(b, c, e) {
            c = c.split(" ");
            for (var l = [], q = c[0], x = 1; x < c.length; x++) {
                var C = c[x];
                b.measureText(q + " " + C).width < e ? q += " " + C : (l.push(q),
                q = C)
            }
            l.push(q);
            return l
        },
        update: function() {
            this.controller.isPaused ? this.refreshAnchoredPosition() : this.parent()
        },
        draw: function() {
            if (!(this.controller.totalLoadedAssets < ig.gameData.prizes.length)) {
                var b = ig.system.context;
                b.save();
                if (ig.ua.mobile)
                    if (ig.system.width < ig.system.height)
                        var c = 1.9 + this.selectedScale
                          , e = 2.05
                          , l = 150;
                    else
                        c = .83 + this.selectedScale,
                        e = .91,
                        l = 0;
                else
                    c = .7 + this.selectedScale,
                    e = .8,
                    l = 0;
                this.setAnchoredPosition(this.baseX * e, this.baseY * e, this.anchorTo);
                e = this.pos.x + .5 * this.size.x;
                l = this.pos.y + .5 * this.size.y + l;
                b.translate(e, l);
                b.scale(c, c);
                b.translate(-e, -l);
                if (this.isBlurred) {
                    for (var q = 0, x = 1 / (2 * Math.PI), C = -5; 5 >= C; C += 2)
                        for (var A = -5; 5 >= A; A += 2)
                            q += x * Math.exp(-(A * A + C * C) / 2);
                    for (C = -5; 5 >= C; C += 2)
                        for (A = -5; 5 >= A; A += 2)
                            b.globalAlpha = x * Math.exp(-(A * A + C * C) / 2) / q * 5,
                            this.pos.x = e - .5 * this.size.x + A,
                            this.pos.y = l - .5 * this.size.y + C,
                            this.solidBg[this.variant] && ig.responsive.drawScaledImage(this.solidBg[this.variant], e, l, 1, 1, .5, .5),
                            this.parent(),
                            this.curSelected ? this.borderSelectedBg[this.variant] && ig.responsive.drawScaledImage(this.borderSelectedBg[this.variant], e, l, 1, 1, .5, .5) : this.borderBg[this.variant] && ig.responsive.drawScaledImage(this.borderBg[this.variant], e, l, 1, 1, .5, .5);
                    b.restore();
                    b.save();
                    b.fillStyle = "#ffffff";
                    b.textAlign = "center";
                    b.textBaseline = "middle";
                    b.shadowOffsetX = 0;
                    b.shadowOffsetY = 2;
                    b.shadowColor = "rgba(0,0,0,1)";
                    yPos = ig.ua.mobile ? ig.system.width < ig.system.height ? 1200 : 800 : 800;
                    b.translate(e, l);
                    b.scale(c, c);
                    b.translate(-e, -l);
                    c = 1 == this.names.length ? .43 * this.size.y : .38 * this.size.y;
                    b.font = 1 == this.names.length ? "bold 21px oxanium" : "bold 20px oxanium";
                    for (q = 0; q < this.names.length; q++)
                        b.fillText(this.names[q], e, l + c + q * this.size.y * .1);
                    b.restore();
                    this.pos.x = e;
                    this.pos.y = l
                } else {
                    this.solidBg[this.variant] && ig.responsive.drawScaledImage(this.solidBg[this.variant], e, l, 1, 1, .5, .5);
                    this.pos.y = l - .5 * this.size.y;
                    this.parent();
                    this.curSelected ? this.borderSelectedBg[this.variant] && ig.responsive.drawScaledImage(this.borderSelectedBg[this.variant], e, l, 1, 1, .5, .5) : this.borderBg[this.variant] && ig.responsive.drawScaledImage(this.borderBg[this.variant], e, l, 1, 1, .5, .5);
                    b.restore();
                    b.save();
                    b.fillStyle = "#ffffff";
                    b.textAlign = "center";
                    b.textBaseline = "middle";
                    b.shadowOffsetX = 0;
                    b.shadowOffsetY = 2;
                    b.shadowColor = "rgba(0,0,0,1)";
                    yPos = ig.ua.mobile ? ig.system.width < ig.system.height ? 1200 : 800 : 800;
                    b.translate(e, l);
                    b.scale(c, c);
                    b.translate(-e, -l);
                    c = 1 == this.names.length ? .43 * this.size.y : .38 * this.size.y;
                    b.font = 1 == this.names.length ? "bold 21px oxanium" : "bold 20px oxanium";
                    for (q = 0; q < this.names.length; q++)
                        b.fillText(this.names[q], e, l + c + q * this.size.y * .1);
                    b.restore()
                }
                b.globalAlpha = 1;
                b.save();
                b.restore()
            }
        },
        selected: function(b) {
            this.selectedScale = (this.curSelected = b) ? .1 : 0
        }
    })
});
ig.baked = !0;
ig.module("game.entities.objects.wheel").requires("impact.entity", "game.entities.objects.wheel-prize").defines(function() {
    EntityWheel = ig.Entity.extend({
        zIndex: 100,
        size: {
            x: 1014,
            y: 917
        },
        anchorPivot: {
            x: .5,
            y: .5
        },
        wheelFrameOffset: {
            x: -44,
            y: -44
        },
        sliceNumber: 0,
        sliceBackgroundImages: [],
        sliceBackgroundColors: [],
        prizeImages: [],
        prizeImageOffset: 199,
        sliceTextColors: [],
        sliceTexts: [],
        sliceTextSize: 0,
        sliceTextMaxWidth: 0,
        sliceTextOffset: 199,
        sliceTextMaxLength: 10,
        isSpinning: !1,
        spin_duration: 5,
        spin_prerollAngle: 1440,
        pointerAnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/wheel/wheel-pointer.png",120,148),
        pointerOffset: {
            x: 223,
            y: -107
        },
        pointerSpeed: 20,
        tickSound: "",
        tickSoundPlayed: !1,
        ticking_rangeLeft: 6,
        ticking_rangeRight: 14,
        ticking_safeZone: 3,
        scale: {
            x: 1,
            y: 1
        },
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            this.slices = _STRINGS.WheelSlices;
            for (b = 0; b < this.slices.length; b++)
                c = this.slices[b],
                this.sliceTexts.push(c.sliceText),
                this.sliceTextColors.push(c.sliceTextColor);
            this.pointer = new ig.Animation(this.pointerAnimSheet,1,[0],!0);
            this.pointer.pivot = {
                x: 60,
                y: 61
            };
            this.sliceNumber = this.slices.length;
            this.slice_angle = 360 / this.sliceNumber;
            this.slice_edgeOffsetAngle = -this.slice_angle / 2;
            this.prizeImageOffset = _STRINGS.Wheel.prizeImageOffset;
            this.sliceTextOffset = _STRINGS.Wheel.sliceTextOffset;
            this.sliceTextSize = _STRINGS.Wheel.sliceTextSize;
            this.sliceTextMaxWidth = Math.tan(this.toRadian(.5 * this.slice_angle)) * this.sliceTextOffset * 2;
            this.sliceTextMaxWidth -= Math.min(20, .2 * this.sliceTextMaxWidth);
            this.angle = this.elapsed = this.spin_index = 0;
            this.reorderPrizes();
            this.loadWheelAssets()
        },
        reorderPrizes: function() {},
        loadWheelAssets: function() {
            this.totalLoadedAssets = 0;
            if (ig.gameData && ig.gameData.prizes) {
                this.prizeData = [];
                for (var b = ig.gameData.prizes, c = 8 == b.length ? [{
                    x: 0,
                    y: -380
                }, {
                    x: 260,
                    y: -315
                }, {
                    x: 347,
                    y: -75
                }, {
                    x: 260,
                    y: 165
                }, {
                    x: 0,
                    y: 230
                }, {
                    x: -260,
                    y: 165
                }, {
                    x: -347,
                    y: -75
                }, {
                    x: -260,
                    y: -315
                }] : 7 == b.length ? [{
                    x: 150,
                    y: -370
                }, {
                    x: 330,
                    y: -135
                }, {
                    x: 270,
                    y: 120
                }, {
                    x: 0,
                    y: 230
                }, {
                    x: -270,
                    y: 120
                }, {
                    x: -330,
                    y: -135
                }, {
                    x: -150,
                    y: -370
                }] : 6 == b.length ? [{
                    x: 170,
                    y: -350
                }, {
                    x: 330,
                    y: -80
                }, {
                    x: 170,
                    y: 190
                }, {
                    x: -170,
                    y: 190
                }, {
                    x: -330,
                    y: -80
                }, {
                    x: -170,
                    y: -350
                }] : 5 == b.length ? [{
                    x: 190,
                    y: -350
                }, {
                    x: 310,
                    y: -40
                }, {
                    x: 0,
                    y: 230
                }, {
                    x: -310,
                    y: -40
                }, {
                    x: -190,
                    y: -350
                }] : 4 == b.length ? [{
                    x: 230,
                    y: -350
                }, {
                    x: 230,
                    y: 170
                }, {
                    x: -230,
                    y: 170
                }, {
                    x: -230,
                    y: -350
                }] : 3 == b.length ? [{
                    x: 280,
                    y: -280
                }, {
                    x: 0,
                    y: 230
                }, {
                    x: -280,
                    y: -280
                }] : 2 == b.length ? [{
                    x: -320,
                    y: -100
                }, {
                    x: 320,
                    y: -100
                }] : [{
                    x: 0,
                    y: -380
                }], e = 0, l; e < b.length; e++)
                    if (l = ig.Image.cache[b[e].image_url]) {
                        var q = new ig.AnimationSheet(b[e].image_url,l.width,l.height);
                        q = ig.game.spawnEntity(EntityWheelPrize, c[e].x, c[e].y, {
                            anchorTo: "center",
                            sheet: q,
                            name: b[e].name,
                            displayConfig: b[e].display_config,
                            id: b[e].id,
                            controller: this
                        });
                        this.prizeData.push({
                            path: b[e].image_url,
                            image: l,
                            entity: q,
                            id: b[e].id,
                            name: b[e].name,
                            description: b[e].description,
                            displayConfig: b[e].display_config
                        });
                        ++this.totalLoadedAssets == ig.gameData.prizes.length && (this.controller.btn_spin.isLocked = !1)
                    } else
                        l = new ig.Image(b[e].image_url),
                        l.idx = e,
                        l.controller = this,
                        l.loadCallback = function(x, C) {
                            if (C) {
                                C = ig.gameData.prizes;
                                var A = 8 == C.length ? [{
                                    x: 0,
                                    y: -380
                                }, {
                                    x: 260,
                                    y: -315
                                }, {
                                    x: 347,
                                    y: -75
                                }, {
                                    x: 260,
                                    y: 165
                                }, {
                                    x: 0,
                                    y: 230
                                }, {
                                    x: -260,
                                    y: 165
                                }, {
                                    x: -347,
                                    y: -75
                                }, {
                                    x: -260,
                                    y: -315
                                }] : 7 == C.length ? [{
                                    x: 150,
                                    y: -370
                                }, {
                                    x: 330,
                                    y: -135
                                }, {
                                    x: 270,
                                    y: 120
                                }, {
                                    x: 0,
                                    y: 230
                                }, {
                                    x: -270,
                                    y: 120
                                }, {
                                    x: -330,
                                    y: -135
                                }, {
                                    x: -150,
                                    y: -370
                                }] : 6 == C.length ? [{
                                    x: 170,
                                    y: -350
                                }, {
                                    x: 330,
                                    y: -80
                                }, {
                                    x: 170,
                                    y: 190
                                }, {
                                    x: -170,
                                    y: 190
                                }, {
                                    x: -330,
                                    y: -80
                                }, {
                                    x: -170,
                                    y: -350
                                }] : 5 == C.length ? [{
                                    x: 190,
                                    y: -350
                                }, {
                                    x: 310,
                                    y: -40
                                }, {
                                    x: 0,
                                    y: 230
                                }, {
                                    x: -310,
                                    y: -40
                                }, {
                                    x: -190,
                                    y: -350
                                }] : 4 == C.length ? [{
                                    x: 230,
                                    y: -350
                                }, {
                                    x: 230,
                                    y: 170
                                }, {
                                    x: -230,
                                    y: 170
                                }, {
                                    x: -230,
                                    y: -350
                                }] : 3 == C.length ? [{
                                    x: 280,
                                    y: -280
                                }, {
                                    x: 0,
                                    y: 230
                                }, {
                                    x: -280,
                                    y: -280
                                }] : 2 == C.length ? [{
                                    x: -320,
                                    y: -100
                                }, {
                                    x: 320,
                                    y: -100
                                }] : [{
                                    x: 0,
                                    y: -380
                                }]
                                  , E = new ig.AnimationSheet(x,this.width,this.height);
                                A = ig.game.spawnEntity(EntityWheelPrize, A[this.idx].x, A[this.idx].y, {
                                    anchorTo: "center",
                                    sheet: E,
                                    name: C[this.idx].name,
                                    displayConfig: C[this.idx].display_config,
                                    id: C[this.idx].id,
                                    controller: this.controller
                                });
                                this.controller.prizeData[this.idx] = {
                                    path: x,
                                    image: this,
                                    entity: A,
                                    id: C[this.idx].id,
                                    name: C[this.idx].name,
                                    description: C[this.idx].description,
                                    displayConfig: C[this.idx].display_config
                                };
                                ++this.controller.totalLoadedAssets == ig.gameData.prizes.length && (this.controller.controller.btn_spin.isLocked = !1)
                            }
                        }
            }
        },
        update: function() {
            if (this.controller.isPaused)
                this.refreshAnchoredPosition();
            else if (this.parent(),
            this.isSpinning && (this.elapsed += ig.system.tick / this.spin_duration,
            1 >= this.elapsed ? this.angle = this.easeFn(0, this.targetAngle, this.elapsed) : (this.angle = this.targetAngle,
            this.isSpinning = !1,
            this.onSpinFinished())),
            this.ticking_value = this.slice_angle - (this.angle - this.slice_edgeOffsetAngle) % this.slice_angle,
            this.ticking_value > this.slice_angle - this.ticking_rangeRight)
                this.pointer.angle = this.toRadian(2 * (-this.ticking_rangeLeft - (this.slice_angle - this.ticking_value))),
                this.tickSoundPlayed = !1;
            else if (this.ticking_value < this.ticking_rangeLeft)
                this.pointer.angle = this.toRadian(2 * (this.ticking_value - this.ticking_rangeLeft));
            else if (this.pointer.angle < this.toRadian(-1)) {
                this.tickSoundPlayed || (ig.soundHandler.sfxPlayer.play("tick"),
                this.tickSoundPlayed = !0);
                this.pointer.angle = this.lerpFn(this.pointer.angle, 0, ig.system.tick * this.pointerSpeed);
                var b = 360 / ig.gameData.prizes.length
                  , c = this.angle - 22.5 + b;
                c -= 360 * Math.floor(c / 360);
                b = Math.floor(c / b);
                for (c = 0; c < this.prizeData.length; c++)
                    b == c ? this.prizeData[c].entity.selected(!0) : this.prizeData[c].entity.selected(!1)
            }
        },
        draw: function() {
            this.parent();
            var b = ig.system.context;
            b.save();
            b.fillStyle = "#ffffff";
            b.textAlign = "center";
            b.textBaseline = "middle"
        },
        spin: function(b) {
            this.isSpinning || (this.isSpinning = !0,
            this.elapsed = 0,
            this.spin_index = b,
            b = this.ticking_rangeRight + this.ticking_safeZone,
            this.randomness = b + Math.random() * (this.slice_angle + this.ticking_rangeLeft - this.ticking_safeZone - b) << 0,
            this.targetAngle = this.slice_angle * this.spin_index + this.randomness + this.spin_prerollAngle + this.slice_edgeOffsetAngle + (180 <= this.slice_angle * this.spin_index ? 360 : 0),
            console.log(this.targetAngle))
        },
        onSpinFinished: function() {
            this.controller.getPrize(this.spin_index)
        },
        prizeHighlightAnimation: function() {},
        easeFn: function(b, c, e) {
            e = 1 - --e * e * e * e;
            return c * e + b * (1 - e)
        },
        lerpFn: function(b, c, e) {
            return (1 - e) * b + e * c
        },
        toRadian: function(b) {
            return Math.PI / 180 * b
        },
        setBlur(b) {
            for (var c = 0; c < this.prizeData.length; c++)
                this.prizeData[c].entity.isBlurred = b
        }
    })
});
ig.baked = !0;
ig.module("game.entities.objects.character").requires("impact.entity").defines(function() {
    EntityCharacter = ig.Entity.extend({
        zIndex: 90,
        size: {
            x: 396,
            y: 640
        },
        anchorPivot: {
            x: .5,
            y: .5
        },
        danceAnimSheets: [new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_00.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_01.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_02.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_03.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_04.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_05.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_06.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_07.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_08.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_00.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_01.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_02.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_03.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_04.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_05.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_06.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_07.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_dance_08.png",396,640)],
        idleAnimSheets: [new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_00.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_01.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_02.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_03.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_04.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_05.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_06.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_07.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_08.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_09.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_10.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_11.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_12.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_13.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_14.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_15.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_16.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_17.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_18.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_19.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_20.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_21.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_22.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_23.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_24.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_25.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_26.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_27.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_28.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_29.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_30.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_31.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_32.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_00.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_01.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_02.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_03.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_04.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_05.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_06.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_07.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_08.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_09.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_10.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_11.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_12.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_13.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_14.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_15.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_16.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_17.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_18.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_19.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_20.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_21.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_22.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_23.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_24.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_25.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_26.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_27.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_28.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_29.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_30.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_31.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_32.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_33.png",396,640), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/character/char_idle_2_34.png",396,640)],
        animState: "",
        isBlurred: !1,
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            for (b = 0; b < this.idleAnimSheets.length; b++)
                this.anims["idle_" + b] = new ig.Animation(this.idleAnimSheets[b],.07,[0],!0);
            for (b = 0; b < this.danceAnimSheets.length; b++)
                this.anims["dance_" + b] = new ig.Animation(this.danceAnimSheets[b],.1,[0],!0),
                b >= Math.floor(.5 * this.danceAnimSheets.length) && (this.anims["dance_" + b].flip.x = !0);
            this.changeAnim("idle")
        },
        changeAnim: function(b) {
            this.state = b;
            this.idx = 0;
            this.currentAnim = this.anims[b + "_0"];
            this.currentAnim.rewind();
            ig.responsive.toAnchor(0, 0, "center-bottom");
            var c = ig.system.width < ig.system.height ? .4 * -ig.system.height : .5 * -ig.system.height;
            "dance" == b ? this.setAnchoredPosition(20, c, "center-bottom") : this.setAnchoredPosition(70, c, "center-bottom")
        },
        updateAnim: function() {
            "dance" == this.state ? ++this.idx > this.danceAnimSheets.length - 1 && (this.idx = 0) : "idle" == this.state && ++this.idx > this.idleAnimSheets.length - 1 && (this.idx = 0);
            this.currentAnim = this.anims[this.state + "_" + this.idx];
            this.currentAnim.rewind()
        },
        update: function() {
            this.parent();
            0 < this.currentAnim.loopCount && this.updateAnim();
            ig.ua.mobile ? ig.system.width < ig.system.height ? this.setScale(2, 2) : this.setScale(1, 1) : this.setScale(1, 1)
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            if (this.isBlurred) {
                for (var c = this.pos.x, e = this.pos.y, l = 0, q = 1 / (2 * Math.PI), x = -5; 5 >= x; x += 2)
                    for (var C = -5; 5 >= C; C += 2)
                        l += q * Math.exp(-(C * C + x * x) / 2);
                for (x = -5; 5 >= x; x += 2)
                    for (C = -5; 5 >= C; C += 2)
                        b.globalAlpha = q * Math.exp(-(C * C + x * x) / 2) / l * 5,
                        this.pos.x = c + C,
                        this.pos.y = e + x,
                        this.parent();
                this.pos.x = c;
                this.pos.y = e
            } else
                this.parent();
            b.restore()
        },
        reorient: function(b) {
            this.setAnchoredPosition("dance" == this.state ? 20 : 70, ig.ua.mobile ? "portrait" == b ? .42 * -ig.system.height : .52 * -ig.system.height : .51 * -ig.system.height, "center-bottom")
        },
        setBlur: function(b) {
            this.isBlurred = b
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.popup-settings").requires("impact.entity").defines(function() {
    EntityPopupSettings = ig.Entity.extend({
        zIndex: 1E3,
        size: {
            x: 850,
            y: 1E3
        },
        anchorPivot: {
            x: .5,
            y: .5
        },
        animSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/popup.png",850,1E3),
        settingsBG: new ig.Image("/static/spin-arena/v1/media/graphics/game/settings-bg.png"),
        settingsBGOffset: {
            x: 50,
            y: 185
        },
        btn_on_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/btn-on.png",366,120),
        btn_off_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/btn-off.png",366,120),
        btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/button.png",366,120),
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.setAnchoredPosition(b, c, e.anchorTo || "center");
            this.addAnim("default", 1, [0], !0);
            this.btn_resume = ig.game.spawnEntity(EntityButton, 0, 376, {
                name: "btn_resume",
                label: _STRINGS.Game.Resume,
                fontColor: "#ffffff",
                fontSize: 65,
                size: {
                    x: 366,
                    y: 120
                },
                anchorTo: "center",
                anchorPivot: {
                    x: .5,
                    y: .5
                },
                animations: {
                    normal: {
                        animSheet: this.btn_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    }
                },
                onReleased: this.onResumeButtonClick.bind(this),
                zIndex: this.zIndex + 1
            });
            this.btn_music = ig.game.spawnEntity(EntityButtonToggle, 173, -215, {
                name: "btn_music",
                label_on: _STRINGS.Game.On,
                label_off: _STRINGS.Game.Off,
                fontColor: "#ffffff",
                fontSize: 65,
                size: {
                    x: 366,
                    y: 120
                },
                anchorTo: "center",
                anchorPivot: {
                    x: .5,
                    y: .5
                },
                animations: {
                    on_normal: {
                        animSheet: this.btn_on_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    },
                    off_normal: {
                        animSheet: this.btn_off_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    }
                },
                onReleased: this.ToggleMusic.bind(this),
                zIndex: this.zIndex + 1,
                isOn: ig.game.sessionData.music
            });
            this.btn_sound = ig.game.spawnEntity(EntityButtonToggle, 173, -26, {
                name: "btn_sound",
                label_on: _STRINGS.Game.On,
                label_off: _STRINGS.Game.Off,
                fontColor: "#ffffff",
                fontSize: 65,
                size: {
                    x: 366,
                    y: 120
                },
                anchorTo: "center",
                anchorPivot: {
                    x: .5,
                    y: .5
                },
                animations: {
                    on_normal: {
                        animSheet: this.btn_on_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    },
                    off_normal: {
                        animSheet: this.btn_off_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    }
                },
                onReleased: this.ToggleSound.bind(this),
                zIndex: this.zIndex + 1,
                isOn: ig.game.sessionData.sound
            })
        },
        update: function() {
            this.parent()
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            b.fillStyle = "rgba(0,0,0," + .5 * this.currentAnim.alpha + ")";
            b.fillRect(0, 0, ig.system.width, ig.system.height);
            this.parent();
            b.globalAlpha = this.currentAnim.alpha;
            this.settingsBG.draw(this.pos.x + this.settingsBGOffset.x, this.pos.y + this.settingsBGOffset.y);
            b.fillStyle = "#5367ee";
            b.font = "100px metropolis";
            b.textAlign = "center";
            b.textBaseline = "middle";
            b.fillText(_STRINGS.Game.Settings, this.pos.x + .5 * this.size.x, this.pos.y + 96);
            b.fillStyle = "#003499";
            b.font = "65px metropolis";
            b.textAlign = "left";
            b.fillText(_STRINGS.Game.BGM, this.pos.x + 138, this.pos.y + 289);
            b.fillStyle = "#003499";
            b.font = "65px metropolis";
            b.textAlign = "left";
            b.fillText(_STRINGS.Game.SFX, this.pos.x + 138, this.pos.y + 479);
            b.restore()
        },
        ToggleMusic: function() {
            ig.game.sessionData.music ? (ig.game.sessionData.music = !1,
            ig.soundHandler.muteBGM()) : (ig.game.sessionData.music = !0,
            ig.soundHandler.unmuteBGM());
            ig.soundHandler.sfxPlayer.play("button");
            this.btn_music.isOn = ig.game.sessionData.music;
            ig.game.saveAll()
        },
        ToggleSound: function() {
            ig.game.sessionData.sound ? (ig.game.sessionData.sound = !1,
            ig.soundHandler.muteSFX()) : (ig.game.sessionData.sound = !0,
            ig.soundHandler.unmuteSFX(),
            ig.soundHandler.sfxPlayer.play("button"));
            this.btn_sound.isOn = ig.game.sessionData.sound;
            ig.game.saveAll()
        },
        onHomeButtonClick: function() {
            this.controller.toHomeScene();
            ig.soundHandler.sfxPlayer.play("button")
        },
        onResumeButtonClick: function() {
            this.controller.unpause();
            ig.soundHandler.sfxPlayer.play("button")
        },
        kill: function() {
            this.parent();
            this.btn_resume.kill();
            this.btn_music.kill();
            this.btn_sound.kill()
        },
        moveIn: function() {
            this.anchoredPositionY += 500;
            this.currentAnim.alpha = 0;
            this.tween({
                anchoredPositionY: this.anchoredPositionY - 500,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.btn_resume.anchoredPositionY += 500;
            this.btn_resume.currentAnim.alpha = 0;
            this.btn_resume.tween({
                anchoredPositionY: this.btn_resume.anchoredPositionY - 500,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.btn_music.anchoredPositionY += 500;
            this.btn_music.currentAnim.alpha = 0;
            this.btn_music.tween({
                anchoredPositionY: this.btn_music.anchoredPositionY - 500,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.btn_sound.anchoredPositionY += 500;
            this.btn_sound.currentAnim.alpha = 0;
            this.btn_sound.tween({
                anchoredPositionY: this.btn_sound.anchoredPositionY - 500,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start()
        },
        moveOut: function(b) {
            this.tween({
                anchoredPositionY: this.anchoredPositionY + 500,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut,
                onComplete: b
            }).start();
            this.btn_resume.tween({
                anchoredPositionY: this.btn_resume.anchoredPositionY + 500,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start();
            this.btn_music.tween({
                anchoredPositionY: this.btn_music.anchoredPositionY + 500,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start();
            this.btn_sound.tween({
                anchoredPositionY: this.btn_sound.anchoredPositionY + 500,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start()
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.popup-lose").requires("impact.entity").defines(function() {
    EntityPopupLose = ig.Entity.extend({
        zIndex: 1E3,
        size: {
            x: 850,
            y: 1E3
        },
        anchorPivot: {
            x: .5,
            y: .5
        },
        animSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/popup.png",850,1E3),
        btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/button.png",366,120),
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.setAnchoredPosition(b, c, e.anchorTo || "center");
            this.addAnim("default", 1, [0], !0);
            this.btn_exit = ig.game.spawnEntity(EntityButton, 0, 332, {
                name: "btn_exit",
                label: _STRINGS.Game.Exit,
                fontColor: "#ffffff",
                fontSize: 65,
                size: {
                    x: 366,
                    y: 120
                },
                anchorTo: "center",
                anchorPivot: {
                    x: .5,
                    y: .5
                },
                animations: {
                    normal: {
                        animSheet: this.btn_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    }
                },
                onReleased: this.onExitButtonClick.bind(this),
                zIndex: this.zIndex + 1
            })
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            b.fillStyle = "rgba(0,0,0,0.5)";
            b.fillRect(0, 0, ig.system.width, ig.system.height);
            this.parent();
            b.fillStyle = "#003499";
            b.font = "85px metropolis";
            b.textAlign = "center";
            b.textBaseline = "middle";
            b.fillText(_STRINGS.Game.YouLost, this.pos.x + .5 * this.size.x, this.pos.y + 181);
            b.fillStyle = "#5367ee";
            b.font = "100px metropolis";
            b.fillText(_STRINGS.Game.BetterLuck, this.pos.x + .5 * this.size.x, this.pos.y + 430);
            b.fillText(_STRINGS.Game.NextTime, this.pos.x + .5 * this.size.x, this.pos.y + 550);
            b.restore()
        },
        onExitButtonClick: function() {
            this.controller.toHomeScene();
            ig.soundHandler.sfxPlayer.play("button")
        },
        kill: function() {
            this.parent();
            this.btn_exit.kill()
        },
        moveIn: function() {
            this.anchoredPositionY += 500;
            this.currentAnim.alpha = 0;
            this.tween({
                anchoredPositionY: this.anchoredPositionY - 500,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.btn_exit.anchoredPositionY += 500;
            this.btn_exit.currentAnim.alpha = 0;
            this.btn_exit.tween({
                anchoredPositionY: this.btn_exit.anchoredPositionY - 500,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start()
        },
        moveOut: function(b) {
            this.tween({
                anchoredPositionY: this.anchoredPositionY + 500,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut,
                onComplete: b
            }).start();
            this.btn_exit.tween({
                anchoredPositionY: this.btn_exit.anchoredPositionY + 500,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start()
        }
    })
});
ig.baked = !0;
ig.module("plugins.imageblender").requires("impact.image").defines(function() {
    ig.Image.inject({
        load: function(b) {
            this.loaded ? b && b(this.path, !0) : (!this.loaded && ig.ready ? (this.loadCallback = b || null,
            this.data = new Image,
            this.data.onload = this.onload.bind(this),
            this.data.onerror = this.onerror.bind(this),
            this.data.src = ig.prefix + this.path.split("#")[0] + ig.nocache) : ig.addResource(this),
            ig.Image.cache[this.path] = this)
        },
        onload: function(b) {
            this.parent(b);
            if (-1 !== this.path.indexOf("#")) {
                this.convertToCanvas();
                b = this.data.getContext("2d");
                for (var c = b.getImageData(0, 0, this.data.width, this.data.height), e = c.data, l = e.length, q = function(F) {
                    F = F.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function(G, M, O, V) {
                        return M + M + O + O + V + V
                    });
                    return (F = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(F)) ? {
                        r: parseInt(F[1], 16) / 255,
                        g: parseInt(F[2], 16) / 255,
                        b: parseInt(F[3], 16) / 255
                    } : null
                }(this.path.split("#").pop()), x = q.r, C = q.g, A = q.b, E = 0; E < l; E += 4) {
                    q = e[E + 3] / 255;
                    var N = q + 1 - 1 * q
                      , g = e[E] / 255 * q
                      , r = e[E + 1] / 255 * q
                      , y = e[E + 2] / 255 * q;
                    N = 255 / N;
                    e[E] = (g * x + x * (1 - q)) * N;
                    e[E + 1] = (r * C + C * (1 - q)) * N;
                    e[E + 2] = (y * A + A * (1 - q)) * N
                }
                b.putImageData(c, 0, 0)
            }
        },
        convertToCanvas: function() {
            if (!this.data.getContext) {
                var b = ig.$new("canvas");
                b.width = this.width;
                b.height = this.height;
                b.getContext("2d").drawImage(this.data, 0, 0, this.width, this.height, 0, 0, this.width, this.height);
                this.data = b
            }
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.burst").requires("impact.entity", "plugins.imageblender").defines(function() {
    EntityBurst = ig.Entity.extend({
        checkAgainst: ig.Entity.TYPE.NONE,
        image: new ig.Image("/static/spin-arena/v1/media/graphics/game/burst.png"),
        center: null,
        fillScale: null,
        angle: 0,
        variant: "",
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.size = new Vector2(1914,1914);
            b = "";
            "grandprize" == this.variant ? b = "#f4da80" : "rare" == this.variant ? b = "#f29ea3" : "common" == this.variant && (b = "#FFFFFF");
            this.image = new ig.Image("/static/spin-arena/v1/media/graphics/game/burst.png" + b);
            this.alpha = 0;
            this.tween({
                alpha: .7
            }, .5, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start()
        },
        update: function() {
            this.parent();
            this.angle += 25 * ig.system.tick
        },
        draw: function() {
            this.parent();
            ig.responsive.toAnchor(0, 0, "center-middle");
            if (ig.system.width > ig.system.height)
                var b = ig.system.width / this.size.x * 1.7
                  , c = ig.system.width / this.size.y * 1.7;
            else
                b = ig.system.height / this.size.x * 1.7,
                c = ig.system.height / this.size.y * 1.7;
            var e = ig.system.context;
            e.save();
            e.globalAlpha = this.alpha;
            var l = ig.responsive.toAnchor(0, 0, "center-middle")
              , q = this.toRadian(this.angle);
            ig.responsive.drawScaledRotateImage(this.image, l.x, l.y, b, c, .5, .5, q);
            e.restore()
        },
        toRadian: function(b) {
            return Math.PI / 180 * b
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.prize").requires("impact.entity").defines(function() {
    EntityPrize = ig.Entity.extend({
        checkAgainst: ig.Entity.TYPE.NONE,
        anchorPivot: {
            x: .5,
            y: .5
        },
        variant: "",
        textAlpha: 0,
        solidBg: {
            common: new ig.Image("/static/spin-arena/v1/media/graphics/game/common-solidbg.png"),
            rare: new ig.Image("/static/spin-arena/v1/media/graphics/game/rare-solidbg.png"),
            grandprize: new ig.Image("/static/spin-arena/v1/media/graphics/game/grandprize-solidbg.png")
        },
        borderBg: {
            common: new ig.Image("/static/spin-arena/v1/media/graphics/game/common-border.png"),
            rare: new ig.Image("/static/spin-arena/v1/media/graphics/game/rare-border.png"),
            grandprize: new ig.Image("/static/spin-arena/v1/media/graphics/game/grandprize-border.png")
        },
        borderSelectedBg: {
            common: new ig.Image("/static/spin-arena/v1/media/graphics/game/common-border-selected.png"),
            rare: new ig.Image("/static/spin-arena/v1/media/graphics/game/rare-border-selected.png"),
            grandprize: new ig.Image("/static/spin-arena/v1/media/graphics/game/grandprize-border-selected.png")
        },
        name: "",
        names: [],
        init: function(b, c, e) {
            this.baseScaleX = (638 / e.sheet.width).toFixed(3);
            this.baseScaleY = (638 / e.sheet.height).toFixed(3);
            this.scale = {
                x: this.baseScaleX,
                y: this.baseScaleY
            };
            this.size = {
                x: e.sheet.width,
                y: e.sheet.height
            };
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            this.anims.idle = new ig.Animation(e.sheet,1,[0],!0);
            this.currentAnim = this.anims.idle;
            this.variant = e.displayConfig.variant;
            this.prizeScale3 = this.prizeScale2 = this.prizeScale1 = 0;
            this.angle3 = this.toRadian(-270);
            this.textAlpha = 0;
            this.tween({
                prizeScale1: this.size.x / 290
            }, .55, {
                easing: ig.Tween.Easing.Quadratic.EaseOut,
                delay: .1
            }).start();
            this.tween({
                prizeScale2: .8
            }, .45, {
                easing: ig.Tween.Easing.Quadratic.EaseOut,
                delay: .3
            }).start();
            this.tween({
                prizeScale3: this.size.x / 290,
                angle3: this.toRadian(0),
                textAlpha: 1
            }, .65, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start();
            this.name = ig.game.gamePrize.name;
            this.names = this.getLines(ig.system.context, this.name, 90)
        },
        getLines: function(b, c, e) {
            c = c.split(" ");
            for (var l = [], q = c[0], x = 1; x < c.length; x++) {
                var C = c[x];
                b.measureText(q + " " + C).width < e ? q += " " + C : (l.push(q),
                q = C)
            }
            l.push(q);
            return l
        },
        update: function() {
            this.controller.isPaused ? this.refreshAnchoredPosition() : this.parent()
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            var c = ig.ua.mobile ? ig.system.width < ig.system.height ? 2 : .8 : .8;
            ig.responsive.drawScaledRotateImage(this.solidBg[this.variant], this.pos.x + .5 * this.size.x, this.pos.y + .5 * this.size.y, this.prizeScale3 * c, this.prizeScale3 * c, .5, .5, this.angle3);
            b.save();
            b.translate(this.pos.x + .5 * this.size.x, this.pos.y + .5 * this.size.x);
            b.scale(this.prizeScale2 * c, this.prizeScale2 * c);
            b.translate(-(this.pos.x + .5 * this.size.x), -(this.pos.y + .5 * this.size.x));
            this.parent();
            b.restore();
            ig.responsive.drawScaledRotateImage(this.borderBg[this.variant], this.pos.x + .5 * this.size.x, this.pos.y + .5 * this.size.y, this.prizeScale3 * c, this.prizeScale3 * c, .5, .5, this.angle3);
            b.save();
            b.fillStyle = "#ffffff";
            b.textAlign = "center";
            b.textBaseline = "middle";
            b.shadowOffsetX = 0;
            b.shadowOffsetY = 4;
            b.shadowColor = "rgba(0,0,0,1)";
            b.globalAlpha = this.textAlpha;
            var e = 1 == this.names.length ? .43 * this.size.y : .4 * this.size.y;
            b.font = "40px oxanium";
            b.translate(this.pos.x, this.pos.y);
            b.scale(c, c);
            b.translate(-this.pos.x, -this.pos.y);
            for (var l = 0; l < this.names.length; l++)
                2 == c ? b.fillText(this.names[l], this.pos.x + .25 * this.size.x, this.pos.y + .11 * this.size.y + e + l * this.size.y * .06) : 1 == c ? b.fillText(this.names[l], this.pos.x + .5 * this.size.x, this.pos.y + .36 * this.size.y + e + l * this.size.y * .06) : b.fillText(this.names[l], this.pos.x + .63 * this.size.x, this.pos.y + .48 * this.size.y + e + l * this.size.y * .06);
            b.restore()
        },
        toRadian: function(b) {
            return Math.PI / 180 * b
        }
    })
});
ig.baked = !0;
ig.module("game.entities.objects.confetti").requires("impact.entity").defines(function() {
    EntityConfetti = ig.Entity.extend({
        size: {
            x: 1920,
            y: 1080
        },
        anchorPivot: {
            x: 0,
            y: 0
        },
        loopCount: 0,
        confettiAnimSheets: [new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-0.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-1.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-2.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-3.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-4.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-5.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-6.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-7.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-8.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/confetti/confetti-9.png",1920,1080)],
        animState: "",
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            for (b = 0; b < this.confettiAnimSheets.length; b++)
                this.anims["idle_" + b] = new ig.Animation(this.confettiAnimSheets[b],.05,[0],!0);
            this.changeAnim("idle");
            multiplier = ig.ua.mobile ? window.innerWidth < window.innerHeight ? 2 : 1 : 1;
            this.setAnchoredPosition(0, 0, "top-left");
            this.loopCount = 0
        },
        changeAnim: function(b) {
            this.state = b;
            this.idx = 0;
            this.currentAnim = this.anims[b + "_0"];
            this.currentAnim.rewind()
        },
        updateAnim: function() {
            "idle" == this.state && ++this.idx > this.confettiAnimSheets.length - 1 && (this.idx = 0,
            2 == ++this.loopCount && this.kill());
            this.currentAnim = this.anims[this.state + "_" + this.idx];
            this.currentAnim.rewind()
        },
        update: function() {
            this.parent();
            0 < this.currentAnim.loopCount && this.updateAnim()
        },
        draw: function() {
            this.parent()
        },
        reorient: function(b) {
            ig.responsive.toAnchor(0, 0, "top-left");
            if (ig.ua.mobile)
                if ("portrait" == b) {
                    b = ig.system.height / 1920;
                    var c = ig.system.height / 1920
                } else
                    b = ig.system.width / 1920,
                    c = ig.system.height / 1080;
            else
                b = ig.system.width / 1920,
                c = ig.system.height / 1080;
            this.setScale(b, c);
            console.log(b + " - " + c)
        }
    })
});
ig.baked = !0;
ig.module("game.entities.objects.sparkle").requires("impact.entity").defines(function() {
    EntitySparkle = ig.Entity.extend({
        size: {
            x: 1920,
            y: 1080
        },
        anchorPivot: {
            x: 0,
            y: 0
        },
        loopCount: 0,
        sparkleAnimSheets: {
            rare: [new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-0.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-1.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-2.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-3.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-4.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-5.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-6.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/rare-7.png",1920,1080)],
            grandprize: [new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-0.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-1.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-2.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-3.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-4.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-5.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-6.png",1920,1080), new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sparkle/grandprize-7.png",1920,1080)]
        },
        animState: "",
        variant: "",
        init: function(b, c, e) {
            this.parent(b, c, e);
            e.anchorTo && this.setAnchoredPosition(b, c, e.anchorTo);
            for (b = 0; b < this.sparkleAnimSheets[this.variant].length; b++)
                this.anims["idle_" + b] = new ig.Animation(this.sparkleAnimSheets[this.variant][b],.1,[0],!0);
            this.changeAnim("idle");
            multiplier = ig.ua.mobile ? window.innerWidth < window.innerHeight ? 2 : 1 : 1;
            this.setAnchoredPosition(0, 0, "top-left");
            this.loopCount = 0
        },
        changeAnim: function(b) {
            this.state = b;
            this.idx = 0;
            this.currentAnim = this.anims[b + "_0"];
            this.currentAnim.rewind()
        },
        updateAnim: function() {
            "idle" == this.state && ++this.idx > this.sparkleAnimSheets[this.variant].length - 1 && (this.idx = 0);
            this.currentAnim = this.anims[this.state + "_" + this.idx];
            this.currentAnim.rewind()
        },
        update: function() {
            this.parent();
            0 < this.currentAnim.loopCount && this.updateAnim()
        },
        draw: function() {
            this.parent()
        },
        reorient: function(b) {
            ig.responsive.toAnchor(0, 0, "top-left");
            if (ig.ua.mobile)
                if ("portrait" == b) {
                    b = ig.system.height / 1920;
                    var c = ig.system.height / 1920
                } else
                    b = ig.system.width / 1920,
                    c = ig.system.height / 1080;
            else
                b = ig.system.width / 1920,
                c = ig.system.height / 1080;
            this.setScale(b, c)
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.popup-win").requires("impact.entity", "game.entities.ui.burst", "game.entities.ui.prize", "game.entities.objects.confetti", "game.entities.objects.sparkle").defines(function() {
    EntityPopupWin = ig.Entity.extend({
        zIndex: 1E3,
        size: {
            x: 850,
            y: 1E3
        },
        anchorPivot: {
            x: .5,
            y: .5
        },
        animSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/popup.png",0,0),
        exit_btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/button-2.png",338,102),
        link_btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/button-1.png",338,102),
        header: new ig.Image("/static/spin-arena/v1/media/graphics/game/win-bg.png"),
        headerOffset: {
            x: 72,
            y: 60
        },
        prizeText: "",
        prizeTextSize: 150,
        prizeTextOffset: 400,
        prizeTextMaxWidth: 0,
        prizeImage: null,
        prizeImageOffset: 475,
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.setAnchoredPosition(b, c, e.anchorTo || "center");
            this.addAnim("default", 1, [0], !0);
            this.prizeTextMaxWidth = this.size.x - Math.min(100, .2 * this.size.x);
            this.prizeImageOffset = _STRINGS.WinPopup.prizeImageOffset;
            this.variant = ig.game.gamePrize.display_config.variant;
            this.burst = ig.game.spawnEntity(EntityBurst, 0, 0, {
                zIndex: this.zIndex,
                variant: this.variant
            });
            this.prizeTextSize = this.prizeImage ? _STRINGS.WinPopup.prizeTextSizeWithImage : _STRINGS.WinPopup.prizeTextSize;
            this.prizeTextOffset = this.prizeImage ? _STRINGS.WinPopup.prizeTextOffsetWithImage : _STRINGS.WinPopup.prizeTextOffset;
            b = ig.system.context;
            b.save();
            b.font = this.prizeTextSize + "px oxanium";
            if ("string" == typeof this.prizeText)
                c = b.measureText(this.prizeText).width,
                c > this.prizeTextMaxWidth && (this.prizeTextSize *= this.prizeTextMaxWidth / c);
            else if (0 < this.prizeText.length) {
                for (e = 0; e < this.prizeText.length; e++)
                    c = b.measureText(this.prizeText[e]).width,
                    c > this.prizeTextMaxWidth && (this.prizeTextSize *= this.prizeTextMaxWidth / c,
                    b.font = this.prizeTextSize + "px oxanium");
                this.prizeTextLineHeight = 1.2 * this.prizeTextSize;
                this.prizeTextOffset -= this.prizeTextLineHeight * (this.prizeText.length - 1) * .5
            }
            b.restore();
            ig.ua.mobile ? ig.system.width < ig.system.height ? (b = 1250,
            c = 450) : (b = 390,
            c = 480) : (b = 410,
            c = 200);
            e = GameConfiguration ? GameConfiguration.text.playAgainButton : "Putar lagi";
            this.btn_exit = ig.game.spawnEntity(EntityButton, c, b, {
                name: "btn_exit",
                label: e,
                fontColor: "#ffffff",
                fontSize: 50,
                buttonScale: 0,
                baseScale: 0,
                size: {
                    x: 338,
                    y: 102
                },
                anchorTo: "center",
                anchorPivot: {
                    x: .5,
                    y: .5
                },
                animations: {
                    normal: {
                        animSheet: this.exit_btn_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    }
                },
                onReleased: this.onExitButtonClick.bind(this),
                zIndex: this.zIndex + 1
            });
            e = GameConfiguration ? GameConfiguration.text.goToRewardsPageButton : "Lihat hadiah";
            this.btn_link = ig.game.spawnEntity(EntityButton, -c, b, {
                name: "btn_link",
                label: e,
                fontColor: "#1111a8",
                fontSize: 50,
                buttonScale: 0,
                baseScale: 0,
                size: {
                    x: 338,
                    y: 102
                },
                anchorTo: "center",
                anchorPivot: {
                    x: .5,
                    y: .5
                },
                animations: {
                    normal: {
                        animSheet: this.link_btn_AnimSheet,
                        frameTime: 1,
                        sequence: [0],
                        stop: !0
                    }
                },
                onReleased: this.onLinkButtonClick.bind(this),
                zIndex: this.zIndex + 1
            });
            if (b = ig.Image.cache[ig.game.gamePrize.image_url])
                b = new ig.AnimationSheet(ig.game.gamePrize.image_url,b.width,b.height),
                this.prize = ig.game.spawnEntity(EntityPrize, 0, -70, {
                    anchorTo: "center",
                    sheet: b,
                    displayConfig: ig.game.gamePrize.display_config,
                    zIndex: this.zIndex + 1,
                    controller: this
                });
            b = ig.responsive.toAnchor(0, 0, "top-left");
            "grandprize" == this.variant ? (this.sparkle = ig.game.spawnEntity(EntitySparkle, b.x, b.y, {
                zIndex: this.zIndex,
                variant: this.variant
            }),
            this.confetti = ig.game.spawnEntity(EntityConfetti, b.x, b.y, {
                zIndex: this.zIndex + 1
            })) : "rare" == this.variant && (this.sparkle = ig.game.spawnEntity(EntitySparkle, b.x, b.y, {
                zIndex: this.zIndex,
                variant: this.variant
            }));
            this.reorient({
                detail: {
                    orientation: window.innerHeight < window.innerWidth ? "landscape" : "portrait"
                }
            });
            this.btnLinkScale = this.btnExitScale = this.descTextscale = this.headerTextscale = this.alpha = 0;
            this.btn_link.setScale(0, 0);
            this.btn_exit.setScale(0, 0);
            this.btn_exit.baseScale = 1E-6;
            this.btn_exit.buttonScale = 1E-6;
            this.btn_link.baseScale = 1E-6;
            this.btn_link.buttonScale = 1E-6;
            this.animating = !0;
            this.tween({
                alpha: 1
            }, .3, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start();
            this.tween({
                headerTextscale: 1
            }, .5, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.tween({
                descTextscale: 1
            }, .5, {
                easing: ig.Tween.Easing.Back.EaseOut,
                delay: .4
            }).start();
            this.tween({
                btnLinkScale: 1
            }, .5, {
                easing: ig.Tween.Easing.Back.EaseOut,
                delay: .5
            }).start();
            this.tween({
                btnExitScale: 1
            }, .5, {
                easing: ig.Tween.Easing.Back.EaseOut,
                delay: .6,
                onComplete: function() {
                    this.animating = !1
                }
                .bind(this)
            }).start()
        },
        update: function() {
            this.parent();
            if (this.animating) {
                if (ig.ua.mobile)
                    if (ig.system.width < ig.system.height)
                        var b = 2
                          , c = 1250
                          , e = 450;
                    else
                        b = 1,
                        c = 410,
                        e = 480;
                else
                    b = 1,
                    c = 430,
                    e = 200;
                this.btn_exit.setAnchoredPosition(e, c, "center");
                this.btn_link.setAnchoredPosition(-e, c, "center");
                this.btn_link.baseScale = this.btn_link.buttonScale = this.btnLinkScale * b;
                this.btn_exit.baseScale = this.btn_exit.buttonScale = this.btnExitScale * b
            }
        },
        draw: function() {
            var b = ig.system.context;
            b.save();
            b.globalAlpha = this.alpha;
            b.fillStyle = "rgba(0,0,0,0.5)";
            b.fillRect(0, 0, ig.system.width, ig.system.height);
            this.burst.draw();
            this.sparkle && this.sparkle.draw();
            this.parent();
            b.fillStyle = "#ffffff";
            b.font = "85px oxanium";
            b.textAlign = "center";
            b.textBaseline = "middle";
            b.shadowOffsetX = 0;
            b.shadowOffsetY = 7;
            b.shadowColor = "rgba(0,0,0,0.5)";
            if (ig.ua.mobile)
                if (ig.system.width < ig.system.height)
                    var c = -300
                      , e = 2;
                else
                    c = 120,
                    e = 1;
            else
                c = 120,
                e = 1;
            b.translate(this.pos.x + .5 * this.size.x, this.pos.y + c);
            b.scale(this.headerTextscale * e, this.headerTextscale * e);
            b.translate(-(this.pos.x + .5 * this.size.x), -(this.pos.y + c));
            b.fillText(GameConfiguration ? GameConfiguration.text.congratulationTitle : "\ud83c\udf89 Selamat \ud83c\udf89", this.pos.x + .5 * this.size.x, this.pos.y + c);
            b.restore();
            b.save();
            b.fillStyle = "#ffffff";
            b.font = "50px oxanium";
            b.textAlign = "center";
            b.textBaseline = "middle";
            b.shadowOffsetX = 0;
            b.shadowOffsetY = 5;
            b.shadowColor = "rgba(0,0,0,0.5)";
            var l = [];
            ig.ua.mobile ? ig.system.width < ig.system.height ? (c = 1200,
            l[0] = GameConfiguration ? GameConfiguration.text.congratulationDescription : "Kamu berhasil mendapatkan",
            l[1] = ig.game.gamePrize.name) : (c = 750,
            l[0] = GameConfiguration ? GameConfiguration.text.congratulationDescription + " " + ig.game.gamePrize.name : "Kamu berhasil mendapatkan") : (c = 750,
            l[0] = GameConfiguration ? GameConfiguration.text.congratulationDescription + " " + ig.game.gamePrize.name : "Kamu berhasil mendapatkan");
            b.translate(this.pos.x + .5 * this.size.x, this.pos.y + c);
            b.scale(this.descTextscale * e, this.descTextscale * e);
            b.translate(-(this.pos.x + .5 * this.size.x), -(this.pos.y + c));
            for (e = 0; e < l.length; e++)
                b.fillText(l[e], this.pos.x + .5 * this.size.x, this.pos.y + c + .05 * this.size.y * e);
            b.restore();
            b.save();
            b.fillStyle = "#ffffff";
            b.font = "50px oxanium";
            b.textAlign = "center";
            b.textBaseline = "middle";
            b.shadowOffsetX = 0;
            b.shadowOffsetY = 5;
            b.shadowColor = "rgba(0,0,0,1)";
            b.restore()
        },
        onExitButtonClick: async function() {
            GameConfiguration && await GameConfiguration.clickPlayAgainButton().then(async function() {
                this.controller.toHomeScene();
                console.log("play again button")
            }
            .bind(this));
            ig.soundHandler.sfxPlayer.play("button")
        },
        onLinkButtonClick: function() {
            GameConfiguration && GameConfiguration.clickViewPrizeButton();
            console.log("view prize button");
            ig.soundHandler.sfxPlayer.play("button")
        },
        kill: function() {
            this.parent();
            this.btn_exit.kill();
            this.btn_link.kill()
        },
        moveIn: function() {
            this.anchoredPositionY += 0;
            this.currentAnim.alpha = 0;
            this.tween({
                anchoredPositionY: this.anchoredPositionY - 0,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.btn_exit.anchoredPositionY += 0;
            this.btn_exit.currentAnim.alpha = 0;
            this.btn_exit.tween({
                anchoredPositionY: this.btn_exit.anchoredPositionY - 0,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start();
            this.btn_link.anchoredPositionY += 0;
            this.btn_link.currentAnim.alpha = 0;
            this.btn_link.tween({
                anchoredPositionY: this.btn_link.anchoredPositionY - 0,
                currentAnim: {
                    alpha: 1
                }
            }, .35, {
                easing: ig.Tween.Easing.Back.EaseOut
            }).start()
        },
        moveOut: function(b) {
            this.tween({
                anchoredPositionY: this.anchoredPositionY + 0,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut,
                onComplete: b
            }).start();
            this.btn_exit.tween({
                anchoredPositionY: this.btn_exit.anchoredPositionY + 0,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start();
            this.btn_link.tween({
                anchoredPositionY: this.btn_link.anchoredPositionY + 0,
                currentAnim: {
                    alpha: 0
                }
            }, .2, {
                easing: ig.Tween.Easing.Quadratic.EaseOut
            }).start()
        },
        reorient: function(b) {
            if (ig.ua.mobile)
                if ("portrait" == b)
                    var c = 2
                      , e = 1250
                      , l = 450;
                else
                    c = 1,
                    e = 410,
                    l = 480;
            else
                c = 1,
                e = 430,
                l = 200;
            this.btn_exit && (this.btn_exit.setAnchoredPosition(l, e, "center"),
            this.btn_exit.baseScale = c,
            this.btn_exit.buttonScale = c);
            this.btn_link && (this.btn_link.setAnchoredPosition(-l, e, "center"),
            this.btn_link.baseScale = c,
            this.btn_link.buttonScale = c);
            this.confetti && this.confetti.reorient(b);
            this.sparkle && this.sparkle.reorient(b)
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.screen-overlay").requires("impact.entity").defines(function() {
    EntityScreenOverlay = ig.Entity.extend({
        zIndex: 9999,
        color: "#000000",
        alpha: 0,
        draw: function() {
            var b = ig.system.context;
            b.save();
            b.fillStyle = this.color;
            b.globalAlpha = this.alpha;
            b.fillRect(0, 0, ig.system.width, ig.system.height);
            b.restore()
        },
        fade: function(b, c, e, l) {
            this.alpha = b;
            this.tween({
                alpha: c
            }, e, {
                onComplete: l
            }).start()
        },
        transitionFade: function(b, c) {
            this.alpha = 0;
            c = this.tween({
                alpha: 1
            }, b, {
                onComplete: c
            });
            b = this.tween({
                alpha: 0
            }, b);
            c.chain(b);
            c.start()
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.background").requires("impact.entity").defines(function() {
    EntityBackground = ig.Entity.extend({
        checkAgainst: ig.Entity.TYPE.NONE,
        desktop: new ig.Image("/static/spin-arena/v1/media/graphics/game/desktop-bg.png"),
        mobile: new ig.Image("/static/spin-arena/v1/media/graphics/game/mobile-bg.png"),
        center: null,
        fillScale: null,
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.size = ig.ua.mobile ? new Vector2(1080,1920) : new Vector2(1920,1080)
        },
        draw: function() {
            this.parent();
            var b = ig.responsive.toAnchor(0, 0, "center-middle");
            if (ig.ua.mobile) {
                var c = ig.system.width / this.size.x
                  , e = ig.system.height / this.size.y;
                this.mobile && ig.responsive.drawScaledImage(this.mobile, b.x, b.y, c, e, .5, .5)
            } else
                c = ig.system.width / this.size.x,
                e = ig.system.height / this.size.y,
                this.desktop && ig.responsive.drawScaledImage(this.desktop, b.x, b.y, c, e, .5, .5)
        }
    })
});
ig.baked = !0;
ig.module("game.entities.ui.background-blur").requires("impact.entity").defines(function() {
    EntityBackgroundBlur = ig.Entity.extend({
        checkAgainst: ig.Entity.TYPE.NONE,
        desktop: new ig.Image("/static/spin-arena/v1/media/graphics/game/desktop-bg-blur.png"),
        mobile: new ig.Image("/static/spin-arena/v1/media/graphics/game/mobile-bg-blur.png"),
        center: null,
        fillScale: null,
        visible: !1,
        init: function(b, c, e) {
            this.parent(b, c, e);
            this.size = ig.ua.mobile ? new Vector2(1080,1920) : new Vector2(1920,1080);
            this.visible = !1
        },
        draw: function() {
            if (this.visible) {
                this.parent();
                var b = ig.responsive.toAnchor(0, 0, "center-middle");
                if (ig.ua.mobile) {
                    var c = ig.system.width / this.size.x
                      , e = ig.system.height / this.size.y;
                    this.mobile && ig.responsive.drawScaledImage(this.mobile, b.x, b.y, c, e, .5, .5)
                } else
                    c = ig.system.width / this.size.x,
                    e = ig.system.height / this.size.y,
                    this.desktop && ig.responsive.drawScaledImage(this.desktop, b.x, b.y, c, e, .5, .5)
            }
        },
        setVisible: function(b) {
            this.visible = b
        }
    })
});
ig.baked = !0;
ig.module("game.entities.buttons.button-gift").requires("game.entities.buttons.button").defines(function() {
    EntityButtonGift = EntityButton.extend({
        type: ig.Entity.TYPE.A,
        gravityFactor: 0,
        controller: null,
        pendingReward: 0,
        gift_btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/gift-box.png",197,102),
        gift_btn_2_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/gift-box-2.png",197,102),
        size: {
            x: 197,
            y: 102
        },
        name: "gift_button",
        init: async function(b, c, e) {
            this.pendingReward = e.pendingReward;
            e.animations = {
                normal: {
                    animSheet: 0 < this.pendingReward ? this.gift_btn_2_AnimSheet : this.gift_btn_AnimSheet,
                    frameTime: 1,
                    sequence: [0],
                    stop: !0
                }
            };
            this.parent(b, c, e);
            this.label = 0 < this.pendingReward ? this.pendingReward : "";
            this.textOffset.x = -45;
            this.fontSize = 9 > this.label ? 30 : 99 > this.label ? 25 : 999 > this.label ? 20 : 15
        },
        draw: function() {
            this.parent();
            if (this.label) {
                var b = ig.system.context;
                b.save();
                b.globalAlpha = this.currentAnim.alpha;
                var c = this.pos.x + .5 * this.size.x + this.textOffset.x * (ig.ua.mobile ? ig.system.width < ig.system.height ? 2 : 1 : 1)
                  , e = this.pos.y + .5 * this.size.y + this.textOffset.y;
                b.beginPath();
                b.arc(c, e, 25 * this.scale.y, 0, 2 * Math.PI);
                b.fillStyle = "#ed6c02";
                b.fill();
                b.font = this.fontSize * this.scale.y + "px oxanium";
                b.fillStyle = this.fontColor;
                b.textAlign = "center";
                b.textBaseline = "middle";
                b.fillText(this.label, c, e);
                b.restore()
            }
        },
        clicked: function() {},
        clicking: function() {},
        released: function() {
            this.controller.isPaused || (console.log("gift button"),
            GameConfiguration && GameConfiguration.clickGiftBoxButton(),
            ig.soundHandler.sfxPlayer.play("button"))
        }
    })
});
ig.baked = !0;
ig.module("game.entities.controllers.game-controller").requires("impact.entity", "game.entities.objects.wheel", "game.entities.objects.character", "game.entities.ui.popup-settings", "game.entities.ui.popup-lose", "game.entities.ui.popup-win", "game.entities.ui.screen-overlay", "game.entities.ui.background", "game.entities.ui.background-blur", "game.entities.buttons.button-gift").defines(function() {
    EntityGameController = ig.Entity.extend({
        isPaused: !1,
        fullscreenEnter: new ig.Image("/static/spin-arena/v1/media/graphics/game/btn-expand.png"),
        fullscreenExit: new ig.Image("/static/spin-arena/v1/media/graphics/game/btn-shrink.png"),
        tokenIcon: new ig.Image("/static/spin-arena/v1/media/graphics/game/token-icon.png"),
        btn_pause_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/btn-pause.png",118,118),
        btn_spin_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/button.png",615,234),
        btn_spin_click_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/button-press.png",615,234),
        token_btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/token.png",338,102),
        info_btn_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/info.png",102,102),
        btn_on_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sound_active_2x.png",76,76),
        btn_off_AnimSheet: new ig.AnimationSheet("/static/spin-arena/v1/media/graphics/game/sound_inactive_2x.png",76,76),
        init: async function(b, c, e) {
            this.parent(b, c, e);
            if (!ig.global.wm) {
                ig.game.spawnEntity(EntityBackground, 0, 0, {});
                b = GameConfiguration ? GameConfiguration.text.playButton : "MAIN";
                if (ig.ua.mobile)
                    if (c = "bottom",
                    ig.system.width < ig.system.height) {
                        var l = -450
                          , q = 2
                          , x = -40;
                        e = -15 * b.length * q
                    } else
                        l = -200,
                        q = 1,
                        x = -40,
                        e = -15 * b.length;
                else
                    c = "center",
                    l = 420,
                    q = .8,
                    x = -40,
                    e = -15 * b.length;
                this.btn_spin = ig.game.spawnEntity(EntityButton, 0, l, {
                    name: "btn_spin",
                    label: b,
                    fontColor: "#ffffff",
                    fontSize: 0 < b.length ? 4 / b.length * 60 : 60,
                    fontPreFix: "1000 ",
                    textOffset: {
                        x: e,
                        y: x
                    },
                    onClicked: function() {
                        this.textOffset.y = ig.ua.mobile ? ig.system.width < ig.system.height ? -60 : -25 : -25
                    },
                    onReleased: function() {
                        this.textOffset.y = ig.ua.mobile ? ig.system.width < ig.system.height ? -100 : -40 : -40
                    },
                    onLeaved: function() {
                        this.textOffset.y = ig.ua.mobile ? ig.system.width < ig.system.height ? -100 : -40 : -40
                    },
                    size: {
                        x: 615,
                        y: 234
                    },
                    buttonScale: q,
                    baseScale: q,
                    anchorTo: c,
                    highlightScale: 1,
                    animations: {
                        normal: {
                            animSheet: this.btn_spin_AnimSheet,
                            frameTime: 1,
                            sequence: [0],
                            stop: !0
                        },
                        clicked: {
                            animSheet: this.btn_spin_click_AnimSheet,
                            frameTime: 1,
                            sequence: [0],
                            stop: !0
                        }
                    },
                    onReleased: this.onSpinButtonClick.bind(this),
                    zIndex: this.zIndex
                });
                this.btn_spin.isLocked = !0;
                if (ig.ua.mobile)
                    if (ig.system.width < ig.system.height) {
                        var C = 450;
                        l = 200;
                        q = 2
                    } else
                        C = 250,
                        l = 120,
                        q = 1;
                else
                    C = 250,
                    l = 160,
                    q = 1;
                this.btn_token = ig.game.spawnEntity(EntityButton, C, l, {
                    name: "btn_token",
                    label: ig.gameData ? ig.gameData.tokenBalance.toString() : "?",
                    fontColor: "#1111a8",
                    fontSize: 50,
                    buttonScale: q,
                    baseScale: q,
                    textOffset: {
                        x: 50,
                        y: 0
                    },
                    size: {
                        x: 338,
                        y: 102
                    },
                    anchorTo: "top-left",
                    anchorPivot: {
                        x: .5,
                        y: .5
                    },
                    animations: {
                        normal: {
                            animSheet: this.token_btn_AnimSheet,
                            frameTime: 1,
                            sequence: [0],
                            stop: !0
                        }
                    },
                    onReleased: this.onTokenButtonClick.bind(this),
                    zIndex: this.zIndex + 1
                });
                GameConfiguration && await GameConfiguration.getBalance().then(function(A) {
                    this.btn_token.label = A
                }
                .bind(this)).catch(function(A) {
                    console.log(A);
                    this.btn_token.label = "?"
                }
                .bind(this));
                ig.ua.mobile ? ig.system.width < ig.system.height ? (C = -650,
                l = 200,
                q = 2) : (C = -400,
                l = 120,
                q = 1) : (C = -400,
                l = 120,
                q = 1);
                this.btn_info = ig.game.spawnEntity(EntityButton, C, l, {
                    name: "btn_info",
                    label: "",
                    fontColor: "#ffffff",
                    fontSize: 50,
                    buttonScale: q,
                    baseScale: q,
                    size: {
                        x: 102,
                        y: 102
                    },
                    anchorTo: "top-right",
                    anchorPivot: {
                        x: .5,
                        y: .5
                    },
                    animations: {
                        normal: {
                            animSheet: this.info_btn_AnimSheet,
                            frameTime: 1,
                            sequence: [0],
                            stop: !0
                        }
                    },
                    onReleased: this.onInfoButtonClick.bind(this),
                    zIndex: this.zIndex + 1
                });
                ig.ua.mobile ? ig.system.width < ig.system.height ? (C = -300,
                l = 200,
                q = 2) : (C = -200,
                l = 120,
                q = 1) : (C = -200,
                l = 120,
                q = 1);
                GameConfiguration && await GameConfiguration.getPendingRewards().then(function(A) {
                    console.log(A);
                    ig.gameData && (ig.gameData.pendingReward = A || 0);
                    this.btn_gift = ig.game.spawnEntity(EntityButtonGift, C, l, {
                        controller: this,
                        name: "btn_gift",
                        label: "",
                        fontColor: "#ffffff",
                        fontSize: 30,
                        buttonScale: q,
                        baseScale: q,
                        anchorTo: "top-right",
                        anchorPivot: {
                            x: .5,
                            y: .5
                        },
                        zIndex: this.zIndex + 1,
                        pendingReward: ig.gameData ? ig.gameData.pendingReward : "?"
                    })
                }
                .bind(this)).catch(function(A) {
                    console.log(A);
                    ig.gameData && (ig.gameData.pendingReward = 0);
                    this.btn_gift = ig.game.spawnEntity(EntityButtonGift, C, l, {
                        controller: this,
                        name: "btn_gift",
                        label: "",
                        fontColor: "#ffffff",
                        fontSize: 30,
                        buttonScale: q,
                        baseScale: q,
                        anchorTo: "top-right",
                        anchorPivot: {
                            x: .5,
                            y: .5
                        },
                        zIndex: this.zIndex + 1,
                        pendingReward: ig.gameData ? ig.gameData.pendingReward : "?"
                    })
                }
                .bind(this));
                q = ig.ua.mobile ? ig.system.width < ig.system.height ? 3 : 2 : 2;
                this.btn_sound = ig.game.spawnEntity(EntityButtonToggle, 130, -250, {
                    name: "btn_sound",
                    size: {
                        x: 76,
                        y: 76
                    },
                    buttonScale: q,
                    baseScale: q,
                    anchorTo: "bottom-left",
                    anchorPivot: {
                        x: .5,
                        y: .5
                    },
                    animations: {
                        on_normal: {
                            animSheet: this.btn_on_AnimSheet,
                            frameTime: 1,
                            sequence: [0],
                            stop: !0
                        },
                        off_normal: {
                            animSheet: this.btn_off_AnimSheet,
                            frameTime: 1,
                            sequence: [0],
                            stop: !0
                        }
                    },
                    onReleased: this.toggleSound.bind(this),
                    zIndex: this.zIndex + 1,
                    isOn: ig.game.sessionData.sound
                });
                this.blur = ig.game.spawnEntity(EntityBackgroundBlur, 0, 0, {});
                this.blur.setVisible(!1);
                this.character = ig.game.spawnEntity(EntityCharacter, 70, -20, {
                    anchorTo: "center",
                    controller: this
                });
                this.wheel = ig.game.spawnEntity(EntityWheel, 0, -90, {
                    anchorTo: "center",
                    controller: this
                });
                this.screen_fade = ig.game.spawnEntity(EntityScreenOverlay, 0, 0, {
                    color: "#f4f4f4"
                });
                this.screen_fade.fade(1, 0, .2, function() {
                    this.screen_fade.kill()
                }
                .bind(this));
                ig.game.sortEntitiesDeferred();
                ig.soundHandler.bgmPlayer2.play(null, !0);
                ig.game.currentGameState = "NOT_STARTED";
                ig.domHandler.getElementById("#play")[0].addEventListener("reorientate", this.reorient.bind(this));
                this.reorient({
                    detail: {
                        orientation: window.innerHeight < window.innerWidth ? "landscape" : "portrait"
                    }
                })
            }
        },
        update: function() {
            this.parent();
            if (ig.ua.mobile) {
                var b = "bottom";
                if (ig.system.width < ig.system.height)
                    var c = -500
                      , e = 2;
                else
                    c = -110,
                    e = 1
            } else
                b = "center",
                c = 420,
                e = .8;
            this.btn_spin && (this.btn_spin.baseScale = e,
            this.btn_spin.buttonScale = e,
            this.btn_spin.setAnchoredPosition(0, c, b));
            ig.ua.mobile ? ig.system.width < ig.system.height ? (c = 0,
            e = 2) : (c = 0,
            e = 1) : (c = 0,
            e = .8);
            b = "center";
            this.wheel && (this.wheel.setScale(e, e),
            this.wheel.setAnchoredPosition(0, c, b));
            ig.ua.mobile ? ig.system.width < ig.system.height ? (c = -300,
            e = 1.7) : (c = -150,
            e = 1) : (c = -150,
            e = .8);
            this.btn_sound && (this.btn_sound.setScale(e, e),
            this.btn_sound.buttonScale = this.btn_sound.baseScale = e,
            this.btn_sound.setAnchoredPosition(130, c, "bottom-left"))
        },
        draw: function() {
            this.parent();
            ig.responsive.toAnchor(0, 0, "top-left").y += 60;
            ig.responsive.toAnchor(0, 0, "top-right").y += 60;
            var b = ig.system.context;
            b.save();
            if (this.blur && !this.blur.visible) {
                if (ig.ua.mobile)
                    if (ig.system.width < ig.system.height)
                        var c = 2
                          , e = 0
                          , l = 70
                          , q = 310;
                    else
                        c = .8,
                        e = 0,
                        l = 60,
                        q = 160;
                else
                    c = .7,
                    e = 0,
                    l = 40,
                    q = 130;
                b.fillStyle = "#ffffff";
                b.font = "900 " + this.btn_spin.fontSize * c + "px oxanium";
                b.textAlign = "left";
                b.textBaseline = "middle";
                b.fillText(ig.gameData ? ig.gameData.tokenCost : "?", this.btn_spin.pos.x + .5 * this.btn_spin.size.x + l, this.btn_spin.pos.y + .5 * this.btn_spin.size.y + e + this.btn_spin.textOffset.y);
                ig.responsive.drawScaledImage(this.tokenIcon, this.btn_spin.pos.x + .5 * this.btn_spin.size.x + q, this.btn_spin.pos.y + .5 * this.btn_spin.size.y + e + this.btn_spin.textOffset.y, 1.5 * c, 1.5 * c, .5, .5)
            }
            b.restore()
        },
        pause: function() {
            this.isPaused = !0;
            this.btn_spin.isLocked = !0;
            this.popup_settings = ig.game.spawnEntity(EntityPopupSettings, 0, 0, {
                controller: this
            });
            this.popup_settings.moveIn();
            ig.game.sortEntitiesDeferred()
        },
        unpause: function() {
            this.isPaused = !1;
            this.wheel.isSpinning || (this.btn_spin.isLocked = !1);
            this.popup_settings && this.popup_settings.moveOut(function() {
                this.popup_settings.kill()
            }
            .bind(this))
        },
        onPauseButtonClick: function() {
            this.isPaused || (this.pause(),
            ig.soundHandler.sfxPlayer.play("button"))
        },
        onSpinButtonClick: function() {
            this.isPaused || (this.btn_spin.isLocked = !0,
            this.pullGatcha())
        },
        pullGatcha: async function() {
            await GameConfiguration.pullGacha().then(async function(b) {
                console.log("pull gatcha:");
                console.log(b);
                try {
                    await GameConfiguration.getBalance().then(function(c) {
                        ig.gameData.tokenBalance = c;
                        this.btn_token.label = ig.gameData.tokenBalance.toString()
                    }
                    .bind(this)).catch(function(c) {
                        console.log(c);
                        this.btn_token.label = "?"
                    }
                    .bind(this))
                } catch (c) {}
                ig.game.gamePrize = b;
                b = parseInt(ig.game.gamePrize.id) - 1;
                this.wheel.spin(b);
                ig.soundHandler.sfxPlayer.play("button");
                ig.game.currentGameState = "PLAYING";
                this.character.changeAnim("dance")
            }
            .bind(this)).catch(function(b) {
                console.log(b);
                this.btn_spin.isLocked = !1
            }
            .bind(this))
        },
        toggleSound: function() {
            ig.game.sessionData.sound ? (ig.game.sessionData.sound = !1,
            ig.game.sessionData.music = !1,
            ig.soundHandler.muteSFX(),
            ig.soundHandler.muteBGM()) : (ig.game.sessionData.sound = !0,
            ig.game.sessionData.music = !0,
            ig.soundHandler.unmuteSFX(),
            ig.soundHandler.unmuteBGM(),
            ig.soundHandler.sfxPlayer.play("button"));
            this.btn_sound.isOn = ig.game.sessionData.sound;
            GameConfiguration && GameConfiguration.clickSoundIconButton();
            console.log("toggle sound button");
            ig.game.saveAll()
        },
        onTokenButtonClick: function() {
            this.isPaused || (console.log("token button"),
            GameConfiguration && GameConfiguration.clickTokenMutationButton(),
            ig.soundHandler.sfxPlayer.play("button"))
        },
        onInfoButtonClick: function() {
            this.isPaused || (console.log("info button"),
            GameConfiguration && GameConfiguration.clickQuestionMarkButton(),
            ig.soundHandler.sfxPlayer.play("button"))
        },
        getPrize: function(b) {
            b = _STRINGS.WheelSlices[b];
            this.isPaused = !0;
            var c = "string" == typeof b.prizeImage ? new ig.Image(b.prizeImage) : null;
            this.popup_win = ig.game.spawnEntity(EntityPopupWin, 0, 0, {
                controller: this,
                prizeText: b.prizeTextFull,
                prizeImage: c
            });
            ig.soundHandler.sfxPlayer.play("win");
            ig.soundHandler.sfxPlayer.play("win2");
            this.wheel.setBlur(!0);
            this.character.setBlur(!0);
            ig.game.sortEntitiesDeferred();
            ig.game.isSkip || (ig.game.slice_number = 0);
            ig.game.currentGameState = "STOP";
            this.blur.setVisible(!0)
        },
        toHomeScene: function() {
            ig.game.isSkip = !1;
            ig.game.director.jumpTo(LevelGame);
            ig.soundHandler.sfxPlayer.stop("win")
        },
        reorient: function(b) {
            console.log(b.detail.orientation);
            var c = GameConfiguration ? GameConfiguration.text.playButton : "MAIN";
            if (ig.ua.mobile) {
                var e = "bottom";
                if ("portrait" == b.detail.orientation)
                    var l = -450
                      , q = 2
                      , x = -100
                      , C = -20 * c.length * q;
                else
                    l = -200,
                    q = 1,
                    x = -50,
                    C = -15 * c.length
            } else
                e = "center",
                l = 420,
                q = .8,
                x = -40,
                C = -15 * c.length;
            c = 0 < c.length ? 4 / c.length * 70 : 70;
            this.btn_spin && (this.btn_spin.fontSize = c,
            this.btn_spin.baseScale = q,
            this.btn_spin.buttonScale = q,
            this.btn_spin.setAnchoredPosition(0, l, e),
            this.btn_spin.textOffset.y = x,
            this.btn_spin.textOffset.x = C);
            ig.ua.mobile ? "portrait" == b.detail.orientation ? (l = -300,
            q = 1.7) : (l = -150,
            q = 1) : (l = -150,
            q = .8);
            this.btn_sound && (this.btn_sound.setScale(q, q),
            this.btn_sound.buttonScale = this.btn_sound.baseScale = q,
            this.btn_sound.setAnchoredPosition(130, l, "bottom-left"));
            ig.ua.mobile ? "portrait" == b.detail.orientation ? (e = -300,
            l = 600,
            q = 2) : (e = -200,
            l = 160,
            q = 1) : (e = -200,
            l = 200,
            q = 1);
            this.btn_gift && (this.btn_gift.baseScale = q,
            this.btn_gift.buttonScale = q,
            this.btn_gift.setAnchoredPosition(e, l, "top-right"));
            ig.ua.mobile ? "portrait" == b.detail.orientation ? (e = -650,
            l = 600,
            q = 2) : (e = -400,
            l = 160,
            q = 1) : (e = -400,
            l = 200,
            q = 1);
            this.btn_info && (this.btn_info.baseScale = q,
            this.btn_info.buttonScale = q,
            this.btn_info.setAnchoredPosition(e, l, "top-right"));
            ig.ua.mobile ? "portrait" == b.detail.orientation ? (e = 450,
            l = 600,
            q = 2) : (e = 250,
            l = 160,
            q = 1) : (e = 250,
            l = 200,
            q = 1);
            this.btn_token && (this.btn_token.baseScale = q,
            this.btn_token.buttonScale = q,
            this.btn_token.setAnchoredPosition(e, l, "top-left"));
            this.character && this.character.reorient(b.detail.orientation);
            this.popup_win && this.popup_win.reorient(b.detail.orientation)
        }
    })
});
ig.baked = !0;
ig.module("game.levels.opening").requires("impact.image", "game.entities.opening-kitty").defines(function() {
    LevelOpening = {
        entities: [{
            type: "EntityOpeningKitty",
            x: 520,
            y: 212
        }],
        layer: []
    }
});
ig.baked = !0;
ig.module("game.levels.form").requires("impact.image", "game.entities.controllers.form-controller").defines(function() {
    LevelForm = {
        entities: [{
            type: "EntityFormController",
            x: -50,
            y: -50
        }],
        layer: []
    }
});
ig.baked = !0;
ig.module("game.levels.game").requires("impact.image", "game.entities.controllers.game-controller", "game.entities.pointer").defines(function() {
    LevelGame = {
        entities: [{
            type: "EntityGameController",
            x: 0,
            y: 0
        }, {
            type: "EntityPointer",
            x: 0,
            y: 0
        }],
        layer: []
    }
});
ig.baked = !0;
ig.module("game.main").requires("impact.game", "plugins.patches.fps-limit-patch", "plugins.patches.timer-patch", "plugins.patches.user-agent-patch", "plugins.patches.webkit-image-smoothing-patch", "plugins.patches.windowfocus-onMouseDown-patch", "plugins.patches.input-patch", "plugins.data.vector", "plugins.data.color-rgb", "plugins.font.font-loader", "plugins.handlers.dom-handler", "plugins.handlers.size-handler", "plugins.handlers.api-handler", "plugins.handlers.visibility-handler", "plugins.audio.sound-handler", "plugins.io.io-manager", "plugins.io.storage-manager", "plugins.splash-loader", "plugins.tween", "plugins.tweens-handler", "plugins.scale", "plugins.url-parameters", "plugins.director", "plugins.impact-storage", "plugins.fullscreen", "plugins.responsive.responsive-plugin", "plugins.responsive.responsive-anchor-pivot", "plugins.branding.splash", "game.entities.branding-logo-placeholder", "game.entities.buttons.button-more-games", "game.entities.opening-kitty", "game.entities.pointer", "game.entities.pointer-selector", "game.entities.select", "game.entities.buttons.button", "game.entities.buttons.button-toggle", "game.entities.controllers.form-controller", "game.entities.controllers.game-controller", "game.levels.opening", "game.levels.form", "game.levels.game").defines(function() {
    this.START_OBFUSCATION;
    this.FRAMEBREAKER;
    MyGame = ig.Game.extend({
        name: "spin-to-win-demo",
        version: "1.0.0",
        frameworkVersion: "1.0.17",
        sessionData: {},
        io: null,
        paused: !1,
        tweens: null,
        musicVolume: .5,
        soundVolume: 1,
        isSkip: !1,
        gameData: {},
        init: function() {
            this.tweens = new ig.TweensHandler;
            this.setupMarketJsGameCenter();
            this.io = new IoManager;
            this.setupUrlParams = new ig.UrlParameters;
            this.removeLoadingWheel();
            this.setupStorageManager();
            this.setupVolume();
            this.finalize()
        },
        initData: function() {
            return this.sessionData = {
                sound: !0,
                music: !0
            }
        },
        setupVolume: function() {
            ig.soundHandler.bgmPlayer2.volume(this.musicVolume);
            ig.soundHandler.sfxPlayer.volume(this.soundVolume);
            this.sessionData.music || ig.soundHandler.muteBGM();
            this.sessionData.sound || ig.soundHandler.muteSFX()
        },
        setupMarketJsGameCenter: function() {
            if (_SETTINGS)
                if (_SETTINGS.MarketJSGameCenter) {
                    var b = ig.domHandler.getElementByClass("gamecenter-activator");
                    _SETTINGS.MarketJSGameCenter.Activator.Enabled && _SETTINGS.MarketJSGameCenter.Activator.Position && (console.log("MarketJSGameCenter activator settings present ...."),
                    ig.domHandler.css(b, {
                        position: "absolute",
                        left: _SETTINGS.MarketJSGameCenter.Activator.Position.Left,
                        top: _SETTINGS.MarketJSGameCenter.Activator.Position.Top,
                        "z-index": 3
                    }));
                    ig.domHandler.show(b)
                } else
                    console.log("MarketJSGameCenter settings not defined in game settings")
        },
        finalize: async function() {
            this.start();
            ig.sizeHandler.reorient()
        },
        removeLoadingWheel: function() {
            try {
                $("#ajaxbar").css("background", "none")
            } catch (b) {
                console.log(b)
            }
        },
        showDebugMenu: function() {
            console.log("showing debug menu ...");
            ig.Entity._debugShowBoxes = !0;
            $(".ig_debug").show()
        },
        start: function() {
            this.resetPlayerStats();
            this.director = new ig.Director(this,[LevelOpening, LevelGame]);
            if (_SETTINGS.Branding.Splash.Enabled)
                try {
                    this.branding = new ig.BrandingSplash
                } catch (b) {
                    console.log(b),
                    console.log("Loading original levels ..."),
                    this.director.loadLevel(this.director.currentLevel)
                }
            else
                this.director.loadLevel(this.director.currentLevel);
            (_SETTINGS.Branding.Splash.Enabled || _SETTINGS.DeveloperBranding.Splash.Enabled) && this.spawnEntity(EntityPointerSelector, 50, 50)
        },
        fpsCount: function() {
            this.fpsTimer || (this.fpsTimer = new ig.Timer(1));
            this.fpsTimer && 0 > this.fpsTimer.delta() ? null != this.fpsCounter ? this.fpsCounter++ : this.fpsCounter = 0 : (ig.game.fps = this.fpsCounter,
            this.fpsCounter = 0,
            this.fpsTimer.reset())
        },
        endGame: function() {
            console.log("End game");
            ig.soundHandler.bgmPlayer2.stop();
            ig.apiHandler.run("MJSEnd")
        },
        resetPlayerStats: function() {
            ig.log("resetting player stats ...");
            this.playerStats = {
                id: this.playerStats ? this.playerStats.id : null
            }
        },
        pauseGame: function() {
            ig.system.stopRunLoop.call(ig.system);
            ig.game.tweens.onSystemPause();
            console.log("Game Paused")
        },
        resumeGame: function() {
            ig.system.startRunLoop.call(ig.system);
            ig.game.tweens.onSystemResume();
            console.log("Game Resumed")
        },
        showOverlay: function(b) {
            for (i = 0; i < b.length; i++)
                $("#" + b[i]) && $("#" + b[i]).show(),
                document.getElementById(b[i]) && (document.getElementById(b[i]).style.visibility = "visible")
        },
        hideOverlay: function(b) {
            for (i = 0; i < b.length; i++)
                $("#" + b[i]) && $("#" + b[i]).hide(),
                document.getElementById(b[i]) && (document.getElementById(b[i]).style.visibility = "hidden")
        },
        currentBGMVolume: 1,
        addition: .1,
        update: function() {
            this.paused ? (this.updateWhilePaused(),
            this.checkWhilePaused()) : (this.parent(),
            this.tweens.update(this.tweens.now()),
            ig.ua.mobile && ig.soundHandler && ig.soundHandler.forceLoopBGM())
        },
        updateWhilePaused: function() {
            for (var b = 0; b < this.entities.length; b++)
                this.entities[b].ignorePause && this.entities[b].update()
        },
        checkWhilePaused: function() {
            for (var b = {}, c = 0; c < this.entities.length; c++) {
                var e = this.entities[c];
                if (e.ignorePause && (e.type != ig.Entity.TYPE.NONE || e.checkAgainst != ig.Entity.TYPE.NONE || e.collides != ig.Entity.COLLIDES.NEVER))
                    for (var l = {}, q = Math.floor(e.pos.y / this.cellSize), x = Math.floor((e.pos.x + e.size.x) / this.cellSize) + 1, C = Math.floor((e.pos.y + e.size.y) / this.cellSize) + 1, A = Math.floor(e.pos.x / this.cellSize); A < x; A++)
                        for (var E = q; E < C; E++)
                            if (b[A])
                                if (b[A][E]) {
                                    for (var N = b[A][E], g = 0; g < N.length; g++)
                                        e.touches(N[g]) && !l[N[g].id] && (l[N[g].id] = !0,
                                        ig.Entity.checkPair(e, N[g]));
                                    N.push(e)
                                } else
                                    b[A][E] = [e];
                            else
                                b[A] = {},
                                b[A][E] = [e]
            }
        },
        draw: function() {
            this.parent();
            this.dctf()
        },
        dctf: function() {
            this.COPYRIGHT
        },
        clearCanvas: function(b, c, e) {
            var l = b.canvas;
            b.clearRect(0, 0, c, e);
            l.style.display = "none";
            l.offsetHeight;
            l.style.display = "inherit"
        },
        drawDebug: function() {
            if (!ig.global.wm && (this.debugEnable(),
            this.viewDebug && (ig.system.context.fillStyle = "#000000",
            ig.system.context.globalAlpha = .35,
            ig.system.context.fillRect(0, 0, ig.system.width / 4, ig.system.height),
            ig.system.context.globalAlpha = 1,
            this.debug && 0 < this.debug.length)))
                for (i = 0; i < this.debug.length; i++)
                    ig.system.context.font = "10px Arial",
                    ig.system.context.fillStyle = "#ffffff",
                    ig.system.context.fillText(this.debugLine - this.debug.length + i + ": " + this.debug[i], 10, 50 + 10 * i)
        },
        debugCL: function(b) {
            this.debug ? (50 > this.debug.length || this.debug.splice(0, 1),
            this.debug.push(b),
            this.debugLine++) : (this.debug = [],
            this.debugLine = 1,
            this.debug.push(b));
            console.log(b)
        },
        debugEnable: function() {
            ig.input.pressed("click") && (this.debugEnableTimer = new ig.Timer(2));
            this.debugEnableTimer && 0 > this.debugEnableTimer.delta() ? ig.input.released("click") && (this.debugEnableTimer = null) : this.debugEnableTimer && 0 < this.debugEnableTimer.delta() && (this.debugEnableTimer = null,
            this.viewDebug = this.viewDebug ? !1 : !0)
        },
        sendGameJsonInfo: function() {}
    });
    ig.gameConfigInter = setInterval(async function() {
        null !== GameConfiguration && void 0 !== GameConfiguration && (clearInterval(ig.gameConfigInter),
        ig.domHandler = null,
        ig.domHandler = new ig.DomHandler,
        ig.domHandler.forcedDeviceDetection(),
        ig.domHandler.forcedDeviceRotation(),
        ig.apiHandler = new ig.ApiHandler,
        ig.sizeHandler = new ig.SizeHandler(ig.domHandler),
        ig.ua.mobile ? (ig.Sound.enabled = !1,
        ig.main("#canvas", MyGame, 60, ig.sizeHandler.mobile.actualResolution.x, ig.sizeHandler.mobile.actualResolution.y, ig.sizeHandler.scale, ig.SplashLoader),
        ig.sizeHandler.resize()) : ig.main("#canvas", MyGame, 60, ig.sizeHandler.desktop.actualResolution.x, ig.sizeHandler.desktop.actualResolution.y, ig.sizeHandler.scale, ig.SplashLoader),
        ig.visibilityHandler = new ig.VisibilityHandler,
        ig.soundHandler = null,
        ig.soundHandler = new ig.SoundHandler,
        ig.sizeHandler.reorient())
    }
    .bind(this), 500);
    this.DOMAINLOCK_BREAKOUT_ATTEMPT;
    this.END_OBFUSCATION
});
